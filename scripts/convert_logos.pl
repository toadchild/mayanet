#!/opt/local/bin/perl

# A script that can convert a batch of unit logos to the correct sizes and
# filenames needed by MayaNet.
#
# In order to be able to convert the images bundled in the downloadable version
# of Army 6 (SVG format), ImageMagick is required.  To install:
#   1. Install MacPorts from http://macports.org/
#   2. sudo port install ImageMagick +rsvg
#   3. sudo port install p5-perlmagick

use strict;
use warnings;

use Image::Magick;

open(INPUT, "<", "image_mapping.csv");
my $path = $ARGV[0];
if(!$path){
    warn "Must specify path to images!";
    exit 1;
}

my $scales = [
    {
        prefix => '../Toolbox/images/',
        suffix => '_logo.png',
        size => 40,
    },
    {
        prefix => '../Toolbox/images/',
        suffix => '_logo@2x.png',
        size => 80,
    },
    {
        prefix => '../Toolbox/images/small_logos/',
        suffix => '_logo_small.png',
        size => 16,
    },
];


for my $l (<INPUT>){
    next if $l =~ m/^#/;

    chomp $l;
    next if !$l;

    my ($src_name, $dst_name) = split(/:/, $l);
    die if !$src_name;

    print "$src_name -> $dst_name\n";
    my $src = new Image::Magick;
    $src->Set(background=>"none");
    $src->Read("$path/$src_name.svg");

    for my $scale (@$scales){
        my $size = $scale->{size};
        my $dst = $src->Clone();

        $dst->Resize(width=>$size, height=>$size);

        my $out_name = $scale->{prefix} . $dst_name . $scale->{suffix};
        $dst->Strip();
        $dst->Write($out_name);
    }
}
