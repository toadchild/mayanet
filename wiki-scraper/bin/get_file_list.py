#!/usr/bin/env python

import codecs
import json
import sys
import urllib
import urllib2

def get_page_list(base_url, page_filter, filename, dedup=None):
    # Wiki rejects requests from default python urllib UA
    headers = {
            'User-Agent': 'MayaNet scraper. Contact jonathan@ghostlords.com'
    }

    args = {
            'action': 'query',
            'list': 'allpages',
            'apfilterredir': page_filter,
            'aplimit': '500',
            'format': 'json',
    }

    api_url = base_url + 'api.php'
    print api_url

    pages = []
    done = False
    while not done:
        data_str = urllib.urlencode(args)
        request = urllib2.Request(api_url, data_str, headers)

        http = urllib2.urlopen(request)
        results_str = http.read()

        results = json.loads(results_str)
        pages += results['query']['allpages']

        if 'query-continue' in results:
            for key in results['query-continue']['allpages']:
                args[key] = results['query-continue']['allpages'][key]
        else:
            done = True

    dedup_map = {}
    if dedup:
        for page in dedup:
            dedup_map[page['title'].lower()] = True

    out = codecs.open(filename, 'w', 'utf-8')
    for page in pages:
        if page['title'].lower() not in dedup_map:
            out.write(page['title'] + '\n')

    return pages


def main():
    base_url = sys.argv[1]
    plist = sys.argv[2]
    rlist = sys.argv[3]

    pages = get_page_list(base_url, 'nonredirects', plist)
    get_page_list(base_url, 'redirects', rlist, dedup=pages)


if __name__ == '__main__':
    main()
