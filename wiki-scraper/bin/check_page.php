<?php
//This file checks for a id="contents" element. If it exists, it echos "0", if not it echos "1"

$file = $argv[1];

$doc = new DOMDocument('1.0','UTF-8');
libxml_use_internal_errors(true);
$doc->loadHTMLFile($file);
libxml_clear_errors();
$xpath = new DOMXpath($doc);

$nodes=$xpath->query("//*[@id='message']");
if($nodes->length != 0)
{
	echo 1;
}
else
{
	echo 0;
}
?>
