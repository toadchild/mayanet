<?php

function startsWith($haystack, $needle)
{
    return $needle === "" || strpos($haystack, $needle) === 0;
}

$file = $argv[1];
$copyFile = $argv[2];
echo "processing file $file into $copyFile\n";

$doc = new DOMDocument('1.0','UTF-8');
libxml_use_internal_errors(true);
$doc->loadHTMLFile($file);
libxml_clear_errors();

$xpath = new DOMXpath($doc);

$toDeleteList = array("//*[@id='banner']", 
    "//*[@id='siteSub']",
    "//*[@id='contentSub']", 
    "//*[@id='jump-to-nav']",
    "//*[@id='breadcrumbs2']",  
    "//*[@id='title']",  
    "//*[@class='tabArea']", 
    "//script", 
    "//*[@id='footer']", 
    "//*[@class='wikiGlobalFooter']", 
    "//*[@class='printfooter']",
    "//*[@class='catlinks']",
    "//comment()"
);

foreach ($toDeleteList as $toDelete) {
    $nodeList = $xpath->query($toDelete);
    foreach ($nodeList as $node) {
        $node->parentNode->removeChild($node);
    }
}

foreach ($xpath->query("//*[@onload]") as $toRemoveAttr)
{
    $toRemoveAttr->removeAttribute("onload");
}

foreach ($xpath->query("//*[@style]") as $toRemoveAttr)
{
    $toRemoveAttr->removeAttribute("style");
}

foreach ($xpath->query("//*[@srcset]") as $toRemoveAttr)
{
    $toRemoveAttr->removeAttribute("srcset");
}


$node=$xpath->query("//*[@id='content']")->item(0);

$new_doc = new DOMDocument('1.0','UTF-8');
libxml_use_internal_errors(true);
$template = file_get_contents('../page.template');
$new_doc->loadHTML($template);
libxml_clear_errors();
$new_doc->encoding = 'UTF-8'; // insert proper
$body = $new_doc->getElementById('container');
$node = $new_doc->importNode($node,true);
$body->appendChild($node);

$date_node = $new_doc->getElementById('capture_date');
$date_node->appendChild(new DOMText(gmdate(DATE_RFC2822)));

$new_doc->saveHTMLFile($copyFile);

$output = file_get_contents($copyFile);

$output = str_replace(array("&ordm;"), array("&deg;"),$output);
file_put_contents($copyFile,$output);

?>
