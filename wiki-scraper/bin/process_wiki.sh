#!/bin/bash

targetDir="$1"
plist="$2"
wikiFile="$3"
lang="$4"
baseUrl="$5"
outdir="$6"
phpDir=`pwd`/`dirname $0`

pushd "$targetDir" || exit 1

echo "processing data from dir $targetDir, page file $plist, wiki file $wikiFile, lang $lang"

echo "removing js"
find * -type f -name '*.js' -delete

echo "copying files"

find * -type f | while read file; do
    newName="$outdir/$file"
    dir=`dirname "$newName"`
    mkdir -p "${dir}"

    if file -b "$file" | grep -q PNG; then
        # flatten PNGs
        convert -background white -flatten "$file" "$newName"
    elif [[ "$file" == *.html ]] ; then
        # convert HTML files to template
        php $phpDir/clean_wiki_page.php "$file" "$newName" || echo "error on $file"
    else
        # copy others normally
        cp "$file" "$newName"
    fi
done

echo "parsing pages from files $plist"
echo -n > ${wikiFile}
{
    first=`mktemp /tmp/file.XXXXXX`
    echo -e "\t{\n\t\t\"lang\":\"${lang}\",\n\t\t\"baseurl\":\"${baseUrl}\",\n\t\t\"pages\":[\n"
    cat $plist | egrep -v '^(Bookmarks|Wiki.*)$' | sed  -E -e 's#%28#(#g' -e 's#%29#)#g' -e 's#%C2%B0#°#g' -e 's#%2C#,#g' -e 's#%E2%80%9C#“#g' -e 's#%E2%80%9D#”#g' | while read page; do
        fileName="${lang}/${page}.html"
        if [ -e "${outdir}/$fileName" ]; then
            [ -e $first ] && rm $first ||  echo ","
            echo -e "\t\t{\n\t\t\t\"filename\":\"${fileName}\",\n\t\t\t\"pagename\":\"${page}\"\n\t\t}"
        else
            echo "********** skipping page $page, no match" >&2
        fi
    done
    echo -e "\t\t]"
    echo -e "\t}"
} >> "${wikiFile}"
