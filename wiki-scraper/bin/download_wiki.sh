#!/bin/bash

baseUrl="$1"
targetDir="$2"
pages="$3"
redirects="$4"
phpDir=`pwd`/`dirname $0`

pushd "$targetDir" || exit 1

ua='MayaNet scraper. Contact jonathan@ghostlords.com'

$phpDir/get_file_list.py $baseUrl $pages $redirects

echo "got `wc -l < $plist` pages to fetch"

cat $pages $redirects | while read page; do
	echo "${baseUrl}${page}"
done | wget -U "$ua" -e robots=off -p -k -E -H -nc --no-host-directories --restrict-file-names=ascii -R favicon.ico -i -

echo "downloaded wiki $baseUrl in `pwd`"
popd

echo "pages list in $pages; redirects in $redirects"

