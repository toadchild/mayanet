//
//  AspectSegmentedControl.h
//  Toolbox
//
//  Created by Paul on 11/4/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AspectSegmentedControl : UISegmentedControl
typedef void (^ UnsetBlock)(AspectSegmentedControl*);

@property (nonatomic, strong) UnsetBlock onUnsetSegmentedControl;
@end
