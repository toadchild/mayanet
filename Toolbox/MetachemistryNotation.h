#import "_MetachemistryNotation.h"

@interface MetachemistryNotation : _MetachemistryNotation {}
// Custom logic goes here.

-(NotationType) type;

@end
