//
//  WikiWebViewController.h
//  Toolbox
//
//  Created by Paul on 11/13/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AppDelegate;
@class WikiEntry;

typedef void (^ WebViewLoadCompletionBlock)();

@interface WikiWebViewController : UIViewController <UIWebViewDelegate,UIActionSheetDelegate>
{
    BOOL hideOnce;
    AppDelegate* appDelegate;
    NSUInteger entryIndex;
    WebViewLoadCompletionBlock completionBlock;
    UIActionSheet* browseActionSheet;
}
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) UIBarButtonItem *backNavButton;
@property (strong, nonatomic) UIBarButtonItem *actionNavButton;
@property (strong, nonatomic) UIBarButtonItem *forwardNavButton;
@property (strong,nonatomic) NSMutableArray* wikiEntriesList;
-(void)setWikiEntry:(WikiEntry*)wikiEntry webViewLoadCompletion:(WebViewLoadCompletionBlock)completion;
@end
