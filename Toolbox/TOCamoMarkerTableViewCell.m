//
//  TOCamoMarkerTableViewCell.m
//  Toolbox
//
//  Created by Paul on 12/6/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "TOCamoMarkerTableViewCell.h"
#import "TOCamoNote.h"
#import "GroupMember.h"

@implementation TOCamoMarkerTableViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(NSArray*)markers
{
    return @[self.camoMarker1, self.camoMarker2, self.camoMarker3];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(Note*)note
{
    if(!_note)
    {
        _note = [TOCamoNote insertInManagedObjectContext:self.groupMember.managedObjectContext];
        _note.groupMember = self.groupMember;
        _note.notation = self.notation;
    }
    return _note;
}

@end
