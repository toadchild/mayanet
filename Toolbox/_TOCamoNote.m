// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TOCamoNote.m instead.

#import "_TOCamoNote.h"

@implementation TOCamoNoteID
@end

@implementation _TOCamoNote

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TOCamoNote" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TOCamoNote";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TOCamoNote" inManagedObjectContext:moc_];
}

- (TOCamoNoteID*)objectID {
	return (TOCamoNoteID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

#if TARGET_OS_IPHONE

#endif

@end

