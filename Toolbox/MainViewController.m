//
//  MasterViewController.m
//  Toolbox
//
//  Created by Paul on 10/20/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "MainViewController.h"
#import "DetailViewController.h"
#import "ArmyViewController.h"
#import "Army.h"
#import "AppDelegate.h"
#import "UIColor+ColorPalette.h"
#import "Unit.h"


@interface MainViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation MainViewController

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    self.managedObjectContext = delegate.managedObjectContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [Army entityInManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setIncludesSubentities:TRUE];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setRelationshipKeyPathsForPrefetching:@[@"sectorials"]];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSError* error;
    armies = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error != nil)
    {
        
    }
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.rightBarButtonItem = nil;
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    self.navigationController.navigationBar.titleTextAttributes =
    @{NSForegroundColorAttributeName: [UIColor titleColor]};
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarTintColor:[UIColor barColor]];
    [self.navigationController.navigationBar setTintColor:[UIColor barItemColor]];
    
    [self.navigationController setToolbarHidden:TRUE animated:TRUE];
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch(section)
    {
        case 0:
            return 1;
        case 1:
            return [armies count];
        case 2:
        case 3:
        case 4:
        case 5:
            return 1;
        default:
            return 0;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

 #pragma mark - Navigation
 
 
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
     if([segue.identifier isEqualToString:@"ViewArmy"])
     {
         ArmyViewController *con = [segue destinationViewController];
        [con setArmy:sender];
         [con setManagedObjectContext:self.managedObjectContext];
     }
 }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        [self performSegueWithIdentifier:@"ViewRosterList" sender:self];
    }
    else if(indexPath.section == 1)
    {
        Army* army = [armies objectAtIndex:[indexPath row]];
        [self performSegueWithIdentifier:@"ViewArmy" sender:army];
    }
    else if(indexPath.section == 2)
    {
        [self performSegueWithIdentifier:@"ViewWiki" sender:nil];
    }
    else if(indexPath.section == 3)
    {
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        MiddleNavigationController* con = delegate.containerViewController.middleViewController;
        [con showSettings];
        //Hide Browse if selecting from roster in Portrait
        if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
        {
            AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            if([delegate.containerViewController isRightViewControllerVisible])
                [delegate.containerViewController displayLeftViewController:FALSE animationCompletionBlock:nil];
        }
    }
    else if(indexPath.section == 4)
    {
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        MiddleNavigationController* con = delegate.containerViewController.middleViewController;
        [con showIntro];
        //Hide Browse if selecting from roster in Portrait
        if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
        {
            AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            if([delegate.containerViewController isRightViewControllerVisible])
                [delegate.containerViewController displayLeftViewController:FALSE animationCompletionBlock:nil];
        }
    }
    else if(indexPath.section == 5)
    {
        if([MFMailComposeViewController canSendMail])
        {
            //Show Mail feedback form
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
            [controller setToRecipients:@[@"mayanet@ghostlords.com"]];
            NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
            [controller setSubject:[NSString stringWithFormat: @"MayaNet v%@ Feedback", version]];
            NSString *body = @"Please provide your feedback below.\n\n";
            [controller setMessageBody:body isHTML:NO];
            if (controller) [self presentViewController:controller animated:TRUE completion:^{}];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Mail Not Setup" message:@"You will need to setup Mail to provide feedback. You can also send feedback to mayanet@ghostlords.com" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
    }
}

-(NSString*)generateUnitDebug:(int) unit_id
{
    NSMutableArray *strings = [NSMutableArray array];
    
    NSArray *units = [Unit allUnitsWithID:unit_id];
    for (Unit* unit in units) {
        [strings addObject:[unit dumpDebug]];
        [strings addObject:@""];
    }
    
    return [strings componentsJoinedByString:@"\n"];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:TRUE completion:^{}];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        cell.textLabel.text = @"All Rosters";
        cell.imageView.image = [UIImage imageNamed:@"images/app_icon.png"];
    }
    else if(indexPath.section == 1)
    {
        Army *army = [armies objectAtIndex:indexPath.row];
        cell.textLabel.text = army.name;
        NSString* imageTitle = [army imageTitle];
        cell.imageView.image = [UIImage imageNamed:imageTitle];
    }
    else if(indexPath.section == 2)
    {
        cell.textLabel.text = @"Wiki";
        cell.imageView.image = [UIImage imageNamed:@"739-question-big.png"];
    }
    else if(indexPath.section == 3)
    {
        cell.textLabel.text = @"Settings";
        cell.imageView.image = [UIImage imageNamed:@"740-gear.png"];
    }
    else if(indexPath.section == 4)
    {
        cell.textLabel.text = @"Manual";
        cell.imageView.image = [UIImage imageNamed:@"images/app_icon_inverted.png"];
    }
    else if(indexPath.section == 5)
    {
        cell.textLabel.text = @"Submit Feedback";
        cell.imageView.image = [UIImage imageNamed:@"704-compose-big.png"];
    }
}

-(void)refresh
{
    
}

-(void)clearSelection
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:TRUE];
}

@end

