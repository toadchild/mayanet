// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CamoNote.m instead.

#import "_CamoNote.h"

const struct CamoNoteAttributes CamoNoteAttributes = {
	.number = @"number",
	.pattern = @"pattern",
};

@implementation CamoNoteID
@end

@implementation _CamoNote

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"CamoNote" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"CamoNote";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"CamoNote" inManagedObjectContext:moc_];
}

- (CamoNoteID*)objectID {
	return (CamoNoteID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"numberValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"number"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"patternValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"pattern"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic number;

- (int16_t)numberValue {
	NSNumber *result = [self number];
	return [result shortValue];
}

- (void)setNumberValue:(int16_t)value_ {
	[self setNumber:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveNumberValue {
	NSNumber *result = [self primitiveNumber];
	return [result shortValue];
}

- (void)setPrimitiveNumberValue:(int16_t)value_ {
	[self setPrimitiveNumber:[NSNumber numberWithShort:value_]];
}

@dynamic pattern;

- (int16_t)patternValue {
	NSNumber *result = [self pattern];
	return [result shortValue];
}

- (void)setPatternValue:(int16_t)value_ {
	[self setPattern:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePatternValue {
	NSNumber *result = [self primitivePattern];
	return [result shortValue];
}

- (void)setPrimitivePatternValue:(int16_t)value_ {
	[self setPrimitivePattern:[NSNumber numberWithShort:value_]];
}

#if TARGET_OS_IPHONE

#endif

@end

