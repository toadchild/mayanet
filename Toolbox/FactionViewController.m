//
//  FactionViewController.m
//  Toolbox
//
//  Created by Paul Clark on 5/1/14.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "FactionViewController.h"
#import "Unit.h"
#import "Util.h"
#import "UnitType.h"
#import "UnitAspects.h"
#import "UnitOption.h"
#import "Weapon.h"
#import "SpecialRule.h"
#import "UnitProfile.h"

@interface FactionViewController ()

@end

@implementation FactionViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    searchTypes = [[NSMutableArray alloc] init];
    searchUnitsByType = [[NSMutableDictionary alloc] init];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.scopeButtonTitles = @[@"Name", @"Weapons", @"Rules"];
    self.searchController.searchBar.delegate = self;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    [self.searchController.searchBar sizeToFit];
    
    isSearching = false;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(!isSearching)
    {
        return 1 + [types count] + 1;
    }
    else
    {
        return 1 + [searchTypes count] + 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    [self configureCell:cell tableView:tableView atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell tableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
{

}

-(NSPredicate*)searchPredicate:(NSPredicate*)otherPredicate
{
    return otherPredicate;
}


-(NSArray*) unitsOfType:(UnitType*)unitType includeMerc:(BOOL)includeMerc
{
    return [self unitsOfType:unitType includeMerc:includeMerc additionalPredicate:NULL];
}

-(NSArray*) unitsOfType:(UnitType*)unitType includeMerc:(BOOL)includeMerc additionalPredicate:(NSPredicate*)additionalPredicate
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* entity = [Unit entityInManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"isc" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"type = %@", unitType];
    
    if(!includeMerc)
    {
        NSPredicate *otherPredicate = [NSPredicate predicateWithFormat:
                                  @"isMerc = %@", [NSNumber numberWithBool:NO]];
        predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate,otherPredicate]];
    }
    
    if(additionalPredicate)
    {
        predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate,additionalPredicate]];
    }
    
    NSPredicate *searchPredicate = [self searchPredicate:predicate];

    [fetchRequest setPredicate:searchPredicate];

    NSError* error;
    NSArray* unitsOfType = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    return unitsOfType;
}

-(void) updateSearchFilter:(NSString*)filter searchType:(SearchType)searchType includeMerc:(BOOL)includeMerc;
{
    
    [searchTypes removeAllObjects];
    [searchUnitsByType removeAllObjects];
    for(UnitType* unitType in types)
    {
        NSArray* results = nil;
        if(searchType == SearchTypeName)
        {
            results = [self searchUnitsByName:filter unitType:unitType includeMerc:includeMerc];
        }
        
        if(searchType == SearchTypeWeapon)
        {
            results = [self searchUnitsByWeapon:filter unitType:unitType includeMerc:includeMerc];
        }
        
        if(searchType == SearchTypeRules)
        {
            results = [self searchUnitsBySpecialRule:filter unitType:unitType includeMerc:includeMerc];
        }
        
        if([results count])
        {
            [searchTypes addObject:unitType];
            [searchUnitsByType setObject:results forKey:unitType.code];
        }
    }
}

-(NSArray*) searchUnitsByName:(NSString*)filter unitType:(UnitType*)unitType includeMerc:(BOOL)includeMerc
{
    NSPredicate *namePredicate = [NSPredicate predicateWithFormat: @"(isc CONTAINS[cd] %@)", filter];

    return [self unitsOfType:unitType includeMerc:includeMerc additionalPredicate:namePredicate];
}

-(NSArray*) searchUnitsByWeapon:(NSString*)filter unitType:(UnitType*)unitType includeMerc:(BOOL)includeMerc
{
    NSArray* unitsOfType = [self unitsOfType:unitType includeMerc:includeMerc];
    NSMutableArray* results = [[NSMutableArray alloc] init];
    for(Unit* unit in unitsOfType)
    {
        BOOL modelFound = FALSE;
        //Check base unit;
        for(Weapon* weapon in unit.aspects.weapons)
        {
            if([weapon.name rangeOfString:filter options:NSCaseInsensitiveSearch].location != NSNotFound)
            {
                [results addObject:unit];
                modelFound = TRUE;
                break;
            }
        }
        if(modelFound)
            continue;

        //Check options;
        for(UnitOption* unitOption in unit.options)
        {
            for(Weapon* weapon in unitOption.weapons)
            {
                if([weapon.name rangeOfString:filter options:NSCaseInsensitiveSearch].location != NSNotFound)
                {
                    [results addObject:unit];
                    modelFound = TRUE;
                    break;
                }
            }
            if(modelFound)
            {
                break;
            }
        }
        if(modelFound)
            continue;
        
        //Check profiles
        for(UnitProfile* profile in unit.profiles)
        {
            UnitAspects* profileAspects = profile.aspects;
            for(Weapon* weapon in profileAspects.weapons)
            {
                if([weapon.name rangeOfString:filter options:NSCaseInsensitiveSearch].location != NSNotFound)
                {
                    [results addObject:unit];
                    modelFound = TRUE;
                    break;
                }
            }
            if(modelFound)
            {
                break;
            }
        }
    }
    return results;
}

-(NSArray*) searchUnitsBySpecialRule:(NSString*)filter unitType:(UnitType*)unitType includeMerc:(BOOL)includeMerc
{
    NSArray* unitsOfType = [self unitsOfType:unitType includeMerc:includeMerc];
    NSMutableArray* results = [[NSMutableArray alloc] init];
    for(Unit* unit in unitsOfType)
    {
        BOOL modelFound = FALSE;
        //Check base unit;
        for(SpecialRule* rule in unit.aspects.specialRules)
        {
            if([rule.name rangeOfString:filter options:NSCaseInsensitiveSearch].location != NSNotFound)
            {
                [results addObject:unit];
                modelFound = TRUE;
                break;
            }
        }
        if(modelFound)
            continue;
        
        //Check options;
        for(UnitOption* unitOption in unit.options)
        {
            for(SpecialRule* rule in unitOption.specialRules)
            {
                if([rule.name rangeOfString:filter options:NSCaseInsensitiveSearch].location != NSNotFound)
                {
                    [results addObject:unit];
                    modelFound = TRUE;
                    break;
                }
            }
            if(modelFound)
            {
                break;
            }
        }
        if(modelFound)
            continue;

        //Check profiles
        for(UnitProfile* profile in unit.profiles)
        {
            UnitAspects* profileAspects = profile.aspects;
            for(SpecialRule* rule in profileAspects.specialRules)
            {
                if([rule.name rangeOfString:filter options:NSCaseInsensitiveSearch].location != NSNotFound)
                {
                    [results addObject:unit];
                    modelFound = TRUE;
                    break;
                }
            }
            if(modelFound)
            {
                break;
            }
        }
    }
    return results;
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = searchController.searchBar.text;
    NSInteger selectedScopeButtonIndex = searchController.searchBar.selectedScopeButtonIndex;
    if([searchString length])
    {
        isSearching = true;
        [self updateSearchFilter:searchString searchType:(SearchType)selectedScopeButtonIndex includeMerc:[self includeMerc]];
    }
    else
    {
        isSearching = false;
    }
    [self.tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    [self updateSearchResultsForSearchController:self.searchController];
}

-(Unit*) unitAtIndexPath:(NSIndexPath*)indexPath tableView:(UITableView *)tableView
{
    if(!isSearching)
    {
        NSInteger index = indexPath.section-typeOffset;
        UnitType* type = [types objectAtIndex:index];
        NSArray* units = [unitsByType objectForKey:type.code];
        return [units objectAtIndex:indexPath.row];
    }
    else
    {
        NSInteger index = indexPath.section-typeOffset;
        UnitType* type = [searchTypes objectAtIndex:index];
        NSArray* units = [searchUnitsByType objectForKey:type.code];
        return [units objectAtIndex:indexPath.row];
    }
}

-(BOOL)includeMerc
{
    return FALSE;
}

- (IBAction)addRoster:(id)sender
{
    
}

-(void)clearSelection
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:TRUE];
}

#pragma mark Containable protocol implementation
-(void)refresh
{
    
}


@end
