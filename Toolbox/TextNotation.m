#import "TextNotation.h"
#import "AppDelegate.h"


@interface TextNotation ()

// Private interface goes here.

@end


@implementation TextNotation

// Custom logic goes here.
-(NotationType) type
{
    return eTextNotation;
}

+(TextNotation*)getTextNotation
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // Custom logic goes here.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [self entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError* error;
    NSArray* results = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [results count] != 0)
    {
        return [results firstObject];
    }
    
    return [TextNotation insertInManagedObjectContext:delegate.managedObjectContext];
}


@end
