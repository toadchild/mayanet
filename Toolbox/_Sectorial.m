// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Sectorial.m instead.

#import "_Sectorial.h"

const struct SectorialAttributes SectorialAttributes = {
	.abbr = @"abbr",
	.id = @"id",
	.legacyName = @"legacyName",
	.name = @"name",
	.rgbBackground = @"rgbBackground",
	.rgbPrimary = @"rgbPrimary",
	.rgbSecondary = @"rgbSecondary",
	.showMercs = @"showMercs",
};

const struct SectorialRelationships SectorialRelationships = {
	.army = @"army",
	.rosters = @"rosters",
	.units = @"units",
};

@implementation SectorialID
@end

@implementation _Sectorial

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Sectorial" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Sectorial";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Sectorial" inManagedObjectContext:moc_];
}

- (SectorialID*)objectID {
	return (SectorialID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"showMercsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"showMercs"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic abbr;

@dynamic id;

- (int32_t)idValue {
	NSNumber *result = [self id];
	return [result intValue];
}

- (void)setIdValue:(int32_t)value_ {
	[self setId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdValue {
	NSNumber *result = [self primitiveId];
	return [result intValue];
}

- (void)setPrimitiveIdValue:(int32_t)value_ {
	[self setPrimitiveId:[NSNumber numberWithInt:value_]];
}

@dynamic legacyName;

@dynamic name;

@dynamic rgbBackground;

@dynamic rgbPrimary;

@dynamic rgbSecondary;

@dynamic showMercs;

- (BOOL)showMercsValue {
	NSNumber *result = [self showMercs];
	return [result boolValue];
}

- (void)setShowMercsValue:(BOOL)value_ {
	[self setShowMercs:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveShowMercsValue {
	NSNumber *result = [self primitiveShowMercs];
	return [result boolValue];
}

- (void)setPrimitiveShowMercsValue:(BOOL)value_ {
	[self setPrimitiveShowMercs:[NSNumber numberWithBool:value_]];
}

@dynamic army;

@dynamic rosters;

- (NSMutableOrderedSet*)rostersSet {
	[self willAccessValueForKey:@"rosters"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"rosters"];

	[self didAccessValueForKey:@"rosters"];
	return result;
}

@dynamic units;

- (NSMutableSet*)unitsSet {
	[self willAccessValueForKey:@"units"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"units"];

	[self didAccessValueForKey:@"units"];
	return result;
}

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newRostersFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Roster" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"sectorial == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newUnitsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Unit" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"sectorial == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

@implementation _Sectorial (RostersCoreDataGeneratedAccessors)
- (void)addRosters:(NSOrderedSet*)value_ {
	[self.rostersSet unionOrderedSet:value_];
}
- (void)removeRosters:(NSOrderedSet*)value_ {
	[self.rostersSet minusOrderedSet:value_];
}
- (void)addRostersObject:(Roster*)value_ {
	[self.rostersSet addObject:value_];
}
- (void)removeRostersObject:(Roster*)value_ {
	[self.rostersSet removeObject:value_];
}
- (void)insertObject:(Roster*)value inRostersAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"rosters"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self rosters]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"rosters"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"rosters"];
}
- (void)removeObjectFromRostersAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"rosters"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self rosters]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"rosters"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"rosters"];
}
- (void)insertRosters:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"rosters"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self rosters]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"rosters"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"rosters"];
}
- (void)removeRostersAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"rosters"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self rosters]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"rosters"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"rosters"];
}
- (void)replaceObjectInRostersAtIndex:(NSUInteger)idx withObject:(Roster*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"rosters"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self rosters]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"rosters"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"rosters"];
}
- (void)replaceRostersAtIndexes:(NSIndexSet *)indexes withRosters:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"rosters"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self rosters]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"rosters"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"rosters"];
}
@end

