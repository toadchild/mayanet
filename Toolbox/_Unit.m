// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Unit.m instead.

#import "_Unit.h"

const struct UnitAttributes UnitAttributes = {
	.ava = @"ava",
	.id = @"id",
	.isMerc = @"isMerc",
	.isc = @"isc",
	.legacyIsc = @"legacyIsc",
	.mercAva = @"mercAva",
	.name = @"name",
	.note = @"note",
	.sharedAvaId = @"sharedAvaId",
	.strOverride = @"strOverride",
	.wOverride = @"wOverride",
};

const struct UnitRelationships UnitRelationships = {
	.army = @"army",
	.aspects = @"aspects",
	.notes = @"notes",
	.options = @"options",
	.profiles = @"profiles",
	.sectorial = @"sectorial",
	.specopBuilds = @"specopBuilds",
	.specops = @"specops",
	.type = @"type",
};

@implementation UnitID
@end

@implementation _Unit

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Unit" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Unit";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Unit" inManagedObjectContext:moc_];
}

- (UnitID*)objectID {
	return (UnitID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"avaValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ava"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isMercValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isMerc"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"mercAvaValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"mercAva"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"sharedAvaIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sharedAvaId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"strOverrideValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"strOverride"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"wOverrideValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"wOverride"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic ava;

- (int16_t)avaValue {
	NSNumber *result = [self ava];
	return [result shortValue];
}

- (void)setAvaValue:(int16_t)value_ {
	[self setAva:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveAvaValue {
	NSNumber *result = [self primitiveAva];
	return [result shortValue];
}

- (void)setPrimitiveAvaValue:(int16_t)value_ {
	[self setPrimitiveAva:[NSNumber numberWithShort:value_]];
}

@dynamic id;

- (int32_t)idValue {
	NSNumber *result = [self id];
	return [result intValue];
}

- (void)setIdValue:(int32_t)value_ {
	[self setId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdValue {
	NSNumber *result = [self primitiveId];
	return [result intValue];
}

- (void)setPrimitiveIdValue:(int32_t)value_ {
	[self setPrimitiveId:[NSNumber numberWithInt:value_]];
}

@dynamic isMerc;

- (BOOL)isMercValue {
	NSNumber *result = [self isMerc];
	return [result boolValue];
}

- (void)setIsMercValue:(BOOL)value_ {
	[self setIsMerc:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsMercValue {
	NSNumber *result = [self primitiveIsMerc];
	return [result boolValue];
}

- (void)setPrimitiveIsMercValue:(BOOL)value_ {
	[self setPrimitiveIsMerc:[NSNumber numberWithBool:value_]];
}

@dynamic isc;

@dynamic legacyIsc;

@dynamic mercAva;

- (int16_t)mercAvaValue {
	NSNumber *result = [self mercAva];
	return [result shortValue];
}

- (void)setMercAvaValue:(int16_t)value_ {
	[self setMercAva:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveMercAvaValue {
	NSNumber *result = [self primitiveMercAva];
	return [result shortValue];
}

- (void)setPrimitiveMercAvaValue:(int16_t)value_ {
	[self setPrimitiveMercAva:[NSNumber numberWithShort:value_]];
}

@dynamic name;

@dynamic note;

@dynamic sharedAvaId;

- (int32_t)sharedAvaIdValue {
	NSNumber *result = [self sharedAvaId];
	return [result intValue];
}

- (void)setSharedAvaIdValue:(int32_t)value_ {
	[self setSharedAvaId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSharedAvaIdValue {
	NSNumber *result = [self primitiveSharedAvaId];
	return [result intValue];
}

- (void)setPrimitiveSharedAvaIdValue:(int32_t)value_ {
	[self setPrimitiveSharedAvaId:[NSNumber numberWithInt:value_]];
}

@dynamic strOverride;

- (BOOL)strOverrideValue {
	NSNumber *result = [self strOverride];
	return [result boolValue];
}

- (void)setStrOverrideValue:(BOOL)value_ {
	[self setStrOverride:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveStrOverrideValue {
	NSNumber *result = [self primitiveStrOverride];
	return [result boolValue];
}

- (void)setPrimitiveStrOverrideValue:(BOOL)value_ {
	[self setPrimitiveStrOverride:[NSNumber numberWithBool:value_]];
}

@dynamic wOverride;

- (BOOL)wOverrideValue {
	NSNumber *result = [self wOverride];
	return [result boolValue];
}

- (void)setWOverrideValue:(BOOL)value_ {
	[self setWOverride:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveWOverrideValue {
	NSNumber *result = [self primitiveWOverride];
	return [result boolValue];
}

- (void)setPrimitiveWOverrideValue:(BOOL)value_ {
	[self setPrimitiveWOverride:[NSNumber numberWithBool:value_]];
}

@dynamic army;

@dynamic aspects;

@dynamic notes;

- (NSMutableOrderedSet*)notesSet {
	[self willAccessValueForKey:@"notes"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"notes"];

	[self didAccessValueForKey:@"notes"];
	return result;
}

@dynamic options;

- (NSMutableOrderedSet*)optionsSet {
	[self willAccessValueForKey:@"options"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"options"];

	[self didAccessValueForKey:@"options"];
	return result;
}

@dynamic profiles;

- (NSMutableOrderedSet*)profilesSet {
	[self willAccessValueForKey:@"profiles"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"profiles"];

	[self didAccessValueForKey:@"profiles"];
	return result;
}

@dynamic sectorial;

@dynamic specopBuilds;

- (NSMutableSet*)specopBuildsSet {
	[self willAccessValueForKey:@"specopBuilds"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specopBuilds"];

	[self didAccessValueForKey:@"specopBuilds"];
	return result;
}

@dynamic specops;

@dynamic type;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newNotesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"UnitNote" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"unit == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newOptionsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"UnitOption" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"units CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newProfilesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"UnitProfile" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"unit == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecopBuildsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsBuild" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"unit == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

@implementation _Unit (NotesCoreDataGeneratedAccessors)
- (void)addNotes:(NSOrderedSet*)value_ {
	[self.notesSet unionOrderedSet:value_];
}
- (void)removeNotes:(NSOrderedSet*)value_ {
	[self.notesSet minusOrderedSet:value_];
}
- (void)addNotesObject:(UnitNote*)value_ {
	[self.notesSet addObject:value_];
}
- (void)removeNotesObject:(UnitNote*)value_ {
	[self.notesSet removeObject:value_];
}
- (void)insertObject:(UnitNote*)value inNotesAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"notes"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self notes]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"notes"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"notes"];
}
- (void)removeObjectFromNotesAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"notes"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self notes]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"notes"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"notes"];
}
- (void)insertNotes:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"notes"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self notes]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"notes"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"notes"];
}
- (void)removeNotesAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"notes"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self notes]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"notes"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"notes"];
}
- (void)replaceObjectInNotesAtIndex:(NSUInteger)idx withObject:(UnitNote*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"notes"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self notes]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"notes"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"notes"];
}
- (void)replaceNotesAtIndexes:(NSIndexSet *)indexes withNotes:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"notes"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self notes]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"notes"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"notes"];
}
@end

@implementation _Unit (OptionsCoreDataGeneratedAccessors)
- (void)addOptions:(NSOrderedSet*)value_ {
	[self.optionsSet unionOrderedSet:value_];
}
- (void)removeOptions:(NSOrderedSet*)value_ {
	[self.optionsSet minusOrderedSet:value_];
}
- (void)addOptionsObject:(UnitOption*)value_ {
	[self.optionsSet addObject:value_];
}
- (void)removeOptionsObject:(UnitOption*)value_ {
	[self.optionsSet removeObject:value_];
}
- (void)insertObject:(UnitOption*)value inOptionsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"options"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self options]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"options"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"options"];
}
- (void)removeObjectFromOptionsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"options"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self options]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"options"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"options"];
}
- (void)insertOptions:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"options"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self options]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"options"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"options"];
}
- (void)removeOptionsAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"options"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self options]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"options"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"options"];
}
- (void)replaceObjectInOptionsAtIndex:(NSUInteger)idx withObject:(UnitOption*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"options"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self options]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"options"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"options"];
}
- (void)replaceOptionsAtIndexes:(NSIndexSet *)indexes withOptions:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"options"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self options]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"options"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"options"];
}
@end

@implementation _Unit (ProfilesCoreDataGeneratedAccessors)
- (void)addProfiles:(NSOrderedSet*)value_ {
	[self.profilesSet unionOrderedSet:value_];
}
- (void)removeProfiles:(NSOrderedSet*)value_ {
	[self.profilesSet minusOrderedSet:value_];
}
- (void)addProfilesObject:(UnitProfile*)value_ {
	[self.profilesSet addObject:value_];
}
- (void)removeProfilesObject:(UnitProfile*)value_ {
	[self.profilesSet removeObject:value_];
}
- (void)insertObject:(UnitProfile*)value inProfilesAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"profiles"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self profiles]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"profiles"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"profiles"];
}
- (void)removeObjectFromProfilesAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"profiles"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self profiles]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"profiles"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"profiles"];
}
- (void)insertProfiles:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"profiles"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self profiles]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"profiles"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"profiles"];
}
- (void)removeProfilesAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"profiles"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self profiles]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"profiles"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"profiles"];
}
- (void)replaceObjectInProfilesAtIndex:(NSUInteger)idx withObject:(UnitProfile*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"profiles"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self profiles]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"profiles"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"profiles"];
}
- (void)replaceProfilesAtIndexes:(NSIndexSet *)indexes withProfiles:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"profiles"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self profiles]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"profiles"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"profiles"];
}
@end

