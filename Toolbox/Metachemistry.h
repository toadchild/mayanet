#import "_Metachemistry.h"

@interface Metachemistry : _Metachemistry {}
// Custom logic goes here.

+(Metachemistry*)rollWithLevelTwo:(BOOL)levelTwo;

@end
