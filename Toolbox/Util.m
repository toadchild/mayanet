//
//  Util.m
//  Toolbox
//
//  Created by Paul on 10/28/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "Util.h"

@implementation Util

+(NSString*)urlSafeBase64:(NSString*)base64
{
    NSString* urlSafe =[base64 stringByReplacingOccurrencesOfString:@"+" withString:@"-"];
    urlSafe =[urlSafe stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    urlSafe =[urlSafe stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
    return urlSafe;
}

+(NSString*)base64FromURLSafe:(NSString*)urlSafe
{
    NSString* base64 =[urlSafe stringByReplacingOccurrencesOfString:@"-" withString:@"+"];
    base64 =[base64 stringByReplacingOccurrencesOfString:@"_" withString:@"/"];
    base64 =[base64 stringByReplacingOccurrencesOfString:@"%3D" withString:@"="];
    return base64;
}

+(NSString*) concatArray:(NSArray*)stringArray
{
    NSString* returnString = @"";
    for(NSString* string in stringArray)
    {
        if([returnString compare:@""] == NSOrderedSame)
        {
            returnString = string;
        }
        else
        {
            returnString = [NSString stringWithFormat:@"%@, %@",returnString, string];
        }
    }
    return returnString;
}

+(NSString*) conditionalConcatArray:(NSArray*)stringArray seperator:(NSString*)seperator
{
    NSString* returnString = @"";
    for(NSString* string in stringArray)
    {
        if([returnString compare:@""] == NSOrderedSame)
        {
            returnString = string;
        }
        else
        {
            //If the element is not empty insert seperator
            if([string compare:@""] != NSOrderedSame)
            {
                returnString = [NSString stringWithFormat:@"%@%@",returnString, seperator];
            }
            
            returnString = [NSString stringWithFormat:@"%@%@",returnString, string];
        }
    
    }
    return returnString;
}



@end
