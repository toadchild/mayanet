// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TextNote.m instead.

#import "_TextNote.h"

const struct TextNoteAttributes TextNoteAttributes = {
	.text = @"text",
};

@implementation TextNoteID
@end

@implementation _TextNote

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TextNote" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TextNote";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TextNote" inManagedObjectContext:moc_];
}

- (TextNoteID*)objectID {
	return (TextNoteID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic text;

#if TARGET_OS_IPHONE

#endif

@end

