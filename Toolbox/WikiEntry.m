#import "WikiEntry.h"
#import "AppDelegate.h"

@interface WikiEntry ()

// Private interface goes here.

@end


@implementation WikiEntry

// Create object cache for fast lookups.
static NSMutableDictionary* wikiEntryDictionary = nil;
+(void) initialize
{
    wikiEntryDictionary = [[NSMutableDictionary alloc] init];
}

// Custom logic goes here.
- (NSString *)nameFirstLetter {
    [self willAccessValueForKey:@"nameFirstLetter"];
    NSString *aString = [[self valueForKey:@"name"] uppercaseString];
    
    // support UTF-16:
    NSString *stringToReturn = [aString substringWithRange:[aString rangeOfComposedCharacterSequenceAtIndex:0]];
    
    // OR no UTF-16 support:
    //NSString *stringToReturn = [aString substringToIndex:1];
    
    [self didAccessValueForKey:@"nameFirstLetter"];
    return stringToReturn;
}

+(WikiEntry*) wikiEntryWithName:(NSString*)name
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    return [WikiEntry wikiEntryWithName:name andDelegate:delegate];
}

+(WikiEntry*) wikiEntryWithName:(NSString*)name andDelegate:(AppDelegate *)delegate
{
    WikiEntry* wikiEntry = [wikiEntryDictionary valueForKey:name];
    if(wikiEntry)
    {
        return wikiEntry;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [WikiEntry entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchLimit:1];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"name = %@", name];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    NSArray* results = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [results count] == 1)
    {
        WikiEntry *wikiEntry = [results objectAtIndex:0];
        [wikiEntryDictionary setObject:wikiEntry forKey:name];
        return wikiEntry;
    }
    return nil;
}

-(NSString*) urlEscapedName {
    return [self.name stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
}

@end
