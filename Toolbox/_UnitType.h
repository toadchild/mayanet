// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UnitType.h instead.

#import <CoreData/CoreData.h>

extern const struct UnitTypeAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *sortOrder;
} UnitTypeAttributes;

extern const struct UnitTypeRelationships {
	__unsafe_unretained NSString *profiles;
	__unsafe_unretained NSString *specOps;
	__unsafe_unretained NSString *units;
} UnitTypeRelationships;

@class UnitProfile;
@class SpecOps;
@class Unit;

@interface UnitTypeID : NSManagedObjectID {}
@end

@interface _UnitType : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UnitTypeID* objectID;

@property (nonatomic, strong) NSString* code;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sortOrder;

@property (atomic) int16_t sortOrderValue;
- (int16_t)sortOrderValue;
- (void)setSortOrderValue:(int16_t)value_;

//- (BOOL)validateSortOrder:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *profiles;

- (NSMutableSet*)profilesSet;

@property (nonatomic, strong) NSSet *specOps;

- (NSMutableSet*)specOpsSet;

@property (nonatomic, strong) NSSet *units;

- (NSMutableSet*)unitsSet;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newProfilesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecOpsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newUnitsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _UnitType (ProfilesCoreDataGeneratedAccessors)
- (void)addProfiles:(NSSet*)value_;
- (void)removeProfiles:(NSSet*)value_;
- (void)addProfilesObject:(UnitProfile*)value_;
- (void)removeProfilesObject:(UnitProfile*)value_;

@end

@interface _UnitType (SpecOpsCoreDataGeneratedAccessors)
- (void)addSpecOps:(NSSet*)value_;
- (void)removeSpecOps:(NSSet*)value_;
- (void)addSpecOpsObject:(SpecOps*)value_;
- (void)removeSpecOpsObject:(SpecOps*)value_;

@end

@interface _UnitType (UnitsCoreDataGeneratedAccessors)
- (void)addUnits:(NSSet*)value_;
- (void)removeUnits:(NSSet*)value_;
- (void)addUnitsObject:(Unit*)value_;
- (void)removeUnitsObject:(Unit*)value_;

@end

@interface _UnitType (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveCode;
- (void)setPrimitiveCode:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveSortOrder;
- (void)setPrimitiveSortOrder:(NSNumber*)value;

- (int16_t)primitiveSortOrderValue;
- (void)setPrimitiveSortOrderValue:(int16_t)value_;

- (NSMutableSet*)primitiveProfiles;
- (void)setPrimitiveProfiles:(NSMutableSet*)value;

- (NSMutableSet*)primitiveSpecOps;
- (void)setPrimitiveSpecOps:(NSMutableSet*)value;

- (NSMutableSet*)primitiveUnits;
- (void)setPrimitiveUnits:(NSMutableSet*)value;

@end
