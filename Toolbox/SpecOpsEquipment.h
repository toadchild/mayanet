#import "_SpecOpsEquipment.h"

@interface SpecOpsEquipment : _SpecOpsEquipment {}
+(SpecOpsEquipment*) specOpsEquipmentWithName:(NSString*)name;
@end
