// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CCSkill.h instead.

#import <CoreData/CoreData.h>

extern const struct CCSkillAttributes {
	__unsafe_unretained NSString *attMod;
	__unsafe_unretained NSString *burstMod;
	__unsafe_unretained NSString *damageMod;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *oppMod;
	__unsafe_unretained NSString *special;
} CCSkillAttributes;

@interface CCSkillID : NSManagedObjectID {}
@end

@interface _CCSkill : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CCSkillID* objectID;

@property (nonatomic, strong) NSString* attMod;

//- (BOOL)validateAttMod:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* burstMod;

//- (BOOL)validateBurstMod:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* damageMod;

//- (BOOL)validateDamageMod:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* oppMod;

//- (BOOL)validateOppMod:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* special;

//- (BOOL)validateSpecial:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

#endif

@end

@interface _CCSkill (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAttMod;
- (void)setPrimitiveAttMod:(NSString*)value;

- (NSString*)primitiveBurstMod;
- (void)setPrimitiveBurstMod:(NSString*)value;

- (NSString*)primitiveDamageMod;
- (void)setPrimitiveDamageMod:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveOppMod;
- (void)setPrimitiveOppMod:(NSString*)value;

- (NSString*)primitiveSpecial;
- (void)setPrimitiveSpecial:(NSString*)value;

@end
