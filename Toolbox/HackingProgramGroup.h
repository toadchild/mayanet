#import "_HackingProgramGroup.h"

@interface HackingProgramGroup : _HackingProgramGroup {}
+(HackingProgramGroup*) groupWithName:(NSString*)name;
@end
