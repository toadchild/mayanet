//
//  NotationTypeDef.h
//  Toolbox
//
//  Created by Paul on 12/12/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#ifndef Toolbox_NotationTypeDef_h
#define Toolbox_NotationTypeDef_h

enum NotationType
{
    eInvalid,
    eCamoNotation,
    eTOCamoNotation,
    eBootyNotation,
    eBootyLevel2Notation,
    eMetachemistryNotation,
    eMetachemistryLevel2Notation,
    eTextNotation,
}typedef NotationType;

#endif
