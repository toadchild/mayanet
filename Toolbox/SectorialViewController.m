//
//  ArmyViewController.m
//  Toolbox
//
//  Created by Paul on 10/20/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "SectorialViewController.h"
#import "Army.h"
#import "Sectorial.h"
#import "Unit.h"
#import "Util.h"
#import "UnitType.h"
#import "RosterListViewController.h"
#import "AppDelegate.h"

@interface SectorialViewController ()

@end

@implementation SectorialViewController

@synthesize sectorial;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    typeOffset = 1;
    [self setTitle: [self.sectorial abbr] ? [self.sectorial abbr] : [self.sectorial name]];
}

-(void)refreshUnits
{
    [self loadUnits];
    [self.tableView reloadData];
}

-(void) loadUnits
{
    //Get all the types
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [UnitType entityInManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setIncludesSubentities:FALSE];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    [fetchRequest setPredicate:nil];
    
    NSError* error;
    types = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error != nil)
    {
        
    }

    NSMutableDictionary* sortedUnitsByType =[[NSMutableDictionary alloc] init];
    for(UnitType* unitType in types)
    {
        NSArray* unitsOfType = [self unitsOfType:unitType includeMerc:self.sectorial.showMercsValue];
        if([unitsOfType count])
        {
            [sortedUnitsByType setObject:unitsOfType forKey:unitType.code];
        }
    }
    
    unitsByType = sortedUnitsByType;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationController.navigationBar.titleTextAttributes =
    @{NSForegroundColorAttributeName: [self.sectorial primaryColor]};
}

-(void)viewWillAppear:(BOOL)animated
{
    [self refreshUnits];
    self.tableView.hidden = FALSE;
    
    [self.navigationController.navigationBar setBarTintColor:[self.sectorial backgroundColor]];
    [self.navigationController.navigationBar setTintColor:[self.sectorial secondaryColor]];
    
    [super viewWillAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.tableView.hidden = TRUE;
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(!isSearching)
    {
        return 1 + [types count];
    }
    else
    {
        return 1 + [searchTypes count];
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    UnitType* type;
    NSArray* units;
    switch(section)
    {
        case 0:
                return 1;
        default:
            if(!isSearching)
            {
                type = [types objectAtIndex:section-1];
                units = [unitsByType objectForKey:type.code];
            }
            else
            {
                type = [searchTypes objectAtIndex:section-1];
                units = [searchUnitsByType objectForKey:type.code];
            }
            return [units count];
    }
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    UnitType* type;
    NSArray* units;
    switch(section)
    {
        case 0:
            return @"Rosters";
        default:
            if(!isSearching)
            {
                type = [types objectAtIndex:section-1];
                units = [unitsByType objectForKey:type.code];
                if([units count] != 0)
                {
                    return type.name;
                }
            }
            else
            {
                type = [searchTypes objectAtIndex:section-1];
                units = [searchUnitsByType objectForKey:type.code];
                if([units count] != 0)
                {
                    return type.name;
                }
            }
    }
    return nil;
}



#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"ViewRosterList"])
    {
        RosterListViewController *con = [segue destinationViewController];
        [con setSectorial:self.sectorial];
        [con setManagedObjectContext:self.managedObjectContext];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        [self performSegueWithIdentifier:@"ViewRosterList" sender:self.sectorial];
    }
    else
    {
        Unit* unit = [self unitAtIndexPath:indexPath tableView:tableView];
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        MiddleNavigationController* con = delegate.containerViewController.middleViewController;
        [con showUnit:unit];
    }
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section > 0)
    {
        [self.tableView selectRowAtIndexPath:indexPath animated:FALSE scrollPosition:UITableViewScrollPositionNone];
        Unit* unit = [self unitAtIndexPath:indexPath tableView:tableView];
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        MiddleNavigationController* con = delegate.containerViewController.middleViewController;
        [con showUnitDetail:unit];
    }
}



- (void)configureCell:(UITableViewCell *)cell tableView:(UITableView*)tableView atIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.section == 0)
    {
        NSString* title = cell.textLabel.text = [sectorial name];
        cell.textLabel.text = title;
        NSString* imageTitle = [sectorial imageTitle];
        cell.imageView.image = [UIImage imageNamed:imageTitle];
        cell.textLabel.text = @"View Rosters";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    else
    {
        Unit* unit = [self unitAtIndexPath:indexPath tableView:tableView];
        NSString* title = cell.textLabel.text = [unit isc];
        cell.textLabel.text = title;
        NSString* imageTitle = [unit imageTitle];
        cell.imageView.image = [UIImage imageNamed:imageTitle];
        cell.accessoryType = UITableViewCellAccessoryDetailButton;
        
    }
}

-(NSPredicate*)searchPredicate:(NSPredicate*)otherPredicate
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"sectorial = %@", self.sectorial];
    return [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate,otherPredicate]];
}

- (IBAction)addRoster:(id)sender
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    if(self.sectorial)
    {
        [dict setObject:self.sectorial forKey:@"sectorial"];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewRoster" object:dict];
}

-(BOOL)includeMerc
{
    return self.sectorial.showMercsValue;
}

@end
