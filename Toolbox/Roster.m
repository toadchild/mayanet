#import "Roster.h"
#import "Army.h"
#import "CombatGroup.h"
#import "Sectorial.h"
#import "AppDelegate.h"
#import "UnitOption.h"
#import "Unit.h"
#import "UnitProfile.h"
#import "GroupMember.h"
#import "SpecOpsBuild.h"
#import "SpecOpsBaseUnitOption.h"
#import "SpecOpsAspects.h"
#import "RosterValidator.h"
#import "Util.h"
#import "SpecOps.h"
#import "SpecOpsWeapon.h"
#import "SpecOpsSpecialRule.h"
#import "SpecOpsEquipment.h"
#import "Weapon.h"
#import "SpecialRule.h"
#import "Note.h"
#import "UnitAspects.h"
@interface Roster ()

// Private interface goes here.

@end

// Change this when you want to force a JSON serialization break
// Done here to prevent N2 lsits from trying to import in N3.
#define JSON_FORMAT_VERSION 2
// Starting with Human Sphere N3 we use integer IDs instead of string names.
#define JSON_FORMAT_VERSION_UNIT_ID 3

@implementation Roster
@synthesize failedValidators;

static bool inUpgradeMode = FALSE;
    
- (void)awakeFromInsert
{
    [super awakeFromInsert];
    self.dateCreated = [NSDate date];
}

-(void)awakeFromFetch
{
    [super awakeFromFetch];
}

-(void)prepareForDeletion
{
}

-(void)didSave
{
}
    
+(Roster*) currentRoster
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [Roster entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setIncludesSubentities:TRUE];
    
    [fetchRequest setFetchLimit:1];

    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"isCurrent = %@", [NSNumber numberWithBool:TRUE]];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    NSArray* results = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [results count] == 1)
    {
        return [results objectAtIndex:0];
    }
    return nil;
}

+(BOOL)setCurrentRoster:(Roster*)newCurrentRoster
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    Roster* currentRoster = [Roster currentRoster];
    if(currentRoster != nil)
    {
        currentRoster.isCurrent = [NSNumber numberWithBool:FALSE];
    }
    if(newCurrentRoster != nil)
    {
        newCurrentRoster.isCurrent = [NSNumber numberWithBool:TRUE];
    }
    NSError* error;
    [delegate.managedObjectContext save:&error];
    return (error != nil);
}

    
+(Roster*) alternateRoster
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [Roster entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setIncludesSubentities:TRUE];
    
    [fetchRequest setFetchLimit:1];

    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"isAlternate = %@", [NSNumber numberWithBool:TRUE]];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    NSArray* results = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [results count] == 1)
    {
        return [results objectAtIndex:0];
    }
    return nil;
}

+(BOOL)setAlternateRoster:(Roster*)newAlternateRoster
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    Roster* alternateRoster = [Roster alternateRoster];
    if(alternateRoster != nil)
    {
        alternateRoster.isAlternate = [NSNumber numberWithBool:FALSE];
    }
    if(newAlternateRoster != nil)
    {
        newAlternateRoster.isAlternate = [NSNumber numberWithBool:TRUE];
    }
    NSError* error;
    [delegate.managedObjectContext save:&error];
    return (error != nil);
}

+(Roster*)createNewRosterWithSectorial:(Sectorial *)sectorial name:(NSString *)name pointCap:(NSInteger)pointCap
{
    Roster* roster = [Roster createNewRosterWithName:name pointCap:pointCap];
    if(roster)
    {
        roster.sectorial = sectorial;
        roster.army = sectorial.army;
        [roster clearRosterCache];
    }
    [roster onRosterChange];
    return roster;
}
+(Roster*)createNewRosterWithArmy:(Army *)army name:(NSString *)name pointCap:(NSInteger)pointCap
{
    Roster* roster = [Roster createNewRosterWithName:name pointCap:pointCap];
    if(roster)
    {
        roster.army = army;
        [roster clearRosterCache];
    }
    [roster onRosterChange];
    return roster;
}

+(Roster*)createNewRosterWithName:(NSString *)name pointCap:(NSInteger)pointCap
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    Roster* roster = [Roster insertInManagedObjectContext:[delegate managedObjectContext]];
    
    //Create first Combat Group
    CombatGroup* firstCombatGroup = [CombatGroup insertInManagedObjectContext:[delegate managedObjectContext]];
    
    [roster addCombatGroupsObject:firstCombatGroup];
    
    roster.name = name;
    roster.pointcap = [NSNumber numberWithInteger:pointCap];
    
    return roster;
}

+(NSString*)newRosterTitle
{
    return @"Untitled Roster";
}

-(BOOL) testUnit:(Unit*) unit unitOption:(UnitOption*)unitOption
{
    if(self.sectorial != nil)
    {
        if(unit.sectorial != self.sectorial)
            return FALSE;
    }
    else if(self.army != nil)
    {
        if(unit.army != self.army)
            return FALSE;
    }
    return true;
}

-(void) resetDead
{
    for(CombatGroup* combatGroup in self.combatGroups)
    {
        for(GroupMember* member in combatGroup.groupMembers)
        {
            member.isDeadValue = FALSE;
        }
    }
}

-(void) reset
{
    [self.combatGroupsSet removeAllObjects];
    
    //Create first Combat Group
    CombatGroup* firstCombatGroup = [CombatGroup insertInManagedObjectContext:[self managedObjectContext]];
    
    [self addCombatGroupsObject:firstCombatGroup];
    
    [self onRosterChange];
}

-(CombatGroup*) getCombatGroupForNewAddition
{
    return [self getCombatGroupForNewAdditions:1];
}

-(CombatGroup*) getCombatGroupForNewAdditions:(int)neededSlots
{
    for(CombatGroup* combatGroup in [self.combatGroups reversedOrderedSet])
    {
        if([combatGroup numberOfFigures] <= 10-neededSlots)
        {
            return combatGroup;
        }
    }

    CombatGroup* newCombatGroup = [CombatGroup insertInManagedObjectContext:self.managedObjectContext];
    [self addCombatGroupsObject:newCombatGroup];
    return newCombatGroup;

}

-(BOOL) addUnit:(Unit*) unit unitOption:(UnitOption*)unitOption
{
    return [self addUnit:unit unitOption:unitOption combatGroup:nil];
}

-(BOOL) addUnit:(Unit*) unit unitOption:(UnitOption*)unitOption combatGroup:(CombatGroup*)combatGroup;
{
    if(self.sectorial != nil)
    {
        if(unit.sectorial != self.sectorial)
            return FALSE;
    }
    else if(self.army != nil)
    {
        if(unit.army != self.army)
            return FALSE;
    }
    
    //Create new GroupMember
    GroupMember* newGroupMember = [GroupMember insertInManagedObjectContext:self.managedObjectContext];
    newGroupMember.unitOption = unitOption;
    int neededSlots = [newGroupMember takesCombatSlotWithinRoster:self] ? 1 : 0;

    //Detect Independent Profile
    if([unit.profiles count] != 0)
    {
        for(UnitProfile* unitProfile in unit.profiles)
        {
            if(unitProfile.isIndependentValue &&
                (!unitProfile.optionSpecificValue ||
                   (unitProfile.optionSpecificValue && [unitOption.enabledUnitProfile containsObject:unitProfile])))
            {
                // This independent unit profile applies to either all options or this specific one
                GroupMember* newProfileGroupMember;
                newProfileGroupMember = [GroupMember insertInManagedObjectContext:self.managedObjectContext];
                newProfileGroupMember.unitProfile = unitProfile;
        
                [newGroupMember.childrenGroupMembersSet addObject:newProfileGroupMember];
                neededSlots += [newProfileGroupMember takesCombatSlotWithinRoster:self] ? 1 : 0;
            }
        }
    }
    
    CombatGroup* foundCombatGroup = combatGroup ? combatGroup : [self getCombatGroupForNewAdditions:neededSlots];
    //Add to CombatGroup
    [foundCombatGroup addGroupMembersObject:newGroupMember];

    //Add Profile Member(s) to CombatGroup
    for(GroupMember *profileGroupMember in newGroupMember.childrenGroupMembersSet)
    {
        [foundCombatGroup addGroupMembersObject:profileGroupMember];
    }
    
    [self onRosterChange];
    return TRUE;
}

-(BOOL) changeGroupMember:(GroupMember*)groupMember toOption:(UnitOption*)newUnitOption
{
    groupMember.unitOption = newUnitOption;
    
    // If old option had a sub-profile, delete it
    for(GroupMember* child in groupMember.childrenGroupMembers)
    {
        if(child.unitProfile.optionSpecificValue && child.unitProfile.independentCost)
        {
            [self.managedObjectContext deleteObject:child];
        }
    }
    
    // If new option has a sub-profile, add it
    for(UnitProfile* unitProfile in [newUnitOption.enabledUnitProfile allObjects])
    {
        //Add Profile Member to CombatGroup
        GroupMember* newProfileGroupMember = nil;
        
        if(unitProfile.isIndependentValue && unitProfile.independentCostValue)
        {
            newProfileGroupMember = [GroupMember insertInManagedObjectContext:self.managedObjectContext];
            newProfileGroupMember.unitProfile = unitProfile;

            [groupMember.childrenGroupMembersSet addObject:newProfileGroupMember];
            [groupMember.combatGroup addGroupMember:newProfileGroupMember atIndex:[groupMember.combatGroup indexOfGroupMember:groupMember] + 1 withChildren:FALSE];
        }
    }

    [self onRosterChange];
    return TRUE;
}


-(BOOL) addSpecOpsBuild:(SpecOpsBuild*)specOpsBuild
{
    return [self addSpecOpsBuild:specOpsBuild combatGroup:nil];
}

-(BOOL) addSpecOpsBuild:(SpecOpsBuild*)specOpsBuild combatGroup:(CombatGroup*)combatGroup
{
    if(self.sectorial != nil)
    {
        if(specOpsBuild.unit.sectorial != self.sectorial)
            return FALSE;
    }
    else if(self.army != nil)
    {
        if(specOpsBuild.unit.army != self.army)
            return FALSE;
    }
    
    CombatGroup* foundCombatGroup = combatGroup ? combatGroup : [self getCombatGroupForNewAddition];
    
    //Create new GroupMember
    GroupMember* newGroupMember = [GroupMember insertInManagedObjectContext:self.managedObjectContext];
    newGroupMember.specops = specOpsBuild;
    
    //Add to CombatGroup
    [foundCombatGroup addGroupMembersObject:newGroupMember];
    
    [self onRosterChange];
    return TRUE;
    
}

-(void) deleteCombatGroup:(CombatGroup*)combatGroup
{
    [self.managedObjectContext deleteObject:combatGroup];
    [self onRosterChange];
}
    
-(void)deleteGroupMember:(GroupMember*)groupMember
{
    for(GroupMember* child in groupMember.childrenGroupMembers)
    {
        [self.managedObjectContext deleteObject:child];
    }
    [self.managedObjectContext deleteObject:groupMember];
    [self onRosterChange];
}

-(void) onRosterChange
{
    // Only save and validate if we are not currently in app init
    if (!inUpgradeMode) {
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [delegate saveContext];
        [self updateValidation];
    }
}

-(NSInteger) getAVACountUptoGroupMember:(GroupMember*)aGroupMember
{
    NSInteger avaCount = 0;
    Unit* unit;
    if(aGroupMember.unitOption)
    {
        unit = [self getUnitFrom:aGroupMember.unitOption];
    }
    else if(aGroupMember.specops)
    {
        unit = aGroupMember.specops.unit;
    }
    
    // Get the primary owner of the AVA value
    int avaKey = unit.idValue;
    if (unit.sharedAvaIdValue) {
        avaKey = unit.sharedAvaIdValue;
    }
    
    for(CombatGroup* combatGroup in self.combatGroups)
    {
        for(GroupMember* groupMember in combatGroup.groupMembers)
        {
            if(groupMember == aGroupMember)
                return avaCount;
            
            Unit* otherUnit;
            if(groupMember.specops != nil)
            {
                otherUnit = groupMember.specops.unit;
            }
            else
            {
                otherUnit = [self getUnitFrom:groupMember.unitOption];
            }

            // Compare keys, not actual pointer identity
            int otherAvaKey = otherUnit.idValue;
            if (otherUnit.sharedAvaIdValue) {
                otherAvaKey = otherUnit.sharedAvaIdValue;
            }

            if(otherAvaKey == avaKey)
            {
                avaCount++;
            }
        }
    }
    return avaCount;
}

-(NSInteger) getLtCountUptoGroupMember:(GroupMember*)aGroupMember
{
    NSInteger ltCount = 0;

    for(CombatGroup* combatGroup in self.combatGroups)
    {
        for(GroupMember* groupMember in combatGroup.groupMembers)
        {
            if(groupMember == aGroupMember)
                return ltCount;
            if(groupMember.unitOption)
            {
                if([groupMember.unitOption isLt])
                {
                    ltCount++;
                }
            }
        }
    }
    return ltCount;
}

-(Unit*) getUnitFrom:(UnitOption*)unitOption
{
    return [self getUnitFrom:unitOption inheritFromBase:FALSE];
}

-(Unit*) getUnitFrom:(UnitOption*)unitOption inheritFromBase:(BOOL)inherit
{
    for(Unit* unit in unitOption.units)
    {
        if(self.sectorial != nil)
        {
            if(unit.sectorial == self.sectorial || (inherit && unit.army == self.sectorial.army))
                return unit;
        }
        else if(self.army != nil)
        {
            if(unit.army == self.army)
                return unit;
        }
    }
    
    return nil;
}

- (void)addCombatGroupsObject:(CombatGroup *)value {
    NSMutableOrderedSet* tempSet = [NSMutableOrderedSet orderedSetWithOrderedSet:self.combatGroups];
    [tempSet addObject:value];
    self.combatGroups = tempSet;
}

-(NSUInteger)totalPoints
{
    int cost = 0;
    for(CombatGroup* combatGroup in self.combatGroups)
    {
        for(GroupMember* groupMember in combatGroup.groupMembers)
        {
            if(groupMember.specops != nil)
            {
                cost += groupMember.specops.baseUnitOption.unitOption.costValue;
            }
            else
            {
                cost += [groupMember.unitOption.cost integerValue];
            }
        }
    }
    return cost;
}

-(NSUInteger)totalDeadPoints
{
    SpecialRule *gSync = [SpecialRule specialRuleWithName:@"G: Synchronized"];
    SpecialRule *baggage = [SpecialRule specialRuleWithName:@"Baggage"];
    int cost = 0;
    for(CombatGroup* combatGroup in self.combatGroups)
    {
        for(GroupMember* groupMember in combatGroup.groupMembers)
        {
            if(groupMember.isDeadValue)
            {
                if(groupMember.specops != nil)
                {
                    cost += groupMember.specops.baseUnitOption.unitOption.costValue;
                }
                else if(groupMember.unitOption)
                {
                    if(groupMember.unitOption.isIndependentValue)
                    {
                        cost += groupMember.unitOption.independentCostValue;
                    }
                    else
                    {
                        cost += [groupMember.unitOption.cost integerValue];
                    }
                }
                else if(groupMember.unitProfile)
                {
                    // G: Sync independent profiles are not worth points
                    if(![groupMember.unitProfile.aspects.specialRules containsObject:gSync])
                    {
                        cost += groupMember.unitProfile.independentCostValue;
                    }
                }
            }
        }
    }
    return cost;
}


-(NSInteger)pointsToRetreat
{
    double retreatThreshold = 0.25;
    int overageForRetreat = (int)ceil([self totalPlayingPointCap] * retreatThreshold);
    
    return [self totalPlayingPoints] - [self totalDeadPoints] - overageForRetreat;
    
}

-(int)numbOfLivingBaggage
{
    int numbOfBaggage = 0;
    for(CombatGroup* combatGroup in self.combatGroups)
    {
        for(GroupMember* groupMember in combatGroup.groupMembers)
        {
            if(!groupMember.isDeadValue)
            {
                if(groupMember.unitOption)
                {
                    Unit* unit = [self getUnitFrom:groupMember.unitOption];
                    if([unit hasSpecialRule:@"Baggage"])
                    {
                        numbOfBaggage++;
                    }
                    else
                    {
                        for(SpecialRule* specialRule in groupMember.unitOption.specialRules)
                        {
                            if([[specialRule name] isEqualToString:@"Baggage"])
                            {
                                numbOfBaggage++;
                            }
                        }
                    }
                }
            }
        }
    }
    return numbOfBaggage;
}

-(NSUInteger)totalPlayingPoints
{
    return [self totalPoints] + [self numbOfLivingBaggage] * 20;
}

-(NSUInteger)totalPlayingPointCap
{
    return [self pointcapValue] + [self numbOfLivingBaggage] * 20;
}


-(double)totalSWC
{
    double swc = 0;
    for(CombatGroup* combatGroup in self.combatGroups)
    {
        for(GroupMember* groupMember in combatGroup.groupMembers)
        {
            if(groupMember.specops != nil)
            {
                swc += [groupMember.specops.baseUnitOption.unitOption.swc doubleValue];
            }
            else
            {
                swc += [groupMember.unitOption.swc doubleValue];
            }
        }
    }
    if (self.isSoldiersOfFortuneValue) {
        swc += 1.0;
    }

    return swc;
}

-(double)maxSWC
{
    return floor([self.pointcap doubleValue]/50.0);
}

static const int pointCapValues[] = {100, 120, 150, 200, 250, 300, 350, 400};
static const int numPointCapValues = sizeof(pointCapValues) / sizeof(*pointCapValues);

-(void)cyclePointCap
{
    int i;
    for(i = 0; i < numPointCapValues; i++)
    {
        if(pointCapValues[i] == self.pointcapValue)
        {
            break;
        }
    }
    
    self.pointcapValue = pointCapValues[(i + 1) % numPointCapValues];
}

-(NSInteger)costOfModelsWithSWC;
{
    int cost = 0;
    for(CombatGroup* combatGroup in self.combatGroups)
    {
        for(GroupMember* groupMember in combatGroup.groupMembers)
        {
            if(groupMember.specops != nil)
            {
                if([groupMember.specops.baseUnitOption.unitOption.swc doubleValue] != 0.0)
                {
                    cost += groupMember.specops.baseUnitOption.unitOption.costValue;
                }
            }
            else
            {
                if([groupMember.unitOption.swc doubleValue] != 0.0)
                {
                    cost += [groupMember.unitOption.cost integerValue];
                }
            }
        }
    }
    return cost;
}

-(NSInteger)numberOfModels
{
    int nModels = 0;
    for(CombatGroup* combatGroup in self.combatGroups)
    {
        nModels += [combatGroup.groupMembers count];
    }
    return nModels;
}

-(NSInteger)numberOfOrders
{
    return [self numberOfOrdersCountingDead:TRUE];
}

-(NSInteger)numberOfOrdersCountingDead:(BOOL)countingDead
{
    int nOrders = 0;
    for(CombatGroup* combatGroup in self.combatGroups)
    {
        nOrders += [combatGroup numberOfOrdersCountingDead:countingDead];
        
    }
    return nOrders;
}

    
-(NSArray*)unitOptions
{
    NSMutableArray* unitOptions = [[NSMutableArray alloc] init];
    for(CombatGroup* combatGroup in self.combatGroups)
    {
        for(GroupMember* groupMember in combatGroup.groupMembers)
        {
            UnitOption* unitOption = [groupMember unitOption];
            if(unitOption)
            {
                [unitOptions addObject:unitOption];
            }
        }
    }
    return unitOptions;
}

-(NSArray*)specOps
{
    NSMutableArray* specOps = [[NSMutableArray alloc] init];
    for(CombatGroup* combatGroup in self.combatGroups)
    {
        for(GroupMember* groupMember in combatGroup.groupMembers)
        {
            if(groupMember.specops != nil)
            {
                [specOps addObject:groupMember.specops];
            }
        }
    }
    return specOps;
}

-(NSArray*)unitProfiles
{
    NSMutableArray* unitProfiles = [[NSMutableArray alloc] init];
    for(CombatGroup* combatGroup in self.combatGroups)
    {
        for(GroupMember* groupMember in combatGroup.groupMembers)
        {
            UnitProfile* unitProfile = [groupMember unitProfile];
            if(unitProfile)
            {
                [unitProfiles addObject:unitProfile];
            }
        }
    }
    return unitProfiles;
}

-(void)setIsPlayingValue:(BOOL)value_
{
    [super setIsPlayingValue:value_];
    if(!value_)
    {
        //Clear groupMember notes;
        for(CombatGroup* combatGroup in self.combatGroups)
        {
            for(GroupMember* groupMember in combatGroup.groupMembers)
            {
                for(Note* note in groupMember.notes)
                {
                    [self.managedObjectContext deleteObject:note];
                }
            }
        }
        [self.managedObjectContext save:nil];
    }
}
    
-(void)updateValidation
{
    failedValidators = [RosterValidator validate:self];
}

-(BOOL)failedValidation
{
    return [self.failedValidators count] != 0;
}

+(NSArray*)exportAllWithDelegate:(AppDelegate*) delegate
{
    NSMutableArray* array = [[NSMutableArray alloc] init];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [Roster entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError* error;
    NSArray* rosters = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSAssert(error == nil, @"Error fetching list of rosters: %@", error);
    
    for(Roster* roster in rosters) {
        [array addObject:[roster toDictionaryWithDate:TRUE]];
    }
    
    return array;
}

+(void)importAllFromArray:(NSArray*) array withDelegate:(AppDelegate*) delegate
{
    // Suppress managed object context saves during upgrade
    inUpgradeMode = TRUE;
    for(NSDictionary* dict in array) {
        Roster* importedRoster = [Roster insertInManagedObjectContext:delegate.managedObjectContext];
        bool result = [importedRoster fromDictionary:dict];
        NSAssert(result, @"Failed to import saved roster!");
    }
    inUpgradeMode = FALSE;
}

-(NSDictionary*)toDictionaryWithDate:(bool)keepDate
{
    NSMutableDictionary *rosterDictionary = [[NSMutableDictionary alloc] init];
    
    //Create Roster header Dictionary
    NSMutableDictionary* headDictionary = [[NSMutableDictionary alloc] init];
    
    //Set Army/Sectorial Names
    if(self.sectorial)
    {
        [headDictionary setObject:self.sectorial.id forKey:@"sectorial"];
    }
    else
    {
        [headDictionary setObject:self.army.id forKey:@"army"];
    }

    //Set Point Cap
    [headDictionary setObject:self.pointcap forKey:@"pointcap"];
    [headDictionary setObject:self.name forKey:@"name"];
    if (self.isSoldiersOfFortune) {
        [headDictionary setObject:self.isSoldiersOfFortune forKey:@"sof"];
    }
    [headDictionary setObject:[NSNumber numberWithInt:JSON_FORMAT_VERSION_UNIT_ID] forKey:@"version"];
    if (keepDate) {
        [headDictionary setObject:self.dateCreated forKey:@"date"];
    }
    
    [rosterDictionary setObject:headDictionary forKey:@"head"];
    
    NSMutableArray* combatGroupsArray = [[NSMutableArray alloc] init];
    //Loop through combat groups
    for(CombatGroup* group in self.combatGroups)
    {
        NSMutableArray* membersArray = [[NSMutableArray alloc] init];
        
        for(GroupMember* member in group.groupMembers)
        {
            NSDictionary* memberDictionary;
            if(member.unitOption)
            {
                Unit* unit = [self getUnitFrom:member.unitOption];
                if (!unit) {
                    // Unit is somehow corrupted
                    continue;
                }
                memberDictionary = @{@"id": unit.id, @"code": member.unitOption.id};
            }
            else if(member.specops)
            {
                // Sectorials can have spec-ops from other sectorials of the same parent army

                Unit* baseUnit = [member.specops.baseUnitOption.unitOption getUnit];
                Unit* specOpsUnit = member.specops.unit;
                
                if (!baseUnit || !specOpsUnit) {
                    // Corrupted unit entries
                    continue;
                }
                
                //Base unit
                NSMutableDictionary* specOpsDictionary = [[NSMutableDictionary alloc] init];
                [specOpsDictionary setObject:[NSNumber numberWithBool:TRUE] forKey:@"specops"];
                [specOpsDictionary setObject:specOpsUnit.id forKey:@"id"];
                [specOpsDictionary setObject:baseUnit.id forKey:@"base_id"];
                
                //Aspects
                SpecOpsAspects* aspects = member.specops.aspects;
                NSDictionary* aspectsDictionary = [aspects toDictionary];
                
                [specOpsDictionary setObject:aspectsDictionary forKey:@"aspects"];
                
                [specOpsDictionary setObject:[member.specops getWeaponNamesArray] forKey:@"weapons"];
                [specOpsDictionary setObject:[member.specops getSpecialRuleNamesArray] forKey:@"specialRules"];
                
                memberDictionary = specOpsDictionary;
            }
            if(memberDictionary)
            {
                [membersArray addObject:memberDictionary];
            }
        }
        [combatGroupsArray addObject:membersArray];
        
        [rosterDictionary setObject:combatGroupsArray forKey:@"groups"];
    }
    return  rosterDictionary;
}

-(BOOL)fromURLSafeJSON64:(NSString*)urlSafeJSON64;
{
    
    NSError* error;
    NSData* data = [[NSData alloc] initWithBase64EncodedString:[Util base64FromURLSafe:urlSafeJSON64] options:0];
    if(data == nil)
    {
        return FALSE;
    }
    NSDictionary* rosterDictionary = [NSJSONSerialization JSONObjectWithData: data  options:0 error:&error];
    if(error == nil)
    {
        return [self fromDictionary:rosterDictionary];
    }
    return FALSE;
}

// HSN3 version of army import.  Uses integer IDs.
-(BOOL)fromDictionaryV3:(NSDictionary*)rosterDictionary
{
    //Absorb Head
    //Army/Sectorial
    NSDictionary* head = [rosterDictionary objectForKey:@"head"];
    if([head objectForKey:@"sectorial"])
    {
        Sectorial* sectorial = [Sectorial sectorialWithID:[[head objectForKey:@"sectorial"] intValue]];
        if(!sectorial)
        {
            return FALSE;
        }
        self.sectorial = sectorial;
        self.army = sectorial.army;
    }
    else if([head objectForKey:@"army"])
    {
        Army* army = [Army armyWithID:[[head objectForKey:@"army"] intValue]];
        if(!army)
        {
            return FALSE;
        }
        self.army = army;
    }
    else
    {
        return FALSE;
    }
    
    //Point Cap
    if(![head objectForKey:@"pointcap"])
    {
        return FALSE;
    }
    self.pointcap = [head objectForKey:@"pointcap"];
    
    //Name
    if(![head objectForKey:@"name"])
    {
        return FALSE;
    }
    self.name = [head objectForKey:@"name"];
    
    if ([head objectForKey:@"date"]) {
        self.dateCreated = [head objectForKey:@"date"];
    }
    
    if ([head objectForKey:@"sof"]) {
        self.isSoldiersOfFortune = [head objectForKey:@"sof"];
    }
    
    if(![rosterDictionary objectForKey:@"groups"])
    {
        return FALSE;
    }
    NSArray* groups = [rosterDictionary objectForKey:@"groups"];
    for(NSArray* group in groups)
    {
        CombatGroup* combatGroup = [CombatGroup insertInManagedObjectContext:self.managedObjectContext];
        [self addCombatGroupsObject:combatGroup];
        for(NSDictionary* member in group)
        {
            int idVal = [[member objectForKey:@"id"] intValue];
            
            //Test for Spec Ops
            if([member objectForKey:@"specops"])
            {
                SpecOpsBuild* specOpsBuild = [SpecOpsBuild insertInManagedObjectContext:self.managedObjectContext];
                Unit* unit;
                if(self.sectorial)
                {
                    unit = [self.sectorial unitWithID:idVal];
                }
                else
                {
                    unit = [self.army unitWithID:idVal];
                }
                if(!unit || !unit.specops)
                {
                    // Drop this unit and continue processing
                    continue;
                }
                
                specOpsBuild.unit = unit;
                
                int base_id = [[member objectForKey:@"base_id"] intValue];
                
                Unit* baseUnit;
                if(self.sectorial)
                {
                    baseUnit = [self.sectorial unitWithID:base_id];
                    
                    // Sectorials can have spec-ops from other sectorials of the same parent army
                    if(!baseUnit)
                    {
                        baseUnit = [self.sectorial.army unitWithID:base_id];
                    }
                    
                }
                else
                {
                    baseUnit = [self.army unitWithID:base_id];
                }
                if(!baseUnit)
                {
                    // Drop this unit and continue processing
                    continue;
                }
                
                //Find the base UnitOption
                for(SpecOpsBaseUnitOption* baseUnitOption in  unit.specops.baseUnitOptions)
                {
                    UnitOption* unitOption = baseUnitOption.unitOption;
                    if([baseUnit.options containsObject:unitOption])
                    {
                        specOpsBuild.baseUnitOption = baseUnitOption;
                    }
                }
                if(!specOpsBuild.baseUnitOption)
                {
                    // Drop this unit and continue processing
                    continue;
                }
                
                //Aspects
                specOpsBuild.aspects = [SpecOpsAspects insertInManagedObjectContext:self.managedObjectContext];
                
                [specOpsBuild.aspects fromDictionary:[member objectForKey:@"aspects"]];
                
                //Weapons
                for(NSString* weaponName in [member objectForKey:@"weapons"])
                {
                    SpecOpsWeapon* weapon = [SpecOpsWeapon specOpsWeaponWithName:weaponName];
                    if(weapon)
                    {
                        [specOpsBuild addWeaponsObject:weapon];
                    }
                }
                
                //Special Rules and equipment are in a shared list
                for(NSString* specialRuleName in [member objectForKey:@"specialRules"])
                {
                    SpecOpsSpecialRule* specialRule = [SpecOpsSpecialRule specOpsSpecialRuleWithName:specialRuleName];
                    SpecOpsEquipment* equipment = [SpecOpsEquipment specOpsEquipmentWithName:specialRuleName];
                    if(specialRule) {
                        [specOpsBuild addSpecialRulesObject:specialRule];
                    } else if (equipment) {
                        [specOpsBuild addEquipmentObject:equipment];
                    }
                }
                
                [self addSpecOpsBuild:specOpsBuild combatGroup:combatGroup];
            }
            else
            {
                int optionCode = [[member objectForKey:@"code"] intValue];
                
                if(!idVal || !optionCode)
                {
                    // Drop this unit and continue processing
                    continue;
                }
                Unit* unit;
                if(self.sectorial)
                {
                    unit = [self.sectorial unitWithID:idVal];
                }
                else
                {
                    unit = [self.army unitWithID:idVal];
                }
                if(!unit)
                {
                    // Drop this unit and continue processing
                    continue;
                }
                
                UnitOption* unitOption = [unit optionWithID:optionCode];
                if(!unitOption)
                {
                    // Drop this unit and continue processing
                    continue;
                }
                [self addUnit:unit unitOption:unitOption combatGroup:combatGroup];
            }
        }
    }
    return TRUE;
}

-(BOOL)fromDictionary:(NSDictionary*)rosterDictionary
{
    NSDictionary* head = [rosterDictionary objectForKey:@"head"];

    NSString *version = [head objectForKey:@"version"];
    if(!version)
    {
        return FALSE;
    }
    else if([version intValue] == JSON_FORMAT_VERSION_UNIT_ID)
    {
        return [self fromDictionaryV3:rosterDictionary];
    }
    else
    {
        return FALSE;
    }
}

-(NSString*)toURLSafeJSON64;
{
    NSError* error;
    NSData* json = [NSJSONSerialization dataWithJSONObject:[self toDictionaryWithDate:FALSE] options:0 error:&error];
    if(error == nil)
    {
        return [Util urlSafeBase64:[json base64EncodedStringWithOptions:0]];
    }
    return nil;
}

-(NSString*)urlWithWebLink:(BOOL)useWeblink
{
    if(useWeblink)
    {
        return [NSString stringWithFormat:@"http://mayanet.ghostlords.com/open.php?code=%@",[self toURLSafeJSON64]];
    }
    return [NSString stringWithFormat:@"mayanet://open?%@",[self toURLSafeJSON64]];
}
    
-(NSString*)toBBCode
{
    return [self toBBCodeUsingWebLink:TRUE];
}
    
-(NSString*)toBBCodeUsingWebLink:(BOOL)useWeblink
{
    SpecialRule *gSync = [SpecialRule specialRuleWithName:@"G: Synchronized"];

    NSString* parentName = self.sectorial ? self.sectorial.name : self.army.name;
    //Header
    NSString* rosterIcon = self.sectorial ? [self.sectorial webImageTitle] : [self.army webImageTitle];
    
    NSString* header = [NSString stringWithFormat:@"[b][img]%@[/img] %@ - %@[/b] (%lu/%d | %g/%g)\n",rosterIcon, parentName, self.name, (unsigned long)[self totalPoints],self.pointcapValue,[self totalSWC],[self maxSWC]];
    NSMutableArray* groups = [[NSMutableArray alloc] init];
    
    int count = 1;
    for(CombatGroup* combatGroup in self.combatGroups)
    {
        NSMutableArray* members = [[NSMutableArray alloc] init];
        NSString* seperator = @"\n";
        NSString* groupHeader = [NSString stringWithFormat:@"[b]Group #%d[/b] | %d Models | [img]%@[/img] %d [img]%@[/img] %d [img]%@[/img] %d %@",count++,
                           (int)[combatGroup numberOfFigures],
                           [self webImageOfName:@"regular"],
                           (int)[combatGroup numberOfRegularOrders],
                           [self webImageOfName:@"irregular"],
                           (int)[combatGroup numberOfIrregularOrders],
                           [self webImageOfName:@"impetuous"],
                           (int)[combatGroup numberOfImpetuousOrders],
                           seperator
                           ];
        
        for(GroupMember* groupMember in combatGroup.groupMembers)
        {
            NSString* memberIcon = @"";
            NSString* memberTitle = @"";
            NSString* memberCode = @"";
            NSString* memberStats = @" ";
            NSString* memberDetails = @"";
            double    memberSWC = 0.0;
            int memberCost = 0;
            NSString* memberXP = @"";
            
            if(groupMember.unitOption)
            {
                Unit* unit = [self getUnitFrom:groupMember.unitOption];
                memberIcon = [unit webImageTitle];
                memberTitle = [NSString stringWithFormat:@"%@", unit.name];
                memberCode = groupMember.unitOption.code;
                
                NSString* specialRules = [groupMember.unitOption specialRulesDescription];
                if([specialRules length] != 0)
                {
                    specialRules = [NSString stringWithFormat:@"%@, ",specialRules];
                }
                memberDetails = [NSString stringWithFormat:@"%@%@", specialRules,[groupMember.unitOption weaponDescription:unit seperator:@" / "]];
                memberSWC = groupMember.unitOption.independentSWC ? [groupMember.unitOption.independentSWC doubleValue] : [groupMember.unitOption.swc doubleValue];
                memberCost = groupMember.unitOption.independentCostValue ? groupMember.unitOption.independentCostValue : groupMember.unitOption.costValue;
            }
            else if(groupMember.unitProfile)
            {
                UnitProfile *unitProfile = groupMember.unitProfile;
                memberIcon = [unitProfile webImageTitle];
                memberTitle = [NSString stringWithFormat:@"%@", unitProfile.name];
                
                memberDetails = [unitProfile weaponDescriptionWithSeperator:@" / "];
                memberSWC = [unitProfile.independentSWC doubleValue];
                // Show G: Sync units without cost
                if([unitProfile.aspects.specialRules containsObject:gSync])
                {
                    memberCost = 0;
                }
                else
                {
                    memberCost = unitProfile.independentCostValue;
                }
            }
            else if(groupMember.specops)
            {
                Unit* unit = groupMember.specops.unit;
                memberIcon = [unit webImageTitle];
                memberTitle = [NSString stringWithFormat:@"%@", unit.isc];
                NSArray* specOpsStats = @[[groupMember.specops.aspects descriptionWithUnit:unit],[groupMember.specops specialRulesDescription],[groupMember.specops weaponDescription]];
                NSString* stats = [Util conditionalConcatArray:specOpsStats seperator:@", "];
                if([stats length] != 0)
                {
                    memberStats = [NSString stringWithFormat:@" (%@) ",stats];
                }
                
                NSString* specialRules = [groupMember.specops.baseUnitOption.unitOption specialRulesDescription];
                if([specialRules length] != 0)
                {
                    specialRules = [NSString stringWithFormat:@"%@, ",specialRules];
                }
                memberDetails = [NSString stringWithFormat:@"%@%@", specialRules,[groupMember.specops.baseUnitOption.unitOption weaponDescription:unit seperator:@" / "]];
                memberSWC = [groupMember.specops.baseUnitOption.unitOption.swc doubleValue];
                memberCost = groupMember.specops.baseUnitOption.unitOption.costValue;
                
                long xpCost = [groupMember.specops getCost];
                if(xpCost != 0)
                {
                    memberXP = [NSString stringWithFormat:@" | [b]%ld XP[/b]", xpCost ];
                }
                
            }
            NSString* swcString = memberSWC != 0.0 ? [NSString stringWithFormat:@"%g | ",memberSWC] : @"";
            NSString* costString = memberCost != 0 ? [NSString stringWithFormat:@"(%@[b]%d[/b]%@)",swcString, memberCost, memberXP] : @"";
            [members addObject:[NSString stringWithFormat:@"[b][img]%@[/img] %@[/b] [i]%@[/i] |%@%@ %@",
                                                        memberIcon,
                                                        memberTitle,
                                                        memberCode,
                                                        memberStats,
                                                        memberDetails,
                                                        costString
                                                        ]
             ];
        }
        NSString* group = [NSString stringWithFormat:@"%@\n%@",groupHeader,[Util conditionalConcatArray:members seperator:@"\n"]];
        [groups addObject:group];
    }
    
    NSString* footer = [NSString stringWithFormat:@"[img]%@[/img] [url=%@]Open in [b]MayaNet[/b][/url]",[self webImageOfName:@"app"],[self urlWithWebLink:useWeblink]];
    
    NSString* bbCode = [NSString stringWithFormat:@"%@\n%@\n\n%@", header, [Util conditionalConcatArray:groups seperator:@"\n\n"],footer];
    return bbCode;
}

-(NSString*) toEmailHTML
{
    return [NSString stringWithFormat:@"<br><br>--<br>%@",[self toHTML]];
}

-(NSString*)emailHeader
{
    NSString* parentName = self.sectorial ? self.sectorial.name : self.army.name;
    
    return [NSString stringWithFormat:@"[MayaNet] %@ - %@ (%lu/%d | %g/%g)\n",parentName, self.name, (unsigned long)[self totalPoints],self.pointcapValue,[self totalSWC],[self maxSWC]];
}

-(NSString*)webImageOfName:(NSString*)name
{
    return [NSString stringWithFormat:@"%@%@%@", URL_ROOT, name, WEB_IMAGE_SUFFIX];
}

-(NSString*)toHTML
{
    NSString* bbCode = [self toBBCode];
    NSString* html = [bbCode stringByReplacingOccurrencesOfString:@"[b]" withString:@"<span style=\"font-weight:bold\">"];
    html = [html stringByReplacingOccurrencesOfString:@"[/b]" withString:@"</span>"];
    
    html = [html stringByReplacingOccurrencesOfString:@"[i]" withString:@"<span style=\"font-style:italic\">"];
    html = [html stringByReplacingOccurrencesOfString:@"[/i]" withString:@"</span>"];
    
    html = [html stringByReplacingOccurrencesOfString:@"[img]" withString:@"<img src=\""];
    html = [html stringByReplacingOccurrencesOfString:@"[/img]" withString:@"\" />"];
    
    html = [html stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"];

    html = [html stringByReplacingOccurrencesOfString:@"[url=" withString:@"<a href=\""];
    html = [html stringByReplacingOccurrencesOfString:@"]Open" withString:@"\">Open"];
    html = [html stringByReplacingOccurrencesOfString:@"[/url]" withString:@"</a>"];
    
    return html;
}

-(NSString*)toLocalHTML
{
    NSString* html = [self toHTML];
    return [html stringByReplacingOccurrencesOfString:URL_ROOT withString:LOCAL_URL_ROOT];
    
}
    
-(Roster*)duplicate
{
    Roster* duplicate = [Roster insertInManagedObjectContext:self.managedObjectContext];
    [duplicate fromURLSafeJSON64:[self toURLSafeJSON64]];
    [duplicate setName:[duplicate.name stringByAppendingString:@" copy"]];
    [duplicate.managedObjectContext save: nil];
    [self clearRosterCache];
    return duplicate;
}

+(Roster*)rosterFromWebCode:(NSString*)webCode
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    Roster* importedRoster = [Roster insertInManagedObjectContext:delegate.managedObjectContext];
    if(![importedRoster fromURLSafeJSON64:webCode])
    {
        [delegate.managedObjectContext deleteObject:importedRoster];
        [[[UIAlertView alloc] initWithTitle:@"Import Error" message:@"The import code cannot be parsed." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return nil;
    }
    [importedRoster.managedObjectContext save:nil];
    [importedRoster clearRosterCache];
    return importedRoster;
}

-(void)clearRosterCache
{
    self.army.cachedRosters = nil;

    if(self.sectorial)
    {
        self.sectorial.cachedRosters = nil;
        self.sectorial.army.cachedRosters = nil;
    }
}

@end
