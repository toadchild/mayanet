// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SpecOps.h instead.

#import <CoreData/CoreData.h>

extern const struct SpecOpsRelationships {
	__unsafe_unretained NSString *baseUnitOptions;
	__unsafe_unretained NSString *type;
	__unsafe_unretained NSString *unit;
} SpecOpsRelationships;

@class SpecOpsBaseUnitOption;
@class UnitType;
@class Unit;

@interface SpecOpsID : NSManagedObjectID {}
@end

@interface _SpecOps : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SpecOpsID* objectID;

@property (nonatomic, strong) NSOrderedSet *baseUnitOptions;

- (NSMutableOrderedSet*)baseUnitOptionsSet;

@property (nonatomic, strong) UnitType *type;

//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *unit;

- (NSMutableSet*)unitSet;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newBaseUnitOptionsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newUnitFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _SpecOps (BaseUnitOptionsCoreDataGeneratedAccessors)
- (void)addBaseUnitOptions:(NSOrderedSet*)value_;
- (void)removeBaseUnitOptions:(NSOrderedSet*)value_;
- (void)addBaseUnitOptionsObject:(SpecOpsBaseUnitOption*)value_;
- (void)removeBaseUnitOptionsObject:(SpecOpsBaseUnitOption*)value_;

- (void)insertObject:(SpecOpsBaseUnitOption*)value inBaseUnitOptionsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromBaseUnitOptionsAtIndex:(NSUInteger)idx;
- (void)insertBaseUnitOptions:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeBaseUnitOptionsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInBaseUnitOptionsAtIndex:(NSUInteger)idx withObject:(SpecOpsBaseUnitOption*)value;
- (void)replaceBaseUnitOptionsAtIndexes:(NSIndexSet *)indexes withBaseUnitOptions:(NSArray *)values;

@end

@interface _SpecOps (UnitCoreDataGeneratedAccessors)
- (void)addUnit:(NSSet*)value_;
- (void)removeUnit:(NSSet*)value_;
- (void)addUnitObject:(Unit*)value_;
- (void)removeUnitObject:(Unit*)value_;

@end

@interface _SpecOps (CoreDataGeneratedPrimitiveAccessors)

- (NSMutableOrderedSet*)primitiveBaseUnitOptions;
- (void)setPrimitiveBaseUnitOptions:(NSMutableOrderedSet*)value;

- (UnitType*)primitiveType;
- (void)setPrimitiveType:(UnitType*)value;

- (NSMutableSet*)primitiveUnit;
- (void)setPrimitiveUnit:(NSMutableSet*)value;

@end
