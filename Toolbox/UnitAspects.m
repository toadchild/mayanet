#import "UnitAspects.h"


@interface UnitAspects ()

// Private interface goes here.

@end


@implementation UnitAspects

-(void) copyFrom:(UnitAspects*)source
{
    self.mov = source.mov;
    self.cc = source.cc;
    self.bs = source.bs;
    self.ph = source.ph;
    self.wip = source.wip;
    self.arm = source.arm;
    self.bts = source.bts;
    self.wounds = source.wounds;
    self.cube = source.cube;
    self.fury = source.fury;
    self.regular = source.regular;
    self.silhouette = source.silhouette;
    self.hackable = source.hackable;
    self.classification = source.classification;

    self.specialRules = [source.specialRules copy];
    self.weapons = [source.weapons copy];
}

-(UnitAspects*) clone
{
    UnitAspects* newAspects = [UnitAspects insertInManagedObjectContext:self.managedObjectContext];
    [newAspects copyFrom:self];
    return newAspects;
}

-(NSString*)convertedMov
{
    BOOL siUnits = [[NSUserDefaults standardUserDefaults] boolForKey:@"UseSIUnits"];
    if(!siUnits)
    {
        return self.mov;
    }
    else
    {
        NSArray* movList = [self.mov componentsSeparatedByString: @"-"];
        NSString* firstMov = [movList objectAtIndex: 0];
        NSString* secondMov;
        if([movList count] > 1){
            secondMov = [movList objectAtIndex: 1];
        }else{
            secondMov = 0;
        }
        
        double firstMovInteger = [firstMov doubleValue];
        double secondMovInteger = [secondMov doubleValue];
        
        return [NSString stringWithFormat:@"%d-%d", (int)(firstMovInteger*2.5), (int)(secondMovInteger*2.5)];
    }
}
@end
