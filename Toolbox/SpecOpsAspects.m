#import "SpecOpsAspects.h"
#import "Unit.h"
#import "UnitAspects.h"
#import "Util.h"

@interface SpecOpsAspects ()

// Private interface goes here.

@end


@implementation SpecOpsAspects

-(SpecOpsAspects*)createCopy
{
    SpecOpsAspects* aspects = [SpecOpsAspects insertInManagedObjectContext:self.managedObjectContext];
    aspects.armIncrease = self.armIncrease;
    aspects.bsIncrease = self.bsIncrease;
    aspects.btsIncrease = self.btsIncrease;
    aspects.ccIncrease = self.ccIncrease;
    aspects.phIncrease = self.phIncrease;
    aspects.wIncrease = self.wIncrease;
    aspects.wipIncrease = self.wipIncrease;
    return aspects;
}

-(void)reset
{
    self.armIncrease = self.bsIncrease = self.btsIncrease = self.ccIncrease= self.phIncrease = 0;
    self.wIncrease = self.wipIncrease = 0;
}
-(NSDictionary*)toDictionary
{
    NSMutableDictionary* aspectsDictionary = [[NSMutableDictionary alloc] init];
    if(self.armIncreaseValue != 0)
    {
        [aspectsDictionary setObject:self.armIncrease forKey:@"arm"];
    }
    if(self.bsIncreaseValue != 0)
    {
        [aspectsDictionary setObject:self.bsIncrease forKey:@"bs"];
    }
    if(self.btsIncreaseValue != 0)
    {
        [aspectsDictionary setObject:self.btsIncrease forKey:@"bts"];
    }
    if(self.ccIncreaseValue != 0)
    {
        [aspectsDictionary setObject:self.ccIncrease forKey:@"cc"];
    }
    if(self.phIncreaseValue != 0)
    {
        [aspectsDictionary setObject:self.phIncrease forKey:@"ph"];
    }
    if(self.wIncreaseValue != 0)
    {
        [aspectsDictionary setObject:self.wIncrease forKey:@"w"];
    }
    if(self.wipIncreaseValue != 0)
    {
        [aspectsDictionary setObject:self.wipIncrease forKey:@"wip"];
    }
    return aspectsDictionary;
}

-(void)fromDictionary:(NSDictionary*)fromDictionary
{
    if([fromDictionary objectForKey:@"arm"])
    {
        self.armIncrease = [NSNumber numberWithInteger:[[fromDictionary objectForKey:@"arm"] integerValue]];
    }
    if([fromDictionary objectForKey:@"bs"])
    {
         self.bsIncrease = [NSNumber numberWithInteger:[[fromDictionary objectForKey:@"bs"] integerValue]];
    }
    if([fromDictionary objectForKey:@"bts"])
    {
         self.btsIncrease = [NSNumber numberWithInteger:[[fromDictionary objectForKey:@"bts"] integerValue]];
    }
    if([fromDictionary objectForKey:@"cc"])
    {
         self.ccIncrease = [NSNumber numberWithInteger:[[fromDictionary objectForKey:@"cc"] integerValue]];
    }
    if([fromDictionary objectForKey:@"ph"])
    {
         self.phIncrease = [NSNumber numberWithInteger:[[fromDictionary objectForKey:@"ph"] integerValue]];
    }
    if([fromDictionary objectForKey:@"w"])
    {
         self.wIncrease = [NSNumber numberWithInteger:[[fromDictionary objectForKey:@"w"] integerValue]];
    }
    if([fromDictionary objectForKey:@"wip"])
    {
         self.wipIncrease = [NSNumber numberWithInteger:[[fromDictionary objectForKey:@"wip"] integerValue]];
    }
}

-(NSString*)descriptionWithUnit:(Unit*)unit;
{
    UnitAspects* unitAspects = unit.aspects;
    
    NSMutableArray* aspects = [[NSMutableArray alloc] init];
    if(self.armIncreaseValue != 0)
    {
        [aspects addObject:[NSString stringWithFormat:@"%d ARM", [unitAspects.arm intValue] + self.armIncreaseValue]];
    }
    if(self.bsIncreaseValue != 0)
    {
        [aspects addObject:[NSString stringWithFormat:@"%d BS", [unitAspects.bs intValue] + self.bsIncreaseValue]];
    }
    if(self.btsIncreaseValue != 0)
    {
        [aspects addObject:[NSString stringWithFormat:@"%d BTS", [unitAspects.bts intValue] + abs(self.btsIncreaseValue)]];
    }
    if(self.ccIncreaseValue != 0)
    {
        [aspects addObject:[NSString stringWithFormat:@"%d CC", [unitAspects.cc intValue] + self.ccIncreaseValue]];
    }
    if(self.phIncreaseValue != 0)
    {
        [aspects addObject:[NSString stringWithFormat:@"%d PH", [unitAspects.ph intValue] + self.phIncreaseValue]];
    }
    if(self.wIncreaseValue != 0)
    {
        [aspects addObject:[NSString stringWithFormat:@"%d W", [unitAspects.wounds intValue] + self.wIncreaseValue]];
    }
    if(self.wipIncreaseValue != 0)
    {
        [aspects addObject:[NSString stringWithFormat:@"%d WIP", [unitAspects.wip intValue] + self.wipIncreaseValue]];
    }
    
    return [Util conditionalConcatArray:aspects seperator:@", "];
}

@end
