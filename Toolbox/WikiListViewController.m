//
//  WikiViewController.m
//  Toolbox
//
//  Created by Paul on 11/13/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "WikiListViewController.h"
#import "Wiki.h"
#import "WikiEntry.h"
#import "AppDelegate.h"

@interface WikiListViewController ()

@end

@implementation WikiListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchedResultsController:(NSFetchedResultsController *)fetchedResultsController tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WikiEntry *entry = [fetchedResultsController objectAtIndexPath:indexPath];
    
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.containerViewController.middleViewController showWikiEntry:entry];
    
    if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        if([delegate.containerViewController isRightViewControllerVisible])
            [delegate.containerViewController displayLeftViewController:FALSE animationCompletionBlock:nil];
    }
    
    /*
     ModelType *modelType = [[self fetchedResultsControllerForTableView:tableView] objectAtIndexPath:indexPath];
     [self performSegueWithIdentifier:@"toEditor" sender:modelType];
     */
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}

#pragma mark - Fetched results controller

-(NSFetchRequest*) getFetchRequest:(NSString *)searchString;
{
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(caseInsensitiveCompare:)];

    NSPredicate *filterPredicate;
    
    // Set up the fetched results controller.
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [WikiEntry entityInManagedObjectContext:[self managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setIncludesSubentities:TRUE];
    
    NSArray *sortDescriptors = @[sortDescriptor];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSMutableArray *predicateArray = [NSMutableArray array];
    if(searchString != nil && searchString.length)
    {

        [predicateArray addObject:[NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", searchString]];
        
        // your search predicate(s) are added to this array
        // finally add the filter predicate for this view
        if(filterPredicate)
        {
            filterPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[filterPredicate, [NSCompoundPredicate orPredicateWithSubpredicates:predicateArray]]];
        }
        else
        {
            filterPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:predicateArray];
        }
    }
    [fetchRequest setPredicate:filterPredicate];
    
    return fetchRequest;
    
}
-(NSString*) getSectionNameKeyPath
{
    return @"nameFirstLetter";
    
    /*
    BOOL search;
    switch ([self fetchSortIndex:&search]) {
        case 0:
            return (search) ? @"factionName" : nil;
            break;
        case 1:
            return (search) ? @"type" : @"factionName";
            break;
        case 2:
            return (search) ? @"type" : @"subFactionName";
        case 3:
            return (search) ? @"factionName" : @"type";
        default:
            return @"factionName";
            break;
    }
     */
    return nil;
    
}

- (NSArray*) sectionIndexTitlesForTableView:(UITableView *)tableView
{
    BOOL search;
    long idx = [self fetchSortIndex:&search];
    if(!search && idx == 0)
    {
        NSArray* indexTitles = [[self fetchedResultsController] sectionIndexTitles];
        return [@[UITableViewIndexSearch] arrayByAddingObjectsFromArray:indexTitles];
    }
    return nil;
}

- (NSInteger) tableView:(UITableView *)tableView
sectionForSectionIndexTitle:(NSString *)title
                atIndex:(NSInteger)index {
    if (index == 0) {
        [tableView setContentOffset:CGPointMake(0.0, -tableView.contentInset.top)];
        return NSNotFound;
    }
    return index-1;
}


- (void)fetchedResultsController:(NSFetchedResultsController *)fetchedResultsController configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    WikiEntry *entry = [fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = entry.name;
}

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
