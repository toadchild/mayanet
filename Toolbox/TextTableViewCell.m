//
//  TextTableViewCell.m
//  Toolbox
//
//  Created by Jonathan Polley on 1/30/15.
//  Copyright (c) 2015 Paul. All rights reserved.
//

#import "TextTableViewCell.h"
#import "TextNote.h"
#import "TextNotation.h"

@implementation TextTableViewCell

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    if(_note)
    {
        [self setTextValue: self.textNote.text];
    }
}

-(TextNote*)textNote
{
    return (TextNote*)self.note;
}

-(Note*)note
{
    if(!_note)
    {
        _note = [TextNote insertInManagedObjectContext:self.groupMember.managedObjectContext];
        _note.groupMember = self.groupMember;
        _note.notation = self.notation;
    }
    return _note;
}

-(void)setTextValue:(NSString*)text
{
    self.textNote.text = text;
    self.textView.text = text;
}

-(void)textViewDidChange:(UITextView *)textView
{
    [self setTextValue: textView.text];
    
    [self onModified];
}

@end
