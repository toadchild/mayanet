#import "_WikiEntry.h"

@class AppDelegate;
@interface WikiEntry : _WikiEntry {}

+(WikiEntry*)wikiEntryWithName:(NSString*)name;
+(WikiEntry*)wikiEntryWithName:(NSString*)name andDelegate:(AppDelegate*)delegate;

-(NSString*)urlEscapedName;

@end
