// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SpecOpsSpecialRule.m instead.

#import "_SpecOpsSpecialRule.h"

const struct SpecOpsSpecialRuleAttributes SpecOpsSpecialRuleAttributes = {
	.cost = @"cost",
};

const struct SpecOpsSpecialRuleRelationships SpecOpsSpecialRuleRelationships = {
	.army = @"army",
	.specialRule = @"specialRule",
	.specops = @"specops",
};

@implementation SpecOpsSpecialRuleID
@end

@implementation _SpecOpsSpecialRule

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SpecOpsSpecialRule" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SpecOpsSpecialRule";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SpecOpsSpecialRule" inManagedObjectContext:moc_];
}

- (SpecOpsSpecialRuleID*)objectID {
	return (SpecOpsSpecialRuleID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"costValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cost"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cost;

- (int16_t)costValue {
	NSNumber *result = [self cost];
	return [result shortValue];
}

- (void)setCostValue:(int16_t)value_ {
	[self setCost:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCostValue {
	NSNumber *result = [self primitiveCost];
	return [result shortValue];
}

- (void)setPrimitiveCostValue:(int16_t)value_ {
	[self setPrimitiveCost:[NSNumber numberWithShort:value_]];
}

@dynamic army;

@dynamic specialRule;

@dynamic specops;

- (NSMutableSet*)specopsSet {
	[self willAccessValueForKey:@"specops"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specops"];

	[self didAccessValueForKey:@"specops"];
	return result;
}

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newSpecopsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsBuild" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"specialRules CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

