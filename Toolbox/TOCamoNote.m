#import "TOCamoNote.h"


@interface TOCamoNote ()

// Private interface goes here.

@end


@implementation TOCamoNote

// Custom logic goes here.
-(NotationType) type
{
    return eTOCamoNotation;
}
@end
