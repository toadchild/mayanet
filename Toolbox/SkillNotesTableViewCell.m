//
//  SkillNotesTableViewCell.h
//  Toolbox
//
//  Created by Jonathan Polley on 12/23/18.
//  Copyright © 2018 Jonathan Polley. All rights reserved.
//

#import "SkillNotesTableViewCell.h"

@implementation SkillNotesTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

@end
