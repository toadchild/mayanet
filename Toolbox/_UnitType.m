// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UnitType.m instead.

#import "_UnitType.h"

const struct UnitTypeAttributes UnitTypeAttributes = {
	.code = @"code",
	.name = @"name",
	.sortOrder = @"sortOrder",
};

const struct UnitTypeRelationships UnitTypeRelationships = {
	.profiles = @"profiles",
	.specOps = @"specOps",
	.units = @"units",
};

@implementation UnitTypeID
@end

@implementation _UnitType

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UnitType" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UnitType";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UnitType" inManagedObjectContext:moc_];
}

- (UnitTypeID*)objectID {
	return (UnitTypeID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"sortOrderValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"sortOrder"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic code;

@dynamic name;

@dynamic sortOrder;

- (int16_t)sortOrderValue {
	NSNumber *result = [self sortOrder];
	return [result shortValue];
}

- (void)setSortOrderValue:(int16_t)value_ {
	[self setSortOrder:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSortOrderValue {
	NSNumber *result = [self primitiveSortOrder];
	return [result shortValue];
}

- (void)setPrimitiveSortOrderValue:(int16_t)value_ {
	[self setPrimitiveSortOrder:[NSNumber numberWithShort:value_]];
}

@dynamic profiles;

- (NSMutableSet*)profilesSet {
	[self willAccessValueForKey:@"profiles"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"profiles"];

	[self didAccessValueForKey:@"profiles"];
	return result;
}

@dynamic specOps;

- (NSMutableSet*)specOpsSet {
	[self willAccessValueForKey:@"specOps"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specOps"];

	[self didAccessValueForKey:@"specOps"];
	return result;
}

@dynamic units;

- (NSMutableSet*)unitsSet {
	[self willAccessValueForKey:@"units"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"units"];

	[self didAccessValueForKey:@"units"];
	return result;
}

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newProfilesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"UnitProfile" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"type == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecOpsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOps" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"type == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newUnitsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Unit" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"type == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

