//
//  LeftNavigationController.h
//  Toolbox
//
//  Created by Paul on 11/14/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Army, Sectorial;

@interface LeftNavigationController : UINavigationController

-(void)goToArmy:(Army*)army;
-(void)goToSectorial:(Sectorial*)sectorial;

@end
