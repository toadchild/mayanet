// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Notation.h instead.

#import <CoreData/CoreData.h>

extern const struct NotationRelationships {
	__unsafe_unretained NSString *notes;
	__unsafe_unretained NSString *specialRule;
} NotationRelationships;

@class Note;
@class SpecialRule;

@interface NotationID : NSManagedObjectID {}
@end

@interface _Notation : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) NotationID* objectID;

@property (nonatomic, strong) NSSet *notes;

- (NSMutableSet*)notesSet;

@property (nonatomic, strong) SpecialRule *specialRule;

//- (BOOL)validateSpecialRule:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newNotesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _Notation (NotesCoreDataGeneratedAccessors)
- (void)addNotes:(NSSet*)value_;
- (void)removeNotes:(NSSet*)value_;
- (void)addNotesObject:(Note*)value_;
- (void)removeNotesObject:(Note*)value_;

@end

@interface _Notation (CoreDataGeneratedPrimitiveAccessors)

- (NSMutableSet*)primitiveNotes;
- (void)setPrimitiveNotes:(NSMutableSet*)value;

- (SpecialRule*)primitiveSpecialRule;
- (void)setPrimitiveSpecialRule:(SpecialRule*)value;

@end
