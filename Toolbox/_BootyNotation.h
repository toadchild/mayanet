// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BootyNotation.h instead.

#import <CoreData/CoreData.h>
#import "Notation.h"

extern const struct BootyNotationAttributes {
	__unsafe_unretained NSString *levelTwo;
} BootyNotationAttributes;

@interface BootyNotationID : NotationID {}
@end

@interface _BootyNotation : Notation {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BootyNotationID* objectID;

@property (nonatomic, strong) NSNumber* levelTwo;

@property (atomic) BOOL levelTwoValue;
- (BOOL)levelTwoValue;
- (void)setLevelTwoValue:(BOOL)value_;

//- (BOOL)validateLevelTwo:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

#endif

@end

@interface _BootyNotation (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveLevelTwo;
- (void)setPrimitiveLevelTwo:(NSNumber*)value;

- (BOOL)primitiveLevelTwoValue;
- (void)setPrimitiveLevelTwoValue:(BOOL)value_;

@end
