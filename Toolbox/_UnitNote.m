// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UnitNote.m instead.

#import "_UnitNote.h"

const struct UnitNoteAttributes UnitNoteAttributes = {
	.note = @"note",
	.title = @"title",
};

const struct UnitNoteRelationships UnitNoteRelationships = {
	.unit = @"unit",
};

@implementation UnitNoteID
@end

@implementation _UnitNote

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UnitNote" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UnitNote";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UnitNote" inManagedObjectContext:moc_];
}

- (UnitNoteID*)objectID {
	return (UnitNoteID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic note;

@dynamic title;

@dynamic unit;

#if TARGET_OS_IPHONE

#endif

@end

