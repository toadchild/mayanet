//
//  ExportRosterViewController.h
//  Toolbox
//
//  Created by Paul on 12/4/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Roster.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@protocol ExportRosterDelegate <NSObject>

-(void) onExportRosterDone;

@end

@interface ExportRosterViewController : UIViewController <MFMailComposeViewControllerDelegate, UIWebViewDelegate,UIAlertViewDelegate>
{
    UIAlertView* exportAlertView;
}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *clipboardBarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *emailBarButton;
@property (strong, nonatomic) IBOutlet UIWebView *previewWebView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *linkBarButton;
@property (strong, nonatomic) Roster* roster;
@property (strong, nonatomic) id<ExportRosterDelegate> delegate;

- (IBAction)onEmail:(id)sender;
- (IBAction)onClipboard:(id)sender;
- (IBAction)onLink:(id)sender;
//- (IBAction)onLink:(id)sender;
- (IBAction)onDone:(id)sender;

@end
