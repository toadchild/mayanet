#import "_Note.h"
#import "NotationTypeDef.h"

@interface Note : _Note {}
// Custom logic goes here.
-(NotationType) type;
@end
