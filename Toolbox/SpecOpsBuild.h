#import "_SpecOpsBuild.h"

@interface SpecOpsBuild : _SpecOpsBuild {}
// Custom logic goes here.

-(void) toggleSpecOpsWeapon:(SpecOpsWeapon*)weapon;
-(void) toggleSpecOpsSpecialRule:(SpecOpsSpecialRule*)specialRule;
-(void) toggleSpecOpsEquipment:(SpecOpsEquipment*)equipment;

-(BOOL) isBaseUnitOption:(SpecOpsBaseUnitOption*)baseUnitOption;

-(BOOL) hasWeapon:(SpecOpsWeapon*)weapon;
-(BOOL) hasSpecialRule:(SpecOpsSpecialRule*)specialRule;
-(BOOL) hasEquipment:(SpecOpsEquipment*)equipment;

-(NSArray*) getSpecialRulesArray;

-(NSArray*) getSpecialRuleNamesArray;

-(NSInteger) getCost;

-(NSArray*) getWeaponsArrayWithModes:(BOOL)showModes;

-(NSArray*) getWeaponNamesArray;

-(NSString*) weaponDescription;
-(NSString*) weaponDescriptionWithSeperator:(NSString*)seperator;

-(NSString*) specialRulesDescription;

-(void) reset;
@end
