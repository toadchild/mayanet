// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to HackingDevice.h instead.

#import <CoreData/CoreData.h>

extern const struct HackingDeviceAttributes {
	__unsafe_unretained NSString *name;
} HackingDeviceAttributes;

extern const struct HackingDeviceRelationships {
	__unsafe_unretained NSString *groups;
	__unsafe_unretained NSString *upgrades;
} HackingDeviceRelationships;

@class HackingProgramGroup;
@class HackingProgram;

@interface HackingDeviceID : NSManagedObjectID {}
@end

@interface _HackingDevice : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) HackingDeviceID* objectID;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *groups;

- (NSMutableSet*)groupsSet;

@property (nonatomic, strong) NSSet *upgrades;

- (NSMutableSet*)upgradesSet;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newGroupsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newUpgradesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _HackingDevice (GroupsCoreDataGeneratedAccessors)
- (void)addGroups:(NSSet*)value_;
- (void)removeGroups:(NSSet*)value_;
- (void)addGroupsObject:(HackingProgramGroup*)value_;
- (void)removeGroupsObject:(HackingProgramGroup*)value_;

@end

@interface _HackingDevice (UpgradesCoreDataGeneratedAccessors)
- (void)addUpgrades:(NSSet*)value_;
- (void)removeUpgrades:(NSSet*)value_;
- (void)addUpgradesObject:(HackingProgram*)value_;
- (void)removeUpgradesObject:(HackingProgram*)value_;

@end

@interface _HackingDevice (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSMutableSet*)primitiveGroups;
- (void)setPrimitiveGroups:(NSMutableSet*)value;

- (NSMutableSet*)primitiveUpgrades;
- (void)setPrimitiveUpgrades:(NSMutableSet*)value;

@end
