#import "Booty.h"
#import "AppDelegate.h"

@interface Booty ()

// Private interface goes here.

@end


@implementation Booty

// Custom logic goes here.

+(Booty*)getBootyWithRoll:(int)roll levelTwo:(BOOL)levelTwo
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [Booty entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setIncludesSubentities:TRUE];
    
    [fetchRequest setFetchLimit:1];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"lowRollRange =< %d AND highRollRange >= %d AND levelTwo = %@", roll, roll, [NSNumber numberWithBool:levelTwo]];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    NSArray* results = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [results count] == 1)
    {
        return [results objectAtIndex:0];
    }
    return nil;
}

+(Booty*)rollWithLevelTwo:(BOOL)levelTwo
{
    int random = arc4random_uniform(20)+1;
    return [self getBootyWithRoll:random levelTwo:levelTwo];
}
@end
