#import "_Sectorial.h"
#import "AppDelegate.h"

enum SectorialIds {
    eSectorialAcontecimento = 1998,
    eSectorialNeoterra = 1997,
    eSectorialMilitaryOrders = 1996,
    eSectorialVaruna = 1995,
    eSectorialLegacyJSA = 2998,
    eSectorialImperialService = 2997,
    eSectorialInvincibleArmy = 2996,
    eSectorialCaledonian = 3998,
    eSectorialMerovingian = 3997,
    eSectorialUSAriadna = 3996,
    eSectorialTartary = 3995,
    eSectorialQapuKhalqi = 4998,
    eSectorialHassassin = 4997,
    eSectorialRamah = 4996,
    eSectorialCorregidor = 5998,
    eSectorialBakunin = 5997,
    eSectorialTunguska = 5996,
    eSectorialShasvastii = 6998,
    eSectorialMorat = 6997,
    eSectorialOnyx = 6996,
    eSectorialSteelPhalanx = 7998,
    eSectorialOperationsSubsection = 7997,
    eSectorialBayram = 9998,
    eSectorialJSA = 9997,
    eSectorialIkari = 9996,
    eSectorialStarco = 9995,
    eSectorialForeign = 9994,
    eSectorialDahshat = 9993,
    eSectorialSpiral = 9992,
};

@interface Sectorial : _Sectorial {}

@property (nonatomic) NSOrderedSet *cachedRosters;
@property (nonatomic) NSMutableDictionary *unitsByID;
@property (nonatomic) NSMutableDictionary *unitsByLegacyISC;

+(NSArray*)allWithDelegate:(AppDelegate*)delegate;
+(Sectorial*)sectorialWithID:(int)id;
+(Sectorial*)sectorialWithID:(int)id withDelegate:(AppDelegate*)delegate;
+(Sectorial*)sectorialWithIDCreatingNew:(int)id withDelegate:(AppDelegate*)delegate;

-(Unit*)unitWithID:(int)id;
-(Unit*)unitWithLegacyISC:(NSString*)isc;

-(UIColor*)backgroundColor;
-(UIColor*)primaryColor;
-(UIColor*)secondaryColor;

-(NSString*)imageTitle;
-(NSString*)webImageTitle;

@end
