//
//  AppDelegate.m
//  Toolbox
//
//  Created by Paul on 10/20/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "AppDelegate.h"

#import "MainViewController.h"
#import "DetailViewManager.h"
#import "ContainerViewController.h"
#import "Roster.h"
#import "ImportRosterViewController.h"
#import "UIColor+ColorPalette.h"

@interface AppDelegate() <ImportRosterDelegate, UIAlertViewDelegate, UIAppearanceContainer>
@end

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor barColor]];
    [[UINavigationBar appearance] setTintColor:[UIColor barItemColor]];
    
    [[UINavigationBar appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor titleColor],
      NSForegroundColorAttributeName,nil]];
    
    NSString* path = [[NSBundle mainBundle] pathForResource:@"UserDefaults" ofType:@"plist"];
    NSDictionary* userDefaults = [NSDictionary dictionaryWithContentsOfFile:path];
    [[NSUserDefaults standardUserDefaults] registerDefaults:userDefaults];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if([[url host] compare:@"open"] == NSOrderedSame)
    {
        NSString* armyCode = [url query];
        importedRoster = [Roster rosterFromWebCode:armyCode];
        
        
        if(importedRoster)
        {
            //Ask user if they want to keep roster
            ImportRosterViewController* con = [self.containerViewController.storyboard instantiateViewControllerWithIdentifier:@"ImportRoster"];
            if(con)
            {
                con.delegate = self;
                con.roster = importedRoster;
                [self.containerViewController presentViewController:con animated:TRUE completion:nil];
            }
            return TRUE;
        }
    }
    return FALSE;
}

-(void)onImportRosterDone:(BOOL)import
{
    imported = import;
    if(imported)
    {
        
        importAlertView = [[UIAlertView alloc] initWithTitle:@"Import Roster" message:@"The Roster was successfully imported." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    }
    else
    {
        [self.managedObjectContext deleteObject:importedRoster];
        importedRoster = nil;
        
        importAlertView = [[UIAlertView alloc] initWithTitle:@"Import Roster" message:@"The Roster was not imported." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];

    }
    [importAlertView show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView == importAlertView)
    {
        [self.containerViewController dismissViewControllerAnimated:TRUE completion:^{
            if(self->imported)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"RosterSelection" object:self->importedRoster];
                if(self->importedRoster.sectorial == nil)
                {
                    [self.containerViewController.leftViewController goToArmy:self->importedRoster.army];
                }
                else
                {
                    [self.containerViewController.leftViewController goToSectorial:self->importedRoster.sectorial];
                }
            }
        }];
    }
}


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
        [_managedObjectContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Toolbox" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // The only difference between the 1.0 and 1.1 schemas was an added table (Migrations).
    // This is not detected as a "difference", causing the 1.0-1.1.1 upgrade to be run instead of the 1.1-1.1.1
    // This has been worked around by renaming the 1.0 upgrade to be alphabetically last (ZZZ-1.0-1.1.1).
    // Delete the old ones just to be safe.
    NSURL *mapping_1_0_to_1_1 = [[self applicationBundleDirectory] URLByAppendingPathComponent:@"1.0-1.1.cdm"];
    NSURL *mapping_1_0_to_1_1_1 = [[self applicationBundleDirectory] URLByAppendingPathComponent:@"1.0-1.1.1.cdm"];
    [[NSFileManager defaultManager] removeItemAtURL:mapping_1_0_to_1_1 error:nil];
    [[NSFileManager defaultManager] removeItemAtURL:mapping_1_0_to_1_1_1 error:nil];
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Toolbox_N3.sqlite"];
    
    // handle db upgrade
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption: @(YES),
                              NSInferMappingModelAutomaticallyOption: @(YES),
                              };
    
#ifdef DEBUG
    //[[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
#endif
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];

    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

// Returns the URL to the application's Bundle directory.
- (NSURL *)applicationBundleDirectory
{
    return [NSURL fileURLWithPath:[[NSBundle mainBundle] resourcePath]];
}

@end
