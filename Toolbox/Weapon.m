#import "Weapon.h"
#import "AppDelegate.h"
#import "Util.h"
#import "Unit.h"
#import "UnitAspects.h"
@interface Weapon ()

// Private interface goes here.

@end


@implementation Weapon
+(Weapon*) weaponWithName:(NSString*)name
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    return [Weapon weaponWithName:name andDelegate:delegate];
}

+(Weapon*) weaponWithName:(NSString*)name andDelegate:(AppDelegate *)delegate
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [Weapon entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchLimit:1];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"name = %@", name];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    NSArray* results = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [results count] == 1)
    {
        return [results objectAtIndex:0];
    }
    return nil;
}

+(NSString*) descriptionFromWeaponsArray:(NSArray*)theWeapons
{
    return [Weapon descriptionFromWeaponsArray:theWeapons seperator:@", "];
}


+(NSString*) descriptionFromWeaponsArray:(NSArray*)theWeapons seperator:(NSString*)seperator
{
    NSMutableArray* stringArray = [[NSMutableArray alloc] init];
    
    NSMutableArray* ccWeapons = [[NSMutableArray alloc] init];
    for(Weapon* weapon in theWeapons)
    {
        if(weapon.closeCombatValue)
        {
            [ccWeapons addObject:weapon];
            continue;
        }
        [stringArray addObject:weapon.name];
    }
    
    NSString* bswString = [Util concatArray:stringArray];
    
    [stringArray removeAllObjects];
    
    for(Weapon* weapon in ccWeapons)
    {
        [stringArray addObject:weapon.name];
    }
    NSString* ccString = [Util concatArray:stringArray];
    
    
    return [Util conditionalConcatArray:@[bswString, ccString] seperator:seperator];
}

-(NSString*) damageDescriptorWithUnit:(UnitAspects*)unit
{
    NSNumber *number = self.damage;
    
    if(number == nil)
    {
        return @"--";
    }
    else if([number intValue] == WEAPON_DAM_PH)
    {
        return [NSString stringWithFormat:@"%d(PH)", [unit.ph intValue]];
    }
    else if([number intValue] == WEAPON_DAM_PH_1)
    {
        return [NSString stringWithFormat:@"%d(PH-1)", [unit.ph intValue]-1];
    }
    else if([number intValue] == WEAPON_DAM_PH_2)
    {
        return [NSString stringWithFormat:@"%d(PH-2)", [unit.ph intValue]-2];
    }
    else if([number intValue] == WEAPON_DAM_WIP)
    {
        return [NSString stringWithFormat:@"%d(WIP)", [unit.wip intValue]];
    }
    else if([number intValue] == WEAPON_DAM_STAR)
    {
        return @"*";
    }
    else if([number intValue] == WEAPON_DAM_MARK)
    {
        return @"TARGETED";
    }
    else
    {
        return [number stringValue];
    }
    return @"";
}

+(NSString*) rangeFromNSNumber:(NSNumber*)number
{
    BOOL siUnits = [[NSUserDefaults standardUserDefaults] boolForKey:@"UseSIUnits"];
    double factor = siUnits ? 2.5 : 1;
    return [NSString stringWithFormat:@"%ld", (long)([number integerValue]*factor)];
}

+(NSString*) modFromNSNumber:(NSNumber*)number
{
    if(number == nil)
    {
        return @"--";
    }
    else
    {
        return [NSString stringWithFormat:@"%+ld", (long)([number integerValue])];
    }
}

+(UIColor*)weaponDescriptorColorFromNSNumber:(NSNumber*)number
{
    if(!number) {
        return nil;
    }
    
    switch ([number integerValue]) {
        //dodgerblue rgb(30, 144, 255)
        case 0:
            return [UIColor colorWithRed:30./255. green:144./255. blue:255./255. alpha:1];
            break;
        //limegreen rgb(50, 205, 50)
        case 3:
            return [UIColor colorWithRed:50./255. green:205./255. blue:50./255. alpha:1];
        //greenyellow rgb(173, 255, 47)
        case 6:
            return [UIColor colorWithRed:173./255. green:255./255. blue:47./255. alpha:1];
        //gold rgb(255, 215, 0)
        case -3:
            return [UIColor colorWithRed:255./255. green:215./255. blue:0./255. alpha:1];
        //rgb(178, 0, 0)
        case -6:
            return [UIColor colorWithRed:178./255. green:0 blue:0 alpha:1];
        default:
            break;
    }
    return nil;
}

-(Weapon*) createCopyWithDelegate:(AppDelegate *)delegate
{
    Weapon* newWeapon = [Weapon insertInManagedObjectContext:delegate.managedObjectContext];
    [newWeapon copyFromWeapon:self];
    
    return newWeapon;
}

-(void) copyFromWeapon:(Weapon*)weapon
{
    self.altProfile = weapon.altProfile;
    self.ammo = weapon.ammo;
    self.attribute = weapon.attribute;
    self.burst = weapon.burst;
    self.closeCombat = weapon.closeCombat;
    self.damage = weapon.damage;
    self.longDistance = weapon.longDistance;
    self.longModifier = weapon.longModifier;
    self.maxDistance = weapon.maxDistance;
    self.maxModifier = weapon.maxModifier;
    self.mediumDistance = weapon.mediumDistance;
    self.mediumModifier = weapon.mediumModifier;
    self.mode = weapon.mode;
    self.name = weapon.name;
    self.note = weapon.note;
    self.shortDistance = weapon.shortDistance;
    self.shortModifier = weapon.shortModifier;
    self.suppressive = weapon.suppressive;
    self.templateType = weapon.templateType;
    self.uses = weapon.uses;
    self.wikiEntry = weapon.wikiEntry;
}

-(NSString*) getExtrasString
{
    BOOL isTemplate = ![self.templateType isEqualToString: @"No"];
    NSString* template = [NSString stringWithFormat:@"%@", isTemplate ? self.templateType : @""];
    
    BOOL isCC = [self.closeCombat boolValue];
    NSString* cc = [NSString stringWithFormat:@"%@", isCC ? @"CC" : @""];
    NSString* usesString = self.usesValue ? [NSString stringWithFormat:@"Disposable (%d)",self.usesValue] : @"";
    NSString* note = self.note != nil ? self.note : @"";
    NSString* suppressive = self.suppressiveValue ? @"Suppressive Fire" : @"";
    
    NSString *attrString = @"";
    if(self.attribute && [self.attribute isEqualToString: @"PH"])
    {
        attrString = @"Throwing Weapon";
    }
    else if(self.attribute && [self.attribute isEqualToString: @"WIP"])
    {
        attrString = @"Technical Weapon";
    }
    
    return [Util conditionalConcatArray:@[template, usesString, attrString, cc, suppressive, note] seperator:@", "];
}

@end
