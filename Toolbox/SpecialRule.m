#import "SpecialRule.h"
#import "AppDelegate.h"
#import "Util.h"

@interface SpecialRule ()

// Private interface goes here.

@end


@implementation SpecialRule

+(SpecialRule*)specialRuleWithName:(NSString*)name
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    return [SpecialRule specialRuleWithName:name andDelegate:delegate];
}

+(SpecialRule*)specialRuleWithName:(NSString*)name andDelegate:(AppDelegate*)delegate
{
    // Custom logic goes here.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [SpecialRule entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"name = %@", name ];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    NSArray* results = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [results count] == 1)
    {
        return [results firstObject];
    }
    return nil;
}

+(NSString*) descriptionFromSpecialRulesArray:(NSArray*)specialRulesArray
{
    NSMutableArray* stringArray = [[NSMutableArray alloc] init];
    
    for(SpecialRule* specialRule in specialRulesArray)
    {
        [stringArray addObject:specialRule.name];
    }
    return [Util concatArray:stringArray];
}



@end
