//
//  ContainerViewController.h
//  Toolbox
//
//  Created by Paul on 10/30/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MiddleNavigationController.h"
#import "LeftNavigationController.h"

typedef void (^ AnimationCompletionBlock)(BOOL);


@interface ContainerViewController : UIViewController <UIGestureRecognizerDelegate>
{
    BOOL rightViewControllerVisible;
    BOOL leftViewControllerVisible;
    UIScreenEdgePanGestureRecognizer *leftScreenEdgePanGestureRecognizer;
    UIScreenEdgePanGestureRecognizer *rightScreenEdgePanGestureRecognizer;
    UIPanGestureRecognizer* leftPanGestureRecognizer;
    UIPanGestureRecognizer* rightPanGestureRecognizer;
    UIView* middleRightSeperator;
    UIView* middleLeftSeperator;
    BOOL validTouch;
    
}
@property (weak, nonatomic) IBOutlet LeftNavigationController *leftViewController;
@property (weak, nonatomic) IBOutlet MiddleNavigationController *middleViewController;
@property (weak, nonatomic) IBOutlet UINavigationController *rightViewController;

// If set to yes interactivly swiping the sliding view is possible. Defaults to YES
// now only sliding the leftstaticview is supported
@property (assign, nonatomic) BOOL allowInteractiveSliding;


-(BOOL) isRightViewControllerVisible;
-(BOOL) isLeftViewControllerVisible;
    
-(void) toggleRightViewController:(AnimationCompletionBlock) animationCompletionBlock;
-(void) toggleLeftViewController:(AnimationCompletionBlock) animationCompletionBlock;

-(void) displayLeftViewController:(BOOL)display animationCompletionBlock:(AnimationCompletionBlock) animationCompletionBlock;
-(void) displayRightViewController:(BOOL)display animationCompletionBlock:(AnimationCompletionBlock) animationCompletionBlock;

-(void) refreshLeft;
-(void) refreshMiddle;
-(void) refreshRight;

-(void) clearSelectionLeft;
-(void) clearSelectionMiddle;
-(void) clearSelectionRight;
@end
