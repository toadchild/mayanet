// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CombatGroup.m instead.

#import "_CombatGroup.h"

const struct CombatGroupRelationships CombatGroupRelationships = {
	.groupMembers = @"groupMembers",
	.roster = @"roster",
};

@implementation CombatGroupID
@end

@implementation _CombatGroup

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"CombatGroup" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"CombatGroup";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"CombatGroup" inManagedObjectContext:moc_];
}

- (CombatGroupID*)objectID {
	return (CombatGroupID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic groupMembers;

- (NSMutableOrderedSet*)groupMembersSet {
	[self willAccessValueForKey:@"groupMembers"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"groupMembers"];

	[self didAccessValueForKey:@"groupMembers"];
	return result;
}

@dynamic roster;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newGroupMembersFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"GroupMember" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"combatGroup == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

@implementation _CombatGroup (GroupMembersCoreDataGeneratedAccessors)
- (void)addGroupMembers:(NSOrderedSet*)value_ {
	[self.groupMembersSet unionOrderedSet:value_];
}
- (void)removeGroupMembers:(NSOrderedSet*)value_ {
	[self.groupMembersSet minusOrderedSet:value_];
}
- (void)addGroupMembersObject:(GroupMember*)value_ {
	[self.groupMembersSet addObject:value_];
}
- (void)removeGroupMembersObject:(GroupMember*)value_ {
	[self.groupMembersSet removeObject:value_];
}
- (void)insertObject:(GroupMember*)value inGroupMembersAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"groupMembers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self groupMembers]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"groupMembers"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"groupMembers"];
}
- (void)removeObjectFromGroupMembersAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"groupMembers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self groupMembers]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"groupMembers"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"groupMembers"];
}
- (void)insertGroupMembers:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"groupMembers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self groupMembers]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"groupMembers"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"groupMembers"];
}
- (void)removeGroupMembersAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"groupMembers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self groupMembers]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"groupMembers"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"groupMembers"];
}
- (void)replaceObjectInGroupMembersAtIndex:(NSUInteger)idx withObject:(GroupMember*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"groupMembers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self groupMembers]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"groupMembers"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"groupMembers"];
}
- (void)replaceGroupMembersAtIndexes:(NSIndexSet *)indexes withGroupMembers:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"groupMembers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self groupMembers]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"groupMembers"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"groupMembers"];
}
@end

