#import "CamoNotation.h"


@interface CamoNotation ()

// Private interface goes here.

@end


@implementation CamoNotation

// Custom logic goes here.
-(NotationType) type
{
    return eCamoNotation;
}
@end
