#import "_CombatGroup.h"

@interface CombatGroup : _CombatGroup {}

-(NSInteger) numberOfRegularOrders;
-(NSInteger) numberOfIrregularOrders;
-(NSInteger) numberOfImpetuousOrders;
-(NSInteger) numberOfOrders;
-(NSInteger) numberOfFigures;

-(NSInteger) numberOfRegularOrdersCountingDead:(BOOL)countDead;
-(NSInteger) numberOfIrregularOrdersCountingDead:(BOOL)countDead;
-(NSInteger) numberOfImpetuousOrdersCountingDead:(BOOL)countDead;
-(NSInteger) numberOfOrdersCountingDead:(BOOL)countDead;
-(NSInteger) numberOfFiguresCountingDead:(BOOL)countDead;
    
-(NSArray*) unitOptions;

-(void)removeGroupMember:(GroupMember *)groupMember withChildren:(BOOL) children;
-(void)addGroupMember:(GroupMember *)groupMember atIndex:(NSUInteger) i withChildren:(BOOL) children;
-(NSUInteger)indexOfGroupMember:(GroupMember*)groupMember;


@end
