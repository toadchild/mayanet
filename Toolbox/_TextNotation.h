// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TextNotation.h instead.

#import <CoreData/CoreData.h>
#import "Notation.h"

@interface TextNotationID : NotationID {}
@end

@interface _TextNotation : Notation {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TextNotationID* objectID;

#if TARGET_OS_IPHONE

#endif

@end

@interface _TextNotation (CoreDataGeneratedPrimitiveAccessors)

@end
