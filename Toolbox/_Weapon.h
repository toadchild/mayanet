// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Weapon.h instead.

#import <CoreData/CoreData.h>

extern const struct WeaponAttributes {
	__unsafe_unretained NSString *altProfile;
	__unsafe_unretained NSString *ammo;
	__unsafe_unretained NSString *attribute;
	__unsafe_unretained NSString *burst;
	__unsafe_unretained NSString *closeCombat;
	__unsafe_unretained NSString *damage;
	__unsafe_unretained NSString *longDistance;
	__unsafe_unretained NSString *longModifier;
	__unsafe_unretained NSString *maxDistance;
	__unsafe_unretained NSString *maxModifier;
	__unsafe_unretained NSString *mediumDistance;
	__unsafe_unretained NSString *mediumModifier;
	__unsafe_unretained NSString *mode;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *note;
	__unsafe_unretained NSString *shortDistance;
	__unsafe_unretained NSString *shortModifier;
	__unsafe_unretained NSString *suppressive;
	__unsafe_unretained NSString *templateType;
	__unsafe_unretained NSString *uses;
} WeaponAttributes;

extern const struct WeaponRelationships {
	__unsafe_unretained NSString *booty;
	__unsafe_unretained NSString *profiles;
	__unsafe_unretained NSString *specops;
	__unsafe_unretained NSString *units;
	__unsafe_unretained NSString *wikiEntry;
} WeaponRelationships;

@class Booty;
@class UnitOption;
@class SpecOpsWeapon;
@class UnitAspects;
@class WikiEntry;

@interface WeaponID : NSManagedObjectID {}
@end

@interface _Weapon : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) WeaponID* objectID;

@property (nonatomic, strong) NSString* altProfile;

//- (BOOL)validateAltProfile:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* ammo;

//- (BOOL)validateAmmo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* attribute;

//- (BOOL)validateAttribute:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* burst;

//- (BOOL)validateBurst:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* closeCombat;

@property (atomic) BOOL closeCombatValue;
- (BOOL)closeCombatValue;
- (void)setCloseCombatValue:(BOOL)value_;

//- (BOOL)validateCloseCombat:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* damage;

@property (atomic) int16_t damageValue;
- (int16_t)damageValue;
- (void)setDamageValue:(int16_t)value_;

//- (BOOL)validateDamage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* longDistance;

@property (atomic) int16_t longDistanceValue;
- (int16_t)longDistanceValue;
- (void)setLongDistanceValue:(int16_t)value_;

//- (BOOL)validateLongDistance:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* longModifier;

@property (atomic) int16_t longModifierValue;
- (int16_t)longModifierValue;
- (void)setLongModifierValue:(int16_t)value_;

//- (BOOL)validateLongModifier:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* maxDistance;

@property (atomic) int16_t maxDistanceValue;
- (int16_t)maxDistanceValue;
- (void)setMaxDistanceValue:(int16_t)value_;

//- (BOOL)validateMaxDistance:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* maxModifier;

@property (atomic) int16_t maxModifierValue;
- (int16_t)maxModifierValue;
- (void)setMaxModifierValue:(int16_t)value_;

//- (BOOL)validateMaxModifier:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* mediumDistance;

@property (atomic) int16_t mediumDistanceValue;
- (int16_t)mediumDistanceValue;
- (void)setMediumDistanceValue:(int16_t)value_;

//- (BOOL)validateMediumDistance:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* mediumModifier;

@property (atomic) int16_t mediumModifierValue;
- (int16_t)mediumModifierValue;
- (void)setMediumModifierValue:(int16_t)value_;

//- (BOOL)validateMediumModifier:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* mode;

//- (BOOL)validateMode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* note;

//- (BOOL)validateNote:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* shortDistance;

@property (atomic) int16_t shortDistanceValue;
- (int16_t)shortDistanceValue;
- (void)setShortDistanceValue:(int16_t)value_;

//- (BOOL)validateShortDistance:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* shortModifier;

@property (atomic) int16_t shortModifierValue;
- (int16_t)shortModifierValue;
- (void)setShortModifierValue:(int16_t)value_;

//- (BOOL)validateShortModifier:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* suppressive;

@property (atomic) int16_t suppressiveValue;
- (int16_t)suppressiveValue;
- (void)setSuppressiveValue:(int16_t)value_;

//- (BOOL)validateSuppressive:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* templateType;

//- (BOOL)validateTemplateType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* uses;

@property (atomic) int16_t usesValue;
- (int16_t)usesValue;
- (void)setUsesValue:(int16_t)value_;

//- (BOOL)validateUses:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *booty;

- (NSMutableSet*)bootySet;

@property (nonatomic, strong) NSSet *profiles;

- (NSMutableSet*)profilesSet;

@property (nonatomic, strong) NSSet *specops;

- (NSMutableSet*)specopsSet;

@property (nonatomic, strong) NSSet *units;

- (NSMutableSet*)unitsSet;

@property (nonatomic, strong) WikiEntry *wikiEntry;

//- (BOOL)validateWikiEntry:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newBootyFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newProfilesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecopsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newUnitsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _Weapon (BootyCoreDataGeneratedAccessors)
- (void)addBooty:(NSSet*)value_;
- (void)removeBooty:(NSSet*)value_;
- (void)addBootyObject:(Booty*)value_;
- (void)removeBootyObject:(Booty*)value_;

@end

@interface _Weapon (ProfilesCoreDataGeneratedAccessors)
- (void)addProfiles:(NSSet*)value_;
- (void)removeProfiles:(NSSet*)value_;
- (void)addProfilesObject:(UnitOption*)value_;
- (void)removeProfilesObject:(UnitOption*)value_;

@end

@interface _Weapon (SpecopsCoreDataGeneratedAccessors)
- (void)addSpecops:(NSSet*)value_;
- (void)removeSpecops:(NSSet*)value_;
- (void)addSpecopsObject:(SpecOpsWeapon*)value_;
- (void)removeSpecopsObject:(SpecOpsWeapon*)value_;

@end

@interface _Weapon (UnitsCoreDataGeneratedAccessors)
- (void)addUnits:(NSSet*)value_;
- (void)removeUnits:(NSSet*)value_;
- (void)addUnitsObject:(UnitAspects*)value_;
- (void)removeUnitsObject:(UnitAspects*)value_;

@end

@interface _Weapon (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAltProfile;
- (void)setPrimitiveAltProfile:(NSString*)value;

- (NSString*)primitiveAmmo;
- (void)setPrimitiveAmmo:(NSString*)value;

- (NSString*)primitiveAttribute;
- (void)setPrimitiveAttribute:(NSString*)value;

- (NSString*)primitiveBurst;
- (void)setPrimitiveBurst:(NSString*)value;

- (NSNumber*)primitiveCloseCombat;
- (void)setPrimitiveCloseCombat:(NSNumber*)value;

- (BOOL)primitiveCloseCombatValue;
- (void)setPrimitiveCloseCombatValue:(BOOL)value_;

- (NSNumber*)primitiveDamage;
- (void)setPrimitiveDamage:(NSNumber*)value;

- (int16_t)primitiveDamageValue;
- (void)setPrimitiveDamageValue:(int16_t)value_;

- (NSNumber*)primitiveLongDistance;
- (void)setPrimitiveLongDistance:(NSNumber*)value;

- (int16_t)primitiveLongDistanceValue;
- (void)setPrimitiveLongDistanceValue:(int16_t)value_;

- (NSNumber*)primitiveLongModifier;
- (void)setPrimitiveLongModifier:(NSNumber*)value;

- (int16_t)primitiveLongModifierValue;
- (void)setPrimitiveLongModifierValue:(int16_t)value_;

- (NSNumber*)primitiveMaxDistance;
- (void)setPrimitiveMaxDistance:(NSNumber*)value;

- (int16_t)primitiveMaxDistanceValue;
- (void)setPrimitiveMaxDistanceValue:(int16_t)value_;

- (NSNumber*)primitiveMaxModifier;
- (void)setPrimitiveMaxModifier:(NSNumber*)value;

- (int16_t)primitiveMaxModifierValue;
- (void)setPrimitiveMaxModifierValue:(int16_t)value_;

- (NSNumber*)primitiveMediumDistance;
- (void)setPrimitiveMediumDistance:(NSNumber*)value;

- (int16_t)primitiveMediumDistanceValue;
- (void)setPrimitiveMediumDistanceValue:(int16_t)value_;

- (NSNumber*)primitiveMediumModifier;
- (void)setPrimitiveMediumModifier:(NSNumber*)value;

- (int16_t)primitiveMediumModifierValue;
- (void)setPrimitiveMediumModifierValue:(int16_t)value_;

- (NSString*)primitiveMode;
- (void)setPrimitiveMode:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveNote;
- (void)setPrimitiveNote:(NSString*)value;

- (NSNumber*)primitiveShortDistance;
- (void)setPrimitiveShortDistance:(NSNumber*)value;

- (int16_t)primitiveShortDistanceValue;
- (void)setPrimitiveShortDistanceValue:(int16_t)value_;

- (NSNumber*)primitiveShortModifier;
- (void)setPrimitiveShortModifier:(NSNumber*)value;

- (int16_t)primitiveShortModifierValue;
- (void)setPrimitiveShortModifierValue:(int16_t)value_;

- (NSNumber*)primitiveSuppressive;
- (void)setPrimitiveSuppressive:(NSNumber*)value;

- (int16_t)primitiveSuppressiveValue;
- (void)setPrimitiveSuppressiveValue:(int16_t)value_;

- (NSString*)primitiveTemplateType;
- (void)setPrimitiveTemplateType:(NSString*)value;

- (NSNumber*)primitiveUses;
- (void)setPrimitiveUses:(NSNumber*)value;

- (int16_t)primitiveUsesValue;
- (void)setPrimitiveUsesValue:(int16_t)value_;

- (NSMutableSet*)primitiveBooty;
- (void)setPrimitiveBooty:(NSMutableSet*)value;

- (NSMutableSet*)primitiveProfiles;
- (void)setPrimitiveProfiles:(NSMutableSet*)value;

- (NSMutableSet*)primitiveSpecops;
- (void)setPrimitiveSpecops:(NSMutableSet*)value;

- (NSMutableSet*)primitiveUnits;
- (void)setPrimitiveUnits:(NSMutableSet*)value;

- (WikiEntry*)primitiveWikiEntry;
- (void)setPrimitiveWikiEntry:(WikiEntry*)value;

@end
