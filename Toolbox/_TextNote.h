// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TextNote.h instead.

#import <CoreData/CoreData.h>
#import "Note.h"

extern const struct TextNoteAttributes {
	__unsafe_unretained NSString *text;
} TextNoteAttributes;

@interface TextNoteID : NoteID {}
@end

@interface _TextNote : Note {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TextNoteID* objectID;

@property (nonatomic, strong) NSString* text;

//- (BOOL)validateText:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

#endif

@end

@interface _TextNote (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveText;
- (void)setPrimitiveText:(NSString*)value;

@end
