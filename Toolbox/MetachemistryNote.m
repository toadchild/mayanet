#import "MetachemistryNote.h"


@interface MetachemistryNote ()

// Private interface goes here.

@end


@implementation MetachemistryNote

// Custom logic goes here.
-(NotationType) type
{
    if([self levelTwoValue])
    {
        return eMetachemistryLevel2Notation;
    }
    return eMetachemistryNotation;
}

@end
