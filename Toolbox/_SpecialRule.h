// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SpecialRule.h instead.

#import <CoreData/CoreData.h>

extern const struct SpecialRuleAttributes {
	__unsafe_unretained NSString *name;
} SpecialRuleAttributes;

extern const struct SpecialRuleRelationships {
	__unsafe_unretained NSString *booty;
	__unsafe_unretained NSString *choices;
	__unsafe_unretained NSString *choicesParent;
	__unsafe_unretained NSString *metachemistry;
	__unsafe_unretained NSString *notation;
	__unsafe_unretained NSString *profiles;
	__unsafe_unretained NSString *specopsEquipment;
	__unsafe_unretained NSString *specopsSkill;
	__unsafe_unretained NSString *units;
	__unsafe_unretained NSString *wikiEntry;
} SpecialRuleRelationships;

@class Booty;
@class SpecialRule;
@class SpecialRule;
@class Metachemistry;
@class Notation;
@class UnitOption;
@class SpecOpsEquipment;
@class SpecOpsSpecialRule;
@class UnitAspects;
@class WikiEntry;

@interface SpecialRuleID : NSManagedObjectID {}
@end

@interface _SpecialRule : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SpecialRuleID* objectID;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *booty;

- (NSMutableSet*)bootySet;

@property (nonatomic, strong) NSOrderedSet *choices;

- (NSMutableOrderedSet*)choicesSet;

@property (nonatomic, strong) NSSet *choicesParent;

- (NSMutableSet*)choicesParentSet;

@property (nonatomic, strong) NSSet *metachemistry;

- (NSMutableSet*)metachemistrySet;

@property (nonatomic, strong) Notation *notation;

//- (BOOL)validateNotation:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *profiles;

- (NSMutableSet*)profilesSet;

@property (nonatomic, strong) NSSet *specopsEquipment;

- (NSMutableSet*)specopsEquipmentSet;

@property (nonatomic, strong) NSSet *specopsSkill;

- (NSMutableSet*)specopsSkillSet;

@property (nonatomic, strong) NSSet *units;

- (NSMutableSet*)unitsSet;

@property (nonatomic, strong) WikiEntry *wikiEntry;

//- (BOOL)validateWikiEntry:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newBootyFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newChoicesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newChoicesParentFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newMetachemistryFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newProfilesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecopsEquipmentFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecopsSkillFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newUnitsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _SpecialRule (BootyCoreDataGeneratedAccessors)
- (void)addBooty:(NSSet*)value_;
- (void)removeBooty:(NSSet*)value_;
- (void)addBootyObject:(Booty*)value_;
- (void)removeBootyObject:(Booty*)value_;

@end

@interface _SpecialRule (ChoicesCoreDataGeneratedAccessors)
- (void)addChoices:(NSOrderedSet*)value_;
- (void)removeChoices:(NSOrderedSet*)value_;
- (void)addChoicesObject:(SpecialRule*)value_;
- (void)removeChoicesObject:(SpecialRule*)value_;

- (void)insertObject:(SpecialRule*)value inChoicesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromChoicesAtIndex:(NSUInteger)idx;
- (void)insertChoices:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeChoicesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInChoicesAtIndex:(NSUInteger)idx withObject:(SpecialRule*)value;
- (void)replaceChoicesAtIndexes:(NSIndexSet *)indexes withChoices:(NSArray *)values;

@end

@interface _SpecialRule (ChoicesParentCoreDataGeneratedAccessors)
- (void)addChoicesParent:(NSSet*)value_;
- (void)removeChoicesParent:(NSSet*)value_;
- (void)addChoicesParentObject:(SpecialRule*)value_;
- (void)removeChoicesParentObject:(SpecialRule*)value_;

@end

@interface _SpecialRule (MetachemistryCoreDataGeneratedAccessors)
- (void)addMetachemistry:(NSSet*)value_;
- (void)removeMetachemistry:(NSSet*)value_;
- (void)addMetachemistryObject:(Metachemistry*)value_;
- (void)removeMetachemistryObject:(Metachemistry*)value_;

@end

@interface _SpecialRule (ProfilesCoreDataGeneratedAccessors)
- (void)addProfiles:(NSSet*)value_;
- (void)removeProfiles:(NSSet*)value_;
- (void)addProfilesObject:(UnitOption*)value_;
- (void)removeProfilesObject:(UnitOption*)value_;

@end

@interface _SpecialRule (SpecopsEquipmentCoreDataGeneratedAccessors)
- (void)addSpecopsEquipment:(NSSet*)value_;
- (void)removeSpecopsEquipment:(NSSet*)value_;
- (void)addSpecopsEquipmentObject:(SpecOpsEquipment*)value_;
- (void)removeSpecopsEquipmentObject:(SpecOpsEquipment*)value_;

@end

@interface _SpecialRule (SpecopsSkillCoreDataGeneratedAccessors)
- (void)addSpecopsSkill:(NSSet*)value_;
- (void)removeSpecopsSkill:(NSSet*)value_;
- (void)addSpecopsSkillObject:(SpecOpsSpecialRule*)value_;
- (void)removeSpecopsSkillObject:(SpecOpsSpecialRule*)value_;

@end

@interface _SpecialRule (UnitsCoreDataGeneratedAccessors)
- (void)addUnits:(NSSet*)value_;
- (void)removeUnits:(NSSet*)value_;
- (void)addUnitsObject:(UnitAspects*)value_;
- (void)removeUnitsObject:(UnitAspects*)value_;

@end

@interface _SpecialRule (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSMutableSet*)primitiveBooty;
- (void)setPrimitiveBooty:(NSMutableSet*)value;

- (NSMutableOrderedSet*)primitiveChoices;
- (void)setPrimitiveChoices:(NSMutableOrderedSet*)value;

- (NSMutableSet*)primitiveChoicesParent;
- (void)setPrimitiveChoicesParent:(NSMutableSet*)value;

- (NSMutableSet*)primitiveMetachemistry;
- (void)setPrimitiveMetachemistry:(NSMutableSet*)value;

- (Notation*)primitiveNotation;
- (void)setPrimitiveNotation:(Notation*)value;

- (NSMutableSet*)primitiveProfiles;
- (void)setPrimitiveProfiles:(NSMutableSet*)value;

- (NSMutableSet*)primitiveSpecopsEquipment;
- (void)setPrimitiveSpecopsEquipment:(NSMutableSet*)value;

- (NSMutableSet*)primitiveSpecopsSkill;
- (void)setPrimitiveSpecopsSkill:(NSMutableSet*)value;

- (NSMutableSet*)primitiveUnits;
- (void)setPrimitiveUnits:(NSMutableSet*)value;

- (WikiEntry*)primitiveWikiEntry;
- (void)setPrimitiveWikiEntry:(WikiEntry*)value;

@end
