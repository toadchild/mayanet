// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Booty.m instead.

#import "_Booty.h"

const struct BootyAttributes BootyAttributes = {
	.highRollRange = @"highRollRange",
	.levelTwo = @"levelTwo",
	.lowRollRange = @"lowRollRange",
};

const struct BootyRelationships BootyRelationships = {
	.notes = @"notes",
	.specialRule = @"specialRule",
	.weapon = @"weapon",
};

@implementation BootyID
@end

@implementation _Booty

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Booty" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Booty";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Booty" inManagedObjectContext:moc_];
}

- (BootyID*)objectID {
	return (BootyID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"highRollRangeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"highRollRange"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"levelTwoValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"levelTwo"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"lowRollRangeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"lowRollRange"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic highRollRange;

- (int16_t)highRollRangeValue {
	NSNumber *result = [self highRollRange];
	return [result shortValue];
}

- (void)setHighRollRangeValue:(int16_t)value_ {
	[self setHighRollRange:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveHighRollRangeValue {
	NSNumber *result = [self primitiveHighRollRange];
	return [result shortValue];
}

- (void)setPrimitiveHighRollRangeValue:(int16_t)value_ {
	[self setPrimitiveHighRollRange:[NSNumber numberWithShort:value_]];
}

@dynamic levelTwo;

- (BOOL)levelTwoValue {
	NSNumber *result = [self levelTwo];
	return [result boolValue];
}

- (void)setLevelTwoValue:(BOOL)value_ {
	[self setLevelTwo:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveLevelTwoValue {
	NSNumber *result = [self primitiveLevelTwo];
	return [result boolValue];
}

- (void)setPrimitiveLevelTwoValue:(BOOL)value_ {
	[self setPrimitiveLevelTwo:[NSNumber numberWithBool:value_]];
}

@dynamic lowRollRange;

- (int16_t)lowRollRangeValue {
	NSNumber *result = [self lowRollRange];
	return [result shortValue];
}

- (void)setLowRollRangeValue:(int16_t)value_ {
	[self setLowRollRange:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveLowRollRangeValue {
	NSNumber *result = [self primitiveLowRollRange];
	return [result shortValue];
}

- (void)setPrimitiveLowRollRangeValue:(int16_t)value_ {
	[self setPrimitiveLowRollRange:[NSNumber numberWithShort:value_]];
}

@dynamic notes;

- (NSMutableSet*)notesSet {
	[self willAccessValueForKey:@"notes"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"notes"];

	[self didAccessValueForKey:@"notes"];
	return result;
}

@dynamic specialRule;

@dynamic weapon;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newNotesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"BootyNote" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"booty == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

