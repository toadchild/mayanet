#import "SpecOpsBuild.h"
#import "SpecOpsSpecialRule.h"
#import "SpecOpsEquipment.h"
#import "SpecOpsWeapon.h"
#import "SpecOpsAspects.h"
#import "SpecOpsBaseUnitOption.h"
#import "UnitOption.h"
#import "UnitAspects.h"
#import "Weapon.h"
#import "Unit.h"
#import "SpecialRule.h"
#import "Army.h"
#import "Sectorial.h"

@interface SpecOpsBuild ()

// Private interface goes here.

@end


@implementation SpecOpsBuild

-(void) toggleSpecOpsWeapon:(SpecOpsWeapon*)weapon
{
    for(SpecOpsWeapon* specOpsweapon in self.weapons)
    {
        if(specOpsweapon == weapon)
        {
            [self removeWeaponsObject:specOpsweapon];
            return;
        }
    }
    [self addWeaponsObject:weapon];
}

-(void) toggleSpecOpsSpecialRule:(SpecOpsSpecialRule*)specialRule
{
    for(SpecOpsSpecialRule* specOpspecialRule in self.specialRules)
    {
        if(specOpspecialRule == specialRule)
        {
            [self removeSpecialRulesObject:specOpspecialRule];
            return;
        }
    }
    [self addSpecialRulesObject:specialRule];
}

-(void) toggleSpecOpsEquipment:(SpecOpsEquipment*)equipment
{
    for(SpecOpsEquipment* specOpEquip in self.equipment)
    {
        if(equipment == specOpEquip)
        {
            [self removeEquipmentObject:specOpEquip];
            return;
        }
    }
    [self addEquipmentObject:equipment];
}

-(BOOL) isBaseUnitOption:(SpecOpsBaseUnitOption*)baseUnitOption
{
    return self.baseUnitOption == baseUnitOption;
}

-(BOOL) hasWeapon:(SpecOpsWeapon*)weapon
{
    for(SpecOpsWeapon* specOpsweapon in self.weapons)
    {
        if(specOpsweapon.weapon == weapon.weapon)
            return TRUE;
    }
    return FALSE;
}

-(BOOL)hasSpecialRule:(SpecOpsSpecialRule*)specialRule
{
    for(SpecOpsSpecialRule* specOpspecialRule in self.specialRules)
    {
        if(specOpspecialRule.specialRule == specialRule.specialRule)
        {
            return TRUE;
        }
    }
    return FALSE;
}

-(BOOL)hasEquipment:(SpecOpsEquipment*)equipment
{
    for(SpecOpsEquipment* specOpEquip in self.equipment)
    {
        if(equipment.specialRule == specOpEquip.specialRule)
        {
            return TRUE;
        }
    }
    return FALSE;
}

-(NSArray*) getSpecialRulesArray
{
    NSMutableArray* specialRules = [[NSMutableArray alloc] init];
    for(SpecOpsSpecialRule* specOpspecialRule in self.specialRules)
    {
        [specialRules addObject:specOpspecialRule.specialRule];
    }
    for(SpecOpsEquipment* specOpsEquip in self.equipment)
    {
        [specialRules addObject:specOpsEquip.specialRule];
    }
    return specialRules;
}

-(NSArray*) getWeaponsArrayWithModes:(BOOL)showModes
{
    NSMutableArray* weapons = [[NSMutableArray alloc] init];
    for(SpecOpsWeapon* specOpsWeapon in self.weapons)
    {
        [weapons addObject:specOpsWeapon.weapon];

        if(showModes){
            Weapon *altWeapon = specOpsWeapon.weapon;
            while(altWeapon.altProfile)
            {
                altWeapon = [Weapon weaponWithName:altWeapon.altProfile];
                [weapons addObject: altWeapon];
            }
        }
    }
    return weapons;
}

-(NSArray*) getSpecialRuleNamesArray
{
    NSMutableArray* specialRules = [[NSMutableArray alloc] init];
    for(SpecOpsSpecialRule* specOpspecialRule in self.specialRules)
    {
        [specialRules addObject:specOpspecialRule.specialRule.name];
    }
    for(SpecOpsEquipment* specOpsEquip in self.equipment)
    {
        [specialRules addObject:specOpsEquip.specialRule.name];
    }
    return specialRules;
}

-(NSArray*) getWeaponNamesArray
{
    NSMutableArray* weapons = [[NSMutableArray alloc] init];
    for(SpecOpsWeapon* specOpsWeapon in self.weapons)
    {
        [weapons addObject:specOpsWeapon.weapon.name];
    }
    return weapons;
}

-(NSString*) weaponDescription
{
    return [self weaponDescriptionWithSeperator:@", "];
}

-(NSString*) weaponDescriptionWithSeperator:(NSString*)seperator;
{
    //NSArray* unitOptionWeapons = [self.baseUnitOption.unitOption.weapons array];
    //NSArray* unitWeapons = [unit.aspects.weapons array];
    NSArray* buildWeapons = [self getWeaponsArrayWithModes:FALSE];
    
    //NSArray* theWeapons = [unitOptionWeapons arrayByAddingObjectsFromArray:unitWeapons];
    //theWeapons = [theWeapons arrayByAddingObjectsFromArray:buildWeapons];
    
    return [Weapon descriptionFromWeaponsArray:buildWeapons seperator:seperator];
}

-(NSString*) specialRulesDescription
{
    //NSArray* unitSpecialRules;
    //NSArray* unitOptionSpecialRules;
    NSArray* specOpsSpecialRules;
    
    //unitSpecialRules = [unit.aspects.specialRules array];
    //unitOptionSpecialRules = [self.baseUnitOption.unitOption.specialRules array];
    specOpsSpecialRules = [self getSpecialRulesArray];
    
    //NSArray* specialRules = [unitOptionSpecialRules arrayByAddingObjectsFromArray:unitSpecialRules];
    //specialRules = [specialRules arrayByAddingObjectsFromArray:specOpsSpecialRules];
    
    return [SpecialRule descriptionFromSpecialRulesArray:specOpsSpecialRules];
}

-(NSInteger) getCost
{
    NSInteger cost = 0;
    
    SpecOpsAspects* specOpsAspects = self.aspects;
    switch (specOpsAspects.ccIncreaseValue) {
        case 2:
            cost += 2;
            break;
        case 5:
            cost += 5;
            break;
        case 10:
            cost += 10;
            break;
        default:
            break;
    }

    switch (specOpsAspects.bsIncreaseValue) {
        case 1:
            cost += 2;
            break;
        case 2:
            cost += 5;
            break;
        case 3:
            cost += 10;
            break;
        default:
            break;
    }
  
    switch (specOpsAspects.phIncreaseValue) {
        case 1:
            cost += 2;
            break;
        case 3:
            cost += 5;
            break;
        default:
            break;
    }
 
    switch (specOpsAspects.wipIncreaseValue) {
        case 1:
            cost += 2;
            break;
        case 3:
            cost += 5;
            break;
        default:
            break;
    }
 
    switch (specOpsAspects.armIncreaseValue) {
        case 1:
            cost += 5;
            break;
        case 3:
            cost += 10;
            break;
        default:
            break;
    }
   
    switch (abs(specOpsAspects.btsIncreaseValue)) {
        case 3:
            cost += 2;
            break;
        case 6:
            cost += 5;
            break;
        case 9:
            cost += 10;
            break;
        default:
            break;
    }
    switch (specOpsAspects.wIncreaseValue) {
        case 1:
            cost += 10;
            break;
        default:
            break;
    }
    
    for(SpecOpsSpecialRule* specOpspecialRule in self.specialRules)
    {
        cost += specOpspecialRule.costValue;
    }
    
    for(SpecOpsEquipment* equipment in self.equipment)
    {
        cost += equipment.costValue;
    }
    
    for(SpecOpsWeapon* specOpsweapon in self.weapons)
    {
        cost += specOpsweapon.costValue;
    }
    return cost;
}

-(void)reset
{
    self.aspects = [SpecOpsAspects insertInManagedObjectContext:self.managedObjectContext];
    [self.specialRulesSet removeAllObjects];
    [self.weaponsSet removeAllObjects];
    self.baseUnitOption = nil;
    [self.managedObjectContext save:nil];
}

@end
