//
//  FeedbackFormViewController.m
//  Toolbox
//
//  Created by Paul on 11/6/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "FeedbackFormViewController.h"
#import "TestFlight.h"

@interface FeedbackFormViewController ()

@end

@implementation FeedbackFormViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.feedbackTextView becomeFirstResponder];
    [self.feedbackTextView setSelectedTextRange:[self.feedbackTextView textRangeFromPosition:self.feedbackTextView.beginningOfDocument toPosition:self.feedbackTextView.endOfDocument]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel:(id)sender
{
    [self.delegate onFeedbackFormComplete];
}

- (IBAction)submit:(id)sender
{
    NSString* text = [self.feedbackTextView text];
    if(
       [text compare: @"Insert Feedback Here"] != NSOrderedSame
       && [text compare: @""] != NSOrderedSame
      )
    {
        [TestFlight submitFeedback:text];
        [self.delegate onFeedbackFormComplete];
    }
}
@end
