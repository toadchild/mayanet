// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to HackingProgramGroup.h instead.

#import <CoreData/CoreData.h>

extern const struct HackingProgramGroupAttributes {
	__unsafe_unretained NSString *name;
} HackingProgramGroupAttributes;

extern const struct HackingProgramGroupRelationships {
	__unsafe_unretained NSString *devices;
	__unsafe_unretained NSString *programs;
} HackingProgramGroupRelationships;

@class HackingDevice;
@class HackingProgram;

@interface HackingProgramGroupID : NSManagedObjectID {}
@end

@interface _HackingProgramGroup : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) HackingProgramGroupID* objectID;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *devices;

- (NSMutableSet*)devicesSet;

@property (nonatomic, strong) NSSet *programs;

- (NSMutableSet*)programsSet;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newDevicesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newProgramsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _HackingProgramGroup (DevicesCoreDataGeneratedAccessors)
- (void)addDevices:(NSSet*)value_;
- (void)removeDevices:(NSSet*)value_;
- (void)addDevicesObject:(HackingDevice*)value_;
- (void)removeDevicesObject:(HackingDevice*)value_;

@end

@interface _HackingProgramGroup (ProgramsCoreDataGeneratedAccessors)
- (void)addPrograms:(NSSet*)value_;
- (void)removePrograms:(NSSet*)value_;
- (void)addProgramsObject:(HackingProgram*)value_;
- (void)removeProgramsObject:(HackingProgram*)value_;

@end

@interface _HackingProgramGroup (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSMutableSet*)primitiveDevices;
- (void)setPrimitiveDevices:(NSMutableSet*)value;

- (NSMutableSet*)primitivePrograms;
- (void)setPrimitivePrograms:(NSMutableSet*)value;

@end
