// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MetachemistryNote.h instead.

#import <CoreData/CoreData.h>
#import "Note.h"

extern const struct MetachemistryNoteAttributes {
	__unsafe_unretained NSString *levelTwo;
} MetachemistryNoteAttributes;

extern const struct MetachemistryNoteRelationships {
	__unsafe_unretained NSString *metachemistry;
} MetachemistryNoteRelationships;

@class Metachemistry;

@interface MetachemistryNoteID : NoteID {}
@end

@interface _MetachemistryNote : Note {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MetachemistryNoteID* objectID;

@property (nonatomic, strong) NSNumber* levelTwo;

@property (atomic) BOOL levelTwoValue;
- (BOOL)levelTwoValue;
- (void)setLevelTwoValue:(BOOL)value_;

//- (BOOL)validateLevelTwo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Metachemistry *metachemistry;

//- (BOOL)validateMetachemistry:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

#endif

@end

@interface _MetachemistryNote (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveLevelTwo;
- (void)setPrimitiveLevelTwo:(NSNumber*)value;

- (BOOL)primitiveLevelTwoValue;
- (void)setPrimitiveLevelTwoValue:(BOOL)value_;

- (Metachemistry*)primitiveMetachemistry;
- (void)setPrimitiveMetachemistry:(Metachemistry*)value;

@end
