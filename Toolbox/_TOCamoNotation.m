// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TOCamoNotation.m instead.

#import "_TOCamoNotation.h"

@implementation TOCamoNotationID
@end

@implementation _TOCamoNotation

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TOCamoNotation" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TOCamoNotation";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TOCamoNotation" inManagedObjectContext:moc_];
}

- (TOCamoNotationID*)objectID {
	return (TOCamoNotationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

#if TARGET_OS_IPHONE

#endif

@end

