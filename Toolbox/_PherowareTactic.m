// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PherowareTactic.m instead.

#import "_PherowareTactic.h"

const struct PherowareTacticAttributes PherowareTacticAttributes = {
	.ammo = @"ammo",
	.attMod = @"attMod",
	.burst = @"burst",
	.damage = @"damage",
	.effect = @"effect",
	.name = @"name",
	.oppMod = @"oppMod",
	.range = @"range",
	.skill = @"skill",
	.special = @"special",
	.tacticType = @"tacticType",
	.target = @"target",
};

const struct PherowareTacticRelationships PherowareTacticRelationships = {
	.wikiEntry = @"wikiEntry",
};

@implementation PherowareTacticID
@end

@implementation _PherowareTactic

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PherowareTactic" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PherowareTactic";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PherowareTactic" inManagedObjectContext:moc_];
}

- (PherowareTacticID*)objectID {
	return (PherowareTacticID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic ammo;

@dynamic attMod;

@dynamic burst;

@dynamic damage;

@dynamic effect;

@dynamic name;

@dynamic oppMod;

@dynamic range;

@dynamic skill;

@dynamic special;

@dynamic tacticType;

@dynamic target;

@dynamic wikiEntry;

#if TARGET_OS_IPHONE

#endif

@end

