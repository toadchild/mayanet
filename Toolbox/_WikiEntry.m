// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to WikiEntry.m instead.

#import "_WikiEntry.h"

const struct WikiEntryAttributes WikiEntryAttributes = {
	.name = @"name",
	.nameFirstLetter = @"nameFirstLetter",
	.pageName = @"pageName",
	.url = @"url",
};

const struct WikiEntryRelationships WikiEntryRelationships = {
	.hackingPrograms = @"hackingPrograms",
	.pherowareTactics = @"pherowareTactics",
	.specialRules = @"specialRules",
	.weapons = @"weapons",
	.wiki = @"wiki",
};

@implementation WikiEntryID
@end

@implementation _WikiEntry

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"WikiEntry" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"WikiEntry";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"WikiEntry" inManagedObjectContext:moc_];
}

- (WikiEntryID*)objectID {
	return (WikiEntryID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic name;

@dynamic nameFirstLetter;

@dynamic pageName;

@dynamic url;

@dynamic hackingPrograms;

- (NSMutableSet*)hackingProgramsSet {
	[self willAccessValueForKey:@"hackingPrograms"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"hackingPrograms"];

	[self didAccessValueForKey:@"hackingPrograms"];
	return result;
}

@dynamic pherowareTactics;

- (NSMutableSet*)pherowareTacticsSet {
	[self willAccessValueForKey:@"pherowareTactics"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"pherowareTactics"];

	[self didAccessValueForKey:@"pherowareTactics"];
	return result;
}

@dynamic specialRules;

- (NSMutableSet*)specialRulesSet {
	[self willAccessValueForKey:@"specialRules"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specialRules"];

	[self didAccessValueForKey:@"specialRules"];
	return result;
}

@dynamic weapons;

- (NSMutableSet*)weaponsSet {
	[self willAccessValueForKey:@"weapons"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"weapons"];

	[self didAccessValueForKey:@"weapons"];
	return result;
}

@dynamic wiki;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newHackingProgramsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"HackingProgram" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"wikiEntry == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newPherowareTacticsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"PherowareTactic" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"wikiEntry == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecialRulesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecialRule" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"wikiEntry == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newWeaponsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Weapon" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"wikiEntry == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

