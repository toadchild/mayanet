//
//  DetailViewController.h
//  Toolbox
//
//  Created by Paul on 10/20/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedbackFormViewController.h"
#import "Refreshable.h"
@class Roster, Unit, UnitOption,SpecOpsBuild, AspectSegmentedControl, GroupMember, UnitOptionTableViewCell, HackingProgramTableViewCell, PherowareTacticTableViewCell, SkillNotesTableViewCell;

enum DetailViewDisplayMode {
    eNoDisplayMode = -1,
    eBrowseUnitDisplayMode,         //When browsing unit, list out all available options
    eBrowseUnitWeaponDisplayMode,   //When browsing list all weapons available to unit
    eBrowseSpecOpsBuildMode,
    
    eRosterUnitOptionDisplayMode,   //List a specific roster unit option
    eRosterUnitDisplayMode,         //When veiwing a roster unit, list out all available options
    eRosterUnitWeaponDisplayMode,   //List all weapons available to the roster unit

    
    eSpecOpsDisplayMode,    //Display a spec ops
    eSpecOpsRebuildMode,    //Rebuild a spec ops from a previous build (soon)
    eNumberOfBrowseModes
} typedef DetailViewDisplayMode;

// Weapons/Skills grouped into categories
enum SkillSelectorSections{
    eSkillBS,
    eSkillDTW,
    eSkillCC,
    eSkillDeployable,
    eSkillMisc,
    eSkillHacking,
    eSkillPheroware,
    eSkillNotes,
    eSkillOptions,
};


@interface DetailViewController : UIViewController <UISplitViewControllerDelegate, UITableViewDataSource,UITableViewDelegate, FeedbackFormDelegate, UIWebViewDelegate, UIGestureRecognizerDelegate>
{
    NSArray* unitOptions;
    NSMutableArray* availableProfiles;
    NSMutableDictionary *cellHeightCache;
    NSArray* specOpsWeapons;
    NSArray* specOpsSpecialRules;
    NSArray* specOpsEquipment;
    int tapCount;
    long tappedRow;
    NSTimer* tapTimer;
    NSMutableOrderedSet* weapons;
    NSMutableOrderedSet* templateWeapons;
    NSMutableOrderedSet* hacking;
    NSMutableOrderedSet* pheroware;
    NSMutableOrderedSet* notes;
    NSMutableOrderedSet* ccskill;
    NSMutableOrderedSet* ccweapons;
    NSMutableOrderedSet* deployables;
    NSMutableOrderedSet* miscskill;
    enum SkillSelectorSections selectedSkill;
    //BOOL specOpsBuildDisplayMode;
    DetailViewDisplayMode currentDisplayMode;
    DetailViewDisplayMode previousDisplayMode;
    NSString* linkableTemplate;
    NSString* linkableHeaderTemplate;
    CGFloat viewWidth;
    UnitOptionTableViewCell* heightCachedUnitOptionTableViewCell;
    HackingProgramTableViewCell* heightCachedHackingProgramTableViewCell;
    PherowareTacticTableViewCell* heightCachedPherowareTacticTableViewCell;
    SkillNotesTableViewCell* heightCachedSkillNotesTableViewCell;
    Roster* displayedRoster;
    
}
- (IBAction)onSubmitFeedback:(id)sender;
- (IBAction)onResetSpecOpsButtonPressed:(id)sender;
- (IBAction)onAddSpecOpsButtonPressed:(id)sender;

@property (strong, nonatomic) Unit* unit;
@property (strong, nonatomic) UnitOption* unitOption;
@property (strong, nonatomic) GroupMember* groupMember;

@property (strong, nonatomic) SpecOpsBuild* specOpsBuild;
//- (IBAction)onToggleRoster:(UIBarButtonItem*)button;
//- (IBAction)onToggleUnits:(UIBarButtonItem *)sender;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (IBAction)onProfileSelection:(id)sender;
- (IBAction)onSkillSelection:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *unitIconImageView;
@property (weak, nonatomic) IBOutlet UIImageView *unitRegularImageView;
@property (weak, nonatomic) IBOutlet UIImageView *unitImpetuousImageView;
@property (weak, nonatomic) IBOutlet UIImageView *unitCubeImageView;
@property (weak, nonatomic) IBOutlet UIImageView *unitHackableImageView;
@property (weak, nonatomic) IBOutlet UILabel *unitNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitClassificationLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitMOVLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitCCLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitBSLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitPHLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitWIPLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitARMLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitBTSLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitWLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitSLabel;
@property (weak, nonatomic) IBOutlet UILabel *unitAVALabel;
@property (weak, nonatomic) IBOutlet UIWebView *unitSpecialRulesWebView;
@property (weak, nonatomic) IBOutlet UIWebView *unitWeaponsWebView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *profileSelectionSegmentedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *skillSelectionSegmentedControl;

@property (weak, nonatomic) IBOutlet UILabel *woundsHeaderLabel;
@property (weak, nonatomic) IBOutlet UITableView *unitOptionsTableView;
@property (weak, nonatomic) IBOutlet UIButton *specOpsResetButton;
@property (weak, nonatomic) IBOutlet UILabel *specOpsCost;
@property (weak, nonatomic) IBOutlet UIButton *specOpsAddButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *specialRulesHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *weaponsHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cubeHorizontalSpacingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hackableHorizontalSpacingConstraint;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *profileSegmentedControlHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *skillSegmentedControlHeightConstraint;


-(IBAction)aspectSegmentControlChange:(AspectSegmentedControl*)sender;
-(void) unitSelection:(Unit*)unit;
-(void) unitDetailSelection:(Unit*)unit;

-(void) unitSelection:(Unit*)unit groupMember:(GroupMember*)groupMember;
-(void) unitDetailSelection:(Unit*)unit groupMember:(GroupMember*)groupMember;

-(void) specOpsBuildSelection:(SpecOpsBuild*)specOpsBuild;
-(void) specOpsRebuild:(SpecOpsBuild*)specOpsBuild;

-(void)invalidateGroupMember:(GroupMember*)groupMember;

-(void)setDisplayedRoster:(Roster*)roster;

-(void)refresh;
@end
