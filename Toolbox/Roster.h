#import "_Roster.h"
#import "RosterValidator.h"
#import "AppDelegate.h"

@class UnitOption;
@class Unit;
@class GroupMember;
@class SpecOpsBuild;

@interface Roster : _Roster
    
@property (nonatomic, strong,readonly) NSArray* failedValidators;
    
+(Roster*) currentRoster;
+(BOOL) setCurrentRoster:(Roster*)roster;
    
+(Roster*) alternateRoster;
+(BOOL) setAlternateRoster:(Roster*)roster;
    
+(Roster*) createNewRosterWithArmy:(Army*)army name:(NSString*)name pointCap:(NSInteger)pointCap;
+(Roster*) createNewRosterWithSectorial:(Sectorial*)sectorial name:(NSString*)name pointCap:(NSInteger)pointCap;

-(BOOL) testUnit: (Unit*)unit unitOption:(UnitOption*)unitOption;

-(BOOL) addUnit: (Unit*)unit unitOption:(UnitOption*)unitOption;

-(BOOL) changeGroupMember:(GroupMember*)groupMember toOption:(UnitOption*)unitOption;

-(BOOL) addSpecOpsBuild:(SpecOpsBuild*)specOpsBuild;

-(void)deleteCombatGroup:(CombatGroup*)combatGroup;
    
-(void)deleteGroupMember:(GroupMember*)groupMember;
    
-(NSInteger) getAVACountUptoGroupMember:(GroupMember*)aGroupMember;

-(NSInteger) getLtCountUptoGroupMember:(GroupMember*)aGroupMember;

-(Unit*) getUnitFrom:(UnitOption*)unitOption inheritFromBase:(BOOL)inherit;

-(Unit*) getUnitFrom:(UnitOption*)unitOption;

-(NSUInteger)totalPoints;

-(NSUInteger)totalDeadPoints;

-(NSUInteger)totalPlayingPoints;

-(NSUInteger)totalPlayingPointCap;

-(NSInteger)pointsToRetreat;

-(double)totalSWC;

-(double) maxSWC;

-(NSInteger) numberOfOrders;
-(NSInteger)numberOfOrdersCountingDead:(BOOL)countingDead;
-(NSInteger) numberOfModels;
-(NSInteger) costOfModelsWithSWC;
-(void) resetDead;

-(void) reset;

-(NSArray*)unitOptions;
-(NSArray*)specOps;
-(NSArray*)unitProfiles;

-(void) updateValidation;

-(BOOL)failedValidation;

-(void) onRosterChange;

-(void)cyclePointCap;

-(NSString*)toURLSafeJSON64;
-(BOOL)fromURLSafeJSON64:(NSString*)urlSafeJSON64;

-(NSString*)toBBCode;
-(NSString*)toLocalHTML;
-(NSString*) toEmailHTML;
-(NSString*)toHTML;
-(NSString*)emailHeader;

-(NSString*)urlWithWebLink:(BOOL)useWeblink;

-(Roster*)duplicate;

+(Roster*)rosterFromWebCode:(NSString*)webCode;

+(NSString*)newRosterTitle;

-(void)clearRosterCache;

// Bulk export
+(NSArray*)exportAllWithDelegate:(AppDelegate*) delegate;
+(void)importAllFromArray:(NSArray*) array withDelegate:(AppDelegate*) delegate;

@end
