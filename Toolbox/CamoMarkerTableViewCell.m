//
//  CamoMarkerTableViewCell.m
//  Toolbox
//
//  Created by Paul on 12/6/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "CamoMarkerTableViewCell.h"
#import "CamoNote.h"

#import <QuartzCore/QuartzCore.h>
@interface CamoMarkerTableViewCell ()
{
    UIColor* buttonBackgroundColor;
    NSArray* markers;
}
@end

@implementation CamoMarkerTableViewCell


-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {

    }
    return self;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    markers = [self markers];
    buttonBackgroundColor = [self.numberSegmentControl tintColor];
    
    if(_note)
    {
        UIButton* marker = [markers objectAtIndex:self.camoNote.patternValue];
        [self setMarker:marker enabled:TRUE];
        [self.numberSegmentControl setSelectedSegmentIndex:self.camoNote.numberValue];
    }
}

-(NSArray*)markers
{
    return @[self.camoMarker1, self.camoMarker2, self.camoMarker3, self.camoMarker4, self.camoMarker5];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)onCamoNumberSelectionValueChanged:(id)sender
{
    long index = [self.numberSegmentControl selectedSegmentIndex];
    int number = self.camoNote.numberValue;
    
    if(index != number)
    {
        [self.camoNote setNumberValue:index];
        [self.note.managedObjectContext save:nil];
        [self onModified];
    }
}

- (IBAction)onCamoMarkerSelection:(id)sender
{
    [UIView beginAnimations:@"Animation" context:nil];
    for(UIButton* marker in markers)
    {
        if(marker == sender)
        {
            if(![self getMarkerEnabled:marker])
            {
                [self.camoNote setPatternValue:[markers indexOfObject:marker]];
                [self.note.managedObjectContext save:nil];
                [self onModified];
                [self setMarker:marker enabled:TRUE];
            }
        }
        else
        {
            //Unset button background
            [self setMarker:marker enabled:FALSE];
        }
    }
    [UIView commitAnimations];

}

-(void)setMarker:(UIButton*)marker enabled:(BOOL)enabled
{
    if(enabled)
    {
        [marker setBackgroundColor:buttonBackgroundColor];
        [marker.layer setCornerRadius:10];
    }
    else
    {
        //Unset button background
        [marker setBackgroundColor:[UIColor clearColor]];
    }

}

-(BOOL)getMarkerEnabled:(UIButton*)marker
{
    return marker.backgroundColor == buttonBackgroundColor;
}

-(CamoNote*)camoNote
{
    return (CamoNote*)self.note;
}

-(Note*)note
{
    if(!_note)
    {
        _note = [CamoNote insertInManagedObjectContext:self.groupMember.managedObjectContext];
        _note.groupMember = self.groupMember;
        _note.notation = self.notation;
    }
    return _note;
}

@end
