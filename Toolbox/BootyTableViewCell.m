//
//  BootyTableViewCell.m
//  Toolbox
//
//  Created by Paul on 12/6/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "BootyTableViewCell.h"
#import "Booty.h"
#import "BootyNote.h"
#import "BootyNotation.h"
#import "Weapon.h"
#import "SpecialRule.h"

@implementation BootyTableViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)layoutSubviews
{
    [super layoutSubviews];
    if(_note)
    {
        [self setBooty:self.bootyNote.booty];
    }
    else
    {
        if([self.notation type] == eBootyNotation)
        {
            [self.bootyLevel2Preference removeFromSuperview];
        }
    }
}

- (IBAction)onBootyLevel1Preference:(id)sender
{
    Booty* booty = [Booty rollWithLevelTwo:FALSE];
    
    [self setBooty:booty];
    [self onModified];
}

- (IBAction)onBootyLevel2Preference:(id)sender
{
    Booty* booty = [Booty rollWithLevelTwo:TRUE];
    
    [self setBooty:booty];
    [self onModified];
}

-(void)setBooty:(Booty*)booty
{
    self.bootyNote.booty = booty;
    
    NSString* outcome;
    
    if(booty.weapon)
    {
        outcome = booty.weapon.name;
    }
    else if(booty.specialRule)
    {
        outcome = booty.specialRule.name;
    }
    
    self.bootyOutcome.text = [NSString stringWithFormat:@"Booty: %@", outcome];
    [self.bootyOutcome setHidden:FALSE];
    [self.bootyLevel1Preference removeFromSuperview];
    [self.bootyLevel2Preference removeFromSuperview];
}


-(BootyNote*)bootyNote
{
    return (BootyNote*)self.note;
}

-(Note*)note
{
    if(!_note)
    {
        _note = [BootyNote insertInManagedObjectContext:self.groupMember.managedObjectContext];
        _note.groupMember = self.groupMember;
        _note.notation = self.notation;
        
        // The BootyNote needs to have the same levelTwo flag as the BootyNotation
        BootyNote *bootyNote = (BootyNote*)_note;
        BootyNotation *bootyNotation = (BootyNotation*)self.notation;
        bootyNote.levelTwoValue = bootyNotation.levelTwoValue;
    }
    return _note;
}

@end
