//
//  SettingsViewController.m
//  Toolbox
//
//  Created by Paul Clark on 5/1/14.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "SettingsViewController.h"
#import "UIColor+ColorPalette.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.siSwitch setOn:[[NSUserDefaults standardUserDefaults] boolForKey:@"UseSIUnits"]];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarTintColor:[UIColor barColor]];
    [self.navigationController.navigationBar setTintColor:[UIColor barItemColor]];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor titleColor],
      NSForegroundColorAttributeName,nil]];
    
    [self.navigationController setToolbarHidden:TRUE animated:TRUE];
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onSISwitchValueChange:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:[self.siSwitch isOn] forKey:@"UseSIUnits"];
}
@end
