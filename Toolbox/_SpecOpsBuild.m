// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SpecOpsBuild.m instead.

#import "_SpecOpsBuild.h"

const struct SpecOpsBuildAttributes SpecOpsBuildAttributes = {
	.name = @"name",
};

const struct SpecOpsBuildRelationships SpecOpsBuildRelationships = {
	.aspects = @"aspects",
	.baseUnitOption = @"baseUnitOption",
	.equipment = @"equipment",
	.groupMember = @"groupMember",
	.specialRules = @"specialRules",
	.unit = @"unit",
	.weapons = @"weapons",
};

@implementation SpecOpsBuildID
@end

@implementation _SpecOpsBuild

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SpecOpsBuild" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SpecOpsBuild";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SpecOpsBuild" inManagedObjectContext:moc_];
}

- (SpecOpsBuildID*)objectID {
	return (SpecOpsBuildID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic name;

@dynamic aspects;

@dynamic baseUnitOption;

@dynamic equipment;

- (NSMutableSet*)equipmentSet {
	[self willAccessValueForKey:@"equipment"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"equipment"];

	[self didAccessValueForKey:@"equipment"];
	return result;
}

@dynamic groupMember;

@dynamic specialRules;

- (NSMutableSet*)specialRulesSet {
	[self willAccessValueForKey:@"specialRules"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specialRules"];

	[self didAccessValueForKey:@"specialRules"];
	return result;
}

@dynamic unit;

@dynamic weapons;

- (NSMutableSet*)weaponsSet {
	[self willAccessValueForKey:@"weapons"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"weapons"];

	[self didAccessValueForKey:@"weapons"];
	return result;
}

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newEquipmentFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsEquipment" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"specops CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecialRulesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsSpecialRule" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"specops CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newWeaponsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsWeapon" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"specops CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

