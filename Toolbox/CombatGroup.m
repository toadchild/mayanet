#import "CombatGroup.h"
#import "GroupMember.h"
#import "Roster.h"
#import "UnitOption.h"
#import "Unit.h"
#import "UnitProfile.h"
#import "UnitAspects.h"
#import "SpecialRule.h"
#import "SpecOpsBuild.h"
#import "SpecOpsBaseUnitOption.h"

@interface CombatGroup ()

// Private interface goes here.

@end


@implementation CombatGroup

-(void)addGroupMembersObject:(GroupMember *)value_
{
    NSMutableOrderedSet* tempSet = [NSMutableOrderedSet orderedSetWithOrderedSet:self.groupMembers];
    [tempSet addObject:value_];
    self.groupMembers = tempSet;
}

-(void)removeGroupMember:(GroupMember *)groupMember withChildren:(BOOL) children
{
    NSMutableOrderedSet* tempSet = [[NSMutableOrderedSet alloc] initWithOrderedSet: self.groupMembers];
    [tempSet removeObject:groupMember];
    
    if(children)
    {
        //Handle children groupMembers
        for(GroupMember* child in groupMember.childrenGroupMembers)
        {
            [tempSet removeObject:child];
        }
    }
    
    self.groupMembers = tempSet;
}

-(void)addGroupMember:(GroupMember *)groupMember atIndex:(NSUInteger) i withChildren:(BOOL) children
{
    NSMutableOrderedSet* tempSet = [[NSMutableOrderedSet alloc] initWithOrderedSet: self.groupMembers];
    if(i > [tempSet count])
    {
        [tempSet addObject:groupMember];
    }
    else
    {
        [tempSet insertObject:groupMember atIndex:i];
    }
    
    if(children)
    {
        //Handle children groupMembers
        int x=1;
        for(GroupMember* child in groupMember.childrenGroupMembers)
        {
            if(i + x > [tempSet count])
            {
                [tempSet addObject:child];
            }
            else
            {
                [tempSet insertObject:child atIndex:i + x];
            }
            x++;
        }
    }

    self.groupMembers = tempSet;
}

-(NSUInteger)indexOfGroupMember:(GroupMember*)groupMember
{
    return [self.groupMembers indexOfObject:groupMember];
}

-(NSInteger) numberOfRegularOrders
{
    return [self numberOfRegularOrdersCountingDead:TRUE];
}
-(NSInteger) numberOfIrregularOrders
{
    return [self numberOfIrregularOrdersCountingDead:TRUE];
}
-(NSInteger) numberOfImpetuousOrders
{
    return [self numberOfImpetuousOrdersCountingDead:TRUE];
}
-(NSInteger) numberOfOrders
{
    return [self numberOfOrdersCountingDead:TRUE];
}

-(NSInteger) numberOfFigures
{
    return [self numberOfFiguresCountingDead:TRUE];
}

-(NSInteger) numberOfOrdersCountingDead:(BOOL)countDead
{
    return [self numberOfRegularOrdersCountingDead:countDead] + [self numberOfIrregularOrdersCountingDead:countDead];
}

-(NSInteger) numberOfRegularOrdersCountingDead:(BOOL)countDead
{
    BOOL hasJumperModel = FALSE;
    BOOL hasLivingJumperModel = countDead;
    BOOL hasLivingAIBeacon = FALSE;
    int orders = 0;
    for(GroupMember* groupMember in self.groupMembers)
    {
        if([groupMember isJumperWithinRoster:self.roster])
        {
            hasJumperModel = TRUE;
            if(!(!countDead && groupMember.isDeadValue))
            {
                hasLivingJumperModel = TRUE;
            }
        }
        
        if([groupMember isAIBeaconWithinRoster:self.roster])
        {
            if(!(!countDead && groupMember.isDeadValue))
            {
                hasLivingAIBeacon = TRUE;
            }
        }
        
        BOOL providesOrder = [groupMember providesOrderWithinRoster:self.roster];
        if(!providesOrder)
        {
            continue;
        }
        
        if(groupMember.unitProfile)
        {
            if(groupMember.unitProfile.aspects.regularValue)
            {
                if(!(!countDead && groupMember.isDeadValue))
                    orders++;
            }
            continue;
        }
        
        Unit* unit = [groupMember unitWithinRoster:self.roster];
        
        if([unit.aspects.regular boolValue])
        {
            if(!(!countDead && groupMember.isDeadValue))
                orders++;
        }
    }
    if(hasJumperModel && (hasLivingJumperModel || hasLivingAIBeacon))
    {
        orders++;
    }
    return orders;
}

-(NSInteger) numberOfIrregularOrdersCountingDead:(BOOL)countDead
{
    GroupMember* antipodeController = nil;
    int orders = 0;
    for(GroupMember* groupMember in self.groupMembers) {
        if([groupMember isAntipodeWithinRoster:self.roster]) {
            // Only count one antipode from a given pack.
            if(!(!countDead && groupMember.isDeadValue)) {
                if (antipodeController != groupMember.parentGroupMember) {
                    orders++;
                    antipodeController = groupMember.parentGroupMember;
                }
            }
        }
        
        BOOL providesOrder = [groupMember providesOrderWithinRoster:self.roster];
        if(!providesOrder) {
            continue;
        }
        
        if(groupMember.unitProfile) {
            if(!groupMember.unitProfile.aspects.regularValue) {
                if(!(!countDead && groupMember.isDeadValue)) {
                    orders++;
                }
            }
            continue;
        }
        
        Unit* unit = [groupMember unitWithinRoster:self.roster];
        
        if(![unit.aspects.regular boolValue]) {
            if(!(!countDead && groupMember.isDeadValue)) {
                orders++;
            }
        }
    }
    
    return orders;
}
-(NSInteger) numberOfImpetuousOrdersCountingDead:(BOOL)countDead
{
    int orders = 0;
    for(GroupMember* groupMember in self.groupMembers)
    {
        BOOL providesOrder = [groupMember providesOrderWithinRoster:self.roster];
        if(!providesOrder)
        {
            continue;
        }

        if(groupMember.unitProfile)
        {
            if(groupMember.unitProfile.aspects.furyValue == FURY_IMPETUOUS || groupMember.unitProfile.aspects.furyValue == FURY_EXTREME_IMPETUOUS)
            {
                if(!(!countDead && groupMember.isDeadValue))
                orders++;
            }
            continue;
        }

        
        Unit* unit = [groupMember unitWithinRoster:self.roster];

        if(unit.aspects.furyValue == FURY_IMPETUOUS || unit.aspects.furyValue == FURY_EXTREME_IMPETUOUS)
        {
            if(!(!countDead && groupMember.isDeadValue))
                orders++;
        }
    }
    return orders;
}

-(NSInteger) numberOfFiguresCountingDead:(BOOL)countDead
{
    BOOL hasJumperModel = FALSE;
    BOOL hasLivingJumperModel = countDead;
    GroupMember* antipodeController = nil;
    int nFigures = 0;
    for(GroupMember* groupMember in self.groupMembers) {
        if([groupMember isJumperWithinRoster:self.roster]) {
            hasJumperModel = TRUE;
            if(!(!countDead && groupMember.isDeadValue)) {
                hasLivingJumperModel = TRUE;
            }
        }
        
        if([groupMember isAntipodeWithinRoster:self.roster]) {
            // Only count one antipode from a given pack.
            if(!(!countDead && groupMember.isDeadValue)) {
                if (antipodeController != groupMember.parentGroupMember) {
                    nFigures++;
                    antipodeController = groupMember.parentGroupMember;
                }
            }
        }
        
        BOOL takesCombatSlot = [groupMember takesCombatSlotWithinRoster:self.roster];
        
        if(takesCombatSlot) {
            if(!(!countDead && groupMember.isDeadValue)) {
                nFigures++;
            }
        }
    }
    if(hasJumperModel && hasLivingJumperModel) {
        nFigures++;
    }

    return nFigures;

}
    
-(NSArray*)unitOptions
{
    NSMutableArray* unitOptions = [[NSMutableArray alloc] init];

    for(GroupMember* groupMember in self.groupMembers)
    {
        UnitOption* unitOption = [groupMember unitOption];
        if(unitOption)
        {
            [unitOptions addObject:unitOption];
        }
    }
    return unitOptions;
}
    

@end
