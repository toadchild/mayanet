// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CombatGroup.h instead.

#import <CoreData/CoreData.h>

extern const struct CombatGroupRelationships {
	__unsafe_unretained NSString *groupMembers;
	__unsafe_unretained NSString *roster;
} CombatGroupRelationships;

@class GroupMember;
@class Roster;

@interface CombatGroupID : NSManagedObjectID {}
@end

@interface _CombatGroup : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CombatGroupID* objectID;

@property (nonatomic, strong) NSOrderedSet *groupMembers;

- (NSMutableOrderedSet*)groupMembersSet;

@property (nonatomic, strong) Roster *roster;

//- (BOOL)validateRoster:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newGroupMembersFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _CombatGroup (GroupMembersCoreDataGeneratedAccessors)
- (void)addGroupMembers:(NSOrderedSet*)value_;
- (void)removeGroupMembers:(NSOrderedSet*)value_;
- (void)addGroupMembersObject:(GroupMember*)value_;
- (void)removeGroupMembersObject:(GroupMember*)value_;

- (void)insertObject:(GroupMember*)value inGroupMembersAtIndex:(NSUInteger)idx;
- (void)removeObjectFromGroupMembersAtIndex:(NSUInteger)idx;
- (void)insertGroupMembers:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeGroupMembersAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInGroupMembersAtIndex:(NSUInteger)idx withObject:(GroupMember*)value;
- (void)replaceGroupMembersAtIndexes:(NSIndexSet *)indexes withGroupMembers:(NSArray *)values;

@end

@interface _CombatGroup (CoreDataGeneratedPrimitiveAccessors)

- (NSMutableOrderedSet*)primitiveGroupMembers;
- (void)setPrimitiveGroupMembers:(NSMutableOrderedSet*)value;

- (Roster*)primitiveRoster;
- (void)setPrimitiveRoster:(Roster*)value;

@end
