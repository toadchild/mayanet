//
//  ArmyViewController.m
//  Toolbox
//
//  Created by Paul on 10/20/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//
#import "AppDelegate.h"
#import "ArmyViewController.h"
#import "SectorialViewController.h"
#import "DetailViewController.h"
#import "RosterListViewController.h"
#import "MiddleNavigationController.h"
#import "Army.h"
#import "Sectorial.h"
#import "Unit.h"
#import "Util.h"
#import "UnitType.h"
#import "UnitAspects.h"
#import "UnitOption.h"
#import "Weapon.h"
#import "SpecialRule.h"

@interface ArmyViewController ()

@end

@implementation ArmyViewController

@synthesize army;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    typeOffset = 2;
    [self setTitle: [self.army name]];
}

-(void)refreshUnits
{
    [self loadUnits];
    [self.tableView reloadData];
}

-(void) loadUnits
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [Sectorial entityInManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setIncludesSubentities:TRUE];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"army = %@", self.army];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    sectorials = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error != nil)
    {
        
    }
    
    //Get all the types
    entity = [UnitType entityInManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setIncludesSubentities:FALSE];
    
    // Edit the sort key as appropriate.
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortOrder" ascending:YES];
    sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    [fetchRequest setPredicate:nil];
    
    types = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    NSMutableDictionary* sortedUnitsByType =[[NSMutableDictionary alloc] init];
    for(UnitType* unitType in types)
    {
        NSArray* unitsOfType = [self unitsOfType:unitType includeMerc:self.army.showMercsValue];
        if([unitsOfType count])
        {
            [sortedUnitsByType setObject:unitsOfType forKey:unitType.code];
        }
    }
    unitsByType = sortedUnitsByType;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationController.navigationBar.titleTextAttributes =
    @{NSForegroundColorAttributeName: [self.army primaryColor]};
}

-(void)viewWillAppear:(BOOL)animated
{
    [self refreshUnits];
    self.tableView.hidden = FALSE;
    
    [self.navigationController.navigationBar setBarTintColor:[self.army backgroundColor]];
    [self.navigationController.navigationBar setTintColor:[self.army secondaryColor]];
    
    [super viewWillAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.tableView.hidden = TRUE;
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    UnitType* type;
    NSArray* units;
    
    switch(section)
    {
        case 0:
            return [sectorials count];
        case 1:
            return 1;
        default:
            if(!isSearching)
            {
                type = [types objectAtIndex:section-2];
                units = [unitsByType objectForKey:type.code];
            }
            else
            {
                type = [searchTypes objectAtIndex:section-2];
                units = [searchUnitsByType objectForKey:type.code];
            }
            return [units count];
    }
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    UnitType* type;
    NSArray* units;
    switch(section)
    {
        case 0:
            return [sectorials count] != 0 ? @"Sectorials" : NULL;
        case 1:
            return @"Rosters";
        default:
            if(!isSearching)
            {
                type = [types objectAtIndex:section-2];
                units = [unitsByType objectForKey:type.code];
                if([units count] != 0)
                {
                    return type.name;
                }
            }
            else
            {
                type = [searchTypes objectAtIndex:section-2];
                units = [searchUnitsByType objectForKey:type.code];
                if([units count] != 0)
                {
                    return type.name;
                }
            }
            
    }
    return nil;
}



-(NSPredicate*)searchPredicate:(NSPredicate*)otherPredicate
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"army = %@", self.army];
    return [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate,otherPredicate]];
}


#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"ViewSectorial"])
    {
        SectorialViewController *con = [segue destinationViewController];
        [con setSectorial:sender];
        [con setManagedObjectContext:self.managedObjectContext];
    }
    if([segue.identifier isEqualToString:@"ViewRosterList"])
    {
        RosterListViewController *con = [segue destinationViewController];
        [con setArmy:self.army];
        [con setManagedObjectContext:self.managedObjectContext];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        Sectorial* sectorial = [sectorials objectAtIndex:[indexPath row]];
        [self performSegueWithIdentifier:@"ViewSectorial" sender:sectorial];
    }
    else if(indexPath.section == 1)
    {
        [self performSegueWithIdentifier:@"ViewRosterList" sender:self.army];
    }
    else
    {
        Unit* unit = [self unitAtIndexPath:indexPath tableView:tableView];
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        MiddleNavigationController* con = delegate.containerViewController.middleViewController;
        [con showUnit:unit];
    }
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section > 1)
    {
        [self.tableView selectRowAtIndexPath:indexPath animated:FALSE scrollPosition:UITableViewScrollPositionNone];
        Unit* unit = [self unitAtIndexPath:indexPath tableView:tableView];
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        MiddleNavigationController* con = delegate.containerViewController.middleViewController;
        [con showUnitDetail:unit];
    }
}


- (void)configureCell:(UITableViewCell *)cell tableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
    if(indexPath.section == 0)
    {
        Sectorial* sectorial = [sectorials objectAtIndex:indexPath.row];
        NSString* title = cell.textLabel.text = [sectorial name];
        cell.textLabel.text = title;
        NSString* imageTitle = [sectorial imageTitle];
        cell.imageView.image = [UIImage imageNamed:imageTitle];
        cell.imageView.contentMode = UIViewContentModeCenter;
        cell.imageView.frame = CGRectMake(0,0,40,40);
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else if(indexPath.section == 1)
    {
        NSString* title = cell.textLabel.text = [army name];
        cell.textLabel.text = title;
        NSString* imageTitle = [army imageTitle];
        cell.imageView.image = [UIImage imageNamed:imageTitle];
        cell.textLabel.text = @"View Rosters";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    }
    else
    {
        Unit* unit = [self unitAtIndexPath:indexPath tableView:tableView];
        NSString* title = cell.textLabel.text = [unit isc];
        cell.textLabel.text = title;
        NSString* imageTitle = [unit imageTitle];
        cell.imageView.image = [UIImage imageNamed:imageTitle];
        cell.accessoryType = UITableViewCellAccessoryDetailButton;
        //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    }
}


-(void)clearSelection
{
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:TRUE];
}

- (IBAction)addRoster:(id)sender
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    if(self.army)
    {
        [dict setObject:self.army forKey:@"army"];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewRoster" object:dict];
}

-(BOOL)includeMerc
{
    return self.army.showMercsValue;
}

@end
