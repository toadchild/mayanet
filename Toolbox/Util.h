//
//  Util.h
//  Toolbox
//
//  Created by Paul on 10/28/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <Foundation/Foundation.h>

#define URL_ROOT @"http://mayanet.ghostlords.com/images/"
#define LOCAL_URL_ROOT @"images/small_logos/"
#define WEB_IMAGE_SUFFIX @"_logo_small.png"

@interface Util : NSObject

+(NSString*)urlSafeBase64:(NSString*)base64;

+(NSString*)base64FromURLSafe:(NSString*)urlSafe;

+(NSString*) concatArray:(NSArray*)stringArray;

+(NSString*) conditionalConcatArray:(NSArray*)stringArray seperator:(NSString*)seperator;

@end
