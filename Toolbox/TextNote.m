#import "TextNote.h"


@interface TextNote ()

// Private interface goes here.

@end


@implementation TextNote

// Custom logic goes here.
-(NotationType) type
{
    return eTextNotation;
}

@end
