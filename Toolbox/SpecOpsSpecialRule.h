#import "_SpecOpsSpecialRule.h"

@interface SpecOpsSpecialRule : _SpecOpsSpecialRule {}
+(SpecOpsSpecialRule*) specOpsSpecialRuleWithName:(NSString*)name;
@end
