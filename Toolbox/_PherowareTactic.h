// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PherowareTactic.h instead.

#import <CoreData/CoreData.h>

extern const struct PherowareTacticAttributes {
	__unsafe_unretained NSString *ammo;
	__unsafe_unretained NSString *attMod;
	__unsafe_unretained NSString *burst;
	__unsafe_unretained NSString *damage;
	__unsafe_unretained NSString *effect;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *oppMod;
	__unsafe_unretained NSString *range;
	__unsafe_unretained NSString *skill;
	__unsafe_unretained NSString *special;
	__unsafe_unretained NSString *tacticType;
	__unsafe_unretained NSString *target;
} PherowareTacticAttributes;

extern const struct PherowareTacticRelationships {
	__unsafe_unretained NSString *wikiEntry;
} PherowareTacticRelationships;

@class WikiEntry;

@interface PherowareTacticID : NSManagedObjectID {}
@end

@interface _PherowareTactic : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) PherowareTacticID* objectID;

@property (nonatomic, strong) NSString* ammo;

//- (BOOL)validateAmmo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* attMod;

//- (BOOL)validateAttMod:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* burst;

//- (BOOL)validateBurst:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* damage;

//- (BOOL)validateDamage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* effect;

//- (BOOL)validateEffect:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* oppMod;

//- (BOOL)validateOppMod:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* range;

//- (BOOL)validateRange:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* skill;

//- (BOOL)validateSkill:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* special;

//- (BOOL)validateSpecial:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* tacticType;

//- (BOOL)validateTacticType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* target;

//- (BOOL)validateTarget:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) WikiEntry *wikiEntry;

//- (BOOL)validateWikiEntry:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

#endif

@end

@interface _PherowareTactic (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAmmo;
- (void)setPrimitiveAmmo:(NSString*)value;

- (NSString*)primitiveAttMod;
- (void)setPrimitiveAttMod:(NSString*)value;

- (NSString*)primitiveBurst;
- (void)setPrimitiveBurst:(NSString*)value;

- (NSString*)primitiveDamage;
- (void)setPrimitiveDamage:(NSString*)value;

- (NSString*)primitiveEffect;
- (void)setPrimitiveEffect:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveOppMod;
- (void)setPrimitiveOppMod:(NSString*)value;

- (NSString*)primitiveRange;
- (void)setPrimitiveRange:(NSString*)value;

- (NSString*)primitiveSkill;
- (void)setPrimitiveSkill:(NSString*)value;

- (NSString*)primitiveSpecial;
- (void)setPrimitiveSpecial:(NSString*)value;

- (NSString*)primitiveTacticType;
- (void)setPrimitiveTacticType:(NSString*)value;

- (NSString*)primitiveTarget;
- (void)setPrimitiveTarget:(NSString*)value;

- (WikiEntry*)primitiveWikiEntry;
- (void)setPrimitiveWikiEntry:(WikiEntry*)value;

@end
