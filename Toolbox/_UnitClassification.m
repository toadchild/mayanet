// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UnitClassification.m instead.

#import "_UnitClassification.h"

const struct UnitClassificationAttributes UnitClassificationAttributes = {
	.code = @"code",
	.displayName = @"displayName",
};

const struct UnitClassificationRelationships UnitClassificationRelationships = {
	.aspects = @"aspects",
};

@implementation UnitClassificationID
@end

@implementation _UnitClassification

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UnitClassification" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UnitClassification";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UnitClassification" inManagedObjectContext:moc_];
}

- (UnitClassificationID*)objectID {
	return (UnitClassificationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic code;

@dynamic displayName;

@dynamic aspects;

- (NSMutableSet*)aspectsSet {
	[self willAccessValueForKey:@"aspects"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"aspects"];

	[self didAccessValueForKey:@"aspects"];
	return result;
}

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newAspectsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"UnitAspects" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"classification == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

