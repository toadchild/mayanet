//
//  MasterViewController.h
//  Toolbox
//
//  Created by Paul on 10/20/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

#import <CoreData/CoreData.h>
#import "Containable.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface MainViewController : UITableViewController <Containable, MFMailComposeViewControllerDelegate, UIActionSheetDelegate>
{
    NSArray* armies;
    UIActionSheet* userActionSheet;
}
@property (strong, nonatomic) DetailViewController *detailViewController;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

//- (IBAction)onUserSelection:(id)sender;

@end
