// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to HackingProgram.m instead.

#import "_HackingProgram.h"

const struct HackingProgramAttributes HackingProgramAttributes = {
	.ammo = @"ammo",
	.attMod = @"attMod",
	.burst = @"burst",
	.damage = @"damage",
	.effect = @"effect",
	.hide_mods = @"hide_mods",
	.name = @"name",
	.oppMod = @"oppMod",
	.range = @"range",
	.skill = @"skill",
	.special = @"special",
	.target = @"target",
};

const struct HackingProgramRelationships HackingProgramRelationships = {
	.group = @"group",
	.upgrades = @"upgrades",
	.wikiEntry = @"wikiEntry",
};

@implementation HackingProgramID
@end

@implementation _HackingProgram

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"HackingProgram" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"HackingProgram";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"HackingProgram" inManagedObjectContext:moc_];
}

- (HackingProgramID*)objectID {
	return (HackingProgramID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"hide_modsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"hide_mods"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic ammo;

@dynamic attMod;

@dynamic burst;

@dynamic damage;

@dynamic effect;

@dynamic hide_mods;

- (BOOL)hide_modsValue {
	NSNumber *result = [self hide_mods];
	return [result boolValue];
}

- (void)setHide_modsValue:(BOOL)value_ {
	[self setHide_mods:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveHide_modsValue {
	NSNumber *result = [self primitiveHide_mods];
	return [result boolValue];
}

- (void)setPrimitiveHide_modsValue:(BOOL)value_ {
	[self setPrimitiveHide_mods:[NSNumber numberWithBool:value_]];
}

@dynamic name;

@dynamic oppMod;

@dynamic range;

@dynamic skill;

@dynamic special;

@dynamic target;

@dynamic group;

@dynamic upgrades;

- (NSMutableSet*)upgradesSet {
	[self willAccessValueForKey:@"upgrades"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"upgrades"];

	[self didAccessValueForKey:@"upgrades"];
	return result;
}

@dynamic wikiEntry;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newUpgradesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"HackingDevice" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"upgrades CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

