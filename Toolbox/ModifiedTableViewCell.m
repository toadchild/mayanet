//
//  ModifiedTableViewCell.m
//  Toolbox
//
//  Created by Paul on 12/6/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "ModifiedTableViewCell.h"
#import "Note.h"
#import "NSDate+TimeAgo.h"

@implementation ModifiedTableViewCell
@synthesize note = _note;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    if(_note)
    {
        [self setModificationDateLabel:self.note.lastModified];
    }
    else
    {
       self.modifiedLabel.text = @"Never Modified";
    }
}

-(void)onModified
{
    [self recordModification:[NSDate date]];
}

-(void)recordModification:(NSDate*)date
{
    [self.note setLastModified:date];
    [self setModificationDateLabel:date];
    [self.note.managedObjectContext save:nil];
}

-(void)setModificationDateLabel:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    
    NSString *localDateString = [dateFormatter stringFromDate:date];
    
    self.modifiedLabel.text = [NSString stringWithFormat:@"Last Modified: %@ (%@)", localDateString, [date timeAgo]];
    
}

@end
