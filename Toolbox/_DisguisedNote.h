// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DisguisedNote.h instead.

#import <CoreData/CoreData.h>
#import "Note.h"

extern const struct DisguisedNoteRelationships {
	__unsafe_unretained NSString *unitOption;
} DisguisedNoteRelationships;

@class UnitOption;

@interface DisguisedNoteID : NoteID {}
@end

@interface _DisguisedNote : Note {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) DisguisedNoteID* objectID;

@property (nonatomic, strong) UnitOption *unitOption;

//- (BOOL)validateUnitOption:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

#endif

@end

@interface _DisguisedNote (CoreDataGeneratedPrimitiveAccessors)

- (UnitOption*)primitiveUnitOption;
- (void)setPrimitiveUnitOption:(UnitOption*)value;

@end
