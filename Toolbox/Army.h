#import "_Army.h"
#import "AppDelegate.h"

enum ArmyIds {
    eArmyPanOceania = 1999,
    eArmyYuJing = 2999,
    eArmyAriadna = 3999,
    eArmyHaqq = 4999,
    eArmyNomads = 5999,
    eArmyCombined = 6999,
    eArmyAleph = 7999,
    eArmyTohaa = 8999,
    eArmyMercs = 9999,
    eArmyO12 = 10999,
};

@interface Army : _Army {}

@property (nonatomic) NSOrderedSet *cachedRosters;
@property (nonatomic) NSMutableDictionary *unitsByID;
@property (nonatomic) NSMutableDictionary *unitsByLegacyISC;

+(NSArray*)allWithDelegate:(AppDelegate*)delegate;
+(Army*)armyWithID:(int)id;
+(Army*)armyWithID:(int)id withDelegate:(AppDelegate*)delegate;
+(Army*)armyWithIDCreatingNew:(int)id withDelegate:(AppDelegate*)delegate;

-(Unit*)unitWithID:(int)id;
-(Unit*)unitWithLegacyISC:(NSString*)isc;

-(UIColor*)backgroundColor;
-(UIColor*)primaryColor;
-(UIColor*)secondaryColor;

-(NSString*)imageTitle;
-(NSString*)webImageTitle;

@end
