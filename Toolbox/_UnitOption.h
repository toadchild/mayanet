// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UnitOption.h instead.

#import <CoreData/CoreData.h>

extern const struct UnitOptionAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *cost;
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *independentCost;
	__unsafe_unretained NSString *independentSWC;
	__unsafe_unretained NSString *isIndependent;
	__unsafe_unretained NSString *legacyCode;
	__unsafe_unretained NSString *swc;
} UnitOptionAttributes;

extern const struct UnitOptionRelationships {
	__unsafe_unretained NSString *disguisers;
	__unsafe_unretained NSString *enabledUnitProfile;
	__unsafe_unretained NSString *groupMembers;
	__unsafe_unretained NSString *specialRules;
	__unsafe_unretained NSString *specops;
	__unsafe_unretained NSString *units;
	__unsafe_unretained NSString *weapons;
} UnitOptionRelationships;

@class DisguisedNote;
@class UnitProfile;
@class GroupMember;
@class SpecialRule;
@class SpecOpsBaseUnitOption;
@class Unit;
@class Weapon;

@interface UnitOptionID : NSManagedObjectID {}
@end

@interface _UnitOption : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UnitOptionID* objectID;

@property (nonatomic, strong) NSString* code;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cost;

@property (atomic) int16_t costValue;
- (int16_t)costValue;
- (void)setCostValue:(int16_t)value_;

//- (BOOL)validateCost:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* id;

@property (atomic) int32_t idValue;
- (int32_t)idValue;
- (void)setIdValue:(int32_t)value_;

//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* independentCost;

@property (atomic) int16_t independentCostValue;
- (int16_t)independentCostValue;
- (void)setIndependentCostValue:(int16_t)value_;

//- (BOOL)validateIndependentCost:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDecimalNumber* independentSWC;

//- (BOOL)validateIndependentSWC:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isIndependent;

@property (atomic) BOOL isIndependentValue;
- (BOOL)isIndependentValue;
- (void)setIsIndependentValue:(BOOL)value_;

//- (BOOL)validateIsIndependent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* legacyCode;

//- (BOOL)validateLegacyCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDecimalNumber* swc;

//- (BOOL)validateSwc:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) DisguisedNote *disguisers;

//- (BOOL)validateDisguisers:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *enabledUnitProfile;

- (NSMutableSet*)enabledUnitProfileSet;

@property (nonatomic, strong) NSSet *groupMembers;

- (NSMutableSet*)groupMembersSet;

@property (nonatomic, strong) NSOrderedSet *specialRules;

- (NSMutableOrderedSet*)specialRulesSet;

@property (nonatomic, strong) NSSet *specops;

- (NSMutableSet*)specopsSet;

@property (nonatomic, strong) NSSet *units;

- (NSMutableSet*)unitsSet;

@property (nonatomic, strong) NSOrderedSet *weapons;

- (NSMutableOrderedSet*)weaponsSet;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newEnabledUnitProfileFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newGroupMembersFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecialRulesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecopsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newUnitsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newWeaponsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _UnitOption (EnabledUnitProfileCoreDataGeneratedAccessors)
- (void)addEnabledUnitProfile:(NSSet*)value_;
- (void)removeEnabledUnitProfile:(NSSet*)value_;
- (void)addEnabledUnitProfileObject:(UnitProfile*)value_;
- (void)removeEnabledUnitProfileObject:(UnitProfile*)value_;

@end

@interface _UnitOption (GroupMembersCoreDataGeneratedAccessors)
- (void)addGroupMembers:(NSSet*)value_;
- (void)removeGroupMembers:(NSSet*)value_;
- (void)addGroupMembersObject:(GroupMember*)value_;
- (void)removeGroupMembersObject:(GroupMember*)value_;

@end

@interface _UnitOption (SpecialRulesCoreDataGeneratedAccessors)
- (void)addSpecialRules:(NSOrderedSet*)value_;
- (void)removeSpecialRules:(NSOrderedSet*)value_;
- (void)addSpecialRulesObject:(SpecialRule*)value_;
- (void)removeSpecialRulesObject:(SpecialRule*)value_;

- (void)insertObject:(SpecialRule*)value inSpecialRulesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromSpecialRulesAtIndex:(NSUInteger)idx;
- (void)insertSpecialRules:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeSpecialRulesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInSpecialRulesAtIndex:(NSUInteger)idx withObject:(SpecialRule*)value;
- (void)replaceSpecialRulesAtIndexes:(NSIndexSet *)indexes withSpecialRules:(NSArray *)values;

@end

@interface _UnitOption (SpecopsCoreDataGeneratedAccessors)
- (void)addSpecops:(NSSet*)value_;
- (void)removeSpecops:(NSSet*)value_;
- (void)addSpecopsObject:(SpecOpsBaseUnitOption*)value_;
- (void)removeSpecopsObject:(SpecOpsBaseUnitOption*)value_;

@end

@interface _UnitOption (UnitsCoreDataGeneratedAccessors)
- (void)addUnits:(NSSet*)value_;
- (void)removeUnits:(NSSet*)value_;
- (void)addUnitsObject:(Unit*)value_;
- (void)removeUnitsObject:(Unit*)value_;

@end

@interface _UnitOption (WeaponsCoreDataGeneratedAccessors)
- (void)addWeapons:(NSOrderedSet*)value_;
- (void)removeWeapons:(NSOrderedSet*)value_;
- (void)addWeaponsObject:(Weapon*)value_;
- (void)removeWeaponsObject:(Weapon*)value_;

- (void)insertObject:(Weapon*)value inWeaponsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromWeaponsAtIndex:(NSUInteger)idx;
- (void)insertWeapons:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeWeaponsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInWeaponsAtIndex:(NSUInteger)idx withObject:(Weapon*)value;
- (void)replaceWeaponsAtIndexes:(NSIndexSet *)indexes withWeapons:(NSArray *)values;

@end

@interface _UnitOption (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveCode;
- (void)setPrimitiveCode:(NSString*)value;

- (NSNumber*)primitiveCost;
- (void)setPrimitiveCost:(NSNumber*)value;

- (int16_t)primitiveCostValue;
- (void)setPrimitiveCostValue:(int16_t)value_;

- (NSNumber*)primitiveId;
- (void)setPrimitiveId:(NSNumber*)value;

- (int32_t)primitiveIdValue;
- (void)setPrimitiveIdValue:(int32_t)value_;

- (NSNumber*)primitiveIndependentCost;
- (void)setPrimitiveIndependentCost:(NSNumber*)value;

- (int16_t)primitiveIndependentCostValue;
- (void)setPrimitiveIndependentCostValue:(int16_t)value_;

- (NSDecimalNumber*)primitiveIndependentSWC;
- (void)setPrimitiveIndependentSWC:(NSDecimalNumber*)value;

- (NSNumber*)primitiveIsIndependent;
- (void)setPrimitiveIsIndependent:(NSNumber*)value;

- (BOOL)primitiveIsIndependentValue;
- (void)setPrimitiveIsIndependentValue:(BOOL)value_;

- (NSString*)primitiveLegacyCode;
- (void)setPrimitiveLegacyCode:(NSString*)value;

- (NSDecimalNumber*)primitiveSwc;
- (void)setPrimitiveSwc:(NSDecimalNumber*)value;

- (DisguisedNote*)primitiveDisguisers;
- (void)setPrimitiveDisguisers:(DisguisedNote*)value;

- (NSMutableSet*)primitiveEnabledUnitProfile;
- (void)setPrimitiveEnabledUnitProfile:(NSMutableSet*)value;

- (NSMutableSet*)primitiveGroupMembers;
- (void)setPrimitiveGroupMembers:(NSMutableSet*)value;

- (NSMutableOrderedSet*)primitiveSpecialRules;
- (void)setPrimitiveSpecialRules:(NSMutableOrderedSet*)value;

- (NSMutableSet*)primitiveSpecops;
- (void)setPrimitiveSpecops:(NSMutableSet*)value;

- (NSMutableSet*)primitiveUnits;
- (void)setPrimitiveUnits:(NSMutableSet*)value;

- (NSMutableOrderedSet*)primitiveWeapons;
- (void)setPrimitiveWeapons:(NSMutableOrderedSet*)value;

@end
