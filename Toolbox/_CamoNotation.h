// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CamoNotation.h instead.

#import <CoreData/CoreData.h>
#import "Notation.h"

@interface CamoNotationID : NotationID {}
@end

@interface _CamoNotation : Notation {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CamoNotationID* objectID;

#if TARGET_OS_IPHONE

#endif

@end

@interface _CamoNotation (CoreDataGeneratedPrimitiveAccessors)

@end
