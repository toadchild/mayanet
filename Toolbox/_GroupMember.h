// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to GroupMember.h instead.

#import <CoreData/CoreData.h>

extern const struct GroupMemberAttributes {
	__unsafe_unretained NSString *isDead;
} GroupMemberAttributes;

extern const struct GroupMemberRelationships {
	__unsafe_unretained NSString *childrenGroupMembers;
	__unsafe_unretained NSString *combatGroup;
	__unsafe_unretained NSString *notes;
	__unsafe_unretained NSString *parentGroupMember;
	__unsafe_unretained NSString *specops;
	__unsafe_unretained NSString *unitOption;
	__unsafe_unretained NSString *unitProfile;
} GroupMemberRelationships;

@class GroupMember;
@class CombatGroup;
@class Note;
@class GroupMember;
@class SpecOpsBuild;
@class UnitOption;
@class UnitProfile;

@interface GroupMemberID : NSManagedObjectID {}
@end

@interface _GroupMember : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) GroupMemberID* objectID;

@property (nonatomic, strong) NSNumber* isDead;

@property (atomic) BOOL isDeadValue;
- (BOOL)isDeadValue;
- (void)setIsDeadValue:(BOOL)value_;

//- (BOOL)validateIsDead:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSOrderedSet *childrenGroupMembers;

- (NSMutableOrderedSet*)childrenGroupMembersSet;

@property (nonatomic, strong) CombatGroup *combatGroup;

//- (BOOL)validateCombatGroup:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *notes;

- (NSMutableSet*)notesSet;

@property (nonatomic, strong) GroupMember *parentGroupMember;

//- (BOOL)validateParentGroupMember:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) SpecOpsBuild *specops;

//- (BOOL)validateSpecops:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) UnitOption *unitOption;

//- (BOOL)validateUnitOption:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) UnitProfile *unitProfile;

//- (BOOL)validateUnitProfile:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newChildrenGroupMembersFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newNotesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _GroupMember (ChildrenGroupMembersCoreDataGeneratedAccessors)
- (void)addChildrenGroupMembers:(NSOrderedSet*)value_;
- (void)removeChildrenGroupMembers:(NSOrderedSet*)value_;
- (void)addChildrenGroupMembersObject:(GroupMember*)value_;
- (void)removeChildrenGroupMembersObject:(GroupMember*)value_;

- (void)insertObject:(GroupMember*)value inChildrenGroupMembersAtIndex:(NSUInteger)idx;
- (void)removeObjectFromChildrenGroupMembersAtIndex:(NSUInteger)idx;
- (void)insertChildrenGroupMembers:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeChildrenGroupMembersAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInChildrenGroupMembersAtIndex:(NSUInteger)idx withObject:(GroupMember*)value;
- (void)replaceChildrenGroupMembersAtIndexes:(NSIndexSet *)indexes withChildrenGroupMembers:(NSArray *)values;

@end

@interface _GroupMember (NotesCoreDataGeneratedAccessors)
- (void)addNotes:(NSSet*)value_;
- (void)removeNotes:(NSSet*)value_;
- (void)addNotesObject:(Note*)value_;
- (void)removeNotesObject:(Note*)value_;

@end

@interface _GroupMember (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveIsDead;
- (void)setPrimitiveIsDead:(NSNumber*)value;

- (BOOL)primitiveIsDeadValue;
- (void)setPrimitiveIsDeadValue:(BOOL)value_;

- (NSMutableOrderedSet*)primitiveChildrenGroupMembers;
- (void)setPrimitiveChildrenGroupMembers:(NSMutableOrderedSet*)value;

- (CombatGroup*)primitiveCombatGroup;
- (void)setPrimitiveCombatGroup:(CombatGroup*)value;

- (NSMutableSet*)primitiveNotes;
- (void)setPrimitiveNotes:(NSMutableSet*)value;

- (GroupMember*)primitiveParentGroupMember;
- (void)setPrimitiveParentGroupMember:(GroupMember*)value;

- (SpecOpsBuild*)primitiveSpecops;
- (void)setPrimitiveSpecops:(SpecOpsBuild*)value;

- (UnitOption*)primitiveUnitOption;
- (void)setPrimitiveUnitOption:(UnitOption*)value;

- (UnitProfile*)primitiveUnitProfile;
- (void)setPrimitiveUnitProfile:(UnitProfile*)value;

@end
