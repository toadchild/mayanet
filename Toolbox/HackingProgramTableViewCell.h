//
//  HackingProgramTableViewCell.h
//  Toolbox
//
//  Created by Jonathan Polley on 9/29/15.
//  Copyright (c) 2015 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HackingProgram.h"

@interface HackingProgramTableViewCell : UITableViewCell
@property HackingProgram *program;

@property (weak, nonatomic) IBOutlet UILabel *skill;
@property (weak, nonatomic) IBOutlet UILabel *range;
@property (weak, nonatomic) IBOutlet UILabel *target;
@property (weak, nonatomic) IBOutlet UIView *modifiers;
@property (weak, nonatomic) IBOutlet UILabel *attMod;
@property (weak, nonatomic) IBOutlet UILabel *oppMod;
@property (weak, nonatomic) IBOutlet UILabel *burst;
@property (weak, nonatomic) IBOutlet UILabel *damage;
@property (weak, nonatomic) IBOutlet UILabel *ammo;
@property (weak, nonatomic) IBOutlet UILabel *effect;
@property (weak, nonatomic) IBOutlet UILabel *special;
@property (weak, nonatomic) IBOutlet UILabel *programName;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *targetHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rangeTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *modifiersHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *effectsTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *specialHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *specialTopConstraint;

@end
