#import "MetachemistryNotation.h"


@interface MetachemistryNotation ()

// Private interface goes here.

@end


@implementation MetachemistryNotation

// Custom logic goes here.
-(NotationType) type
{
    if([self levelTwoValue])
    {
        return eMetachemistryLevel2Notation;
    }
    return eMetachemistryNotation;
}

@end
