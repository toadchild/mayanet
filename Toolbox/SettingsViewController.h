//
//  SettingsViewController.h
//  Toolbox
//
//  Created by Paul Clark on 5/1/14.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UISwitch *siSwitch;
-(IBAction)onSISwitchValueChange:(id)sender;
@end
