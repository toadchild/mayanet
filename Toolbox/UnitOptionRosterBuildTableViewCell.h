//
//  UnitOptionRosterBuildTableViewCell.h
//  Toolbox
//
//  Created by Paul on 11/1/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnitOptionRosterBuildTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *unitOptionCostLabel;
@property (strong, nonatomic) IBOutlet UILabel *unitOptionSWCLabel;
@property (strong, nonatomic) IBOutlet UILabel *unitNameLabel;
@property (strong, nonatomic) IBOutlet UIView  *costView;
@property (strong, nonatomic) IBOutlet UILabel *unitOptionCodeLabel;
@property (strong, nonatomic) IBOutlet UIImageView *unitIconImageView;
@property (strong, nonatomic) IBOutlet UILabel *unitAvaLabel;
@property (strong, nonatomic) IBOutlet UILabel *costSeperatorLabel;

@end
