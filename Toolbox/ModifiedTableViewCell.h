//
//  ModifiedTableViewCell.h
//  Toolbox
//
//  Created by Paul on 12/6/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupMember.h"
#import "Notation.h"
#import "Note.h"

@class GroupMember, Note, Notation;

@interface ModifiedTableViewCell : UITableViewCell
{
    Note* _note;
}

@property (strong, nonatomic) IBOutlet UILabel *modifiedLabel;
@property (strong, nonatomic) GroupMember* groupMember;
@property (strong, nonatomic) Notation* notation;
@property (strong, nonatomic) Note* note;
-(void)onModified;
@end
