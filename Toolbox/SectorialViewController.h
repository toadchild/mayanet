//
//  ArmyViewController.h
//  Toolbox
//
//  Created by Paul on 10/20/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "FactionViewController.h"

@class Sectorial;

@interface SectorialViewController : FactionViewController

@property (nonatomic, assign) Sectorial* sectorial;

-(void)refreshUnits;

@end
