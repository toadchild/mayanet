//
//  IntroViewController.m
//  Toolbox
//
//  Created by Paul Clark on 2/1/14.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "IntroViewController.h"
#import "UIColor+ColorPalette.h"

@interface IntroViewController ()

@end

@implementation IntroViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.webView.delegate = self;
    NSBundle *bundle = [NSBundle mainBundle];
    NSURL *htmlFile = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",bundle.bundlePath, @"intro.html"]];
    [self.webView loadRequest:[NSURLRequest requestWithURL:htmlFile]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarTintColor:[UIColor barColor]];
    [self.navigationController.navigationBar setTintColor:[UIColor barItemColor]];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor titleColor],
      NSForegroundColorAttributeName,nil]];
    
    [self.navigationController setToolbarHidden:TRUE animated:TRUE];
    [super viewWillAppear:animated];
}


-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if(navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSString* donationString = nil;
        NSLog(@"%@",[[request URL] absoluteString]);
        if([[[request URL] absoluteString] compare:@"mailto:paulryanclark@gmail.com?cc=cash@square.com&subject=$1&"] == NSOrderedSame)
        {
            donationString = @"$1";
        }
        else if([[[request URL] absoluteString] compare:@"mailto:paulryanclark@gmail.com?cc=cash@square.com&subject=$4&"] == NSOrderedSame)
        {
            donationString = @"$4";
        }
        else if([[[request URL] absoluteString] compare:@"mailto:paulryanclark@gmail.com?cc=cash@square.com&subject=$?&"] == NSOrderedSame)
        {
            donationString = @"$?";
        }
        
        if(donationString != nil)
        {
            if([MFMailComposeViewController canSendMail])
            {
                [self showDonationMailController:donationString];
                return FALSE;
            }

        }
        else
        {
            [[UIApplication sharedApplication] openURL:[request URL]];
            return FALSE;
        }
    }
    return TRUE;
}

-(void)showDonationMailController:(NSString*)donationString
{

    //Show Mail feedback form
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setToRecipients:@[@"paulryanclark@gmail.com"]];
    [controller setCcRecipients:@[@"cash@square.com"]];
    [controller setSubject:donationString];
    if (controller) [self presentViewController:controller animated:TRUE completion:^{}];

}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:TRUE completion:^{}];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
