#import "_UnitProfile.h"

@interface UnitProfile : _UnitProfile {}
// Return an identical copy of this profile
-(UnitProfile*) clone;
// Make ourselves into an identical copy of supplied profile
-(void) copyFrom:(UnitProfile*)source;

-(NSString*) weaponDescriptionWithSeperator:(NSString*)seperator;

-(NSString*)imageTitle;
-(NSString*)webImageTitle;

@end
