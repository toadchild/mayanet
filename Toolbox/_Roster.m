// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Roster.m instead.

#import "_Roster.h"

const struct RosterAttributes RosterAttributes = {
	.dateCreated = @"dateCreated",
	.isAlternate = @"isAlternate",
	.isCurrent = @"isCurrent",
	.isPlaying = @"isPlaying",
	.isSoldiersOfFortune = @"isSoldiersOfFortune",
	.name = @"name",
	.pointcap = @"pointcap",
};

const struct RosterRelationships RosterRelationships = {
	.army = @"army",
	.combatGroups = @"combatGroups",
	.sectorial = @"sectorial",
};

@implementation RosterID
@end

@implementation _Roster

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Roster" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Roster";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Roster" inManagedObjectContext:moc_];
}

- (RosterID*)objectID {
	return (RosterID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"isAlternateValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isAlternate"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isCurrentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isCurrent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isPlayingValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isPlaying"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isSoldiersOfFortuneValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isSoldiersOfFortune"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"pointcapValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"pointcap"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic dateCreated;

@dynamic isAlternate;

- (BOOL)isAlternateValue {
	NSNumber *result = [self isAlternate];
	return [result boolValue];
}

- (void)setIsAlternateValue:(BOOL)value_ {
	[self setIsAlternate:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsAlternateValue {
	NSNumber *result = [self primitiveIsAlternate];
	return [result boolValue];
}

- (void)setPrimitiveIsAlternateValue:(BOOL)value_ {
	[self setPrimitiveIsAlternate:[NSNumber numberWithBool:value_]];
}

@dynamic isCurrent;

- (BOOL)isCurrentValue {
	NSNumber *result = [self isCurrent];
	return [result boolValue];
}

- (void)setIsCurrentValue:(BOOL)value_ {
	[self setIsCurrent:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsCurrentValue {
	NSNumber *result = [self primitiveIsCurrent];
	return [result boolValue];
}

- (void)setPrimitiveIsCurrentValue:(BOOL)value_ {
	[self setPrimitiveIsCurrent:[NSNumber numberWithBool:value_]];
}

@dynamic isPlaying;

- (BOOL)isPlayingValue {
	NSNumber *result = [self isPlaying];
	return [result boolValue];
}

- (void)setIsPlayingValue:(BOOL)value_ {
	[self setIsPlaying:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsPlayingValue {
	NSNumber *result = [self primitiveIsPlaying];
	return [result boolValue];
}

- (void)setPrimitiveIsPlayingValue:(BOOL)value_ {
	[self setPrimitiveIsPlaying:[NSNumber numberWithBool:value_]];
}

@dynamic isSoldiersOfFortune;

- (BOOL)isSoldiersOfFortuneValue {
	NSNumber *result = [self isSoldiersOfFortune];
	return [result boolValue];
}

- (void)setIsSoldiersOfFortuneValue:(BOOL)value_ {
	[self setIsSoldiersOfFortune:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsSoldiersOfFortuneValue {
	NSNumber *result = [self primitiveIsSoldiersOfFortune];
	return [result boolValue];
}

- (void)setPrimitiveIsSoldiersOfFortuneValue:(BOOL)value_ {
	[self setPrimitiveIsSoldiersOfFortune:[NSNumber numberWithBool:value_]];
}

@dynamic name;

@dynamic pointcap;

- (int16_t)pointcapValue {
	NSNumber *result = [self pointcap];
	return [result shortValue];
}

- (void)setPointcapValue:(int16_t)value_ {
	[self setPointcap:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePointcapValue {
	NSNumber *result = [self primitivePointcap];
	return [result shortValue];
}

- (void)setPrimitivePointcapValue:(int16_t)value_ {
	[self setPrimitivePointcap:[NSNumber numberWithShort:value_]];
}

@dynamic army;

@dynamic combatGroups;

- (NSMutableOrderedSet*)combatGroupsSet {
	[self willAccessValueForKey:@"combatGroups"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"combatGroups"];

	[self didAccessValueForKey:@"combatGroups"];
	return result;
}

@dynamic sectorial;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newCombatGroupsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"CombatGroup" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"roster == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

@implementation _Roster (CombatGroupsCoreDataGeneratedAccessors)
- (void)addCombatGroups:(NSOrderedSet*)value_ {
	[self.combatGroupsSet unionOrderedSet:value_];
}
- (void)removeCombatGroups:(NSOrderedSet*)value_ {
	[self.combatGroupsSet minusOrderedSet:value_];
}
- (void)addCombatGroupsObject:(CombatGroup*)value_ {
	[self.combatGroupsSet addObject:value_];
}
- (void)removeCombatGroupsObject:(CombatGroup*)value_ {
	[self.combatGroupsSet removeObject:value_];
}
- (void)insertObject:(CombatGroup*)value inCombatGroupsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"combatGroups"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self combatGroups]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"combatGroups"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"combatGroups"];
}
- (void)removeObjectFromCombatGroupsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"combatGroups"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self combatGroups]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"combatGroups"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"combatGroups"];
}
- (void)insertCombatGroups:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"combatGroups"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self combatGroups]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"combatGroups"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"combatGroups"];
}
- (void)removeCombatGroupsAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"combatGroups"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self combatGroups]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"combatGroups"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"combatGroups"];
}
- (void)replaceObjectInCombatGroupsAtIndex:(NSUInteger)idx withObject:(CombatGroup*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"combatGroups"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self combatGroups]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"combatGroups"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"combatGroups"];
}
- (void)replaceCombatGroupsAtIndexes:(NSIndexSet *)indexes withCombatGroups:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"combatGroups"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self combatGroups]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"combatGroups"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"combatGroups"];
}
@end

