#import "_SpecialRule.h"
#import "AppDelegate.h"

@interface SpecialRule : _SpecialRule {}
+(SpecialRule*)specialRuleWithName:(NSString*)name;
+(SpecialRule*)specialRuleWithName:(NSString*)name andDelegate:(AppDelegate*)delegate;
+(NSString*) descriptionFromSpecialRulesArray:(NSArray*)specialRulesArray;
@end
