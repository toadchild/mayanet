//
//  PherowareTacticTableViewCell.h
//  Toolbox
//
//  Created by Jonathan Polley on 12/18/16.
//  Copyright (c) 2016 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PherowareTactic.h"

@interface PherowareTacticTableViewCell : UITableViewCell
@property PherowareTactic *tactic;

@property (weak, nonatomic) IBOutlet UILabel *skill;
@property (weak, nonatomic) IBOutlet UILabel *range;
@property (weak, nonatomic) IBOutlet UILabel *target;
@property (weak, nonatomic) IBOutlet UIView *modifiers;
@property (weak, nonatomic) IBOutlet UILabel *attMod;
@property (weak, nonatomic) IBOutlet UILabel *oppMod;
@property (weak, nonatomic) IBOutlet UILabel *burst;
@property (weak, nonatomic) IBOutlet UILabel *damage;
@property (weak, nonatomic) IBOutlet UILabel *ammo;
@property (weak, nonatomic) IBOutlet UILabel *effect;
@property (weak, nonatomic) IBOutlet UILabel *special;
@property (weak, nonatomic) IBOutlet UILabel *tacticName;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *targetHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rangeTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *modifiersHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *effectsTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *specialHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *specialTopConstraint;

@end
