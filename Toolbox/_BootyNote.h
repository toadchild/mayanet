// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BootyNote.h instead.

#import <CoreData/CoreData.h>
#import "Note.h"

extern const struct BootyNoteAttributes {
	__unsafe_unretained NSString *levelTwo;
} BootyNoteAttributes;

extern const struct BootyNoteRelationships {
	__unsafe_unretained NSString *booty;
} BootyNoteRelationships;

@class Booty;

@interface BootyNoteID : NoteID {}
@end

@interface _BootyNote : Note {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BootyNoteID* objectID;

@property (nonatomic, strong) NSNumber* levelTwo;

@property (atomic) BOOL levelTwoValue;
- (BOOL)levelTwoValue;
- (void)setLevelTwoValue:(BOOL)value_;

//- (BOOL)validateLevelTwo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Booty *booty;

//- (BOOL)validateBooty:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

#endif

@end

@interface _BootyNote (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveLevelTwo;
- (void)setPrimitiveLevelTwo:(NSNumber*)value;

- (BOOL)primitiveLevelTwoValue;
- (void)setPrimitiveLevelTwoValue:(BOOL)value_;

- (Booty*)primitiveBooty;
- (void)setPrimitiveBooty:(Booty*)value;

@end
