// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SpecOpsEquipment.h instead.

#import <CoreData/CoreData.h>

extern const struct SpecOpsEquipmentAttributes {
	__unsafe_unretained NSString *cost;
} SpecOpsEquipmentAttributes;

extern const struct SpecOpsEquipmentRelationships {
	__unsafe_unretained NSString *army;
	__unsafe_unretained NSString *specialRule;
	__unsafe_unretained NSString *specops;
} SpecOpsEquipmentRelationships;

@class Army;
@class SpecialRule;
@class SpecOpsBuild;

@interface SpecOpsEquipmentID : NSManagedObjectID {}
@end

@interface _SpecOpsEquipment : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SpecOpsEquipmentID* objectID;

@property (nonatomic, strong) NSNumber* cost;

@property (atomic) int16_t costValue;
- (int16_t)costValue;
- (void)setCostValue:(int16_t)value_;

//- (BOOL)validateCost:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Army *army;

//- (BOOL)validateArmy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) SpecialRule *specialRule;

//- (BOOL)validateSpecialRule:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *specops;

- (NSMutableSet*)specopsSet;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newSpecopsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _SpecOpsEquipment (SpecopsCoreDataGeneratedAccessors)
- (void)addSpecops:(NSSet*)value_;
- (void)removeSpecops:(NSSet*)value_;
- (void)addSpecopsObject:(SpecOpsBuild*)value_;
- (void)removeSpecopsObject:(SpecOpsBuild*)value_;

@end

@interface _SpecOpsEquipment (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCost;
- (void)setPrimitiveCost:(NSNumber*)value;

- (int16_t)primitiveCostValue;
- (void)setPrimitiveCostValue:(int16_t)value_;

- (Army*)primitiveArmy;
- (void)setPrimitiveArmy:(Army*)value;

- (SpecialRule*)primitiveSpecialRule;
- (void)setPrimitiveSpecialRule:(SpecialRule*)value;

- (NSMutableSet*)primitiveSpecops;
- (void)setPrimitiveSpecops:(NSMutableSet*)value;

@end
