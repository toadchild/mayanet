// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TOCamoNote.h instead.

#import <CoreData/CoreData.h>
#import "CamoNote.h"

@interface TOCamoNoteID : CamoNoteID {}
@end

@interface _TOCamoNote : CamoNote {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TOCamoNoteID* objectID;

#if TARGET_OS_IPHONE

#endif

@end

@interface _TOCamoNote (CoreDataGeneratedPrimitiveAccessors)

@end
