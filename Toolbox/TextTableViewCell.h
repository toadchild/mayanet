//
//  TextTableViewCell.h
//  Toolbox
//
//  Created by Jonathan Polley on 1/30/15.
//  Copyright (c) 2015 Paul. All rights reserved.
//

#import "ModifiedTableViewCell.h"

@interface TextTableViewCell : ModifiedTableViewCell <UITextViewDelegate>


@property (strong, nonatomic) IBOutlet UITextView *textView;

@end
