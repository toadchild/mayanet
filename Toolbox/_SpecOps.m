// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SpecOps.m instead.

#import "_SpecOps.h"

const struct SpecOpsRelationships SpecOpsRelationships = {
	.baseUnitOptions = @"baseUnitOptions",
	.type = @"type",
	.unit = @"unit",
};

@implementation SpecOpsID
@end

@implementation _SpecOps

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SpecOps" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SpecOps";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SpecOps" inManagedObjectContext:moc_];
}

- (SpecOpsID*)objectID {
	return (SpecOpsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic baseUnitOptions;

- (NSMutableOrderedSet*)baseUnitOptionsSet {
	[self willAccessValueForKey:@"baseUnitOptions"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"baseUnitOptions"];

	[self didAccessValueForKey:@"baseUnitOptions"];
	return result;
}

@dynamic type;

@dynamic unit;

- (NSMutableSet*)unitSet {
	[self willAccessValueForKey:@"unit"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"unit"];

	[self didAccessValueForKey:@"unit"];
	return result;
}

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newBaseUnitOptionsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsBaseUnitOption" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"specops == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newUnitFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Unit" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"specops == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

@implementation _SpecOps (BaseUnitOptionsCoreDataGeneratedAccessors)
- (void)addBaseUnitOptions:(NSOrderedSet*)value_ {
	[self.baseUnitOptionsSet unionOrderedSet:value_];
}
- (void)removeBaseUnitOptions:(NSOrderedSet*)value_ {
	[self.baseUnitOptionsSet minusOrderedSet:value_];
}
- (void)addBaseUnitOptionsObject:(SpecOpsBaseUnitOption*)value_ {
	[self.baseUnitOptionsSet addObject:value_];
}
- (void)removeBaseUnitOptionsObject:(SpecOpsBaseUnitOption*)value_ {
	[self.baseUnitOptionsSet removeObject:value_];
}
- (void)insertObject:(SpecOpsBaseUnitOption*)value inBaseUnitOptionsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"baseUnitOptions"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self baseUnitOptions]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"baseUnitOptions"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"baseUnitOptions"];
}
- (void)removeObjectFromBaseUnitOptionsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"baseUnitOptions"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self baseUnitOptions]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"baseUnitOptions"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"baseUnitOptions"];
}
- (void)insertBaseUnitOptions:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"baseUnitOptions"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self baseUnitOptions]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"baseUnitOptions"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"baseUnitOptions"];
}
- (void)removeBaseUnitOptionsAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"baseUnitOptions"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self baseUnitOptions]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"baseUnitOptions"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"baseUnitOptions"];
}
- (void)replaceObjectInBaseUnitOptionsAtIndex:(NSUInteger)idx withObject:(SpecOpsBaseUnitOption*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"baseUnitOptions"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self baseUnitOptions]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"baseUnitOptions"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"baseUnitOptions"];
}
- (void)replaceBaseUnitOptionsAtIndexes:(NSIndexSet *)indexes withBaseUnitOptions:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"baseUnitOptions"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self baseUnitOptions]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"baseUnitOptions"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"baseUnitOptions"];
}
@end

