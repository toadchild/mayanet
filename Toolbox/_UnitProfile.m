// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UnitProfile.m instead.

#import "_UnitProfile.h"

const struct UnitProfileAttributes UnitProfileAttributes = {
	.id = @"id",
	.independentCost = @"independentCost",
	.independentSWC = @"independentSWC",
	.isIndependent = @"isIndependent",
	.isc = @"isc",
	.name = @"name",
	.optionSpecific = @"optionSpecific",
	.strOverride = @"strOverride",
	.wOverride = @"wOverride",
};

const struct UnitProfileRelationships UnitProfileRelationships = {
	.aspects = @"aspects",
	.groupMembers = @"groupMembers",
	.specificOptions = @"specificOptions",
	.type = @"type",
	.unit = @"unit",
};

@implementation UnitProfileID
@end

@implementation _UnitProfile

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UnitProfile" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UnitProfile";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UnitProfile" inManagedObjectContext:moc_];
}

- (UnitProfileID*)objectID {
	return (UnitProfileID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"independentCostValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"independentCost"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isIndependentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isIndependent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"optionSpecificValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"optionSpecific"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"strOverrideValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"strOverride"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"wOverrideValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"wOverride"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic id;

- (int32_t)idValue {
	NSNumber *result = [self id];
	return [result intValue];
}

- (void)setIdValue:(int32_t)value_ {
	[self setId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdValue {
	NSNumber *result = [self primitiveId];
	return [result intValue];
}

- (void)setPrimitiveIdValue:(int32_t)value_ {
	[self setPrimitiveId:[NSNumber numberWithInt:value_]];
}

@dynamic independentCost;

- (int16_t)independentCostValue {
	NSNumber *result = [self independentCost];
	return [result shortValue];
}

- (void)setIndependentCostValue:(int16_t)value_ {
	[self setIndependentCost:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveIndependentCostValue {
	NSNumber *result = [self primitiveIndependentCost];
	return [result shortValue];
}

- (void)setPrimitiveIndependentCostValue:(int16_t)value_ {
	[self setPrimitiveIndependentCost:[NSNumber numberWithShort:value_]];
}

@dynamic independentSWC;

@dynamic isIndependent;

- (BOOL)isIndependentValue {
	NSNumber *result = [self isIndependent];
	return [result boolValue];
}

- (void)setIsIndependentValue:(BOOL)value_ {
	[self setIsIndependent:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsIndependentValue {
	NSNumber *result = [self primitiveIsIndependent];
	return [result boolValue];
}

- (void)setPrimitiveIsIndependentValue:(BOOL)value_ {
	[self setPrimitiveIsIndependent:[NSNumber numberWithBool:value_]];
}

@dynamic isc;

@dynamic name;

@dynamic optionSpecific;

- (BOOL)optionSpecificValue {
	NSNumber *result = [self optionSpecific];
	return [result boolValue];
}

- (void)setOptionSpecificValue:(BOOL)value_ {
	[self setOptionSpecific:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveOptionSpecificValue {
	NSNumber *result = [self primitiveOptionSpecific];
	return [result boolValue];
}

- (void)setPrimitiveOptionSpecificValue:(BOOL)value_ {
	[self setPrimitiveOptionSpecific:[NSNumber numberWithBool:value_]];
}

@dynamic strOverride;

- (BOOL)strOverrideValue {
	NSNumber *result = [self strOverride];
	return [result boolValue];
}

- (void)setStrOverrideValue:(BOOL)value_ {
	[self setStrOverride:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveStrOverrideValue {
	NSNumber *result = [self primitiveStrOverride];
	return [result boolValue];
}

- (void)setPrimitiveStrOverrideValue:(BOOL)value_ {
	[self setPrimitiveStrOverride:[NSNumber numberWithBool:value_]];
}

@dynamic wOverride;

- (BOOL)wOverrideValue {
	NSNumber *result = [self wOverride];
	return [result boolValue];
}

- (void)setWOverrideValue:(BOOL)value_ {
	[self setWOverride:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveWOverrideValue {
	NSNumber *result = [self primitiveWOverride];
	return [result boolValue];
}

- (void)setPrimitiveWOverrideValue:(BOOL)value_ {
	[self setPrimitiveWOverride:[NSNumber numberWithBool:value_]];
}

@dynamic aspects;

@dynamic groupMembers;

- (NSMutableSet*)groupMembersSet {
	[self willAccessValueForKey:@"groupMembers"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"groupMembers"];

	[self didAccessValueForKey:@"groupMembers"];
	return result;
}

@dynamic specificOptions;

- (NSMutableSet*)specificOptionsSet {
	[self willAccessValueForKey:@"specificOptions"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specificOptions"];

	[self didAccessValueForKey:@"specificOptions"];
	return result;
}

@dynamic type;

@dynamic unit;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newGroupMembersFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"GroupMember" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"unitProfile == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecificOptionsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"UnitOption" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"enabledUnitProfile CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

