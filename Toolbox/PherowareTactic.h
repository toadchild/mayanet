#import "_PherowareTactic.h"

@interface PherowareTactic : _PherowareTactic {}
+(PherowareTactic*) tacticWithName:(NSString*)name;
+(NSArray*) allTactics;
@end
