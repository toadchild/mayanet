#import "Sectorial.h"
#import "AppDelegate.h"
#import "Util.h"
#import "Unit.h"
#import "UIColor+HexString.h"

@interface Sectorial ()

// Private interface goes here.

@end


@implementation Sectorial

@synthesize cachedRosters;
@synthesize unitsByID;
@synthesize unitsByLegacyISC;

static NSMutableDictionary* sectorialsDictionary = nil;
+(void) initialize
{
    sectorialsDictionary = [[NSMutableDictionary alloc] init];
}

+(NSArray*)allWithDelegate:(AppDelegate*)delegate
{
    // Custom logic goes here.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [Sectorial entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError* error;
    NSArray* sectorials = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil)
    {
        return sectorials;
    }
    return nil;
}

+(Sectorial*)sectorialWithID:(int)id
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    return [Sectorial sectorialWithID:id withDelegate:delegate];
}

+(Sectorial*)sectorialWithID:(int)id withDelegate:(AppDelegate *)delegate
{
    NSString* idKey = [NSString stringWithFormat:@"%d", id];
    Sectorial* sectorial = [sectorialsDictionary valueForKey:idKey];
    if(sectorial)
    {
        return sectorial;
    }

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [Sectorial entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"id = %d", id ];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    NSArray* sectorials = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [sectorials count] == 1)
    {
        sectorial = [sectorials firstObject];
        [sectorialsDictionary setObject:sectorial forKey:idKey];
        return sectorial;
    }
    return nil;
}

+(Sectorial*)sectorialWithIDCreatingNew:(int)id withDelegate:(AppDelegate *)delegate
{
    Sectorial* sectorial = [self sectorialWithID:id];
    if(!sectorial)
    {
        NSString* idKey = [NSString stringWithFormat:@"%d", id];
        sectorial = [Sectorial insertInManagedObjectContext:[delegate managedObjectContext]];
        sectorial.idValue = id;
        [sectorialsDictionary setObject:sectorial forKey:idKey];
    }
    return sectorial;
}

-(void)initializeUnitsCache
{
    if (!unitsByID)
    {
        [self addObserver:self forKeyPath:@"units" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
        
        unitsByID = [[NSMutableDictionary alloc] init];
        unitsByLegacyISC = [[NSMutableDictionary alloc] init];

        for(Unit* unit in self.units)
        {
            // Unit might not yet have ID if we're upgrading
            if(unit.id)
            {
                unitsByID[[unit.id stringValue]] = unit;
            }
            if(unit.legacyIsc)
            {
                unitsByLegacyISC[unit.legacyIsc] = unit;
            }
        }
    }
}

-(void)awakeFromFetch
{
    [super awakeFromFetch];
    [self initializeUnitsCache];
}

-(void)awakeFromInsert
{
    [super awakeFromInsert];
    [self initializeUnitsCache];
}

-(void)observeValueForKeyPath:(NSString *)keyPath
                     ofObject:(id)object
                       change:(NSDictionary<NSString *, id> *)change
                      context:(void *)context
{
    if([change[NSKeyValueChangeKindKey] intValue] == NSKeyValueChangeRemoval)
    {
        for(Unit* unit in change[NSKeyValueChangeOldKey])
        {
            if(unit.id)
            {
                [unitsByID removeObjectForKey:[unit.id stringValue]];
            }
            if(unit.legacyIsc)
            {
                [unitsByLegacyISC removeObjectForKey:unit.legacyIsc];
            }
        }
    }
    if([change[NSKeyValueChangeKindKey] intValue] == NSKeyValueChangeInsertion)
    {
        for(Unit* unit in change[NSKeyValueChangeNewKey])
        {
            unitsByID[[unit.id stringValue]] = unit;
            if(unit.legacyIsc)
            {
                unitsByLegacyISC[unit.legacyIsc] = unit;
            }
        }
    }
}

-(Unit*)unitWithID:(int)id
{
    [self initializeUnitsCache];
    NSString* idKey = [NSString stringWithFormat:@"%d", id];
    return [unitsByID valueForKey:idKey];
}

-(Unit*)unitWithLegacyISC:(NSString*)isc
{
    [self initializeUnitsCache];
    return [unitsByLegacyISC valueForKey:isc];
}

-(UIColor*)backgroundColor
{
    return [UIColor colorWithHexString:self.rgbBackground];;
}

-(UIColor*)primaryColor
{
    return [UIColor colorWithHexString:self.rgbPrimary];;
}

-(UIColor*)secondaryColor
{
    return [UIColor colorWithHexString:self.rgbSecondary];;
}

-(NSString*)imageTitle
{
    return [NSString stringWithFormat:@"images/%@_logo.png", [self id]];
}

-(NSString*)webImageTitle
{
    return [NSString stringWithFormat:@"%@%@%@", URL_ROOT, [self id], WEB_IMAGE_SUFFIX];
}

@end
