//
//  DataLoadViewController.m
//  Toolbox
//
//  Created by Jonathan Polley on 8/11/16.
//  Copyright © 2016 Paul. All rights reserved.
//

#import "DataLoadViewController.h"
#import "DataParser.h"

@interface DataLoadViewController ()

@end

@implementation DataLoadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated
{
    // Load the unit data.
    [self performSelectorInBackground:@selector(loadData:) withObject:(AppDelegate*)[[UIApplication sharedApplication] delegate]];
    [super viewDidAppear:animated];
}

-(void)loadData:(AppDelegate*)delegate
{
    DataParser* parser = [[DataParser alloc] initWithAppDelegate:delegate andProgressLabel:self.progressMessageLabel];
    [parser parse];
    [parser migrate];
    // Execute the UI update event on the main thread.
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"DataLoaded" sender:self];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
