// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MetachemistryNote.m instead.

#import "_MetachemistryNote.h"

const struct MetachemistryNoteAttributes MetachemistryNoteAttributes = {
	.levelTwo = @"levelTwo",
};

const struct MetachemistryNoteRelationships MetachemistryNoteRelationships = {
	.metachemistry = @"metachemistry",
};

@implementation MetachemistryNoteID
@end

@implementation _MetachemistryNote

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MetachemistryNote" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MetachemistryNote";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MetachemistryNote" inManagedObjectContext:moc_];
}

- (MetachemistryNoteID*)objectID {
	return (MetachemistryNoteID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"levelTwoValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"levelTwo"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic levelTwo;

- (BOOL)levelTwoValue {
	NSNumber *result = [self levelTwo];
	return [result boolValue];
}

- (void)setLevelTwoValue:(BOOL)value_ {
	[self setLevelTwo:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveLevelTwoValue {
	NSNumber *result = [self primitiveLevelTwo];
	return [result boolValue];
}

- (void)setPrimitiveLevelTwoValue:(BOOL)value_ {
	[self setPrimitiveLevelTwo:[NSNumber numberWithBool:value_]];
}

@dynamic metachemistry;

#if TARGET_OS_IPHONE

#endif

@end

