// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Migration.m instead.

#import "_Migration.h"

const struct MigrationAttributes MigrationAttributes = {
	.name = @"name",
};

@implementation MigrationID
@end

@implementation _Migration

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Migration" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Migration";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Migration" inManagedObjectContext:moc_];
}

- (MigrationID*)objectID {
	return (MigrationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic name;

#if TARGET_OS_IPHONE

#endif

@end

