//
//  WikiWebViewController.m
//  Toolbox
//
//  Created by Paul on 11/13/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "WikiWebViewController.h"
#import "AppDelegate.h"
#import "WikiEntry.h"
#import "UIColor+ColorPalette.h"
@interface WikiWebViewController ()

@end

@implementation WikiWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        self.wikiEntriesList = [[NSMutableArray alloc] init];
        entryIndex = NSNotFound;
        completionBlock = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    [super viewDidLoad];

    hideOnce = FALSE;
	// Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self loadWebView];
    self.backNavButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"765-arrow-left.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onBack:)];
    
    self.actionNavButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(onAction:)];
    
    
    UIBarButtonItem* space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    space.width = 15;
    self.forwardNavButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"766-arrow-right.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onForward:)];
    
    NSArray *actionButtonItems = [[NSArray alloc] initWithObjects: self.actionNavButton, self.forwardNavButton, self.backNavButton, nil];

    
    [self.navigationItem setRightBarButtonItems: actionButtonItems animated:TRUE];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor activeBarColor]];
    [self.navigationController.navigationBar setTintColor:[UIColor activeBarItemColor]];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor activeTitleColor],
      NSForegroundColorAttributeName,nil]];
    
    [super viewWillAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated
{
    hideOnce = FALSE;
    [super viewDidDisappear:animated];
}

-(void)loadWebView
{
    NSBundle *bundle = [NSBundle mainBundle];
    WikiEntry* entry = [self.wikiEntriesList objectAtIndex:entryIndex];
    NSURL *htmlFile = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/wiki_en/%@",bundle.bundlePath, entry.url]];
    [self.webView loadRequest:[NSURLRequest requestWithURL:htmlFile]];
}
-(void)setWikiEntry:(WikiEntry*) wikiEntry webViewLoadCompletion:(WebViewLoadCompletionBlock)completion;
{
    //If the list is empty, or if the current entry is the last in the list
    if(entryIndex == NSNotFound || entryIndex == [self.wikiEntriesList count]-1)
    {
        //Add to the end of the list
        [self.wikiEntriesList addObject: wikiEntry];
        entryIndex = [self.wikiEntriesList count]-1;
        NSLog(@"[User Added Entry to end of history] index: %lu  count: %lu", (unsigned long)entryIndex, (unsigned long)[self.wikiEntriesList count]);
    }
    else
    {
        NSRange range = NSMakeRange(0, entryIndex+1);
        NSLog(@"[User Added Entry to middle of history] BEFORE index: %lu  count: %lu", (unsigned long)entryIndex, (unsigned long)[self.wikiEntriesList count]);
        
        //Keep all elements upto index, and then add to end;
        self.wikiEntriesList = [NSMutableArray arrayWithArray:[self.wikiEntriesList objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:range]]];
        [self.wikiEntriesList addObject: wikiEntry];
        entryIndex++;
        
        NSLog(@"[User Added Entry to middle of history] AFTER index: %lu  count: %lu", (unsigned long)entryIndex, (unsigned long)[self.wikiEntriesList count]);
        
    }
    completionBlock = completion;
    if(self.webView)
        [self loadWebView];
    else
    {
        completionBlock();
        completionBlock = nil;
    }
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if(navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSLog(@"%@",[[request URL] absoluteString]);
        if([[[request URL] absoluteString] compare:@"http://wiki.infinitythegame.com/"] == NSOrderedSame)
        {
            [[UIApplication sharedApplication] openURL:[request URL]];
            return FALSE;
        }
        NSString* file = [[request URL] lastPathComponent];
        
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        // Edit the entity name as appropriate.
        NSEntityDescription *entity = [WikiEntry entityInManagedObjectContext:appDelegate.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        [fetchRequest setFetchLimit:1];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"url = %@", file];
        [fetchRequest setPredicate:predicate];
        
        
        NSError* error;
        NSArray* entry = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        if(error == nil && [entry count] == 1)
        {
            NSRange range = NSMakeRange(0, entryIndex+1);
            NSLog(@"[Click Added Entry to middle of history] BEFORE index: %lu  count: %lu", (unsigned long)entryIndex, (unsigned long)[self.wikiEntriesList count]);
            
            //Keep all elements upto index, and then add to end;
            self.wikiEntriesList = [NSMutableArray arrayWithArray:[self.wikiEntriesList objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:range]]];
            [self.wikiEntriesList addObject: [entry firstObject]];
            entryIndex++;
            
            NSLog(@"[Click Added Entry to middle of history] AFTER index: %lu  count: %lu", (unsigned long)entryIndex, (unsigned long)[self.wikiEntriesList count]);

        }
    }
    return TRUE;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    CGSize webContentSize = self.webView.scrollView.contentSize;
    if(!hideOnce && webContentSize.width > self.webView.frame.size.width)
    {
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [delegate.containerViewController displayRightViewController:FALSE animationCompletionBlock:^(BOOL view){
            [self.webView setNeedsLayout];
            self->hideOnce = TRUE;
        }];

        

    }
    WikiEntry* entry = [self.wikiEntriesList objectAtIndex:entryIndex];
    [self setTitle: entry.name];
    [self.backNavButton setEnabled:webView.canGoBack];
    [self.forwardNavButton setEnabled:webView.canGoForward];
    if(completionBlock)
    {
        completionBlock();
        completionBlock = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onBack:(id)sender
{
    if(entryIndex > 0)
    {
        entryIndex--;
        NSLog(@"[User navigated back in history] index: %lu  count: %lu", (unsigned long)entryIndex, (unsigned long)[self.wikiEntriesList count]);
    }
    [self.webView goBack];
}
-(IBAction)onForward:(id)sender
{
    if(entryIndex < [self.wikiEntriesList count]-1)
    {
        entryIndex++;
        NSLog(@"[User navigated forward in history] index: %lu  count: %lu", (unsigned long)entryIndex, (unsigned long)[self.wikiEntriesList count]);
    }
    [self.webView goForward];
}

-(IBAction)onAction:(id)sender
{
    if(browseActionSheet) return;
    
    browseActionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Do you want to open in an external browser?"] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Open" otherButtonTitles: nil];
    [browseActionSheet showFromBarButtonItem:sender animated:TRUE];
}
  
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet == browseActionSheet && buttonIndex == 0)
    {
        WikiEntry* entry = [self.wikiEntriesList objectAtIndex:entryIndex];
        
        NSString* pageNameWithEscapedSpaces = [entry.pageName stringByReplacingOccurrencesOfString:@" " withString:@"_"];
        
        NSString* pageNameURLString = [pageNameWithEscapedSpaces stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString* urlString = [NSString stringWithFormat:@"%@/%@",@"http://wiki.infinitythegame.com/en", pageNameURLString];
        
        NSURL* url = [NSURL URLWithString:urlString];
        [[UIApplication sharedApplication] openURL:url];
        browseActionSheet = nil;
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    browseActionSheet = nil;
}

@end
