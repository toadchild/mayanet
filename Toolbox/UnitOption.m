#import "UnitOption.h"
#import "SpecialRule.h"
#import "Army.h"
#import "Unit.h"
#import "UnitAspects.h"
#import "Weapon.h"
#import "AppDelegate.h"

@interface UnitOption ()

// Private interface goes here.

@end


@implementation UnitOption

-(BOOL) isLt
{
    for(SpecialRule* specialRule in self.specialRules)
    {
        if([specialRule.name isEqualToString: @"Lieutenant"] || [specialRule.name isEqualToString: @"Lieutenant L2"])
        {
            return TRUE;
        }
    }
    return FALSE;
}

-(Unit*) getUnit
{
    for(Unit* unit in self.units)
    {
        return unit;
    }
    return nil;
}

-(NSString*) weaponDescription:(Unit*)unit
{
    return [self weaponDescription:unit seperator:@", "];
}
-(NSString*) weaponDescription:(Unit*)unit seperator:(NSString*)seperator
{
    NSArray* unitOptionWeapons = [self.weapons array];
    NSArray* unitWeapons = [unit.aspects.weapons array];
    NSArray* theWeapons = [unitOptionWeapons arrayByAddingObjectsFromArray:unitWeapons];
    
    return [Weapon descriptionFromWeaponsArray:theWeapons seperator:seperator];
}

-(NSString*) specialRulesDescription
{
    NSArray* unitOptionSpecialRules = [self.specialRules array];
    
    return [SpecialRule descriptionFromSpecialRulesArray:unitOptionSpecialRules];
}

-(UnitOption*) clone
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    UnitOption* cloneOption = [UnitOption insertInManagedObjectContext:delegate.managedObjectContext];
    [cloneOption copyFrom:self];
    
    return cloneOption;
}

-(void) copyFrom:(UnitOption *)source
{
    self.code = source.code;
    self.legacyCode = source.legacyCode;
    self.cost = source.cost;
    self.id = source.id;
    self.independentCost = source.independentCost;
    self.independentSWC = source.independentSWC;
    self.isIndependent = source.isIndependent;
    self.swc = source.swc;
    
    // I think it's ok-ish to do shallow copies on these.
    self.specialRules = source.specialRules;
    self.weapons = source.weapons;
}

@end
