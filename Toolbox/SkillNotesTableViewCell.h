//
//  SkillNotesTableViewCell.h
//  Toolbox
//
//  Created by Jonathan Polley on 12/23/18.
//  Copyright © 2018 Jonathan Polley. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SkillNotesTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *note;

@end
