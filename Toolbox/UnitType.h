#import "_UnitType.h"

@interface UnitType : _UnitType {}
// Custom logic goes here.

-(BOOL) isStructure;
+(UnitType*) unitTypeWithCode:(NSString*)code;
@end
