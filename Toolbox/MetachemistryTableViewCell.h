//
//  MetachemistryTableViewCell.h
//  Toolbox
//
//  Created by Jonathan Polley on 1/29/15.
//  Copyright (c) 2015 Paul. All rights reserved.
//

#import "ModifiedTableViewCell.h"

@interface MetachemistryTableViewCell : ModifiedTableViewCell

- (IBAction)onMetachemistryL1Roll:(id)sender;
- (IBAction)onMetachemistryL2Roll:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *metachemistryL1Button;
@property (strong, nonatomic) IBOutlet UIButton *metachemistryL2Button;
@property (strong, nonatomic) IBOutlet UILabel *metachemistryOutcome;

@end
