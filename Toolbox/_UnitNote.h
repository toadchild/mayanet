// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UnitNote.h instead.

#import <CoreData/CoreData.h>

extern const struct UnitNoteAttributes {
	__unsafe_unretained NSString *note;
	__unsafe_unretained NSString *title;
} UnitNoteAttributes;

extern const struct UnitNoteRelationships {
	__unsafe_unretained NSString *unit;
} UnitNoteRelationships;

@class Unit;

@interface UnitNoteID : NSManagedObjectID {}
@end

@interface _UnitNote : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UnitNoteID* objectID;

@property (nonatomic, strong) NSString* note;

//- (BOOL)validateNote:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* title;

//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Unit *unit;

//- (BOOL)validateUnit:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

#endif

@end

@interface _UnitNote (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveNote;
- (void)setPrimitiveNote:(NSString*)value;

- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;

- (Unit*)primitiveUnit;
- (void)setPrimitiveUnit:(Unit*)value;

@end
