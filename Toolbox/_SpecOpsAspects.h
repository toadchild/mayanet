// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SpecOpsAspects.h instead.

#import <CoreData/CoreData.h>

extern const struct SpecOpsAspectsAttributes {
	__unsafe_unretained NSString *armIncrease;
	__unsafe_unretained NSString *bsIncrease;
	__unsafe_unretained NSString *btsIncrease;
	__unsafe_unretained NSString *ccIncrease;
	__unsafe_unretained NSString *phIncrease;
	__unsafe_unretained NSString *wIncrease;
	__unsafe_unretained NSString *wipIncrease;
} SpecOpsAspectsAttributes;

extern const struct SpecOpsAspectsRelationships {
	__unsafe_unretained NSString *specops;
} SpecOpsAspectsRelationships;

@class SpecOpsBuild;

@interface SpecOpsAspectsID : NSManagedObjectID {}
@end

@interface _SpecOpsAspects : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SpecOpsAspectsID* objectID;

@property (nonatomic, strong) NSNumber* armIncrease;

@property (atomic) int16_t armIncreaseValue;
- (int16_t)armIncreaseValue;
- (void)setArmIncreaseValue:(int16_t)value_;

//- (BOOL)validateArmIncrease:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* bsIncrease;

@property (atomic) int16_t bsIncreaseValue;
- (int16_t)bsIncreaseValue;
- (void)setBsIncreaseValue:(int16_t)value_;

//- (BOOL)validateBsIncrease:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* btsIncrease;

@property (atomic) int16_t btsIncreaseValue;
- (int16_t)btsIncreaseValue;
- (void)setBtsIncreaseValue:(int16_t)value_;

//- (BOOL)validateBtsIncrease:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* ccIncrease;

@property (atomic) int16_t ccIncreaseValue;
- (int16_t)ccIncreaseValue;
- (void)setCcIncreaseValue:(int16_t)value_;

//- (BOOL)validateCcIncrease:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* phIncrease;

@property (atomic) int16_t phIncreaseValue;
- (int16_t)phIncreaseValue;
- (void)setPhIncreaseValue:(int16_t)value_;

//- (BOOL)validatePhIncrease:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* wIncrease;

@property (atomic) int16_t wIncreaseValue;
- (int16_t)wIncreaseValue;
- (void)setWIncreaseValue:(int16_t)value_;

//- (BOOL)validateWIncrease:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* wipIncrease;

@property (atomic) int16_t wipIncreaseValue;
- (int16_t)wipIncreaseValue;
- (void)setWipIncreaseValue:(int16_t)value_;

//- (BOOL)validateWipIncrease:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) SpecOpsBuild *specops;

//- (BOOL)validateSpecops:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

#endif

@end

@interface _SpecOpsAspects (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveArmIncrease;
- (void)setPrimitiveArmIncrease:(NSNumber*)value;

- (int16_t)primitiveArmIncreaseValue;
- (void)setPrimitiveArmIncreaseValue:(int16_t)value_;

- (NSNumber*)primitiveBsIncrease;
- (void)setPrimitiveBsIncrease:(NSNumber*)value;

- (int16_t)primitiveBsIncreaseValue;
- (void)setPrimitiveBsIncreaseValue:(int16_t)value_;

- (NSNumber*)primitiveBtsIncrease;
- (void)setPrimitiveBtsIncrease:(NSNumber*)value;

- (int16_t)primitiveBtsIncreaseValue;
- (void)setPrimitiveBtsIncreaseValue:(int16_t)value_;

- (NSNumber*)primitiveCcIncrease;
- (void)setPrimitiveCcIncrease:(NSNumber*)value;

- (int16_t)primitiveCcIncreaseValue;
- (void)setPrimitiveCcIncreaseValue:(int16_t)value_;

- (NSNumber*)primitivePhIncrease;
- (void)setPrimitivePhIncrease:(NSNumber*)value;

- (int16_t)primitivePhIncreaseValue;
- (void)setPrimitivePhIncreaseValue:(int16_t)value_;

- (NSNumber*)primitiveWIncrease;
- (void)setPrimitiveWIncrease:(NSNumber*)value;

- (int16_t)primitiveWIncreaseValue;
- (void)setPrimitiveWIncreaseValue:(int16_t)value_;

- (NSNumber*)primitiveWipIncrease;
- (void)setPrimitiveWipIncrease:(NSNumber*)value;

- (int16_t)primitiveWipIncreaseValue;
- (void)setPrimitiveWipIncreaseValue:(int16_t)value_;

- (SpecOpsBuild*)primitiveSpecops;
- (void)setPrimitiveSpecops:(SpecOpsBuild*)value;

@end
