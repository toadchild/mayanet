#import "_SpecOpsWeapon.h"

@interface SpecOpsWeapon : _SpecOpsWeapon {}
// Custom logic goes here.
+(SpecOpsWeapon*) specOpsWeaponWithName:(NSString*)name;
@end
