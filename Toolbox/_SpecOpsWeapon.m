// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SpecOpsWeapon.m instead.

#import "_SpecOpsWeapon.h"

const struct SpecOpsWeaponAttributes SpecOpsWeaponAttributes = {
	.cost = @"cost",
};

const struct SpecOpsWeaponRelationships SpecOpsWeaponRelationships = {
	.army = @"army",
	.specops = @"specops",
	.weapon = @"weapon",
};

@implementation SpecOpsWeaponID
@end

@implementation _SpecOpsWeapon

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SpecOpsWeapon" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SpecOpsWeapon";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SpecOpsWeapon" inManagedObjectContext:moc_];
}

- (SpecOpsWeaponID*)objectID {
	return (SpecOpsWeaponID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"costValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cost"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic cost;

- (int16_t)costValue {
	NSNumber *result = [self cost];
	return [result shortValue];
}

- (void)setCostValue:(int16_t)value_ {
	[self setCost:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCostValue {
	NSNumber *result = [self primitiveCost];
	return [result shortValue];
}

- (void)setPrimitiveCostValue:(int16_t)value_ {
	[self setPrimitiveCost:[NSNumber numberWithShort:value_]];
}

@dynamic army;

- (NSMutableSet*)armySet {
	[self willAccessValueForKey:@"army"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"army"];

	[self didAccessValueForKey:@"army"];
	return result;
}

@dynamic specops;

- (NSMutableSet*)specopsSet {
	[self willAccessValueForKey:@"specops"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specops"];

	[self didAccessValueForKey:@"specops"];
	return result;
}

@dynamic weapon;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newArmyFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Army" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"specopWeapons CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecopsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsBuild" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"weapons CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

