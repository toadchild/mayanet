// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to WikiEntry.h instead.

#import <CoreData/CoreData.h>

extern const struct WikiEntryAttributes {
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *nameFirstLetter;
	__unsafe_unretained NSString *pageName;
	__unsafe_unretained NSString *url;
} WikiEntryAttributes;

extern const struct WikiEntryRelationships {
	__unsafe_unretained NSString *hackingPrograms;
	__unsafe_unretained NSString *pherowareTactics;
	__unsafe_unretained NSString *specialRules;
	__unsafe_unretained NSString *weapons;
	__unsafe_unretained NSString *wiki;
} WikiEntryRelationships;

@class HackingProgram;
@class PherowareTactic;
@class SpecialRule;
@class Weapon;
@class Wiki;

@interface WikiEntryID : NSManagedObjectID {}
@end

@interface _WikiEntry : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) WikiEntryID* objectID;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* nameFirstLetter;

//- (BOOL)validateNameFirstLetter:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* pageName;

//- (BOOL)validatePageName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* url;

//- (BOOL)validateUrl:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *hackingPrograms;

- (NSMutableSet*)hackingProgramsSet;

@property (nonatomic, strong) NSSet *pherowareTactics;

- (NSMutableSet*)pherowareTacticsSet;

@property (nonatomic, strong) NSSet *specialRules;

- (NSMutableSet*)specialRulesSet;

@property (nonatomic, strong) NSSet *weapons;

- (NSMutableSet*)weaponsSet;

@property (nonatomic, strong) Wiki *wiki;

//- (BOOL)validateWiki:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newHackingProgramsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newPherowareTacticsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecialRulesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newWeaponsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _WikiEntry (HackingProgramsCoreDataGeneratedAccessors)
- (void)addHackingPrograms:(NSSet*)value_;
- (void)removeHackingPrograms:(NSSet*)value_;
- (void)addHackingProgramsObject:(HackingProgram*)value_;
- (void)removeHackingProgramsObject:(HackingProgram*)value_;

@end

@interface _WikiEntry (PherowareTacticsCoreDataGeneratedAccessors)
- (void)addPherowareTactics:(NSSet*)value_;
- (void)removePherowareTactics:(NSSet*)value_;
- (void)addPherowareTacticsObject:(PherowareTactic*)value_;
- (void)removePherowareTacticsObject:(PherowareTactic*)value_;

@end

@interface _WikiEntry (SpecialRulesCoreDataGeneratedAccessors)
- (void)addSpecialRules:(NSSet*)value_;
- (void)removeSpecialRules:(NSSet*)value_;
- (void)addSpecialRulesObject:(SpecialRule*)value_;
- (void)removeSpecialRulesObject:(SpecialRule*)value_;

@end

@interface _WikiEntry (WeaponsCoreDataGeneratedAccessors)
- (void)addWeapons:(NSSet*)value_;
- (void)removeWeapons:(NSSet*)value_;
- (void)addWeaponsObject:(Weapon*)value_;
- (void)removeWeaponsObject:(Weapon*)value_;

@end

@interface _WikiEntry (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveNameFirstLetter;
- (void)setPrimitiveNameFirstLetter:(NSString*)value;

- (NSString*)primitivePageName;
- (void)setPrimitivePageName:(NSString*)value;

- (NSString*)primitiveUrl;
- (void)setPrimitiveUrl:(NSString*)value;

- (NSMutableSet*)primitiveHackingPrograms;
- (void)setPrimitiveHackingPrograms:(NSMutableSet*)value;

- (NSMutableSet*)primitivePherowareTactics;
- (void)setPrimitivePherowareTactics:(NSMutableSet*)value;

- (NSMutableSet*)primitiveSpecialRules;
- (void)setPrimitiveSpecialRules:(NSMutableSet*)value;

- (NSMutableSet*)primitiveWeapons;
- (void)setPrimitiveWeapons:(NSMutableSet*)value;

- (Wiki*)primitiveWiki;
- (void)setPrimitiveWiki:(Wiki*)value;

@end
