#import "Army.h"
#import "AppDelegate.h"
#import "Util.h"
#import "Unit.h"
#import "UIColor+HexString.h"

@interface Army ()

// Private interface goes here.

@end


@implementation Army

@synthesize cachedRosters;
@synthesize unitsByID;
@synthesize unitsByLegacyISC;

static NSMutableDictionary* armiesDictionary = nil;
+(void) initialize
{
    armiesDictionary = [[NSMutableDictionary alloc] init];
}

+(NSArray*)allWithDelegate:(AppDelegate *)delegate
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [Army entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
 
    NSError* error;
    NSArray* armies = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil)
    {
        return armies;
    }
    return nil;
}

+(Army*)armyWithID:(int)id
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    return [Army armyWithID:id withDelegate:delegate];
}

+(Army*)armyWithID:(int)id withDelegate:(AppDelegate *)delegate
{
    NSString* idKey = [NSString stringWithFormat:@"%d", id];
    Army* army = [armiesDictionary valueForKey:idKey];
    if(army)
    {
        return army;
    }
    
    // Custom logic goes here.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [Army entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"id = %d", id];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    NSArray* armies = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [armies count] == 1)
    {
        army = [armies firstObject];
        [armiesDictionary setObject:army forKey:idKey];
        return army;
    }
    return nil;
}

+(Army*)armyWithIDCreatingNew:(int)id withDelegate:(AppDelegate *)delegate
{
    Army* army = [self armyWithID:id];
    if(!army)
    {
        NSString* idKey = [NSString stringWithFormat:@"%d", id];
        army = [Army insertInManagedObjectContext:[delegate managedObjectContext]];
        army.idValue = id;
        [armiesDictionary setObject:army forKey:idKey];
    }
    return army;
}

-(void)initializeUnitsCache
{
    if (!unitsByID)
    {
        [self addObserver:self forKeyPath:@"units" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
        
        unitsByID = [[NSMutableDictionary alloc] init];
        unitsByLegacyISC = [[NSMutableDictionary alloc] init];

        for(Unit* unit in self.units)
        {
            // Unit might not yet have ID if we're upgrading
            if(unit.id)
            {
                unitsByID[[unit.id stringValue]] = unit;
            }
            if(unit.legacyIsc)
            {
                unitsByLegacyISC[unit.legacyIsc] = unit;
            }
        }
    }
}

-(void)awakeFromFetch
{
    [super awakeFromFetch];
    [self initializeUnitsCache];
}

-(void)awakeFromInsert
{
    [super awakeFromInsert];
    [self initializeUnitsCache];
}

-(void)observeValueForKeyPath:(NSString *)keyPath
                     ofObject:(id)object
                       change:(NSDictionary<NSString *, id> *)change
                      context:(void *)context
{
    if([change[NSKeyValueChangeKindKey] intValue] == NSKeyValueChangeRemoval)
    {
        for(Unit* unit in change[NSKeyValueChangeOldKey])
        {
            if(unit.id)
            {
                [unitsByID removeObjectForKey:[unit.id stringValue]];
            }
            if(unit.legacyIsc)
            {
                [unitsByLegacyISC removeObjectForKey:unit.legacyIsc];
            }
        }
    }
    if([change[NSKeyValueChangeKindKey] intValue] == NSKeyValueChangeInsertion)
    {
        for(Unit* unit in change[NSKeyValueChangeNewKey])
        {
            unitsByID[[unit.id stringValue]] = unit;
            if(unit.legacyIsc)
            {
                unitsByLegacyISC[unit.legacyIsc] = unit;
            }
        }
    }
}

-(Unit*)unitWithID:(int)id
{
    [self initializeUnitsCache];
    NSString* idKey = [NSString stringWithFormat:@"%d", id];
    return [unitsByID valueForKey:idKey];
}

-(Unit*)unitWithLegacyISC:(NSString*)isc
{
    [self initializeUnitsCache];
    return [unitsByLegacyISC valueForKey:isc];
}

-(UIColor*)backgroundColor
{
    return [UIColor colorWithHexString:self.rgbBackground];;
}

-(UIColor*)primaryColor
{
    return [UIColor colorWithHexString:self.rgbPrimary];;
}

-(UIColor*)secondaryColor
{
    return [UIColor colorWithHexString:self.rgbSecondary];;
}

-(NSString*)imageTitle
{
    return [NSString stringWithFormat:@"images/%@_logo.png", [self id]];
}

-(NSString*)webImageTitle
{
    return [NSString stringWithFormat:@"%@%@%@", URL_ROOT, [self id], WEB_IMAGE_SUFFIX];
}

@end
