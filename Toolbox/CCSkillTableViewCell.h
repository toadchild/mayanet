//
//  CCSkillTableViewCell.h
//  Toolbox
//
//  Created by Jonathan Polley on 9/29/15.
//  Copyright (c) 2015 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CCSkill.h"

@interface CCSkillTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *attMod;
@property (strong, nonatomic) IBOutlet UILabel *oppMod;
@property (strong, nonatomic) IBOutlet UILabel *damageMod;
@property (strong, nonatomic) IBOutlet UILabel *burstMod;
@property (strong, nonatomic) IBOutlet UILabel *special;

+(int) cellHeightForCCSkill:(CCSkill*) skill;

@end
