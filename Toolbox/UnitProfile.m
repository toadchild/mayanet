#import "UnitProfile.h"
#import "AppDelegate.h"
#import "UnitAspects.h"
#import "Weapon.h"
#import "Unit.h"
#import "Util.h"

@interface UnitProfile ()

// Private interface goes here.

@end


@implementation UnitProfile

-(UnitProfile*) clone
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    UnitProfile* cloneProfile = [UnitProfile insertInManagedObjectContext:delegate.managedObjectContext];
    cloneProfile.aspects = [UnitAspects insertInManagedObjectContext:[delegate managedObjectContext]];
    [cloneProfile copyFrom:self];
    
    return cloneProfile;
}

-(void) copyFrom:(UnitProfile *)source
{
    self.idValue = source.idValue;
    self.type = source.type;
    self.strOverride = source.strOverride;
    self.wOverride = source.wOverride;
    self.name = source.name;
    self.isc = source.isc;
    self.independentCost = source.independentCost;
    self.independentSWC = source.independentSWC;
    self.isIndependent = source.isIndependent;
    self.optionSpecific = source.optionSpecific;
    [self.aspects copyFrom:source.aspects];
}

-(NSString*) weaponDescriptionWithSeperator:(NSString*)seperator
{
    NSArray* theWeapons = [self.aspects.weapons array];
    
    return [Weapon descriptionFromWeaponsArray:theWeapons seperator:seperator];
}

-(NSString*)imageTitle
{
    return [NSString stringWithFormat:@"images/%@_%@_logo.png", [[self unit] id], self.id];
}

-(NSString*)webImageTitle
{
    return [NSString stringWithFormat:@"%@%@_%@%@", URL_ROOT, [[self unit] id], self.id, WEB_IMAGE_SUFFIX];
}

@end
