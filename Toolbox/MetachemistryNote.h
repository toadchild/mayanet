#import "_MetachemistryNote.h"

@interface MetachemistryNote : _MetachemistryNote {}
// Custom logic goes here.

-(NotationType) type;

@end
