// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Roster.h instead.

#import <CoreData/CoreData.h>

extern const struct RosterAttributes {
	__unsafe_unretained NSString *dateCreated;
	__unsafe_unretained NSString *isAlternate;
	__unsafe_unretained NSString *isCurrent;
	__unsafe_unretained NSString *isPlaying;
	__unsafe_unretained NSString *isSoldiersOfFortune;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *pointcap;
} RosterAttributes;

extern const struct RosterRelationships {
	__unsafe_unretained NSString *army;
	__unsafe_unretained NSString *combatGroups;
	__unsafe_unretained NSString *sectorial;
} RosterRelationships;

@class Army;
@class CombatGroup;
@class Sectorial;

@interface RosterID : NSManagedObjectID {}
@end

@interface _Roster : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) RosterID* objectID;

@property (nonatomic, strong) NSDate* dateCreated;

//- (BOOL)validateDateCreated:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isAlternate;

@property (atomic) BOOL isAlternateValue;
- (BOOL)isAlternateValue;
- (void)setIsAlternateValue:(BOOL)value_;

//- (BOOL)validateIsAlternate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isCurrent;

@property (atomic) BOOL isCurrentValue;
- (BOOL)isCurrentValue;
- (void)setIsCurrentValue:(BOOL)value_;

//- (BOOL)validateIsCurrent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isPlaying;

@property (atomic) BOOL isPlayingValue;
- (BOOL)isPlayingValue;
- (void)setIsPlayingValue:(BOOL)value_;

//- (BOOL)validateIsPlaying:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isSoldiersOfFortune;

@property (atomic) BOOL isSoldiersOfFortuneValue;
- (BOOL)isSoldiersOfFortuneValue;
- (void)setIsSoldiersOfFortuneValue:(BOOL)value_;

//- (BOOL)validateIsSoldiersOfFortune:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* pointcap;

@property (atomic) int16_t pointcapValue;
- (int16_t)pointcapValue;
- (void)setPointcapValue:(int16_t)value_;

//- (BOOL)validatePointcap:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Army *army;

//- (BOOL)validateArmy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSOrderedSet *combatGroups;

- (NSMutableOrderedSet*)combatGroupsSet;

@property (nonatomic, strong) Sectorial *sectorial;

//- (BOOL)validateSectorial:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newCombatGroupsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _Roster (CombatGroupsCoreDataGeneratedAccessors)
- (void)addCombatGroups:(NSOrderedSet*)value_;
- (void)removeCombatGroups:(NSOrderedSet*)value_;
- (void)addCombatGroupsObject:(CombatGroup*)value_;
- (void)removeCombatGroupsObject:(CombatGroup*)value_;

- (void)insertObject:(CombatGroup*)value inCombatGroupsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromCombatGroupsAtIndex:(NSUInteger)idx;
- (void)insertCombatGroups:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeCombatGroupsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInCombatGroupsAtIndex:(NSUInteger)idx withObject:(CombatGroup*)value;
- (void)replaceCombatGroupsAtIndexes:(NSIndexSet *)indexes withCombatGroups:(NSArray *)values;

@end

@interface _Roster (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveDateCreated;
- (void)setPrimitiveDateCreated:(NSDate*)value;

- (NSNumber*)primitiveIsAlternate;
- (void)setPrimitiveIsAlternate:(NSNumber*)value;

- (BOOL)primitiveIsAlternateValue;
- (void)setPrimitiveIsAlternateValue:(BOOL)value_;

- (NSNumber*)primitiveIsCurrent;
- (void)setPrimitiveIsCurrent:(NSNumber*)value;

- (BOOL)primitiveIsCurrentValue;
- (void)setPrimitiveIsCurrentValue:(BOOL)value_;

- (NSNumber*)primitiveIsPlaying;
- (void)setPrimitiveIsPlaying:(NSNumber*)value;

- (BOOL)primitiveIsPlayingValue;
- (void)setPrimitiveIsPlayingValue:(BOOL)value_;

- (NSNumber*)primitiveIsSoldiersOfFortune;
- (void)setPrimitiveIsSoldiersOfFortune:(NSNumber*)value;

- (BOOL)primitiveIsSoldiersOfFortuneValue;
- (void)setPrimitiveIsSoldiersOfFortuneValue:(BOOL)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitivePointcap;
- (void)setPrimitivePointcap:(NSNumber*)value;

- (int16_t)primitivePointcapValue;
- (void)setPrimitivePointcapValue:(int16_t)value_;

- (Army*)primitiveArmy;
- (void)setPrimitiveArmy:(Army*)value;

- (NSMutableOrderedSet*)primitiveCombatGroups;
- (void)setPrimitiveCombatGroups:(NSMutableOrderedSet*)value;

- (Sectorial*)primitiveSectorial;
- (void)setPrimitiveSectorial:(Sectorial*)value;

@end
