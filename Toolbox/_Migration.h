// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Migration.h instead.

#import <CoreData/CoreData.h>

extern const struct MigrationAttributes {
	__unsafe_unretained NSString *name;
} MigrationAttributes;

@interface MigrationID : NSManagedObjectID {}
@end

@interface _Migration : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MigrationID* objectID;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

#endif

@end

@interface _Migration (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

@end
