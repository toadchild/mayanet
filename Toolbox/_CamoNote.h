// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CamoNote.h instead.

#import <CoreData/CoreData.h>
#import "Note.h"

extern const struct CamoNoteAttributes {
	__unsafe_unretained NSString *number;
	__unsafe_unretained NSString *pattern;
} CamoNoteAttributes;

@interface CamoNoteID : NoteID {}
@end

@interface _CamoNote : Note {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CamoNoteID* objectID;

@property (nonatomic, strong) NSNumber* number;

@property (atomic) int16_t numberValue;
- (int16_t)numberValue;
- (void)setNumberValue:(int16_t)value_;

//- (BOOL)validateNumber:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* pattern;

@property (atomic) int16_t patternValue;
- (int16_t)patternValue;
- (void)setPatternValue:(int16_t)value_;

//- (BOOL)validatePattern:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

#endif

@end

@interface _CamoNote (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveNumber;
- (void)setPrimitiveNumber:(NSNumber*)value;

- (int16_t)primitiveNumberValue;
- (void)setPrimitiveNumberValue:(int16_t)value_;

- (NSNumber*)primitivePattern;
- (void)setPrimitivePattern:(NSNumber*)value;

- (int16_t)primitivePatternValue;
- (void)setPrimitivePatternValue:(int16_t)value_;

@end
