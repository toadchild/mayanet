// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Metachemistry.h instead.

#import <CoreData/CoreData.h>

extern const struct MetachemistryAttributes {
	__unsafe_unretained NSString *highRollRange;
	__unsafe_unretained NSString *levelTwo;
	__unsafe_unretained NSString *lowRollRange;
} MetachemistryAttributes;

extern const struct MetachemistryRelationships {
	__unsafe_unretained NSString *notes;
	__unsafe_unretained NSString *specialRule;
} MetachemistryRelationships;

extern const struct MetachemistryFetchedProperties {
	__unsafe_unretained NSString *fetchedProperty;
} MetachemistryFetchedProperties;

@class MetachemistryNote;
@class SpecialRule;

@interface MetachemistryID : NSManagedObjectID {}
@end

@interface _Metachemistry : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MetachemistryID* objectID;

@property (nonatomic, strong) NSNumber* highRollRange;

@property (atomic) int16_t highRollRangeValue;
- (int16_t)highRollRangeValue;
- (void)setHighRollRangeValue:(int16_t)value_;

//- (BOOL)validateHighRollRange:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* levelTwo;

@property (atomic) BOOL levelTwoValue;
- (BOOL)levelTwoValue;
- (void)setLevelTwoValue:(BOOL)value_;

//- (BOOL)validateLevelTwo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* lowRollRange;

@property (atomic) int16_t lowRollRangeValue;
- (int16_t)lowRollRangeValue;
- (void)setLowRollRangeValue:(int16_t)value_;

//- (BOOL)validateLowRollRange:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *notes;

- (NSMutableSet*)notesSet;

@property (nonatomic, strong) NSSet *specialRule;

- (NSMutableSet*)specialRuleSet;

@property (nonatomic, readonly) NSArray *fetchedProperty;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newNotesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecialRuleFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _Metachemistry (NotesCoreDataGeneratedAccessors)
- (void)addNotes:(NSSet*)value_;
- (void)removeNotes:(NSSet*)value_;
- (void)addNotesObject:(MetachemistryNote*)value_;
- (void)removeNotesObject:(MetachemistryNote*)value_;

@end

@interface _Metachemistry (SpecialRuleCoreDataGeneratedAccessors)
- (void)addSpecialRule:(NSSet*)value_;
- (void)removeSpecialRule:(NSSet*)value_;
- (void)addSpecialRuleObject:(SpecialRule*)value_;
- (void)removeSpecialRuleObject:(SpecialRule*)value_;

@end

@interface _Metachemistry (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveHighRollRange;
- (void)setPrimitiveHighRollRange:(NSNumber*)value;

- (int16_t)primitiveHighRollRangeValue;
- (void)setPrimitiveHighRollRangeValue:(int16_t)value_;

- (NSNumber*)primitiveLevelTwo;
- (void)setPrimitiveLevelTwo:(NSNumber*)value;

- (BOOL)primitiveLevelTwoValue;
- (void)setPrimitiveLevelTwoValue:(BOOL)value_;

- (NSNumber*)primitiveLowRollRange;
- (void)setPrimitiveLowRollRange:(NSNumber*)value;

- (int16_t)primitiveLowRollRangeValue;
- (void)setPrimitiveLowRollRangeValue:(int16_t)value_;

- (NSMutableSet*)primitiveNotes;
- (void)setPrimitiveNotes:(NSMutableSet*)value;

- (NSMutableSet*)primitiveSpecialRule;
- (void)setPrimitiveSpecialRule:(NSMutableSet*)value;

@end
