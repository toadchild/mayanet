// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SpecOpsAspects.m instead.

#import "_SpecOpsAspects.h"

const struct SpecOpsAspectsAttributes SpecOpsAspectsAttributes = {
	.armIncrease = @"armIncrease",
	.bsIncrease = @"bsIncrease",
	.btsIncrease = @"btsIncrease",
	.ccIncrease = @"ccIncrease",
	.phIncrease = @"phIncrease",
	.wIncrease = @"wIncrease",
	.wipIncrease = @"wipIncrease",
};

const struct SpecOpsAspectsRelationships SpecOpsAspectsRelationships = {
	.specops = @"specops",
};

@implementation SpecOpsAspectsID
@end

@implementation _SpecOpsAspects

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SpecOpsAspects" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SpecOpsAspects";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SpecOpsAspects" inManagedObjectContext:moc_];
}

- (SpecOpsAspectsID*)objectID {
	return (SpecOpsAspectsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"armIncreaseValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"armIncrease"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"bsIncreaseValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"bsIncrease"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"btsIncreaseValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"btsIncrease"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ccIncreaseValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ccIncrease"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"phIncreaseValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"phIncrease"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"wIncreaseValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"wIncrease"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"wipIncreaseValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"wipIncrease"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic armIncrease;

- (int16_t)armIncreaseValue {
	NSNumber *result = [self armIncrease];
	return [result shortValue];
}

- (void)setArmIncreaseValue:(int16_t)value_ {
	[self setArmIncrease:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveArmIncreaseValue {
	NSNumber *result = [self primitiveArmIncrease];
	return [result shortValue];
}

- (void)setPrimitiveArmIncreaseValue:(int16_t)value_ {
	[self setPrimitiveArmIncrease:[NSNumber numberWithShort:value_]];
}

@dynamic bsIncrease;

- (int16_t)bsIncreaseValue {
	NSNumber *result = [self bsIncrease];
	return [result shortValue];
}

- (void)setBsIncreaseValue:(int16_t)value_ {
	[self setBsIncrease:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveBsIncreaseValue {
	NSNumber *result = [self primitiveBsIncrease];
	return [result shortValue];
}

- (void)setPrimitiveBsIncreaseValue:(int16_t)value_ {
	[self setPrimitiveBsIncrease:[NSNumber numberWithShort:value_]];
}

@dynamic btsIncrease;

- (int16_t)btsIncreaseValue {
	NSNumber *result = [self btsIncrease];
	return [result shortValue];
}

- (void)setBtsIncreaseValue:(int16_t)value_ {
	[self setBtsIncrease:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveBtsIncreaseValue {
	NSNumber *result = [self primitiveBtsIncrease];
	return [result shortValue];
}

- (void)setPrimitiveBtsIncreaseValue:(int16_t)value_ {
	[self setPrimitiveBtsIncrease:[NSNumber numberWithShort:value_]];
}

@dynamic ccIncrease;

- (int16_t)ccIncreaseValue {
	NSNumber *result = [self ccIncrease];
	return [result shortValue];
}

- (void)setCcIncreaseValue:(int16_t)value_ {
	[self setCcIncrease:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCcIncreaseValue {
	NSNumber *result = [self primitiveCcIncrease];
	return [result shortValue];
}

- (void)setPrimitiveCcIncreaseValue:(int16_t)value_ {
	[self setPrimitiveCcIncrease:[NSNumber numberWithShort:value_]];
}

@dynamic phIncrease;

- (int16_t)phIncreaseValue {
	NSNumber *result = [self phIncrease];
	return [result shortValue];
}

- (void)setPhIncreaseValue:(int16_t)value_ {
	[self setPhIncrease:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePhIncreaseValue {
	NSNumber *result = [self primitivePhIncrease];
	return [result shortValue];
}

- (void)setPrimitivePhIncreaseValue:(int16_t)value_ {
	[self setPrimitivePhIncrease:[NSNumber numberWithShort:value_]];
}

@dynamic wIncrease;

- (int16_t)wIncreaseValue {
	NSNumber *result = [self wIncrease];
	return [result shortValue];
}

- (void)setWIncreaseValue:(int16_t)value_ {
	[self setWIncrease:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveWIncreaseValue {
	NSNumber *result = [self primitiveWIncrease];
	return [result shortValue];
}

- (void)setPrimitiveWIncreaseValue:(int16_t)value_ {
	[self setPrimitiveWIncrease:[NSNumber numberWithShort:value_]];
}

@dynamic wipIncrease;

- (int16_t)wipIncreaseValue {
	NSNumber *result = [self wipIncrease];
	return [result shortValue];
}

- (void)setWipIncreaseValue:(int16_t)value_ {
	[self setWipIncrease:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveWipIncreaseValue {
	NSNumber *result = [self primitiveWipIncrease];
	return [result shortValue];
}

- (void)setPrimitiveWipIncreaseValue:(int16_t)value_ {
	[self setPrimitiveWipIncrease:[NSNumber numberWithShort:value_]];
}

@dynamic specops;

#if TARGET_OS_IPHONE

#endif

@end

