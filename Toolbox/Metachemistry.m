#import "Metachemistry.h"
#import "AppDelegate.h"


@interface Metachemistry ()

// Private interface goes here.

@end


@implementation Metachemistry

// Custom logic goes here.

+(Metachemistry*)rollWithLevelTwo:(BOOL)levelTwo
{
    int random = arc4random_uniform(20)+1;
    
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [Metachemistry entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setIncludesSubentities:TRUE];
    
    [fetchRequest setFetchLimit:1];
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"lowRollRange =< %d AND highRollRange >= %d AND levelTwo = %@", random, random, [NSNumber numberWithBool:levelTwo]];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    NSArray* results = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [results count] == 1)
    {
        return [results objectAtIndex:0];
    }
    return nil;
}

@end
