//
//  WeaponTableViewCell.m
//  Toolbox
//
//  Created by Paul on 11/1/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "WeaponTableViewCell.h"

@implementation WeaponTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(int) cellHeightForWeapon:(Weapon*) weapon
{
    int height = 82;
    
    NSString *extra = [weapon getExtrasString];
    if([extra compare: @""] != NSOrderedSame){
        height += 18;
        
        // hackish estimator if it needs a second line
        if(extra.length >= 55){
            height += 18;
        }
    }
    
    return height;
}

+(int) rangeBandDistance:(int) band
{
    switch(band)
    {
        case range8:
            return 8;
            break;
        case range16:
            return 16;
            break;
        case range24:
            return 24;
            break;
        case range32:
            return 32;
            break;
        case range40:
            return 40;
            break;
        case range48:
            return 48;
            break;
        case range96:
            return 96;
            break;
        default:
            NSAssert(FALSE, @"Unknown range band enum %d passed in", band);
            return 0;
    }
}

@end
