#import "_CamoNotation.h"

@interface CamoNotation : _CamoNotation {}
// Custom logic goes here.
-(NotationType) type;
@end
