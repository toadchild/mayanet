// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Weapon.m instead.

#import "_Weapon.h"

const struct WeaponAttributes WeaponAttributes = {
	.altProfile = @"altProfile",
	.ammo = @"ammo",
	.attribute = @"attribute",
	.burst = @"burst",
	.closeCombat = @"closeCombat",
	.damage = @"damage",
	.longDistance = @"longDistance",
	.longModifier = @"longModifier",
	.maxDistance = @"maxDistance",
	.maxModifier = @"maxModifier",
	.mediumDistance = @"mediumDistance",
	.mediumModifier = @"mediumModifier",
	.mode = @"mode",
	.name = @"name",
	.note = @"note",
	.shortDistance = @"shortDistance",
	.shortModifier = @"shortModifier",
	.suppressive = @"suppressive",
	.templateType = @"templateType",
	.uses = @"uses",
};

const struct WeaponRelationships WeaponRelationships = {
	.booty = @"booty",
	.profiles = @"profiles",
	.specops = @"specops",
	.units = @"units",
	.wikiEntry = @"wikiEntry",
};

@implementation WeaponID
@end

@implementation _Weapon

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Weapon" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Weapon";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Weapon" inManagedObjectContext:moc_];
}

- (WeaponID*)objectID {
	return (WeaponID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"closeCombatValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"closeCombat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"damageValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"damage"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"longDistanceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"longDistance"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"longModifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"longModifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"maxDistanceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"maxDistance"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"maxModifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"maxModifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"mediumDistanceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"mediumDistance"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"mediumModifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"mediumModifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"shortDistanceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"shortDistance"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"shortModifierValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"shortModifier"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"suppressiveValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"suppressive"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"usesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"uses"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic altProfile;

@dynamic ammo;

@dynamic attribute;

@dynamic burst;

@dynamic closeCombat;

- (BOOL)closeCombatValue {
	NSNumber *result = [self closeCombat];
	return [result boolValue];
}

- (void)setCloseCombatValue:(BOOL)value_ {
	[self setCloseCombat:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveCloseCombatValue {
	NSNumber *result = [self primitiveCloseCombat];
	return [result boolValue];
}

- (void)setPrimitiveCloseCombatValue:(BOOL)value_ {
	[self setPrimitiveCloseCombat:[NSNumber numberWithBool:value_]];
}

@dynamic damage;

- (int16_t)damageValue {
	NSNumber *result = [self damage];
	return [result shortValue];
}

- (void)setDamageValue:(int16_t)value_ {
	[self setDamage:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveDamageValue {
	NSNumber *result = [self primitiveDamage];
	return [result shortValue];
}

- (void)setPrimitiveDamageValue:(int16_t)value_ {
	[self setPrimitiveDamage:[NSNumber numberWithShort:value_]];
}

@dynamic longDistance;

- (int16_t)longDistanceValue {
	NSNumber *result = [self longDistance];
	return [result shortValue];
}

- (void)setLongDistanceValue:(int16_t)value_ {
	[self setLongDistance:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveLongDistanceValue {
	NSNumber *result = [self primitiveLongDistance];
	return [result shortValue];
}

- (void)setPrimitiveLongDistanceValue:(int16_t)value_ {
	[self setPrimitiveLongDistance:[NSNumber numberWithShort:value_]];
}

@dynamic longModifier;

- (int16_t)longModifierValue {
	NSNumber *result = [self longModifier];
	return [result shortValue];
}

- (void)setLongModifierValue:(int16_t)value_ {
	[self setLongModifier:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveLongModifierValue {
	NSNumber *result = [self primitiveLongModifier];
	return [result shortValue];
}

- (void)setPrimitiveLongModifierValue:(int16_t)value_ {
	[self setPrimitiveLongModifier:[NSNumber numberWithShort:value_]];
}

@dynamic maxDistance;

- (int16_t)maxDistanceValue {
	NSNumber *result = [self maxDistance];
	return [result shortValue];
}

- (void)setMaxDistanceValue:(int16_t)value_ {
	[self setMaxDistance:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveMaxDistanceValue {
	NSNumber *result = [self primitiveMaxDistance];
	return [result shortValue];
}

- (void)setPrimitiveMaxDistanceValue:(int16_t)value_ {
	[self setPrimitiveMaxDistance:[NSNumber numberWithShort:value_]];
}

@dynamic maxModifier;

- (int16_t)maxModifierValue {
	NSNumber *result = [self maxModifier];
	return [result shortValue];
}

- (void)setMaxModifierValue:(int16_t)value_ {
	[self setMaxModifier:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveMaxModifierValue {
	NSNumber *result = [self primitiveMaxModifier];
	return [result shortValue];
}

- (void)setPrimitiveMaxModifierValue:(int16_t)value_ {
	[self setPrimitiveMaxModifier:[NSNumber numberWithShort:value_]];
}

@dynamic mediumDistance;

- (int16_t)mediumDistanceValue {
	NSNumber *result = [self mediumDistance];
	return [result shortValue];
}

- (void)setMediumDistanceValue:(int16_t)value_ {
	[self setMediumDistance:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveMediumDistanceValue {
	NSNumber *result = [self primitiveMediumDistance];
	return [result shortValue];
}

- (void)setPrimitiveMediumDistanceValue:(int16_t)value_ {
	[self setPrimitiveMediumDistance:[NSNumber numberWithShort:value_]];
}

@dynamic mediumModifier;

- (int16_t)mediumModifierValue {
	NSNumber *result = [self mediumModifier];
	return [result shortValue];
}

- (void)setMediumModifierValue:(int16_t)value_ {
	[self setMediumModifier:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveMediumModifierValue {
	NSNumber *result = [self primitiveMediumModifier];
	return [result shortValue];
}

- (void)setPrimitiveMediumModifierValue:(int16_t)value_ {
	[self setPrimitiveMediumModifier:[NSNumber numberWithShort:value_]];
}

@dynamic mode;

@dynamic name;

@dynamic note;

@dynamic shortDistance;

- (int16_t)shortDistanceValue {
	NSNumber *result = [self shortDistance];
	return [result shortValue];
}

- (void)setShortDistanceValue:(int16_t)value_ {
	[self setShortDistance:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveShortDistanceValue {
	NSNumber *result = [self primitiveShortDistance];
	return [result shortValue];
}

- (void)setPrimitiveShortDistanceValue:(int16_t)value_ {
	[self setPrimitiveShortDistance:[NSNumber numberWithShort:value_]];
}

@dynamic shortModifier;

- (int16_t)shortModifierValue {
	NSNumber *result = [self shortModifier];
	return [result shortValue];
}

- (void)setShortModifierValue:(int16_t)value_ {
	[self setShortModifier:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveShortModifierValue {
	NSNumber *result = [self primitiveShortModifier];
	return [result shortValue];
}

- (void)setPrimitiveShortModifierValue:(int16_t)value_ {
	[self setPrimitiveShortModifier:[NSNumber numberWithShort:value_]];
}

@dynamic suppressive;

- (int16_t)suppressiveValue {
	NSNumber *result = [self suppressive];
	return [result shortValue];
}

- (void)setSuppressiveValue:(int16_t)value_ {
	[self setSuppressive:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSuppressiveValue {
	NSNumber *result = [self primitiveSuppressive];
	return [result shortValue];
}

- (void)setPrimitiveSuppressiveValue:(int16_t)value_ {
	[self setPrimitiveSuppressive:[NSNumber numberWithShort:value_]];
}

@dynamic templateType;

@dynamic uses;

- (int16_t)usesValue {
	NSNumber *result = [self uses];
	return [result shortValue];
}

- (void)setUsesValue:(int16_t)value_ {
	[self setUses:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveUsesValue {
	NSNumber *result = [self primitiveUses];
	return [result shortValue];
}

- (void)setPrimitiveUsesValue:(int16_t)value_ {
	[self setPrimitiveUses:[NSNumber numberWithShort:value_]];
}

@dynamic booty;

- (NSMutableSet*)bootySet {
	[self willAccessValueForKey:@"booty"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"booty"];

	[self didAccessValueForKey:@"booty"];
	return result;
}

@dynamic profiles;

- (NSMutableSet*)profilesSet {
	[self willAccessValueForKey:@"profiles"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"profiles"];

	[self didAccessValueForKey:@"profiles"];
	return result;
}

@dynamic specops;

- (NSMutableSet*)specopsSet {
	[self willAccessValueForKey:@"specops"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specops"];

	[self didAccessValueForKey:@"specops"];
	return result;
}

@dynamic units;

- (NSMutableSet*)unitsSet {
	[self willAccessValueForKey:@"units"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"units"];

	[self didAccessValueForKey:@"units"];
	return result;
}

@dynamic wikiEntry;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newBootyFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Booty" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"weapon == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newProfilesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"UnitOption" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"weapons CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecopsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsWeapon" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"weapon == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newUnitsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"UnitAspects" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"weapons CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

