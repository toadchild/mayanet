//
//  FetchViewController.h
//  Damage Tracker
//
//  Created by Paul on 11/30/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface FetchViewController : UITableViewController<NSFetchedResultsControllerDelegate, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate>
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, copy) NSString *savedSearchTerm;
@property (nonatomic) NSInteger savedScopeButtonIndex;
@property (nonatomic, strong) UISegmentedControl *segmentedControl;

@property (nonatomic) NSInteger sortIndex;
@property (nonatomic) BOOL searchWasActive;
@property (nonatomic,strong) IBOutlet UIBarButtonItem* searchToggleButton;

@property (strong, nonatomic) UISearchController *searchController;

-(NSFetchRequest*) getFetchRequest:(NSString *)searchString;
-(NSString*) getSectionNameKeyPath;
- (void)fetchedResultsController:(NSFetchedResultsController *)fetchedResultsController configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (NSInteger) fetchSortIndex: (BOOL*) search;
- (IBAction)sortIndexChanged:(id)sender;
@end

