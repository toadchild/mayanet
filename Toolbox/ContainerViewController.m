//
//  ContainerViewController.m
//  Toolbox
//
//  Created by Paul on 10/30/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "ContainerViewController.h"
#import "Containable.h"
#import "AppDelegate.h"

@interface ContainerViewController ()

@end

@implementation ContainerViewController

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        rightViewControllerVisible = TRUE;
        leftViewControllerVisible = TRUE;
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    delegate.containerViewController = self;

    leftScreenEdgePanGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftScreenEdgePan:)];
    
    leftScreenEdgePanGestureRecognizer.edges = UIRectEdgeLeft;
    
    leftScreenEdgePanGestureRecognizer.delegate = self;
    
    [self.view addGestureRecognizer:leftScreenEdgePanGestureRecognizer];
    
    
    
    leftPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftPan:)];
    
    leftPanGestureRecognizer.delegate = self;
    
    [self.view addGestureRecognizer:leftPanGestureRecognizer];
    

    rightScreenEdgePanGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightScreenEdgePan:)];
    
    rightScreenEdgePanGestureRecognizer.edges = UIRectEdgeRight;
    
    rightScreenEdgePanGestureRecognizer.delegate = self;
    
    [self.view addGestureRecognizer:rightScreenEdgePanGestureRecognizer];
    
    
    rightPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightPan:)];
    
    rightPanGestureRecognizer.delegate = self;
    
    [self.view addGestureRecognizer:rightPanGestureRecognizer];
    
    CGRect left = self.leftViewController.view.superview.frame;
    
    middleLeftSeperator = [[UIView alloc] initWithFrame:CGRectMake(left.size.width, 0, 1, left.size.height)];
    [middleLeftSeperator setBackgroundColor:[UIColor lightGrayColor]];
    [self.view addSubview:middleLeftSeperator];
    [self.view insertSubview:middleLeftSeperator belowSubview:self.rightViewController.view.superview];


    CGRect right = self.rightViewController.view.superview.frame;
    
    CGFloat rightSide = right.origin.x;
    
    middleRightSeperator = [[UIView alloc] initWithFrame:CGRectMake(rightSide, 0, 1, right.size.height)];
    [middleRightSeperator setBackgroundColor:[UIColor lightGrayColor]];
    
    [self.view addSubview:middleRightSeperator];
    
    // Configure the gesture recognizer and attach it to the view.
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(showRightPane)
     name:@"ShowRightPane"
     object:nil];
    
    leftViewControllerVisible = [self isLandscapeMode];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

-(bool)isLandscapeMode
{
    CGSize size = self.view.frame.size;
    return [self isLandscapeModeForSize:size];
}

-(bool)isLandscapeModeForSize:(CGSize) size
{
    return size.width > size.height;
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    leftViewControllerVisible = [self isLandscapeModeForSize:size];
}

-(void)viewDidLayoutSubviews
{
    [self.middleViewController beginAppearanceTransition:TRUE animated:FALSE];
    [self.leftViewController beginAppearanceTransition:leftViewControllerVisible animated:FALSE];
    [self.rightViewController beginAppearanceTransition:rightViewControllerVisible animated:FALSE];
    
    [self setLeftViewControllerFrame:leftViewControllerVisible];
    [self setRightViewControllerFrame:rightViewControllerVisible];
    
    [self.leftViewController endAppearanceTransition];
    [self.rightViewController endAppearanceTransition];
}

#define kPanZoneSize 80
#pragma mark - gesture delegates
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if(gestureRecognizer == leftScreenEdgePanGestureRecognizer)
    {
        if(leftViewControllerVisible)
            return FALSE;
    }
    else if(gestureRecognizer == rightScreenEdgePanGestureRecognizer)
    {
        if(rightViewControllerVisible)
            return FALSE;
    }
    else if(gestureRecognizer == leftPanGestureRecognizer)
    {
        if(!leftViewControllerVisible)
            return FALSE;
        
        UIView* leftView = self.leftViewController.view.superview;
        CGPoint touchPoint = [touch locationInView:self.view];
        if(touchPoint.x < leftView.frame.size.width || touchPoint.x > leftView.frame.size.width + kPanZoneSize)
        {
            return FALSE;
        }
    }
    else if(gestureRecognizer == rightPanGestureRecognizer)
    {
        if(!rightViewControllerVisible)
            return FALSE;
        
        UIView* rightView = self.rightViewController.view.superview;
        CGRect frame = self.view.bounds;
        CGPoint touchPoint = [touch locationInView:self.view];
        if(touchPoint.x > frame.size.width - rightView.frame.size.width || touchPoint.x < frame.size.width - rightView.frame.size.width - kPanZoneSize)
        {
            return FALSE;
        }
        
    }
    return TRUE;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    BOOL result = NO;
    if ((gestureRecognizer == leftScreenEdgePanGestureRecognizer
         || gestureRecognizer == rightScreenEdgePanGestureRecognizer
         || gestureRecognizer == leftPanGestureRecognizer
         || gestureRecognizer == rightPanGestureRecognizer
         ) && [[otherGestureRecognizer view] isDescendantOfView:[gestureRecognizer view]]) {
        result = YES;
    }
    return result;
}
#define kNecessaryVelocity 500
#pragma mark - left sliding from edge
- (void) handleLeftScreenEdgePan:(UIPanGestureRecognizer *)recognizer
{
    //CGPoint touchPoint = [recognizer locationInView:self.view];
    //UIView* leftView = self.leftViewController.view.superview;
    CGPoint velocity = [recognizer velocityInView:self.view];
    
    switch ( recognizer.state )
    {
        case UIGestureRecognizerStateBegan:
        {
            validTouch = !leftViewControllerVisible;
            break;
        }
        case UIGestureRecognizerStateChanged:
        {
            if(!validTouch)
                return;
            
            //CGFloat leftSidePosition = MIN(touchPoint.x-leftView.frame.size.width,0);
            
            //If the user swiped fast enough we don't care about how far they traveled
            if(velocity.x > kNecessaryVelocity)
            {
                [self displayLeftViewController:TRUE animationCompletionBlock:NULL];
                validTouch = FALSE;
            }
            else
            {
                //[self leftControllerViewAppears:leftSidePosition];
            }
            break;
        }
        case UIGestureRecognizerStateEnded:
        {
            break;
        }
        case UIGestureRecognizerStateCancelled:
        {
            
            /*
            if (direction == 1) {
                [self showLeftStaticView:YES];
            }else {
                [self showSlidingViewAnimated:YES];
            }
             */
            break;
        }
        case UIGestureRecognizerStateFailed:
            break;
        default:
            break;
    }
}



#pragma mark - left sliding from center to left edge
- (void) handleLeftPan:(UIPanGestureRecognizer *)recognizer
{
    //CGPoint touchPoint = [recognizer locationInView:self.view];
    //UIView* leftView = self.leftViewController.view.superview;
    CGPoint velocity = [recognizer velocityInView:self.view];
    
    switch ( recognizer.state )
    {
        case UIGestureRecognizerStateBegan:
        {
            validTouch = leftViewControllerVisible;
            break;
        }
        case UIGestureRecognizerStateChanged:
        {
            if(velocity.x > 0)
            {
                validTouch = FALSE;
            }
            if(!validTouch)
                return;
            
            //CGFloat leftSidePosition = MIN(touchPoint.x-leftView.frame.size.width,0);
            
            //If the user swiped fast enough we don't care about how far they traveled
            if(ABS(velocity.x) > kNecessaryVelocity)
            {
                [self displayLeftViewController:FALSE animationCompletionBlock:NULL];
                validTouch = FALSE;
            }
            else
            {
                //[self leftControllerViewAppears:leftSidePosition];
            }
            break;
        }
        case UIGestureRecognizerStateEnded:
        {
            break;
        }
        case UIGestureRecognizerStateCancelled:
        {
            break;
        }
        case UIGestureRecognizerStateFailed:
            break;
        default:
            break;
    }
}


#pragma mark - sliding in from right edge
- (void) handleRightScreenEdgePan:(UIPanGestureRecognizer *)recognizer
{
    //CGPoint touchPoint = [recognizer locationInView:self.view];
    //UIView* rightView = self.rightViewController.view.superview;
    CGPoint velocity = [recognizer velocityInView:self.view];
    
    switch ( recognizer.state )
    {
        case UIGestureRecognizerStateBegan:
        {
            validTouch = !rightViewControllerVisible;
            break;
        }
        case UIGestureRecognizerStateChanged:
        {
            if(!validTouch)
                return;
            
            //If the user swiped fast enough we don't care about how far they traveled
            if(ABS(velocity.x) > kNecessaryVelocity)
            {
                [self displayRightViewController: TRUE animationCompletionBlock:NULL];
                validTouch = FALSE;
            }
            break;
        }
        case UIGestureRecognizerStateEnded:
        {
            break;
        }
        case UIGestureRecognizerStateCancelled:
        {
            break;
        }
        case UIGestureRecognizerStateFailed:
            break;
        default:
            break;
    }
}

#pragma mark - sliding in from right edge
- (void) handleRightPan:(UIPanGestureRecognizer *)recognizer
{
    CGPoint velocity = [recognizer velocityInView:self.view];
    
    switch ( recognizer.state )
    {
        case UIGestureRecognizerStateBegan:
        {
            validTouch = rightViewControllerVisible;
            break;
        }
        case UIGestureRecognizerStateChanged:
        {
            if(velocity.x < 0)
            {
                validTouch = FALSE;
            }
            
            if(!validTouch)
                return;
            
            //If the user swiped fast enough we don't care about how far they traveled
            if(ABS(velocity.x) > kNecessaryVelocity)
            {
                [self displayRightViewController: FALSE animationCompletionBlock:NULL];
                validTouch = FALSE;
            }
            break;
        }
        case UIGestureRecognizerStateEnded:
        {
            break;
        }
        case UIGestureRecognizerStateCancelled:
        {
            break;
        }
        case UIGestureRecognizerStateFailed:
            break;
        default:
            break;
    }
}


-(void)leftControllerViewAppears:(CGFloat)leftSidePosition
{
    UIView* leftView = self.leftViewController.view.superview;
    UIView* middleView = self.middleViewController.view.superview;
    
    [self.middleViewController beginAppearanceTransition:TRUE animated:FALSE];
    [self.leftViewController beginAppearanceTransition:TRUE animated:FALSE];
    
    leftView.frame = CGRectMake(leftSidePosition, leftView.frame.origin.y, leftView.frame.size.width, leftView.frame.size.height);
    middleView.frame = CGRectMake(leftSidePosition+320,
                                  middleView.frame.origin.y,
                                  
                                  1024-
                                  (rightViewControllerVisible ? 320 : 0)        //Is the right side shown
                                  
                                  -(leftView.frame.size.width - ABS(leftSidePosition)),    //How much of the left side is exposed
                                  
                                  middleView.frame.size.height);
    
    [self setSeperatorLocations];
    
    [self.middleViewController endAppearanceTransition];
    [self.leftViewController endAppearanceTransition];
}

-(void)rightControllerViewAppears:(CGFloat)rightSidePosition
{
    UIView* rightView = self.rightViewController.view.superview;
    UIView* middleView = self.middleViewController.view.superview;
    
    [self.middleViewController beginAppearanceTransition:TRUE animated:FALSE];
    [self.rightViewController beginAppearanceTransition:TRUE animated:FALSE];
    
    rightView.frame = CGRectMake(rightSidePosition, rightView.frame.origin.y, rightView.frame.size.width, rightView.frame.size.height);
    
    middleView.frame = CGRectMake((leftViewControllerVisible ? 320 : 0),
                                  middleView.frame.origin.y,
                                  1024 - (leftViewControllerVisible ? 320 : 0) - (1024 - rightSidePosition),
                                  middleView.frame.size.height);
    
    [self setSeperatorLocations];
    
    [self.middleViewController endAppearanceTransition];
    [self.rightViewController endAppearanceTransition];
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"LeftView"])
    {
         self.leftViewController = [segue destinationViewController];
    }
    else if([segue.identifier isEqualToString:@"MiddleView"])
    {
        self.middleViewController = [segue destinationViewController];
    }
    else if([segue.identifier isEqualToString:@"RightView"])
    {
        self.rightViewController = [segue destinationViewController];
    }
}

-(void) toggleRightViewController:(AnimationCompletionBlock) animationCompletionBlock
{
    [self displayRightViewController: !rightViewControllerVisible animationCompletionBlock:^(BOOL finished){
        if(animationCompletionBlock)
        {
            animationCompletionBlock(self->rightViewControllerVisible);
        }
    }];
}

-(void) showRightPane
{
    if(!rightViewControllerVisible)
    {
        [self displayRightViewController:TRUE animationCompletionBlock:nil];
    }
}

-(void) displayRightViewController:(BOOL)display animationCompletionBlock:(AnimationCompletionBlock) animationCompletionBlock
{
    [self.middleViewController beginAppearanceTransition:TRUE animated:FALSE];
    [self.rightViewController beginAppearanceTransition:TRUE animated:FALSE];
    rightViewControllerVisible = display;
    [UIView animateWithDuration:0.25 /*delay:0 options:UIViewAnimationOptionLayoutSubviews */ animations:^{
        [self setRightViewControllerFrame:display];
    } completion:^(BOOL finished) {
        [self.middleViewController endAppearanceTransition];
        [self.rightViewController endAppearanceTransition];
        if(animationCompletionBlock)
        {
            animationCompletionBlock(self->rightViewControllerVisible);
        }
    }];
     
}

-(void) setRightViewControllerFrame:(BOOL)display
{
    CGRect right;
    CGRect frame = self.view.bounds;
    
    if(display)
    {
        right = CGRectMake(frame.size.width-320, 0, 320, frame.size.height);
    }
    else
    {
        right = CGRectMake(frame.size.width, 0, 320, frame.size.height);
    }
    self.rightViewController.view.superview.frame = right;
    [self setMiddleViewControllerFrameWithLeftVisible:leftViewControllerVisible rightVisible:display];
    [self setSeperatorLocations];
}

-(void) toggleLeftViewController:(AnimationCompletionBlock) animationCompletionBlock
{
    
    [self displayLeftViewController: !leftViewControllerVisible animationCompletionBlock:^(BOOL finished){
        if(animationCompletionBlock)
        {
            animationCompletionBlock(self->leftViewControllerVisible);
        }
    }];
}

-(void) displayLeftViewController:(BOOL)display animationCompletionBlock:(AnimationCompletionBlock) animationCompletionBlock
{
    [self.middleViewController beginAppearanceTransition:TRUE animated:FALSE];
    [self.leftViewController beginAppearanceTransition:display animated:FALSE];
    leftViewControllerVisible = display;
    [UIView animateWithDuration:0.25 animations:^{
        [self setLeftViewControllerFrame:display];

    } completion:^(BOOL finished) {
        [self.middleViewController endAppearanceTransition];
        [self.leftViewController endAppearanceTransition];
        if(animationCompletionBlock)
        {
            animationCompletionBlock(self->leftViewControllerVisible);
        }
    }];
    
    [self.middleViewController didMoveToParentViewController:nil];
}

-(void) setLeftViewControllerFrame:(BOOL)display
{
    CGRect left;
    CGRect frame = self.view.bounds;
    if(display)
    {
        left = CGRectMake(0, 0, 320, frame.size.height);
    }
    else
    {
        left = CGRectMake(-320, 0, 320, frame.size.height);
    }
    
    self.leftViewController.view.superview.frame = left;
    [self setMiddleViewControllerFrameWithLeftVisible:display rightVisible:rightViewControllerVisible];
    [self setSeperatorLocations];
}

-(void)setMiddleViewControllerFrameWithLeftVisible:(BOOL)leftVisible rightVisible:(BOOL)rightVisible
{
    CGRect middle;
    CGRect frame = self.view.bounds;
    CGFloat middleWidth = frame.size.width;
    CGFloat middleOffset = 0;

    if(rightVisible)
    {
        middleWidth -= 320;
    }

    if(leftVisible)
    {
        middleWidth -= 320;
        middleOffset += 320;
    }
    
    // If the middle is very narrow, let the contents fall under whatever is on the right.
    if (middleWidth < 320) {
        middleWidth = 320;
    }
    
    middle = CGRectMake(middleOffset, 0, middleWidth, frame.size.height);
    self.middleViewController.view.superview.frame = middle;
}

-(void) setSeperatorLocations
{
    CGRect left = self.leftViewController.view.superview.frame;

    CGRect right = self.rightViewController.view.superview.frame;

    CGFloat rightSide = right.origin.x;
    middleLeftSeperator.frame = CGRectMake(left.size.width-ABS(left.origin.x), 0, 1, left.size.height);
    middleRightSeperator.frame = CGRectMake(rightSide, 0, 1, right.size.height);

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) refreshLeft
{
    id<Containable> leftController = [self.leftViewController.viewControllers lastObject];
    if([leftController respondsToSelector:@selector(refresh)])
        [leftController refresh];
}
-(void) refreshMiddle
{
    id<Containable> midController = [self.middleViewController.viewControllers lastObject];
    if([midController respondsToSelector:@selector(refresh)])
        [midController refresh];
}
-(void) refreshRight
{
    id<Containable> rightController = [self.rightViewController.viewControllers lastObject];
    if([rightController respondsToSelector:@selector(refresh)])
        [rightController refresh];
}

-(void) clearSelectionLeft
{
    id<Containable> leftController = [self.leftViewController.viewControllers lastObject];
    if([leftController respondsToSelector:@selector(clearSelection)])
        [leftController clearSelection];
}
-(void) clearSelectionMiddle
{
    id<Containable> midController = [self.middleViewController.viewControllers lastObject];
    if([midController respondsToSelector:@selector(clearSelection)])
        [midController clearSelection];
}
-(void) clearSelectionRight
{
    id<Containable> rightController = [self.rightViewController.viewControllers lastObject];
    if([rightController respondsToSelector:@selector(clearSelection)])
        [rightController clearSelection];
}

    
-(BOOL) isRightViewControllerVisible
{
    return rightViewControllerVisible;
}
-(BOOL) isLeftViewControllerVisible
{
    return leftViewControllerVisible;
}
@end
