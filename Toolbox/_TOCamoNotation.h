// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TOCamoNotation.h instead.

#import <CoreData/CoreData.h>
#import "Notation.h"

@interface TOCamoNotationID : NotationID {}
@end

@interface _TOCamoNotation : Notation {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) TOCamoNotationID* objectID;

#if TARGET_OS_IPHONE

#endif

@end

@interface _TOCamoNotation (CoreDataGeneratedPrimitiveAccessors)

@end
