#import "UnitClassification.h"
#import "AppDelegate.h"

@interface UnitClassification ()

// Private interface goes here.

@end

@implementation UnitClassification

// Create object cache for fast unit lookups.
static NSMutableDictionary* classificationDictionary = nil;
+(void) initialize
{
    classificationDictionary = [[NSMutableDictionary alloc] init];
}

+(UnitClassification*)classificationWithCode:(NSString*)code
{
    UnitClassification* classification = [classificationDictionary valueForKey:code];
    if(classification)
    {
        return classification;
    }
    
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [UnitClassification entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code = %@", code];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    NSArray* classifications = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSAssert([classifications count] <= 1, @"Duplicate classification entries for %@", code);
    if(error == nil && [classifications count] == 1)
    {
        classification = [classifications firstObject];
        [classificationDictionary setObject:classification forKey:classification.code];
        return classification;
    }
    return nil;
}

+(UnitClassification*)newClassificationWithCode:(NSString*)code
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    UnitClassification* classification = [UnitClassification insertInManagedObjectContext:[delegate managedObjectContext]];
    classification.code = code;
    NSAssert(classificationDictionary[code] == nil, @"Trying to create a unit classification that already exists");
    classificationDictionary[code] = classification;
    return classification;
}
@end
