// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UnitOption.m instead.

#import "_UnitOption.h"

const struct UnitOptionAttributes UnitOptionAttributes = {
	.code = @"code",
	.cost = @"cost",
	.id = @"id",
	.independentCost = @"independentCost",
	.independentSWC = @"independentSWC",
	.isIndependent = @"isIndependent",
	.legacyCode = @"legacyCode",
	.swc = @"swc",
};

const struct UnitOptionRelationships UnitOptionRelationships = {
	.disguisers = @"disguisers",
	.enabledUnitProfile = @"enabledUnitProfile",
	.groupMembers = @"groupMembers",
	.specialRules = @"specialRules",
	.specops = @"specops",
	.units = @"units",
	.weapons = @"weapons",
};

@implementation UnitOptionID
@end

@implementation _UnitOption

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UnitOption" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UnitOption";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UnitOption" inManagedObjectContext:moc_];
}

- (UnitOptionID*)objectID {
	return (UnitOptionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"costValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cost"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"independentCostValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"independentCost"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"isIndependentValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isIndependent"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic code;

@dynamic cost;

- (int16_t)costValue {
	NSNumber *result = [self cost];
	return [result shortValue];
}

- (void)setCostValue:(int16_t)value_ {
	[self setCost:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCostValue {
	NSNumber *result = [self primitiveCost];
	return [result shortValue];
}

- (void)setPrimitiveCostValue:(int16_t)value_ {
	[self setPrimitiveCost:[NSNumber numberWithShort:value_]];
}

@dynamic id;

- (int32_t)idValue {
	NSNumber *result = [self id];
	return [result intValue];
}

- (void)setIdValue:(int32_t)value_ {
	[self setId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdValue {
	NSNumber *result = [self primitiveId];
	return [result intValue];
}

- (void)setPrimitiveIdValue:(int32_t)value_ {
	[self setPrimitiveId:[NSNumber numberWithInt:value_]];
}

@dynamic independentCost;

- (int16_t)independentCostValue {
	NSNumber *result = [self independentCost];
	return [result shortValue];
}

- (void)setIndependentCostValue:(int16_t)value_ {
	[self setIndependentCost:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveIndependentCostValue {
	NSNumber *result = [self primitiveIndependentCost];
	return [result shortValue];
}

- (void)setPrimitiveIndependentCostValue:(int16_t)value_ {
	[self setPrimitiveIndependentCost:[NSNumber numberWithShort:value_]];
}

@dynamic independentSWC;

@dynamic isIndependent;

- (BOOL)isIndependentValue {
	NSNumber *result = [self isIndependent];
	return [result boolValue];
}

- (void)setIsIndependentValue:(BOOL)value_ {
	[self setIsIndependent:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsIndependentValue {
	NSNumber *result = [self primitiveIsIndependent];
	return [result boolValue];
}

- (void)setPrimitiveIsIndependentValue:(BOOL)value_ {
	[self setPrimitiveIsIndependent:[NSNumber numberWithBool:value_]];
}

@dynamic legacyCode;

@dynamic swc;

@dynamic disguisers;

@dynamic enabledUnitProfile;

- (NSMutableSet*)enabledUnitProfileSet {
	[self willAccessValueForKey:@"enabledUnitProfile"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"enabledUnitProfile"];

	[self didAccessValueForKey:@"enabledUnitProfile"];
	return result;
}

@dynamic groupMembers;

- (NSMutableSet*)groupMembersSet {
	[self willAccessValueForKey:@"groupMembers"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"groupMembers"];

	[self didAccessValueForKey:@"groupMembers"];
	return result;
}

@dynamic specialRules;

- (NSMutableOrderedSet*)specialRulesSet {
	[self willAccessValueForKey:@"specialRules"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"specialRules"];

	[self didAccessValueForKey:@"specialRules"];
	return result;
}

@dynamic specops;

- (NSMutableSet*)specopsSet {
	[self willAccessValueForKey:@"specops"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specops"];

	[self didAccessValueForKey:@"specops"];
	return result;
}

@dynamic units;

- (NSMutableSet*)unitsSet {
	[self willAccessValueForKey:@"units"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"units"];

	[self didAccessValueForKey:@"units"];
	return result;
}

@dynamic weapons;

- (NSMutableOrderedSet*)weaponsSet {
	[self willAccessValueForKey:@"weapons"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"weapons"];

	[self didAccessValueForKey:@"weapons"];
	return result;
}

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newEnabledUnitProfileFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"UnitProfile" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"specificOptions CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newGroupMembersFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"GroupMember" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"unitOption == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecialRulesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecialRule" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"profiles CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecopsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsBaseUnitOption" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"unitOption == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newUnitsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Unit" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"options CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newWeaponsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Weapon" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"profiles CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

@implementation _UnitOption (SpecialRulesCoreDataGeneratedAccessors)
- (void)addSpecialRules:(NSOrderedSet*)value_ {
	[self.specialRulesSet unionOrderedSet:value_];
}
- (void)removeSpecialRules:(NSOrderedSet*)value_ {
	[self.specialRulesSet minusOrderedSet:value_];
}
- (void)addSpecialRulesObject:(SpecialRule*)value_ {
	[self.specialRulesSet addObject:value_];
}
- (void)removeSpecialRulesObject:(SpecialRule*)value_ {
	[self.specialRulesSet removeObject:value_];
}
- (void)insertObject:(SpecialRule*)value inSpecialRulesAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"specialRules"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specialRules]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specialRules"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"specialRules"];
}
- (void)removeObjectFromSpecialRulesAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"specialRules"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specialRules]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specialRules"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"specialRules"];
}
- (void)insertSpecialRules:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"specialRules"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specialRules]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specialRules"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"specialRules"];
}
- (void)removeSpecialRulesAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"specialRules"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specialRules]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specialRules"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"specialRules"];
}
- (void)replaceObjectInSpecialRulesAtIndex:(NSUInteger)idx withObject:(SpecialRule*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"specialRules"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specialRules]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specialRules"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"specialRules"];
}
- (void)replaceSpecialRulesAtIndexes:(NSIndexSet *)indexes withSpecialRules:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"specialRules"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specialRules]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specialRules"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"specialRules"];
}
@end

@implementation _UnitOption (WeaponsCoreDataGeneratedAccessors)
- (void)addWeapons:(NSOrderedSet*)value_ {
	[self.weaponsSet unionOrderedSet:value_];
}
- (void)removeWeapons:(NSOrderedSet*)value_ {
	[self.weaponsSet minusOrderedSet:value_];
}
- (void)addWeaponsObject:(Weapon*)value_ {
	[self.weaponsSet addObject:value_];
}
- (void)removeWeaponsObject:(Weapon*)value_ {
	[self.weaponsSet removeObject:value_];
}
- (void)insertObject:(Weapon*)value inWeaponsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"weapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self weapons]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"weapons"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"weapons"];
}
- (void)removeObjectFromWeaponsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"weapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self weapons]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"weapons"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"weapons"];
}
- (void)insertWeapons:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"weapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self weapons]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"weapons"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"weapons"];
}
- (void)removeWeaponsAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"weapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self weapons]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"weapons"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"weapons"];
}
- (void)replaceObjectInWeaponsAtIndex:(NSUInteger)idx withObject:(Weapon*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"weapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self weapons]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"weapons"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"weapons"];
}
- (void)replaceWeaponsAtIndexes:(NSIndexSet *)indexes withWeapons:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"weapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self weapons]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"weapons"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"weapons"];
}
@end

