// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BootyNote.m instead.

#import "_BootyNote.h"

const struct BootyNoteAttributes BootyNoteAttributes = {
	.levelTwo = @"levelTwo",
};

const struct BootyNoteRelationships BootyNoteRelationships = {
	.booty = @"booty",
};

@implementation BootyNoteID
@end

@implementation _BootyNote

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BootyNote" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BootyNote";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BootyNote" inManagedObjectContext:moc_];
}

- (BootyNoteID*)objectID {
	return (BootyNoteID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"levelTwoValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"levelTwo"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic levelTwo;

- (BOOL)levelTwoValue {
	NSNumber *result = [self levelTwo];
	return [result boolValue];
}

- (void)setLevelTwoValue:(BOOL)value_ {
	[self setLevelTwo:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveLevelTwoValue {
	NSNumber *result = [self primitiveLevelTwo];
	return [result boolValue];
}

- (void)setPrimitiveLevelTwoValue:(BOOL)value_ {
	[self setPrimitiveLevelTwo:[NSNumber numberWithBool:value_]];
}

@dynamic booty;

#if TARGET_OS_IPHONE

#endif

@end

