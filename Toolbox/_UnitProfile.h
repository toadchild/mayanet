// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UnitProfile.h instead.

#import <CoreData/CoreData.h>

extern const struct UnitProfileAttributes {
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *independentCost;
	__unsafe_unretained NSString *independentSWC;
	__unsafe_unretained NSString *isIndependent;
	__unsafe_unretained NSString *isc;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *optionSpecific;
	__unsafe_unretained NSString *strOverride;
	__unsafe_unretained NSString *wOverride;
} UnitProfileAttributes;

extern const struct UnitProfileRelationships {
	__unsafe_unretained NSString *aspects;
	__unsafe_unretained NSString *groupMembers;
	__unsafe_unretained NSString *specificOptions;
	__unsafe_unretained NSString *type;
	__unsafe_unretained NSString *unit;
} UnitProfileRelationships;

@class UnitAspects;
@class GroupMember;
@class UnitOption;
@class UnitType;
@class Unit;

@interface UnitProfileID : NSManagedObjectID {}
@end

@interface _UnitProfile : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UnitProfileID* objectID;

@property (nonatomic, strong) NSNumber* id;

@property (atomic) int32_t idValue;
- (int32_t)idValue;
- (void)setIdValue:(int32_t)value_;

//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* independentCost;

@property (atomic) int16_t independentCostValue;
- (int16_t)independentCostValue;
- (void)setIndependentCostValue:(int16_t)value_;

//- (BOOL)validateIndependentCost:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDecimalNumber* independentSWC;

//- (BOOL)validateIndependentSWC:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isIndependent;

@property (atomic) BOOL isIndependentValue;
- (BOOL)isIndependentValue;
- (void)setIsIndependentValue:(BOOL)value_;

//- (BOOL)validateIsIndependent:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* isc;

//- (BOOL)validateIsc:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* optionSpecific;

@property (atomic) BOOL optionSpecificValue;
- (BOOL)optionSpecificValue;
- (void)setOptionSpecificValue:(BOOL)value_;

//- (BOOL)validateOptionSpecific:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* strOverride;

@property (atomic) BOOL strOverrideValue;
- (BOOL)strOverrideValue;
- (void)setStrOverrideValue:(BOOL)value_;

//- (BOOL)validateStrOverride:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* wOverride;

@property (atomic) BOOL wOverrideValue;
- (BOOL)wOverrideValue;
- (void)setWOverrideValue:(BOOL)value_;

//- (BOOL)validateWOverride:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) UnitAspects *aspects;

//- (BOOL)validateAspects:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *groupMembers;

- (NSMutableSet*)groupMembersSet;

@property (nonatomic, strong) NSSet *specificOptions;

- (NSMutableSet*)specificOptionsSet;

@property (nonatomic, strong) UnitType *type;

//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Unit *unit;

//- (BOOL)validateUnit:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newGroupMembersFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecificOptionsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _UnitProfile (GroupMembersCoreDataGeneratedAccessors)
- (void)addGroupMembers:(NSSet*)value_;
- (void)removeGroupMembers:(NSSet*)value_;
- (void)addGroupMembersObject:(GroupMember*)value_;
- (void)removeGroupMembersObject:(GroupMember*)value_;

@end

@interface _UnitProfile (SpecificOptionsCoreDataGeneratedAccessors)
- (void)addSpecificOptions:(NSSet*)value_;
- (void)removeSpecificOptions:(NSSet*)value_;
- (void)addSpecificOptionsObject:(UnitOption*)value_;
- (void)removeSpecificOptionsObject:(UnitOption*)value_;

@end

@interface _UnitProfile (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveId;
- (void)setPrimitiveId:(NSNumber*)value;

- (int32_t)primitiveIdValue;
- (void)setPrimitiveIdValue:(int32_t)value_;

- (NSNumber*)primitiveIndependentCost;
- (void)setPrimitiveIndependentCost:(NSNumber*)value;

- (int16_t)primitiveIndependentCostValue;
- (void)setPrimitiveIndependentCostValue:(int16_t)value_;

- (NSDecimalNumber*)primitiveIndependentSWC;
- (void)setPrimitiveIndependentSWC:(NSDecimalNumber*)value;

- (NSNumber*)primitiveIsIndependent;
- (void)setPrimitiveIsIndependent:(NSNumber*)value;

- (BOOL)primitiveIsIndependentValue;
- (void)setPrimitiveIsIndependentValue:(BOOL)value_;

- (NSString*)primitiveIsc;
- (void)setPrimitiveIsc:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveOptionSpecific;
- (void)setPrimitiveOptionSpecific:(NSNumber*)value;

- (BOOL)primitiveOptionSpecificValue;
- (void)setPrimitiveOptionSpecificValue:(BOOL)value_;

- (NSNumber*)primitiveStrOverride;
- (void)setPrimitiveStrOverride:(NSNumber*)value;

- (BOOL)primitiveStrOverrideValue;
- (void)setPrimitiveStrOverrideValue:(BOOL)value_;

- (NSNumber*)primitiveWOverride;
- (void)setPrimitiveWOverride:(NSNumber*)value;

- (BOOL)primitiveWOverrideValue;
- (void)setPrimitiveWOverrideValue:(BOOL)value_;

- (UnitAspects*)primitiveAspects;
- (void)setPrimitiveAspects:(UnitAspects*)value;

- (NSMutableSet*)primitiveGroupMembers;
- (void)setPrimitiveGroupMembers:(NSMutableSet*)value;

- (NSMutableSet*)primitiveSpecificOptions;
- (void)setPrimitiveSpecificOptions:(NSMutableSet*)value;

- (UnitType*)primitiveType;
- (void)setPrimitiveType:(UnitType*)value;

- (Unit*)primitiveUnit;
- (void)setPrimitiveUnit:(Unit*)value;

@end
