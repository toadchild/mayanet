//
//  RosterListViewController.m
//  Toolbox
//
//  Created by Paul on 11/3/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "RosterListViewController.h"
#import "Roster.h"
#import "Army.h"
#import "Sectorial.h"
#import "AppDelegate.h"

@interface RosterListViewController ()

@end

@implementation RosterListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    tableViewEditting = FALSE;
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.managedObjectContext = delegate.managedObjectContext;
    
    if(self.army == nil && self.sectorial == nil)
    {
        [self setTitle:@"All Rosters"];
        self.navigationItem.rightBarButtonItem = nil;
    }
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(NSOrderedSet*) getRosters
{
    if(self.army != nil)
    {
        if(self.army.cachedRosters == nil)
        {
            self.army.cachedRosters = [self fetchRostersForArmy:self.army orSectorial:nil];
        }
        return self.army.cachedRosters;
    }
    else if(self.sectorial != nil)
    {
        if(self.sectorial.cachedRosters == nil)
        {
            self.sectorial.cachedRosters = [self fetchRostersForArmy:nil orSectorial:self.sectorial];
        }
        return self.sectorial.cachedRosters;
    }
    else
    {
        if(allRosters == nil)
        {
            allRosters = [self fetchRostersForArmy:nil orSectorial:nil];
        }
        return allRosters;
    }
    return nil;
}

-(void) clearRosterCache
{
    if(self.army != nil)
    {
        self.army.cachedRosters = nil;
    }
    
    if(self.sectorial != nil)
    {
        self.sectorial.cachedRosters = nil;
        self.sectorial.army.cachedRosters = nil;
    }

    allRosters = nil;
}

-(NSOrderedSet *)fetchRostersForArmy:(Army*)army orSectorial:(Sectorial*)sectorial
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [Roster entityInManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setIncludesSubentities:TRUE];
    
    if(army)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"army = %@", army];
        [fetchRequest setPredicate:predicate];
    }
    else if(sectorial)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sectorial = %@", sectorial];
        [fetchRequest setPredicate:predicate];
    }
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *nameSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    NSSortDescriptor *armySortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"army.name" ascending:YES];
    NSSortDescriptor *sectorialSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sectorial.name" ascending:YES];
    NSSortDescriptor *dateSortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateCreated" ascending:NO];
    NSArray *sortDescriptors = @[armySortDescriptor, sectorialSortDescriptor, nameSortDescriptor, dateSortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSError* error;
    NSArray* rosters = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil)
    {
        return [[NSOrderedSet alloc] initWithArray:rosters];
    }
    return nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[self getRosters] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Roster* roster = [[self getRosters] objectAtIndex:indexPath.row];
    cell.textLabel.text = roster.name;
    
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
   [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    
    NSDate *date                   = roster.dateCreated;
    NSString *localDateString      = [dateFormatter stringFromDate:date];
    long numbOfOrders = [roster numberOfOrders];
    cell.detailTextLabel.text =  [NSString stringWithFormat:@"%d/%d | %ld Order%@ | %@", (int)[roster totalPoints],[roster pointcapValue], numbOfOrders, numbOfOrders != 1 ? @"s" : @"", localDateString];
    
    cell.imageView.image = [self rosterIcon:roster];
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    Roster* roster = [[self getRosters] objectAtIndex:indexPath.row];
    if(cell)
    {
        if(roster.isCurrentValue || roster.isAlternateValue)
        {
            if(roster.isCurrentValue)
            {
                UIImageView* exchangeNumber = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainRoster.png"]];
                [cell setAccessoryView:exchangeNumber];
            }
            else if(roster.isAlternateValue)
            {
                UIImageView* exchangeNumber = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"altRoster.png"]];
                [cell setAccessoryView:exchangeNumber];
            }
            [cell setSelected:TRUE];
        }
        else
        {
            [cell setSelected:FALSE];
            [cell setAccessoryView:nil];
        }
        
    }
}

-(UIImage*) rosterIcon:(Roster*)roster
{
    NSString* imageTitle;
    if(roster.sectorial)
    {
        imageTitle = [roster.sectorial imageTitle];
    }
    else if(roster.army)
    {
        imageTitle = [roster.army imageTitle];
    }
    
    return [UIImage imageNamed:imageTitle];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Roster* roster = [[self getRosters] objectAtIndex:indexPath.row];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RosterSelection" object:roster];
    [self.tableView reloadData];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Roster* roster = [[self getRosters] objectAtIndex:indexPath.row];
        [roster.managedObjectContext deleteObject:roster];
        [self clearRosterCache];
        // Save the context.
        NSError *error = nil;
         NSManagedObjectContext *context = [roster managedObjectContext];
        if (![context save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }

        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RosterDeletion" object:roster];
        
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}



// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}


/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

#pragma mark Refreshable protocol implementation
-(void)refresh
{
    [self clearRosterCache];
    [self.tableView reloadData];
}

- (IBAction)addRoster:(id)sender
{
    NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
    if(self.army)
    {
        [dict setObject:self.army forKey:@"army"];
    }
    else if(self.sectorial)
    {
        [dict setObject:self.sectorial forKey:@"sectorial"];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewRoster" object:dict];
}

@end
