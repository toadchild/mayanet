//
//  WeaponTableViewCell.h
//  Toolbox
//
//  Created by Paul on 11/1/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Weapon.h"

enum rangeBands {
    range8,
    range16,
    range24,
    range32,
    range40,
    range48,
    range96,
    numRangeBands
};

@interface WeaponTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *weaponName;
@property (strong, nonatomic) IBOutlet UILabel *weaponExtras;

@property (weak, nonatomic) IBOutlet UILabel *range8Distance;
@property (weak, nonatomic) IBOutlet UILabel *range16Distance;
@property (weak, nonatomic) IBOutlet UILabel *range24Distance;
@property (weak, nonatomic) IBOutlet UILabel *range32Distance;
@property (weak, nonatomic) IBOutlet UILabel *range40Distance;
@property (weak, nonatomic) IBOutlet UILabel *range48Distance;
@property (weak, nonatomic) IBOutlet UILabel *range96Distance;

@property (strong, nonatomic) IBOutlet UILabel *range8Modifier;
@property (strong, nonatomic) IBOutlet UILabel *range16Modifier;
@property (strong, nonatomic) IBOutlet UILabel *range24Modifier;
@property (strong, nonatomic) IBOutlet UILabel *range32Modifier;
@property (strong, nonatomic) IBOutlet UILabel *range40Modifier;
@property (strong, nonatomic) IBOutlet UILabel *range48Modifier;
@property (strong, nonatomic) IBOutlet UILabel *range96Modifier;

@property (strong, nonatomic) IBOutlet UILabel *damageValue;
@property (strong, nonatomic) IBOutlet UILabel *burstValue;
@property (strong, nonatomic) IBOutlet UILabel *ammoValue;

+(int) cellHeightForWeapon:(Weapon*) weapon;
+(int) rangeBandDistance:(int) band;

@end
