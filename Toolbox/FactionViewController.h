//
//  ArmyViewController.h
//  Toolbox
//
//  Created by Paul on 10/20/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "Containable.h"

@class Unit, UnitType;

typedef enum SearchTypes{
    SearchTypeName = 0,
    SearchTypeWeapon,
    SearchTypeRules,
} SearchType;

@interface FactionViewController : UITableViewController <Containable, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate>
{
    NSArray* types;
    NSDictionary* unitsByType;
    NSMutableArray* searchTypes;
    NSMutableDictionary* searchUnitsByType;
    int typeOffset;
    bool isSearching;
}
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
- (IBAction)addRoster:(id)sender;
-(Unit*) unitAtIndexPath:(NSIndexPath*)indexPath tableView:(UITableView *)tableView;
-(NSArray*) unitsOfType:(UnitType*)unitType includeMerc:(BOOL)includeMerc;
- (void)configureCell:(UITableViewCell *)cell tableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;
@property (strong, nonatomic) UISearchController *searchController;

-(void)refreshUnits;

@end
