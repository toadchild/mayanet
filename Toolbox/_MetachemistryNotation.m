// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MetachemistryNotation.m instead.

#import "_MetachemistryNotation.h"

const struct MetachemistryNotationAttributes MetachemistryNotationAttributes = {
	.levelTwo = @"levelTwo",
};

@implementation MetachemistryNotationID
@end

@implementation _MetachemistryNotation

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MetachemistryNotation" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MetachemistryNotation";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MetachemistryNotation" inManagedObjectContext:moc_];
}

- (MetachemistryNotationID*)objectID {
	return (MetachemistryNotationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"levelTwoValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"levelTwo"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic levelTwo;

- (BOOL)levelTwoValue {
	NSNumber *result = [self levelTwo];
	return [result boolValue];
}

- (void)setLevelTwoValue:(BOOL)value_ {
	[self setLevelTwo:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveLevelTwoValue {
	NSNumber *result = [self primitiveLevelTwo];
	return [result boolValue];
}

- (void)setPrimitiveLevelTwoValue:(BOOL)value_ {
	[self setPrimitiveLevelTwo:[NSNumber numberWithBool:value_]];
}

#if TARGET_OS_IPHONE

#endif

@end

