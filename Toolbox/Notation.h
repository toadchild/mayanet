#import "_Notation.h"
#import "NotationTypeDef.h"

@interface Notation : _Notation {}
// Custom logic goes here.
-(NotationType) type;
@end
