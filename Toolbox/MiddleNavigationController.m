//
//  MiddeNavigationController.m
//  Toolbox
//
//  Created by Paul on 11/13/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "MiddleNavigationController.h"
#import "IntroViewController.h"

#import "WikiWebViewController.h"
#import "DetailViewController.h"
#import "SettingsViewController.h"
#import "AppDelegate.h"

@interface MiddleNavigationController ()

@end


@implementation MiddleNavigationController

@synthesize detailViewController = _detailViewController;
@synthesize wikiWebViewController = _wikiWebViewController;
@synthesize introViewController = _introViewController;
@synthesize settingsViewController = _settingsViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        self.delegate = self;
        _introViewController = (IntroViewController*)[self topViewController];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    animationInProgress = FALSE;
    self.interactivePopGestureRecognizer.enabled = FALSE;
    //self.detailViewController = (DetailViewController*)self.topViewController;
	// Do any additional setup after loading the view.
}

-(DetailViewController*)detailViewController
{
    if(_detailViewController == nil)
    {
        _detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
    }
    return _detailViewController;
}

-(WikiWebViewController*)wikiWebViewController
{
    if(_wikiWebViewController == nil)
    {
        _wikiWebViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"WikiWebViewController"];
    }
    return _wikiWebViewController;
}

-(SettingsViewController*)settingsViewController
{
    if(_settingsViewController == nil)
    {
        _settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    }
    return _settingsViewController;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) showWikiEntry:(WikiEntry*)wikiEntry
{
    [self showWikiEntry:wikiEntry sender:nil];
}

-(void) showWikiEntry:(WikiEntry*)wikiEntry sender:(id)sender
{
    if(animationInProgress || wikiEntry == nil)
    {
        return;
    }
    //Check to see if the WikiWeb is current View;
    UIViewController* topViewController = [self.viewControllers lastObject];
    
    
    if(topViewController == self.wikiWebViewController)
    {
        [self.wikiWebViewController setWikiEntry:wikiEntry webViewLoadCompletion:nil];
    }
    else
    {
        animationInProgress = TRUE;
        __block id blockself = self;
        __block WikiWebViewController* con = self.wikiWebViewController;
        BOOL shouldPushInstead = [sender isKindOfClass:[DetailViewController class]];
        [self.wikiWebViewController setWikiEntry:wikiEntry webViewLoadCompletion:^{
            if(shouldPushInstead)
            {
                [blockself pushViewController:con animated:FALSE];
            }
            else
            {
                [blockself setViewControllers:@[con] animated:FALSE];
            }
        }];
    }
}

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    animationInProgress = FALSE;
}

-(void)showUnit:(Unit *)unit
{
    if(animationInProgress)
    {
        return;
    }
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.containerViewController clearSelectionRight];
    [self displayDetailViewController];
    [self.self.detailViewController unitSelection:unit];

}

-(void) showUnitDetail:(Unit*)unit
{
    if(animationInProgress)
    {
        return;
    }
    
    [self displayDetailViewController];
    [self.self.detailViewController unitDetailSelection:unit];
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.containerViewController clearSelectionRight];
}

-(void)showUnit:(Unit*)unit groupMember:(GroupMember *)groupMember
{
    if(animationInProgress)
    {
        return;
    }
    [self displayDetailViewController];
    [self.detailViewController unitSelection:unit groupMember:groupMember];
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.containerViewController clearSelectionLeft];

}
    
-(void)showUnitDetail:(Unit*)unit groupMember:(GroupMember *)groupMember
{
    if(animationInProgress)
    {
        return;
    }
    [self displayDetailViewController];
    [self.detailViewController unitDetailSelection:unit groupMember:groupMember];
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.containerViewController clearSelectionLeft];

}

-(void) displayDetailViewController
{
    id topViewController = [self.viewControllers lastObject];
    
    if(topViewController != self.detailViewController)
    {
        animationInProgress = TRUE;
        [self setViewControllers:@[self.detailViewController] animated:FALSE];
    }
}

-(void)showIntro
{
    id topViewController = [self.viewControllers lastObject];
    
    if(topViewController != self.introViewController)
    {
        animationInProgress = TRUE;
        [self setViewControllers:@[self.introViewController] animated:FALSE];
    }
}

-(void)showSettings
{
    id topViewController = [self.viewControllers lastObject];
    
    if(topViewController != self.settingsViewController)
    {
        animationInProgress = TRUE;
        [self setViewControllers:@[self.settingsViewController] animated:FALSE];
    }
}

-(void)showSpecOpsBuild:(SpecOpsBuild*)specOpsBuild
{
    if(animationInProgress)
    {
        return;
    }
    //Check to see if the WikiWeb is current View;
    [self displayDetailViewController];
    [self.detailViewController specOpsBuildSelection:specOpsBuild];
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.containerViewController clearSelectionLeft];
}

-(void)showSpecOpsRebuild:(SpecOpsBuild*)specOpsBuild
{
    if(animationInProgress)
    {
        return;
    }
    //Check to see if the WikiWeb is current View;
    [self displayDetailViewController];
    [self.detailViewController specOpsRebuild:specOpsBuild];
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.containerViewController clearSelectionRight];
}

-(void) invalidateGroupMember:(GroupMember*)groupMember
{
    if(animationInProgress)
    {
        return;
    }
    [self displayDetailViewController];
    [self.detailViewController invalidateGroupMember:groupMember];
}

-(void) setDisplayedRoster:(Roster*)roster
{
    if(animationInProgress)
    {
        return;
    }
    [self.detailViewController setDisplayedRoster:roster];
}

-(void)refresh
{
    [self.detailViewController refresh];
}
@end
