#import "CCSkill.h"
#import "AppDelegate.h"

@interface CCSkill ()

// Private interface goes here.

@end

@implementation CCSkill

+(CCSkill*) skillWithName:(NSString*)name

{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [CCSkill entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchLimit:1];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"name = %@", name];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    NSArray* results = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [results count] == 1)
    {
        return [results objectAtIndex:0];
    }
    return nil;
}

@end
