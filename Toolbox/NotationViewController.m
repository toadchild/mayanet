//
//  NotationViewController.m
//  Toolbox
//
//  Created by Paul on 12/10/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "NotationViewController.h"
#import "GroupMember.h"
#import "Notation.h"
#import "TOCamoNotation.h"
#import "CamoNotation.h"
#import "BootyNotation.h"
#import "Note.h"
#import "CamoMarkerTableViewCell.h"
#import "TOCamoMarkerTableViewCell.h"
#import "BootyTableViewCell.h"

@interface NotationViewController ()
{
    NSArray* notations;
}
@end

@implementation NotationViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.tableView setAlwaysBounceVertical:FALSE];
    
}

-(void)setGroupMember:(GroupMember *)groupMember
{
    notations = [groupMember notations];
    _groupMember = groupMember;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [notations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"Cell";
    
    Notation* notation = [notations objectAtIndex:indexPath.row];
    
    Note* note = [self.groupMember noteFromNotation:notation];
    
    switch([notation type])
    {
        case eCamoNotation:
            CellIdentifier = @"Camo";
            break;
        case eTOCamoNotation:
            CellIdentifier = @"TOCamo";
            break;
        case eBootyNotation:
        case eBootyLevel2Notation:
            CellIdentifier = @"Booty";
            break;
        case eMetachemistryNotation:
        case eMetachemistryLevel2Notation:
            CellIdentifier = @"Metachemistry";
            break;
        case eTextNotation:
            CellIdentifier = @"Text";
            break;
        default:
            break;
    }
    
    ModifiedTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.note = note;
    cell.notation = notation;
    cell.groupMember = self.groupMember;
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

-(void)configureCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath
{
    [cell setNeedsUpdateConstraints];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Notation* notation = [notations objectAtIndex:indexPath.row];
    switch([notation type])
    {
        case eCamoNotation:
        case eTOCamoNotation:
            return 214;
            break;
        case eBootyNotation:
        case eBootyLevel2Notation:
        case eMetachemistryNotation:
        case eMetachemistryLevel2Notation:
            return 88;
            break;
        case eTextNotation:
            return 135;
            break;
        default:
            return 44;
            break;
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

- (IBAction)onClose:(id)sender
{
    [self.delegate onNotationDone];
}
@end
