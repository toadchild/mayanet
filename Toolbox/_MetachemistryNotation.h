// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MetachemistryNotation.h instead.

#import <CoreData/CoreData.h>
#import "Notation.h"

extern const struct MetachemistryNotationAttributes {
	__unsafe_unretained NSString *levelTwo;
} MetachemistryNotationAttributes;

@interface MetachemistryNotationID : NotationID {}
@end

@interface _MetachemistryNotation : Notation {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MetachemistryNotationID* objectID;

@property (nonatomic, strong) NSNumber* levelTwo;

@property (atomic) BOOL levelTwoValue;
- (BOOL)levelTwoValue;
- (void)setLevelTwoValue:(BOOL)value_;

//- (BOOL)validateLevelTwo:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

#endif

@end

@interface _MetachemistryNotation (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveLevelTwo;
- (void)setPrimitiveLevelTwo:(NSNumber*)value;

- (BOOL)primitiveLevelTwoValue;
- (void)setPrimitiveLevelTwoValue:(BOOL)value_;

@end
