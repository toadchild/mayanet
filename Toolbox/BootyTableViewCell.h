//
//  BootyTableViewCell.h
//  Toolbox
//
//  Created by Paul on 12/6/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "ModifiedTableViewCell.h"


@interface BootyTableViewCell : ModifiedTableViewCell
- (IBAction)onBootyLevel1Preference:(id)sender;
- (IBAction)onBootyLevel2Preference:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *bootyLevel1Preference;
@property (strong, nonatomic) IBOutlet UIButton *bootyLevel2Preference;
@property (strong, nonatomic) IBOutlet UILabel *bootyOutcome;

@end
