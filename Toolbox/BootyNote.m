#import "BootyNote.h"


@interface BootyNote ()

// Private interface goes here.

@end


@implementation BootyNote

// Custom logic goes here.
-(NotationType) type
{
    if([self levelTwoValue])
    {
        return eBootyLevel2Notation;
    }
    return eBootyNotation;
}
@end
