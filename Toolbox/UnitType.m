#import "UnitType.h"
#import "AppDelegate.h"

@interface UnitType ()

// Private interface goes here.

@end


@implementation UnitType

// Custom logic goes here.
-(BOOL) isStructure
{
    return ([self.code compare:@"TAG"] == NSOrderedSame || [self.code compare:@"REM"] == NSOrderedSame);
}

+(UnitType*) unitTypeWithCode:(NSString*)code

{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [UnitType entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchLimit:1];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"code = %@", code];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    NSArray* results = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [results count] == 1)
    {
        return [results objectAtIndex:0];
    }
    return nil;
}

@end
