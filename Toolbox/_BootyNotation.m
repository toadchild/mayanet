// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BootyNotation.m instead.

#import "_BootyNotation.h"

const struct BootyNotationAttributes BootyNotationAttributes = {
	.levelTwo = @"levelTwo",
};

@implementation BootyNotationID
@end

@implementation _BootyNotation

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BootyNotation" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BootyNotation";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BootyNotation" inManagedObjectContext:moc_];
}

- (BootyNotationID*)objectID {
	return (BootyNotationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"levelTwoValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"levelTwo"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic levelTwo;

- (BOOL)levelTwoValue {
	NSNumber *result = [self levelTwo];
	return [result boolValue];
}

- (void)setLevelTwoValue:(BOOL)value_ {
	[self setLevelTwo:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveLevelTwoValue {
	NSNumber *result = [self primitiveLevelTwo];
	return [result boolValue];
}

- (void)setPrimitiveLevelTwoValue:(BOOL)value_ {
	[self setPrimitiveLevelTwo:[NSNumber numberWithBool:value_]];
}

#if TARGET_OS_IPHONE

#endif

@end

