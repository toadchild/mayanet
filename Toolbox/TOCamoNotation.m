#import "TOCamoNotation.h"


@interface TOCamoNotation ()

// Private interface goes here.

@end


@implementation TOCamoNotation

// Custom logic goes here.
-(NotationType) type
{
    return eTOCamoNotation;
}
@end
