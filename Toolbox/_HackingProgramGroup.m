// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to HackingProgramGroup.m instead.

#import "_HackingProgramGroup.h"

const struct HackingProgramGroupAttributes HackingProgramGroupAttributes = {
	.name = @"name",
};

const struct HackingProgramGroupRelationships HackingProgramGroupRelationships = {
	.devices = @"devices",
	.programs = @"programs",
};

@implementation HackingProgramGroupID
@end

@implementation _HackingProgramGroup

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"HackingProgramGroup" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"HackingProgramGroup";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"HackingProgramGroup" inManagedObjectContext:moc_];
}

- (HackingProgramGroupID*)objectID {
	return (HackingProgramGroupID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic name;

@dynamic devices;

- (NSMutableSet*)devicesSet {
	[self willAccessValueForKey:@"devices"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"devices"];

	[self didAccessValueForKey:@"devices"];
	return result;
}

@dynamic programs;

- (NSMutableSet*)programsSet {
	[self willAccessValueForKey:@"programs"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"programs"];

	[self didAccessValueForKey:@"programs"];
	return result;
}

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newDevicesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"HackingDevice" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"groups CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newProgramsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"HackingProgram" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"group == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

