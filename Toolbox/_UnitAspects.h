// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UnitAspects.h instead.

#import <CoreData/CoreData.h>

extern const struct UnitAspectsAttributes {
	__unsafe_unretained NSString *arm;
	__unsafe_unretained NSString *bs;
	__unsafe_unretained NSString *bts;
	__unsafe_unretained NSString *cc;
	__unsafe_unretained NSString *cube;
	__unsafe_unretained NSString *fury;
	__unsafe_unretained NSString *hackable;
	__unsafe_unretained NSString *linkable;
	__unsafe_unretained NSString *mov;
	__unsafe_unretained NSString *ph;
	__unsafe_unretained NSString *regular;
	__unsafe_unretained NSString *silhouette;
	__unsafe_unretained NSString *wip;
	__unsafe_unretained NSString *wounds;
} UnitAspectsAttributes;

extern const struct UnitAspectsRelationships {
	__unsafe_unretained NSString *classification;
	__unsafe_unretained NSString *profile;
	__unsafe_unretained NSString *specialRules;
	__unsafe_unretained NSString *unit;
	__unsafe_unretained NSString *weapons;
} UnitAspectsRelationships;

@class UnitClassification;
@class UnitProfile;
@class SpecialRule;
@class Unit;
@class Weapon;

@interface UnitAspectsID : NSManagedObjectID {}
@end

@interface _UnitAspects : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UnitAspectsID* objectID;

@property (nonatomic, strong) NSNumber* arm;

@property (atomic) int16_t armValue;
- (int16_t)armValue;
- (void)setArmValue:(int16_t)value_;

//- (BOOL)validateArm:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* bs;

@property (atomic) int16_t bsValue;
- (int16_t)bsValue;
- (void)setBsValue:(int16_t)value_;

//- (BOOL)validateBs:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* bts;

@property (atomic) int16_t btsValue;
- (int16_t)btsValue;
- (void)setBtsValue:(int16_t)value_;

//- (BOOL)validateBts:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cc;

@property (atomic) int16_t ccValue;
- (int16_t)ccValue;
- (void)setCcValue:(int16_t)value_;

//- (BOOL)validateCc:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* cube;

@property (atomic) int16_t cubeValue;
- (int16_t)cubeValue;
- (void)setCubeValue:(int16_t)value_;

//- (BOOL)validateCube:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* fury;

@property (atomic) int16_t furyValue;
- (int16_t)furyValue;
- (void)setFuryValue:(int16_t)value_;

//- (BOOL)validateFury:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* hackable;

@property (atomic) BOOL hackableValue;
- (BOOL)hackableValue;
- (void)setHackableValue:(BOOL)value_;

//- (BOOL)validateHackable:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* linkable;

@property (atomic) BOOL linkableValue;
- (BOOL)linkableValue;
- (void)setLinkableValue:(BOOL)value_;

//- (BOOL)validateLinkable:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* mov;

//- (BOOL)validateMov:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* ph;

@property (atomic) int16_t phValue;
- (int16_t)phValue;
- (void)setPhValue:(int16_t)value_;

//- (BOOL)validatePh:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* regular;

@property (atomic) BOOL regularValue;
- (BOOL)regularValue;
- (void)setRegularValue:(BOOL)value_;

//- (BOOL)validateRegular:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* silhouette;

@property (atomic) int16_t silhouetteValue;
- (int16_t)silhouetteValue;
- (void)setSilhouetteValue:(int16_t)value_;

//- (BOOL)validateSilhouette:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* wip;

@property (atomic) int16_t wipValue;
- (int16_t)wipValue;
- (void)setWipValue:(int16_t)value_;

//- (BOOL)validateWip:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* wounds;

@property (atomic) int16_t woundsValue;
- (int16_t)woundsValue;
- (void)setWoundsValue:(int16_t)value_;

//- (BOOL)validateWounds:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) UnitClassification *classification;

//- (BOOL)validateClassification:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) UnitProfile *profile;

//- (BOOL)validateProfile:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSOrderedSet *specialRules;

- (NSMutableOrderedSet*)specialRulesSet;

@property (nonatomic, strong) Unit *unit;

//- (BOOL)validateUnit:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSOrderedSet *weapons;

- (NSMutableOrderedSet*)weaponsSet;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newSpecialRulesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newWeaponsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _UnitAspects (SpecialRulesCoreDataGeneratedAccessors)
- (void)addSpecialRules:(NSOrderedSet*)value_;
- (void)removeSpecialRules:(NSOrderedSet*)value_;
- (void)addSpecialRulesObject:(SpecialRule*)value_;
- (void)removeSpecialRulesObject:(SpecialRule*)value_;

- (void)insertObject:(SpecialRule*)value inSpecialRulesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromSpecialRulesAtIndex:(NSUInteger)idx;
- (void)insertSpecialRules:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeSpecialRulesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInSpecialRulesAtIndex:(NSUInteger)idx withObject:(SpecialRule*)value;
- (void)replaceSpecialRulesAtIndexes:(NSIndexSet *)indexes withSpecialRules:(NSArray *)values;

@end

@interface _UnitAspects (WeaponsCoreDataGeneratedAccessors)
- (void)addWeapons:(NSOrderedSet*)value_;
- (void)removeWeapons:(NSOrderedSet*)value_;
- (void)addWeaponsObject:(Weapon*)value_;
- (void)removeWeaponsObject:(Weapon*)value_;

- (void)insertObject:(Weapon*)value inWeaponsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromWeaponsAtIndex:(NSUInteger)idx;
- (void)insertWeapons:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeWeaponsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInWeaponsAtIndex:(NSUInteger)idx withObject:(Weapon*)value;
- (void)replaceWeaponsAtIndexes:(NSIndexSet *)indexes withWeapons:(NSArray *)values;

@end

@interface _UnitAspects (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveArm;
- (void)setPrimitiveArm:(NSNumber*)value;

- (int16_t)primitiveArmValue;
- (void)setPrimitiveArmValue:(int16_t)value_;

- (NSNumber*)primitiveBs;
- (void)setPrimitiveBs:(NSNumber*)value;

- (int16_t)primitiveBsValue;
- (void)setPrimitiveBsValue:(int16_t)value_;

- (NSNumber*)primitiveBts;
- (void)setPrimitiveBts:(NSNumber*)value;

- (int16_t)primitiveBtsValue;
- (void)setPrimitiveBtsValue:(int16_t)value_;

- (NSNumber*)primitiveCc;
- (void)setPrimitiveCc:(NSNumber*)value;

- (int16_t)primitiveCcValue;
- (void)setPrimitiveCcValue:(int16_t)value_;

- (NSNumber*)primitiveCube;
- (void)setPrimitiveCube:(NSNumber*)value;

- (int16_t)primitiveCubeValue;
- (void)setPrimitiveCubeValue:(int16_t)value_;

- (NSNumber*)primitiveFury;
- (void)setPrimitiveFury:(NSNumber*)value;

- (int16_t)primitiveFuryValue;
- (void)setPrimitiveFuryValue:(int16_t)value_;

- (NSNumber*)primitiveHackable;
- (void)setPrimitiveHackable:(NSNumber*)value;

- (BOOL)primitiveHackableValue;
- (void)setPrimitiveHackableValue:(BOOL)value_;

- (NSNumber*)primitiveLinkable;
- (void)setPrimitiveLinkable:(NSNumber*)value;

- (BOOL)primitiveLinkableValue;
- (void)setPrimitiveLinkableValue:(BOOL)value_;

- (NSString*)primitiveMov;
- (void)setPrimitiveMov:(NSString*)value;

- (NSNumber*)primitivePh;
- (void)setPrimitivePh:(NSNumber*)value;

- (int16_t)primitivePhValue;
- (void)setPrimitivePhValue:(int16_t)value_;

- (NSNumber*)primitiveRegular;
- (void)setPrimitiveRegular:(NSNumber*)value;

- (BOOL)primitiveRegularValue;
- (void)setPrimitiveRegularValue:(BOOL)value_;

- (NSNumber*)primitiveSilhouette;
- (void)setPrimitiveSilhouette:(NSNumber*)value;

- (int16_t)primitiveSilhouetteValue;
- (void)setPrimitiveSilhouetteValue:(int16_t)value_;

- (NSNumber*)primitiveWip;
- (void)setPrimitiveWip:(NSNumber*)value;

- (int16_t)primitiveWipValue;
- (void)setPrimitiveWipValue:(int16_t)value_;

- (NSNumber*)primitiveWounds;
- (void)setPrimitiveWounds:(NSNumber*)value;

- (int16_t)primitiveWoundsValue;
- (void)setPrimitiveWoundsValue:(int16_t)value_;

- (UnitClassification*)primitiveClassification;
- (void)setPrimitiveClassification:(UnitClassification*)value;

- (UnitProfile*)primitiveProfile;
- (void)setPrimitiveProfile:(UnitProfile*)value;

- (NSMutableOrderedSet*)primitiveSpecialRules;
- (void)setPrimitiveSpecialRules:(NSMutableOrderedSet*)value;

- (Unit*)primitiveUnit;
- (void)setPrimitiveUnit:(Unit*)value;

- (NSMutableOrderedSet*)primitiveWeapons;
- (void)setPrimitiveWeapons:(NSMutableOrderedSet*)value;

@end
