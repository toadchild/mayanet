#import "HackingProgram.h"
#import "AppDelegate.h"


@interface HackingProgram ()

// Private interface goes here.

@end


@implementation HackingProgram

+(HackingProgram*) programWithName:(NSString*)name

{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [HackingProgram entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchLimit:1];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"name = %@", name];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    NSArray* results = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [results count] == 1)
    {
        return [results objectAtIndex:0];
    }
    return nil;
}

@end
