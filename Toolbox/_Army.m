// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Army.m instead.

#import "_Army.h"

const struct ArmyAttributes ArmyAttributes = {
	.id = @"id",
	.legacyName = @"legacyName",
	.name = @"name",
	.rgbBackground = @"rgbBackground",
	.rgbPrimary = @"rgbPrimary",
	.rgbSecondary = @"rgbSecondary",
	.showMercs = @"showMercs",
};

const struct ArmyRelationships ArmyRelationships = {
	.rosters = @"rosters",
	.sectorials = @"sectorials",
	.specopBaseUnitOption = @"specopBaseUnitOption",
	.specopEquipment = @"specopEquipment",
	.specopSkills = @"specopSkills",
	.specopWeapons = @"specopWeapons",
	.units = @"units",
};

@implementation ArmyID
@end

@implementation _Army

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Army" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Army";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Army" inManagedObjectContext:moc_];
}

- (ArmyID*)objectID {
	return (ArmyID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"showMercsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"showMercs"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic id;

- (int32_t)idValue {
	NSNumber *result = [self id];
	return [result intValue];
}

- (void)setIdValue:(int32_t)value_ {
	[self setId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveIdValue {
	NSNumber *result = [self primitiveId];
	return [result intValue];
}

- (void)setPrimitiveIdValue:(int32_t)value_ {
	[self setPrimitiveId:[NSNumber numberWithInt:value_]];
}

@dynamic legacyName;

@dynamic name;

@dynamic rgbBackground;

@dynamic rgbPrimary;

@dynamic rgbSecondary;

@dynamic showMercs;

- (BOOL)showMercsValue {
	NSNumber *result = [self showMercs];
	return [result boolValue];
}

- (void)setShowMercsValue:(BOOL)value_ {
	[self setShowMercs:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveShowMercsValue {
	NSNumber *result = [self primitiveShowMercs];
	return [result boolValue];
}

- (void)setPrimitiveShowMercsValue:(BOOL)value_ {
	[self setPrimitiveShowMercs:[NSNumber numberWithBool:value_]];
}

@dynamic rosters;

- (NSMutableOrderedSet*)rostersSet {
	[self willAccessValueForKey:@"rosters"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"rosters"];

	[self didAccessValueForKey:@"rosters"];
	return result;
}

@dynamic sectorials;

- (NSMutableSet*)sectorialsSet {
	[self willAccessValueForKey:@"sectorials"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"sectorials"];

	[self didAccessValueForKey:@"sectorials"];
	return result;
}

@dynamic specopBaseUnitOption;

- (NSMutableSet*)specopBaseUnitOptionSet {
	[self willAccessValueForKey:@"specopBaseUnitOption"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specopBaseUnitOption"];

	[self didAccessValueForKey:@"specopBaseUnitOption"];
	return result;
}

@dynamic specopEquipment;

- (NSMutableSet*)specopEquipmentSet {
	[self willAccessValueForKey:@"specopEquipment"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specopEquipment"];

	[self didAccessValueForKey:@"specopEquipment"];
	return result;
}

@dynamic specopSkills;

- (NSMutableSet*)specopSkillsSet {
	[self willAccessValueForKey:@"specopSkills"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specopSkills"];

	[self didAccessValueForKey:@"specopSkills"];
	return result;
}

@dynamic specopWeapons;

- (NSMutableOrderedSet*)specopWeaponsSet {
	[self willAccessValueForKey:@"specopWeapons"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"specopWeapons"];

	[self didAccessValueForKey:@"specopWeapons"];
	return result;
}

@dynamic units;

- (NSMutableSet*)unitsSet {
	[self willAccessValueForKey:@"units"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"units"];

	[self didAccessValueForKey:@"units"];
	return result;
}

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newRostersFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Roster" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"army == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSectorialsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Sectorial" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"army == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecopBaseUnitOptionFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsBaseUnitOption" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"army == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecopEquipmentFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsEquipment" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"army == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecopSkillsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsSpecialRule" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"army == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecopWeaponsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsWeapon" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"army CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newUnitsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Unit" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"army == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

@implementation _Army (RostersCoreDataGeneratedAccessors)
- (void)addRosters:(NSOrderedSet*)value_ {
	[self.rostersSet unionOrderedSet:value_];
}
- (void)removeRosters:(NSOrderedSet*)value_ {
	[self.rostersSet minusOrderedSet:value_];
}
- (void)addRostersObject:(Roster*)value_ {
	[self.rostersSet addObject:value_];
}
- (void)removeRostersObject:(Roster*)value_ {
	[self.rostersSet removeObject:value_];
}
- (void)insertObject:(Roster*)value inRostersAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"rosters"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self rosters]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"rosters"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"rosters"];
}
- (void)removeObjectFromRostersAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"rosters"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self rosters]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"rosters"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"rosters"];
}
- (void)insertRosters:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"rosters"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self rosters]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"rosters"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"rosters"];
}
- (void)removeRostersAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"rosters"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self rosters]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"rosters"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"rosters"];
}
- (void)replaceObjectInRostersAtIndex:(NSUInteger)idx withObject:(Roster*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"rosters"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self rosters]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"rosters"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"rosters"];
}
- (void)replaceRostersAtIndexes:(NSIndexSet *)indexes withRosters:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"rosters"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self rosters]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"rosters"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"rosters"];
}
@end

@implementation _Army (SpecopWeaponsCoreDataGeneratedAccessors)
- (void)addSpecopWeapons:(NSOrderedSet*)value_ {
	[self.specopWeaponsSet unionOrderedSet:value_];
}
- (void)removeSpecopWeapons:(NSOrderedSet*)value_ {
	[self.specopWeaponsSet minusOrderedSet:value_];
}
- (void)addSpecopWeaponsObject:(SpecOpsWeapon*)value_ {
	[self.specopWeaponsSet addObject:value_];
}
- (void)removeSpecopWeaponsObject:(SpecOpsWeapon*)value_ {
	[self.specopWeaponsSet removeObject:value_];
}
- (void)insertObject:(SpecOpsWeapon*)value inSpecopWeaponsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"specopWeapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specopWeapons]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specopWeapons"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"specopWeapons"];
}
- (void)removeObjectFromSpecopWeaponsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"specopWeapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specopWeapons]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specopWeapons"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"specopWeapons"];
}
- (void)insertSpecopWeapons:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"specopWeapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specopWeapons]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specopWeapons"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"specopWeapons"];
}
- (void)removeSpecopWeaponsAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"specopWeapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specopWeapons]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specopWeapons"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"specopWeapons"];
}
- (void)replaceObjectInSpecopWeaponsAtIndex:(NSUInteger)idx withObject:(SpecOpsWeapon*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"specopWeapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specopWeapons]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specopWeapons"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"specopWeapons"];
}
- (void)replaceSpecopWeaponsAtIndexes:(NSIndexSet *)indexes withSpecopWeapons:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"specopWeapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specopWeapons]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specopWeapons"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"specopWeapons"];
}
@end

