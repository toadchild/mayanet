// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SpecialRule.m instead.

#import "_SpecialRule.h"

const struct SpecialRuleAttributes SpecialRuleAttributes = {
	.name = @"name",
};

const struct SpecialRuleRelationships SpecialRuleRelationships = {
	.booty = @"booty",
	.choices = @"choices",
	.choicesParent = @"choicesParent",
	.metachemistry = @"metachemistry",
	.notation = @"notation",
	.profiles = @"profiles",
	.specopsEquipment = @"specopsEquipment",
	.specopsSkill = @"specopsSkill",
	.units = @"units",
	.wikiEntry = @"wikiEntry",
};

@implementation SpecialRuleID
@end

@implementation _SpecialRule

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SpecialRule" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SpecialRule";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SpecialRule" inManagedObjectContext:moc_];
}

- (SpecialRuleID*)objectID {
	return (SpecialRuleID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic name;

@dynamic booty;

- (NSMutableSet*)bootySet {
	[self willAccessValueForKey:@"booty"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"booty"];

	[self didAccessValueForKey:@"booty"];
	return result;
}

@dynamic choices;

- (NSMutableOrderedSet*)choicesSet {
	[self willAccessValueForKey:@"choices"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"choices"];

	[self didAccessValueForKey:@"choices"];
	return result;
}

@dynamic choicesParent;

- (NSMutableSet*)choicesParentSet {
	[self willAccessValueForKey:@"choicesParent"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"choicesParent"];

	[self didAccessValueForKey:@"choicesParent"];
	return result;
}

@dynamic metachemistry;

- (NSMutableSet*)metachemistrySet {
	[self willAccessValueForKey:@"metachemistry"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"metachemistry"];

	[self didAccessValueForKey:@"metachemistry"];
	return result;
}

@dynamic notation;

@dynamic profiles;

- (NSMutableSet*)profilesSet {
	[self willAccessValueForKey:@"profiles"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"profiles"];

	[self didAccessValueForKey:@"profiles"];
	return result;
}

@dynamic specopsEquipment;

- (NSMutableSet*)specopsEquipmentSet {
	[self willAccessValueForKey:@"specopsEquipment"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specopsEquipment"];

	[self didAccessValueForKey:@"specopsEquipment"];
	return result;
}

@dynamic specopsSkill;

- (NSMutableSet*)specopsSkillSet {
	[self willAccessValueForKey:@"specopsSkill"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specopsSkill"];

	[self didAccessValueForKey:@"specopsSkill"];
	return result;
}

@dynamic units;

- (NSMutableSet*)unitsSet {
	[self willAccessValueForKey:@"units"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"units"];

	[self didAccessValueForKey:@"units"];
	return result;
}

@dynamic wikiEntry;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newBootyFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Booty" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"specialRule == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newChoicesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecialRule" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"choicesParent CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newChoicesParentFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecialRule" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"choices CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newMetachemistryFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Metachemistry" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"specialRule CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newProfilesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"UnitOption" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"specialRules CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecopsEquipmentFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsEquipment" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"specialRule == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newSpecopsSkillFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsSpecialRule" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"specialRule == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newUnitsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"UnitAspects" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"specialRules CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

@implementation _SpecialRule (ChoicesCoreDataGeneratedAccessors)
- (void)addChoices:(NSOrderedSet*)value_ {
	[self.choicesSet unionOrderedSet:value_];
}
- (void)removeChoices:(NSOrderedSet*)value_ {
	[self.choicesSet minusOrderedSet:value_];
}
- (void)addChoicesObject:(SpecialRule*)value_ {
	[self.choicesSet addObject:value_];
}
- (void)removeChoicesObject:(SpecialRule*)value_ {
	[self.choicesSet removeObject:value_];
}
- (void)insertObject:(SpecialRule*)value inChoicesAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"choices"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self choices]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"choices"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"choices"];
}
- (void)removeObjectFromChoicesAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"choices"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self choices]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"choices"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"choices"];
}
- (void)insertChoices:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"choices"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self choices]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"choices"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"choices"];
}
- (void)removeChoicesAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"choices"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self choices]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"choices"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"choices"];
}
- (void)replaceObjectInChoicesAtIndex:(NSUInteger)idx withObject:(SpecialRule*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"choices"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self choices]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"choices"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"choices"];
}
- (void)replaceChoicesAtIndexes:(NSIndexSet *)indexes withChoices:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"choices"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self choices]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"choices"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"choices"];
}
@end

