// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UnitClassification.h instead.

#import <CoreData/CoreData.h>

extern const struct UnitClassificationAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *displayName;
} UnitClassificationAttributes;

extern const struct UnitClassificationRelationships {
	__unsafe_unretained NSString *aspects;
} UnitClassificationRelationships;

@class UnitAspects;

@interface UnitClassificationID : NSManagedObjectID {}
@end

@interface _UnitClassification : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UnitClassificationID* objectID;

@property (nonatomic, strong) NSString* code;

//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* displayName;

//- (BOOL)validateDisplayName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *aspects;

- (NSMutableSet*)aspectsSet;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newAspectsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _UnitClassification (AspectsCoreDataGeneratedAccessors)
- (void)addAspects:(NSSet*)value_;
- (void)removeAspects:(NSSet*)value_;
- (void)addAspectsObject:(UnitAspects*)value_;
- (void)removeAspectsObject:(UnitAspects*)value_;

@end

@interface _UnitClassification (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveCode;
- (void)setPrimitiveCode:(NSString*)value;

- (NSString*)primitiveDisplayName;
- (void)setPrimitiveDisplayName:(NSString*)value;

- (NSMutableSet*)primitiveAspects;
- (void)setPrimitiveAspects:(NSMutableSet*)value;

@end
