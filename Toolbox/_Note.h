// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Note.h instead.

#import <CoreData/CoreData.h>

extern const struct NoteAttributes {
	__unsafe_unretained NSString *lastModified;
} NoteAttributes;

extern const struct NoteRelationships {
	__unsafe_unretained NSString *groupMember;
	__unsafe_unretained NSString *notation;
} NoteRelationships;

@class GroupMember;
@class Notation;

@interface NoteID : NSManagedObjectID {}
@end

@interface _Note : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) NoteID* objectID;

@property (nonatomic, strong) NSDate* lastModified;

//- (BOOL)validateLastModified:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) GroupMember *groupMember;

//- (BOOL)validateGroupMember:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Notation *notation;

//- (BOOL)validateNotation:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

#endif

@end

@interface _Note (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveLastModified;
- (void)setPrimitiveLastModified:(NSDate*)value;

- (GroupMember*)primitiveGroupMember;
- (void)setPrimitiveGroupMember:(GroupMember*)value;

- (Notation*)primitiveNotation;
- (void)setPrimitiveNotation:(Notation*)value;

@end
