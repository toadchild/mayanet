// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Unit.h instead.

#import <CoreData/CoreData.h>

extern const struct UnitAttributes {
	__unsafe_unretained NSString *ava;
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *isMerc;
	__unsafe_unretained NSString *isc;
	__unsafe_unretained NSString *legacyIsc;
	__unsafe_unretained NSString *mercAva;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *note;
	__unsafe_unretained NSString *sharedAvaId;
	__unsafe_unretained NSString *strOverride;
	__unsafe_unretained NSString *wOverride;
} UnitAttributes;

extern const struct UnitRelationships {
	__unsafe_unretained NSString *army;
	__unsafe_unretained NSString *aspects;
	__unsafe_unretained NSString *notes;
	__unsafe_unretained NSString *options;
	__unsafe_unretained NSString *profiles;
	__unsafe_unretained NSString *sectorial;
	__unsafe_unretained NSString *specopBuilds;
	__unsafe_unretained NSString *specops;
	__unsafe_unretained NSString *type;
} UnitRelationships;

@class Army;
@class UnitAspects;
@class UnitNote;
@class UnitOption;
@class UnitProfile;
@class Sectorial;
@class SpecOpsBuild;
@class SpecOps;
@class UnitType;

@interface UnitID : NSManagedObjectID {}
@end

@interface _Unit : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) UnitID* objectID;

@property (nonatomic, strong) NSNumber* ava;

@property (atomic) int16_t avaValue;
- (int16_t)avaValue;
- (void)setAvaValue:(int16_t)value_;

//- (BOOL)validateAva:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* id;

@property (atomic) int32_t idValue;
- (int32_t)idValue;
- (void)setIdValue:(int32_t)value_;

//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* isMerc;

@property (atomic) BOOL isMercValue;
- (BOOL)isMercValue;
- (void)setIsMercValue:(BOOL)value_;

//- (BOOL)validateIsMerc:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* isc;

//- (BOOL)validateIsc:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* legacyIsc;

//- (BOOL)validateLegacyIsc:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* mercAva;

@property (atomic) int16_t mercAvaValue;
- (int16_t)mercAvaValue;
- (void)setMercAvaValue:(int16_t)value_;

//- (BOOL)validateMercAva:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* note;

//- (BOOL)validateNote:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* sharedAvaId;

@property (atomic) int32_t sharedAvaIdValue;
- (int32_t)sharedAvaIdValue;
- (void)setSharedAvaIdValue:(int32_t)value_;

//- (BOOL)validateSharedAvaId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* strOverride;

@property (atomic) BOOL strOverrideValue;
- (BOOL)strOverrideValue;
- (void)setStrOverrideValue:(BOOL)value_;

//- (BOOL)validateStrOverride:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* wOverride;

@property (atomic) BOOL wOverrideValue;
- (BOOL)wOverrideValue;
- (void)setWOverrideValue:(BOOL)value_;

//- (BOOL)validateWOverride:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Army *army;

//- (BOOL)validateArmy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) UnitAspects *aspects;

//- (BOOL)validateAspects:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSOrderedSet *notes;

- (NSMutableOrderedSet*)notesSet;

@property (nonatomic, strong) NSOrderedSet *options;

- (NSMutableOrderedSet*)optionsSet;

@property (nonatomic, strong) NSOrderedSet *profiles;

- (NSMutableOrderedSet*)profilesSet;

@property (nonatomic, strong) Sectorial *sectorial;

//- (BOOL)validateSectorial:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *specopBuilds;

- (NSMutableSet*)specopBuildsSet;

@property (nonatomic, strong) SpecOps *specops;

//- (BOOL)validateSpecops:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) UnitType *type;

//- (BOOL)validateType:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newNotesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newOptionsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newProfilesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecopBuildsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _Unit (NotesCoreDataGeneratedAccessors)
- (void)addNotes:(NSOrderedSet*)value_;
- (void)removeNotes:(NSOrderedSet*)value_;
- (void)addNotesObject:(UnitNote*)value_;
- (void)removeNotesObject:(UnitNote*)value_;

- (void)insertObject:(UnitNote*)value inNotesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromNotesAtIndex:(NSUInteger)idx;
- (void)insertNotes:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeNotesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInNotesAtIndex:(NSUInteger)idx withObject:(UnitNote*)value;
- (void)replaceNotesAtIndexes:(NSIndexSet *)indexes withNotes:(NSArray *)values;

@end

@interface _Unit (OptionsCoreDataGeneratedAccessors)
- (void)addOptions:(NSOrderedSet*)value_;
- (void)removeOptions:(NSOrderedSet*)value_;
- (void)addOptionsObject:(UnitOption*)value_;
- (void)removeOptionsObject:(UnitOption*)value_;

- (void)insertObject:(UnitOption*)value inOptionsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromOptionsAtIndex:(NSUInteger)idx;
- (void)insertOptions:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeOptionsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInOptionsAtIndex:(NSUInteger)idx withObject:(UnitOption*)value;
- (void)replaceOptionsAtIndexes:(NSIndexSet *)indexes withOptions:(NSArray *)values;

@end

@interface _Unit (ProfilesCoreDataGeneratedAccessors)
- (void)addProfiles:(NSOrderedSet*)value_;
- (void)removeProfiles:(NSOrderedSet*)value_;
- (void)addProfilesObject:(UnitProfile*)value_;
- (void)removeProfilesObject:(UnitProfile*)value_;

- (void)insertObject:(UnitProfile*)value inProfilesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromProfilesAtIndex:(NSUInteger)idx;
- (void)insertProfiles:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeProfilesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInProfilesAtIndex:(NSUInteger)idx withObject:(UnitProfile*)value;
- (void)replaceProfilesAtIndexes:(NSIndexSet *)indexes withProfiles:(NSArray *)values;

@end

@interface _Unit (SpecopBuildsCoreDataGeneratedAccessors)
- (void)addSpecopBuilds:(NSSet*)value_;
- (void)removeSpecopBuilds:(NSSet*)value_;
- (void)addSpecopBuildsObject:(SpecOpsBuild*)value_;
- (void)removeSpecopBuildsObject:(SpecOpsBuild*)value_;

@end

@interface _Unit (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAva;
- (void)setPrimitiveAva:(NSNumber*)value;

- (int16_t)primitiveAvaValue;
- (void)setPrimitiveAvaValue:(int16_t)value_;

- (NSNumber*)primitiveId;
- (void)setPrimitiveId:(NSNumber*)value;

- (int32_t)primitiveIdValue;
- (void)setPrimitiveIdValue:(int32_t)value_;

- (NSNumber*)primitiveIsMerc;
- (void)setPrimitiveIsMerc:(NSNumber*)value;

- (BOOL)primitiveIsMercValue;
- (void)setPrimitiveIsMercValue:(BOOL)value_;

- (NSString*)primitiveIsc;
- (void)setPrimitiveIsc:(NSString*)value;

- (NSString*)primitiveLegacyIsc;
- (void)setPrimitiveLegacyIsc:(NSString*)value;

- (NSNumber*)primitiveMercAva;
- (void)setPrimitiveMercAva:(NSNumber*)value;

- (int16_t)primitiveMercAvaValue;
- (void)setPrimitiveMercAvaValue:(int16_t)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveNote;
- (void)setPrimitiveNote:(NSString*)value;

- (NSNumber*)primitiveSharedAvaId;
- (void)setPrimitiveSharedAvaId:(NSNumber*)value;

- (int32_t)primitiveSharedAvaIdValue;
- (void)setPrimitiveSharedAvaIdValue:(int32_t)value_;

- (NSNumber*)primitiveStrOverride;
- (void)setPrimitiveStrOverride:(NSNumber*)value;

- (BOOL)primitiveStrOverrideValue;
- (void)setPrimitiveStrOverrideValue:(BOOL)value_;

- (NSNumber*)primitiveWOverride;
- (void)setPrimitiveWOverride:(NSNumber*)value;

- (BOOL)primitiveWOverrideValue;
- (void)setPrimitiveWOverrideValue:(BOOL)value_;

- (Army*)primitiveArmy;
- (void)setPrimitiveArmy:(Army*)value;

- (UnitAspects*)primitiveAspects;
- (void)setPrimitiveAspects:(UnitAspects*)value;

- (NSMutableOrderedSet*)primitiveNotes;
- (void)setPrimitiveNotes:(NSMutableOrderedSet*)value;

- (NSMutableOrderedSet*)primitiveOptions;
- (void)setPrimitiveOptions:(NSMutableOrderedSet*)value;

- (NSMutableOrderedSet*)primitiveProfiles;
- (void)setPrimitiveProfiles:(NSMutableOrderedSet*)value;

- (Sectorial*)primitiveSectorial;
- (void)setPrimitiveSectorial:(Sectorial*)value;

- (NSMutableSet*)primitiveSpecopBuilds;
- (void)setPrimitiveSpecopBuilds:(NSMutableSet*)value;

- (SpecOps*)primitiveSpecops;
- (void)setPrimitiveSpecops:(SpecOps*)value;

- (UnitType*)primitiveType;
- (void)setPrimitiveType:(UnitType*)value;

@end
