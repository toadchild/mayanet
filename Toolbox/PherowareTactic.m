#import "PherowareTactic.h"
#import "AppDelegate.h"

@interface PherowareTactic ()

// Private interface goes here.

@end

@implementation PherowareTactic

+(PherowareTactic*) tacticWithName:(NSString*)name

{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [PherowareTactic entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchLimit:1];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"name = %@", name];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    NSArray* results = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [results count] == 1)
    {
        return [results objectAtIndex:0];
    }
    return nil;
}

+(NSArray*) allTactics
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [PherowareTactic entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError* error;
    NSArray* pherowareTacticList = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if ((error != nil) || (pherowareTacticList == nil)) {
        NSLog(@"Could not load list of pheroware tactics");
        return nil;
    }

    return pherowareTacticList;
}
@end
