#import "GroupMember.h"
#import "Unit.h"
#import "SpecOps.h"
#import "UnitOption.h"
#import "CombatGroup.h"
#import "Roster.h"
#import "SpecOpsBuild.h"
#import "SpecOpsBaseUnitOption.h"
#import "UnitAspects.h"
#import "SpecialRule.h"
#import "Weapon.h"
#import "Note.h"
#import "Notation.h"
#import "UnitProfile.h"
#import "TextNotation.h"

@interface GroupMember ()

// Private interface goes here.

@end


@implementation GroupMember
-(UnitOption*)getUnitOption
{
    if(self.specops != nil)
    {
        return self.specops.baseUnitOption.unitOption;
    }
    else if(self.unitOption)
    {
        return self.unitOption;
    }
    return nil;
}
-(Unit*)unitWithinRoster:(Roster*)roster
{

    Unit* unit;
    if(self.specops != nil)
    {
        unit = [self.specops.baseUnitOption.unitOption getUnit];
    }
    else if(self.unitOption)
    {
        unit = [roster getUnitFrom:self.unitOption];
    }
    else
    {
        unit = self.unitProfile.unit;
    }
    return unit;
}
    
-(BOOL)takesCombatSlotWithinRoster:(Roster*)roster
{
    Unit* unit = [self unitWithinRoster:roster];
    
    BOOL takesCombatSlot = TRUE;
    for(SpecialRule* specialRule in unit.aspects.specialRules)
    {
        if([specialRule.name isEqualToString:@"G: Servant"]
           || [specialRule.name isEqualToString:@"G: Synchronized"]
           || [specialRule.name isEqualToString:@"G: Jumper L1"]
           || [specialRule.name isEqualToString:@"G: Marionette"])
        {
            takesCombatSlot = FALSE;
        }
    }
    if(self.unitOption)
    {
        for(SpecialRule* specialRule in self.unitOption.specialRules)
        {
            if([specialRule.name isEqualToString:@"G: Servant"]
               || [specialRule.name isEqualToString:@"G: Synchronized"]
               || [specialRule.name isEqualToString:@"G: Jumper L1"]
               || [specialRule.name isEqualToString:@"G: Marionette"])
            {
                takesCombatSlot = FALSE;
            }
        }
    }
    if(self.unitProfile)
    {
        for(SpecialRule* specialRule in self.unitProfile.aspects.specialRules)
        {
            if([specialRule.name isEqualToString:@"G: Servant"]
               || [specialRule.name isEqualToString:@"G: Synchronized"]
               || [specialRule.name isEqualToString:@"G: Jumper L1"]
               || [specialRule.name isEqualToString:@"G: Marionette"]
               || [specialRule.name isEqualToString:@"Antipode"])
            {
                takesCombatSlot = FALSE;
            }
        }
    }

    return takesCombatSlot;
}
    
-(BOOL)providesOrderWithinRoster:(Roster*)roster
{
    return [self takesCombatSlotWithinRoster:roster];
}

-(BOOL) isJumperWithinRoster:(Roster*)roster
{
    Unit* unit = [self unitWithinRoster:roster];
    
    BOOL jumper = FALSE;
    for(SpecialRule* specialRule in unit.aspects.specialRules)
    {
        if([specialRule.name compare:@"G: Jumper L1"] == NSOrderedSame)
        {
            jumper = TRUE;
            break;
        }
    }
    for(SpecialRule* specialRule in self.unitOption.specialRules)
    {
        if([specialRule.name compare:@"G: Jumper L1"] == NSOrderedSame)
        {
            jumper = TRUE;
            break;
        }
    }
    return jumper;
}

-(BOOL) isAIBeaconWithinRoster:(Roster*)roster
{
    Unit* unit = [self unitWithinRoster:roster];
    
    BOOL beacon = FALSE;
    for(SpecialRule* specialRule in unit.aspects.specialRules)
    {
        if([specialRule.name compare:@"AI Beacon"] == NSOrderedSame)
        {
            beacon = TRUE;
            break;
        }
    }
    for(SpecialRule* specialRule in self.unitOption.specialRules)
    {
        if([specialRule.name compare:@"AI Beacon"] == NSOrderedSame)
        {
            beacon = TRUE;
            break;
        }
    }
    return beacon;
}

-(BOOL) isAntipodeWithinRoster:(Roster*)roster
{
    BOOL antipode = FALSE;
    for(SpecialRule* specialRule in self.unitProfile.aspects.specialRules)
    {
        if([specialRule.name compare:@"Antipode"] == NSOrderedSame)
        {
            antipode = TRUE;
            break;
        }
    }
    return antipode;
}

-(NSArray*)notations
{
    if(!self.combatGroup || !self.combatGroup.roster)
    {
        return nil;
    }
    NSMutableOrderedSet* notations = [[NSMutableOrderedSet alloc] init];
    Unit* unit = [self unitWithinRoster:self.combatGroup.roster];
    for(SpecialRule* specialRule in unit.aspects.specialRules)
    {
        if(specialRule.notation)
        {
            [notations addObject:specialRule.notation];
        }
    }
    if([unit.profiles count])
    {
        for(UnitProfile* profile in unit.profiles)
        {
            for(SpecialRule* specialRule in profile.aspects.specialRules)
            {
                if(specialRule.notation)
                {
                    [notations addObject:specialRule.notation];
                }
            }
        }
    }
    for(SpecialRule* specialRule in self.unitOption.specialRules)
    {
        if(specialRule.notation)
        {
            [notations addObject:specialRule.notation];
        }
    }
    
    [notations addObject:[TextNotation getTextNotation]];
    return [notations array];
}

-(Note*)noteFromNotation:(Notation*)notation
{
    for(Note* note in self.notes)
    {
        if([note type] == [notation type])
            return note;
    }
    return nil;
    
}
@end
