//
//  ColorCompat.m
//  Toolbox
//
//  Created by Jonathan Polley on 10/27/19.
//  Copyright © 2019 Paul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ColorCompat.h"

UIColor* labelColor() {
    if (@available(iOS 13.0, *)) {
        return [UIColor labelColor];
    } else {
        return [UIColor blackColor];
    }
}

UIColor* systemBackgroundColor() {
    if (@available(iOS 13.0, *)) {
        return [UIColor systemBackgroundColor];
    } else {
        return [UIColor whiteColor];
    }
}

UIColor* deadUnitBackgroundColor() {
    if (@available(iOS 11.0, *)) {
        return [UIColor colorNamed:@"DeadUnitColor"];
    } else {
       return [UIColor colorWithRed:.9 green:.7 blue:.7 alpha:1];
    }
}
    
