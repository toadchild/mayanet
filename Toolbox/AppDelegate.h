//
//  AppDelegate.h
//  Toolbox
//
//  Created by Paul on 10/20/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewManager, Roster;
#import "ContainerViewController.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    Roster* importedRoster;
    UIAlertView* importAlertView;
    BOOL imported;
}
@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (readonly, strong, nonatomic) DetailViewManager *detailViewManager;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) ContainerViewController* containerViewController;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
