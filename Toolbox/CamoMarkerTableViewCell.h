//
//  CamoMarkerTableViewCell.h
//  Toolbox
//
//  Created by Paul on 12/6/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "ModifiedTableViewCell.h"

@class CamoNote;

@interface CamoMarkerTableViewCell : ModifiedTableViewCell

- (IBAction)onCamoNumberSelectionValueChanged:(id)sender;
- (IBAction)onCamoMarkerSelection:(id)sender;
    
@property (strong, nonatomic) IBOutlet UISegmentedControl *numberSegmentControl;
@property (strong, nonatomic) IBOutlet UIButton *camoMarker1;
@property (strong, nonatomic) IBOutlet UIButton *camoMarker2;
@property (strong, nonatomic) IBOutlet UIButton *camoMarker3;
@property (strong, nonatomic) IBOutlet UIButton *camoMarker4;
@property (strong, nonatomic) IBOutlet UIButton *camoMarker5;

-(NSArray*)markers;
@end
