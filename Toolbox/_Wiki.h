// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Wiki.h instead.

#import <CoreData/CoreData.h>

extern const struct WikiAttributes {
	__unsafe_unretained NSString *language;
} WikiAttributes;

extern const struct WikiRelationships {
	__unsafe_unretained NSString *entries;
} WikiRelationships;

@class WikiEntry;

@interface WikiID : NSManagedObjectID {}
@end

@interface _Wiki : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) WikiID* objectID;

@property (nonatomic, strong) NSString* language;

//- (BOOL)validateLanguage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *entries;

- (NSMutableSet*)entriesSet;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newEntriesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _Wiki (EntriesCoreDataGeneratedAccessors)
- (void)addEntries:(NSSet*)value_;
- (void)removeEntries:(NSSet*)value_;
- (void)addEntriesObject:(WikiEntry*)value_;
- (void)removeEntriesObject:(WikiEntry*)value_;

@end

@interface _Wiki (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(NSString*)value;

- (NSMutableSet*)primitiveEntries;
- (void)setPrimitiveEntries:(NSMutableSet*)value;

@end
