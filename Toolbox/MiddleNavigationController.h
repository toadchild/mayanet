//
//  MiddeNavigationController.h
//  Toolbox
//
//  Created by Paul on 11/13/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WikiEntry.h"

@class Unit, GroupMember, SpecOpsBuild, Roster, WikiWebViewController,DetailViewController,IntroViewController, SettingsViewController;

@interface MiddleNavigationController : UINavigationController <UINavigationControllerDelegate>
{
    BOOL animationInProgress;

}

@property (nonatomic, strong, readonly) WikiWebViewController* wikiWebViewController;
@property (nonatomic, strong, readonly) DetailViewController* detailViewController;
@property (nonatomic, strong, readonly) IntroViewController* introViewController;
@property (nonatomic, strong, readonly) SettingsViewController* settingsViewController;
-(void) showIntro;
-(void) showSettings;
-(void) showWikiEntry:(WikiEntry*)wikiEntry;
-(void) showWikiEntry:(WikiEntry*)wikiEntry sender:(id)sender;
-(void) showUnit:(Unit*)unit;
-(void) showUnitDetail:(Unit*)unit;
-(void) showUnit:(Unit*)unit groupMember:(GroupMember*)groupMember;
-(void) showUnitDetail:(Unit*)unit groupMember:(GroupMember*)groupMember;
-(void) invalidateGroupMember:(GroupMember*)groupMember;

-(void) showSpecOpsBuild:(SpecOpsBuild*)specOpsBuild;
-(void) showSpecOpsRebuild:(SpecOpsBuild*)specOpsBuild;
-(void) setDisplayedRoster:(Roster*)roster;
-(void) refresh;
@end
