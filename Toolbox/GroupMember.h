#import "_GroupMember.h"

@class Unit,Roster,Note,Notation;

@interface GroupMember : _GroupMember {}
// Custom logic goes here.
    
-(UnitOption*)getUnitOption;

-(BOOL)takesCombatSlotWithinRoster:(Roster*)roster;
-(BOOL)providesOrderWithinRoster:(Roster*)roster;
-(BOOL) isJumperWithinRoster:(Roster*)roster;
-(BOOL) isAIBeaconWithinRoster:(Roster*)roster;
-(BOOL) isAntipodeWithinRoster:(Roster*)roster;
-(Unit*)unitWithinRoster:(Roster*)roster;

-(NSArray*)notations;
-(Note*)noteFromNotation:(Notation*)notation;
@end
