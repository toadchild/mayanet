//
//  FeedbackFormViewController.h
//  Toolbox
//
//  Created by Paul on 11/6/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FeedbackFormDelegate <NSObject>

-(void) onFeedbackFormComplete;

@end

@interface FeedbackFormViewController : UIViewController
- (IBAction)cancel:(id)sender;
- (IBAction)submit:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *feedbackTextView;
@property id<FeedbackFormDelegate> delegate;
@end
