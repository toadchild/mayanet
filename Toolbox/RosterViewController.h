//
//  RosterViewController.h
//  Toolbox
//
//  Created by Paul on 10/31/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Roster.h"
#import "ExportRosterViewController.h"
#import "NotationViewController.h"
#import "Containable.h"

@interface RosterViewController : UIViewController <UITableViewDelegate, UITableViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,ExportRosterDelegate,NotationDelegate, Containable>
{
    BOOL tableViewEditting;
    NSIndexPath* tappedIndexPath;
    UIActionSheet* deleteActionSheet;
    UIActionSheet* resetActionSheet;
    UIActionSheet* deleteCombatGroupActionSheet;
    UIActionSheet* duplicateActionSheet;
    UIActionSheet* endPlayModeActionSheet;
    NSInteger deleteCombatGroupTag;
    NSIndexPath* lastDeletedIndexPath;
    UIPopoverController* notationPopover;
    UIColor* backgroundColor;
    UIColor* primaryColor;
    UIColor* secondaryColor;
}
- (IBAction)onGoTo:(id)sender;
- (IBAction)exchangeRoster:(id)sender;
- (IBAction)onDuplicateRoster:(id)sender;

- (IBAction)onTogglePlay:(id)sender;
- (IBAction)saveRosterName:(id)sender;
- (IBAction)deleteRoster:(id)sender;
- (IBAction)renameRoster:(id)sender;
- (IBAction)editRoster:(UIBarButtonItem *)sender;
- (IBAction)resetRoster:(id)sender;
- (IBAction)rosterCostCycle:(id)sender;

- (IBAction)onMercenaryToggle:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *mercenaryButton;

-(void)clearSelection;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *editButton;
    
@property (strong, nonatomic) IBOutlet UIBarButtonItem *exportRosterBarButtonItem;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *togglePlayButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *deleteSaveButton;
@property (strong, nonatomic) IBOutlet UILabel *infoLabel;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *goToButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *renameButton;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *resetButton;
@property (strong, nonatomic) IBOutlet UIImageView *factionIconImageView;
@property (strong, nonatomic) IBOutlet UILabel *rosterSWCLabel;
@property (strong, nonatomic) IBOutlet UIButton *rosterCostButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rosterDuplicationButton;
@property (strong, nonatomic) IBOutlet UILabel *rosterExtendedInfo;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *rosterExchangeButton;

@property (strong, nonatomic) IBOutlet UITableView *unitTableView;
@end
