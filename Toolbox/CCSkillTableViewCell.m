//
//  CCSkillTableViewCell.m
//  Toolbox
//
//  Created by Jonathan Polley on 10/25/15.
//  Copyright © 2015 Paul. All rights reserved.
//

#import "CCSkillTableViewCell.h"

@implementation CCSkillTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

+(int) cellHeightForCCSkill:(CCSkill*) skill
{
    if([skill.special length] > 0)
    {
        return 100;
    }
    else
    {
        return 82;
    }
}

@end
