// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Booty.h instead.

#import <CoreData/CoreData.h>

extern const struct BootyAttributes {
	__unsafe_unretained NSString *highRollRange;
	__unsafe_unretained NSString *levelTwo;
	__unsafe_unretained NSString *lowRollRange;
} BootyAttributes;

extern const struct BootyRelationships {
	__unsafe_unretained NSString *notes;
	__unsafe_unretained NSString *specialRule;
	__unsafe_unretained NSString *weapon;
} BootyRelationships;

@class BootyNote;
@class SpecialRule;
@class Weapon;

@interface BootyID : NSManagedObjectID {}
@end

@interface _Booty : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BootyID* objectID;

@property (nonatomic, strong) NSNumber* highRollRange;

@property (atomic) int16_t highRollRangeValue;
- (int16_t)highRollRangeValue;
- (void)setHighRollRangeValue:(int16_t)value_;

//- (BOOL)validateHighRollRange:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* levelTwo;

@property (atomic) BOOL levelTwoValue;
- (BOOL)levelTwoValue;
- (void)setLevelTwoValue:(BOOL)value_;

//- (BOOL)validateLevelTwo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* lowRollRange;

@property (atomic) int16_t lowRollRangeValue;
- (int16_t)lowRollRangeValue;
- (void)setLowRollRangeValue:(int16_t)value_;

//- (BOOL)validateLowRollRange:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *notes;

- (NSMutableSet*)notesSet;

@property (nonatomic, strong) SpecialRule *specialRule;

//- (BOOL)validateSpecialRule:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Weapon *weapon;

//- (BOOL)validateWeapon:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newNotesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _Booty (NotesCoreDataGeneratedAccessors)
- (void)addNotes:(NSSet*)value_;
- (void)removeNotes:(NSSet*)value_;
- (void)addNotesObject:(BootyNote*)value_;
- (void)removeNotesObject:(BootyNote*)value_;

@end

@interface _Booty (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveHighRollRange;
- (void)setPrimitiveHighRollRange:(NSNumber*)value;

- (int16_t)primitiveHighRollRangeValue;
- (void)setPrimitiveHighRollRangeValue:(int16_t)value_;

- (NSNumber*)primitiveLevelTwo;
- (void)setPrimitiveLevelTwo:(NSNumber*)value;

- (BOOL)primitiveLevelTwoValue;
- (void)setPrimitiveLevelTwoValue:(BOOL)value_;

- (NSNumber*)primitiveLowRollRange;
- (void)setPrimitiveLowRollRange:(NSNumber*)value;

- (int16_t)primitiveLowRollRangeValue;
- (void)setPrimitiveLowRollRangeValue:(int16_t)value_;

- (NSMutableSet*)primitiveNotes;
- (void)setPrimitiveNotes:(NSMutableSet*)value;

- (SpecialRule*)primitiveSpecialRule;
- (void)setPrimitiveSpecialRule:(SpecialRule*)value;

- (Weapon*)primitiveWeapon;
- (void)setPrimitiveWeapon:(Weapon*)value;

@end
