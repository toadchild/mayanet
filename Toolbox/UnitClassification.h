#import "_UnitClassification.h"

@interface UnitClassification : _UnitClassification {}

+(UnitClassification*)classificationWithCode:(NSString*)code;
+(UnitClassification*)newClassificationWithCode:(NSString*)code;

@end
