//
//  DataLoadViewController.h
//  Toolbox
//
//  Created by Jonathan Polley on 8/11/16.
//  Copyright © 2016 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataLoadViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *progressMessageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end
