// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DisguisedNote.m instead.

#import "_DisguisedNote.h"

const struct DisguisedNoteRelationships DisguisedNoteRelationships = {
	.unitOption = @"unitOption",
};

@implementation DisguisedNoteID
@end

@implementation _DisguisedNote

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"DisguisedNote" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"DisguisedNote";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"DisguisedNote" inManagedObjectContext:moc_];
}

- (DisguisedNoteID*)objectID {
	return (DisguisedNoteID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic unitOption;

#if TARGET_OS_IPHONE

#endif

@end

