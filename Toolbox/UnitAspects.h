#import "_UnitAspects.h"

@interface UnitAspects : _UnitAspects {}

-(UnitAspects*) clone;
-(void) copyFrom:(UnitAspects*)source;

-(NSString*)convertedMov;

@end

#define FURY_NOT_IMPETUOUS 0
#define FURY_FRENZY 1
#define FURY_IMPETUOUS 2
#define FURY_EXTREME_IMPETUOUS 3
