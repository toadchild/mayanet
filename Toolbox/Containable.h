//
//  Containable.h
//  Toolbox
//
//  Created by Paul Clark on 2/1/14.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Containable <NSObject>

-(void)refresh;
-(void)clearSelection;

@end
