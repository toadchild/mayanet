//
//  PherowareTacticTableViewCell.m
//  Toolbox
//
//  Created by Jonathan Polley on 12/18/16.
//  Copyright (c) 2016 Paul. All rights reserved.
//

#import "PherowareTacticTableViewCell.h"

@implementation PherowareTacticTableViewCell

-(void) updateConstraints
{
    /* Field order:
     Name
     Skill Type
     Target (optional)
     Range
     Modifiers (optional)
     Effect
     Special (optional)
     */
    
    if(_tactic.target && [_tactic.target length])
    {
        self.targetHeight.constant = 18;
        self.rangeTopConstraint.constant = 3;
    }
    else
    {
        self.targetHeight.constant = 0;
        self.rangeTopConstraint.constant = 0;
    }
    
    self.modifiers.hidden = false;
    self.modifiersHeightConstraint.constant = 59.5;
    
    if(_tactic.special && [_tactic.special length])
    {
        self.specialHeightConstraint.constant = 18;
        self.specialTopConstraint.constant = 3;
    }
    else
    {
        self.specialHeightConstraint.constant = 0;
        self.specialTopConstraint.constant = 0;
    }
    
    [super updateConstraints];
}

@end
