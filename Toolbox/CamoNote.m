#import "CamoNote.h"


@interface CamoNote ()

// Private interface goes here.

@end


@implementation CamoNote

// Custom logic goes here.
-(NotationType) type
{
    return eCamoNotation;
}
@end
