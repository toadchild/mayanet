//
//  LeftNavigationController.m
//  Toolbox
//
//  Created by Paul on 11/14/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "LeftNavigationController.h"
#import "ArmyViewController.h"
#import "SectorialViewController.h"
#import "Army.h"
#import "Sectorial.h"
#import "AppDelegate.h"
@interface LeftNavigationController ()

@end



@implementation LeftNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.interactivePopGestureRecognizer.enabled = FALSE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)goToArmy:(Army*)army
{
    //First check to see if action needs to be taken
    id topViewController = [self topViewController];
    if([topViewController isKindOfClass:[ArmyViewController class]])
    {
        //We are viewing an army
        
        //Check if the armies are the same, if so, then nothing needs to occur
        ArmyViewController* armyViewController = (ArmyViewController*)topViewController;
        if(armyViewController.army == army)
        {
            //No action necessary
            return;
        }
    }
    
    //Look to see if Army is in the stack
    for(id viewController in self.viewControllers)
    {
        if([viewController isKindOfClass:[ArmyViewController class]])
        {
            ArmyViewController* armyViewController = (ArmyViewController*)viewController;
            if(armyViewController.army == army)
            {
                NSUInteger index = [self.viewControllers indexOfObject:armyViewController];
                
                NSRange range = NSMakeRange(0, index+1);
                
                NSArray* controllers = [NSArray arrayWithArray:[self.viewControllers objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:range]]];
                
                [self setViewControllers:controllers animated:TRUE];
                return;
            }
        }
    }
    
    //Army is not in stack, force army to stack
    id rootViewController = [self.viewControllers objectAtIndex:0];
    ArmyViewController* armyViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ArmyViewController"];
    armyViewController.army = army;
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    armyViewController.managedObjectContext = delegate.managedObjectContext;
    [self setViewControllers:@[rootViewController,armyViewController] animated:TRUE];
    
}

-(void)goToSectorial:(Sectorial*)sectorial
{
    //First check to see if action needs to be taken
    id topViewController = [self topViewController];
    if([topViewController isKindOfClass:[SectorialViewController class]])
    {
        //We are viewing an army
        
        //Check if the armies are the same, if so, then nothing needs to occur
        SectorialViewController* sectorialViewController = (SectorialViewController*)topViewController;
        if(sectorialViewController.sectorial == sectorial)
        {
            //No action necessary
            return;
        }
    }
    
    //Look to see if Sectorial is in the stack
    for(id viewController in self.viewControllers)
    {
        if([viewController isKindOfClass:[SectorialViewController class]])
        {
            SectorialViewController* sectorialViewController = (SectorialViewController*)viewController;
            if(sectorialViewController.sectorial == sectorial)
            {
                NSUInteger index = [self.viewControllers indexOfObject:sectorialViewController];
                
                NSRange range = NSMakeRange(0, index+1);
                
                NSArray* controllers = [NSArray arrayWithArray:[self.viewControllers objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:range]]];
                
                [self setViewControllers:controllers animated:TRUE];
                return;
            }
        }
    }
    
    //Look to see if Army is in the stack, if so, add new Sectorial View Controller, and show
    for(id viewController in self.viewControllers)
    {
        if([viewController isKindOfClass:[ArmyViewController class]])
        {
            ArmyViewController* armyViewController = (ArmyViewController*)viewController;
            if(armyViewController.army == sectorial.army)
            {
                NSUInteger index = [self.viewControllers indexOfObject:armyViewController];
                
                NSRange range = NSMakeRange(0, index+1);
                
                NSMutableArray* controllers = [NSMutableArray arrayWithArray:[self.viewControllers objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:range]]];
                
                SectorialViewController* sectorialViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SectorialViewController"];
                sectorialViewController.sectorial = sectorial;
                AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                sectorialViewController.managedObjectContext = delegate.managedObjectContext;
                [controllers addObject:sectorialViewController];
                
                [self setViewControllers:controllers animated:TRUE];
                return;
            }
        }
    }
    
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //No Army, and no sectorial in view controller stack, so make fresh controllers for army, and sectorial
    
    SectorialViewController* sectorialViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SectorialViewController"];
    sectorialViewController.sectorial = sectorial;
    sectorialViewController.managedObjectContext = delegate.managedObjectContext;
    
    ArmyViewController* armyViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ArmyViewController"];
    armyViewController.army = sectorial.army;
    armyViewController.title = sectorial.army.name;
    armyViewController.managedObjectContext = delegate.managedObjectContext;
    
    NSArray* controllers = @[[self.viewControllers firstObject],armyViewController,sectorialViewController];
    
    [self setViewControllers:controllers animated:TRUE];
}

@end
