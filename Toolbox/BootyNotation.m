#import "BootyNotation.h"


@interface BootyNotation ()

// Private interface goes here.

@end


@implementation BootyNotation

// Custom logic goes here.
-(NotationType) type
{
    if([self levelTwoValue])
    {
        return eBootyLevel2Notation;
    }
    return eBootyNotation;
}
@end
