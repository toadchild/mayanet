#import "Unit.h"
#import "UnitAspects.h"
#import "UnitOption.h"
#import "UnitProfile.h"
#import "SpecialRule.h"
#import "AppDelegate.h"
#import "Util.h"
#import "Army.h"
#import "Sectorial.h"
#import "GroupMember.h"
#import "CombatGroup.h"

@interface Unit ()

// Private interface goes here.

@end


@implementation Unit

// Create object cache for fast unit lookups.
static NSMutableDictionary* unitDictionary = nil;
+(void) initialize
{
    unitDictionary = [[NSMutableDictionary alloc] init];
}

-(NSArray*) getUnitOptions
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [UnitOption entityInManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setIncludesSubentities:TRUE];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"cost" ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"swc" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor,sortDescriptor2];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"(ANY units == %@)", self];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    return [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
}

-(BOOL) hasSpecialRule:(NSString*)specialRuleName
{
    for(SpecialRule* specialRule in self.aspects.specialRules)
    {
        if([specialRule.name compare:specialRuleName] == NSOrderedSame)
            return TRUE;
    }
    return FALSE;
}

+(Unit*)masterUnitWithID:(int)id withDelegate:(AppDelegate*)delegate
{
    NSString* idKey = [NSString stringWithFormat:@"%d", id];
    Unit* unit = [unitDictionary valueForKey:idKey];
    if(unit)
    {
        return unit;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [Unit entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"id = %d AND army = %@ and sectorial = %@", id, nil, nil];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    NSArray* units = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSAssert([units count] <= 1, @"Duplicate master unit entries for id %d", id);
    if(error == nil && [units count] == 1)
    {
        Unit* unit = [units firstObject];
        [unitDictionary setObject:unit forKey:idKey];
        return unit;
    }
    return nil;
}

+(NSArray*)allUnitsWithID:(int)id
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [Unit entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"id = %d", id];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    NSArray* units = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [units count] > 0)
    {
        return units;
    }
    return nil;
}

-(UnitOption*)optionWithID:(int)id
{
    for(UnitOption* unitOption in self.options)
    {
        if(unitOption.idValue == id)
        {
            return unitOption;
        }
    }
    return nil;
}

-(UnitOption*)optionWithMaxID
{
    UnitOption* maxOption = [self.options firstObject];

    for(UnitOption* unitOption in self.options)
    {
        if(unitOption.idValue > maxOption.idValue)
        {
            maxOption = unitOption;
        }
    }
    return maxOption;
}

-(Unit*) clone
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    Unit* cloneUnit = [Unit insertInManagedObjectContext:[delegate managedObjectContext]];
    cloneUnit.aspects = [UnitAspects insertInManagedObjectContext:[delegate managedObjectContext]];
    [cloneUnit copyFrom:self];

    return cloneUnit;
}

-(void) copyFrom:(Unit *)source
{
    //Copy over
    self.isc = source.isc;
    self.legacyIsc = source.legacyIsc;
    self.id = source.id;
    self.name = source.name;
    self.type = source.type;
    self.ava = source.ava;
    self.sharedAvaId = source.sharedAvaId;
    self.mercAva = 0;
    self.wOverride = source.wOverride;
    self.strOverride = source.strOverride;
    
    // Do not copy army or sectorial fields.

    // Deep copy of aspects
    [self.aspects copyFrom:source.aspects];
    
    // Deep copy of options
    // If we have more options than the source, delete any excess from all rosters.
    // Always delete the option with the highest ID value.  The options list may be backwards.
    while([self.options count] > [source.options count])
    {
        [self deleteOption:[self optionWithMaxID]];
    }
    [self copyOptionsFrom:source];
    
    // Deep copy of profiles
    [self copyProfilesFrom:source];
    
    // Fix up set of per-option profiles
    for(UnitOption *sourceOption in source.options)
    {
        UnitOption *destOption = [self optionWithID:sourceOption.idValue];
        NSAssert(destOption != nil, @"Unable to find destination option with id %d", sourceOption.idValue);
        [destOption.enabledUnitProfileSet removeAllObjects];

        for(UnitProfile *sourceProfile in sourceOption.enabledUnitProfile)
        {
            for(UnitProfile *destProfile in self.profiles)
            {
                if(destProfile.idValue == sourceProfile.idValue)
                {
                    [destOption.enabledUnitProfileSet addObject:destProfile];
                }
            }
        }
    }
}

// Be careful when changing things here; older rosters will have pointers directly to UnitOption objects which are shared between multiple Unit instances in different armies/sectorials.
-(void) copyOptionsFrom:(Unit*)source
{
    NSAssert([self.optionsSet count] <= [source.optionsSet count], @"Unable to safely reduce the number of options");
    
    // Overwrite existing options
    for(UnitOption* source_option in source.optionsSet)
    {
        bool option_found = FALSE;
        for(UnitOption* dest_option in self.optionsSet)
        {
            if(source_option.id == dest_option.id)
            {
                [dest_option copyFrom: source_option];
                option_found = TRUE;
            }
        }
        if(!option_found)
        {
            [self.optionsSet addObject:[source_option clone]];
        }
    }
}

-(void) copyProfilesFrom:(Unit*)source
{
    NSAssert([self.profilesSet count] <= [source.profilesSet count], @"Unable to safely reduce the number of profiles");
    
    // Overwrite existing profiles
    int p;
    for(p = 0; p < [self.profilesSet count]; p++)
    {
        [self.profilesSet[p] copyFrom: source.profilesSet[p]];
    }
    
    // Add new profiles
    for(; p < [source.profilesSet count]; p++)
    {
        [self.profilesSet addObject:[source.profilesSet[p] clone]];
    }
}

+(NSArray*)allUnitsWithLegacyISC:(NSString*)isc
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [Unit entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"legacyIsc = %@", isc];
    [fetchRequest setPredicate:predicate];
    
    NSError* error;
    NSArray* units = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [units count] > 0)
    {
        return units;
    }
    return nil;
}

-(UnitOption*)optionWithLegacyCode:(NSString*)code
{
    for(UnitOption* unitOption in self.options)
    {
        if([unitOption.legacyCode isEqualToString:code])
        {
            return unitOption;
        }
    }
    return nil;
}

-(NSString*)imageTitle
{
    return [NSString stringWithFormat:@"images/%@_logo.png", [self id]];
}

-(NSString*)webImageTitle
{
    return [NSString stringWithFormat:@"%@%@%@", URL_ROOT, [self id], WEB_IMAGE_SUFFIX];
}

-(void)deleteOption:(UnitOption*)option
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [self.optionsSet removeObject:option];

    // Remove this option from all rosters.
    for(GroupMember* member in option.groupMembers)
    {
        [member.combatGroup removeGroupMember:member withChildren:TRUE];
    }
    
    // Delete the option itself.
    [[delegate managedObjectContext] deleteObject:option];
}

-(NSString*)dumpDebug
{
    NSMutableArray* strings = [NSMutableArray array];
    [strings addObject: [NSString stringWithFormat:@"%p %@ in %@ %@", self, self.name, self.army.name, self.sectorial.name]];
    [strings addObject:@"  Profiles:"];
    for(UnitProfile* profile in self.profiles) {
        [strings addObject: [NSString stringWithFormat:@"    %p %@ %@", profile, profile.id, profile.name]];
        if ([profile.specificOptions count])
        {
            [strings addObject:@"    Owning Options:"];
            for(UnitOption* option in profile.specificOptions) {
                [strings addObject: [NSString stringWithFormat:@"      %p %@ %@", option, option.id, option.code]];
            }
        }
    }
    [strings addObject:@"  Options:"];
    for(UnitOption* option in self.options) {
        [strings addObject: [NSString stringWithFormat:@"    %p %@ %@", option, option.id, option.code]];
        if ([option.enabledUnitProfile count])
        {
            [strings addObject:@"    Attached Profiles:"];
            for(UnitProfile* profile in option.enabledUnitProfile)
            {
                [strings addObject: [NSString stringWithFormat:@"      %p %@ %@", profile, profile.id, profile.name]];
            }
        }
    }
    
    return [strings componentsJoinedByString:@"\n"];
}

-(void)delete
{
    // If there is no id, then this unit was deleted.
    for(UnitOption* option in self.optionsSet)
    {
        [self deleteOption:option];
    }
    
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    // Delete the unit.
    [[delegate managedObjectContext] deleteObject:self];
}

@end
