//
//  DataParser.h
//  Toolbox
//
//  Created by Paul on 10/20/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface DataParser : NSObject
{
    NSMutableDictionary* weaponsDictionary;
    NSMutableDictionary* specialRulesDictionary;
    NSMutableDictionary* typesDictionary;
    NSMutableDictionary* specOpsWeaponsDictionary;
    NSMutableDictionary* wikiNameDictionary;
    NSMutableDictionary* hackingProgramDictionary;
    NSMutableDictionary* hackingProgramGroupDictionary;
    NSMutableDictionary* hackingDeviceDictionary;
    NSMutableDictionary* pherowareTacticDictionary;
    NSMutableDictionary* ccSkillDictionary;
    AppDelegate* appDelegate;
    UILabel* progressMessageLabel;
}
-(DataParser*)initWithAppDelegate:(AppDelegate*)delegate andProgressLabel:(UILabel*)label;
-(void) parse;
-(void) migrate;
@end
