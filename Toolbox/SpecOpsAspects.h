#import "_SpecOpsAspects.h"
@class Unit;
@interface SpecOpsAspects : _SpecOpsAspects {}
// Custom logic goes here.
-(SpecOpsAspects*) createCopy;
-(NSDictionary*)toDictionary;
-(void)fromDictionary:(NSDictionary*)fromDictionary;

-(NSString*)descriptionWithUnit:(Unit*)unit;
-(void)reset;
@end
