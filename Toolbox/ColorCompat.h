//
//  ColorCompat.h
//  Toolbox
//
//  Created by Jonathan Polley on 10/27/19.
//  Copyright © 2019 Jonathan Polley. All rights reserved.
//

UIColor* labelColor(void);
UIColor* systemBackgroundColor(void);
UIColor* deadUnitBackgroundColor(void);
