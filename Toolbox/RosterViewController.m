//
//  RosterViewController.m
//  Toolbox
//
//  Created by Paul on 10/31/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "RosterViewController.h"
#import "CombatGroup.h"
#import "GroupMember.h"
#import "UnitOption.h"
#import "Unit.h"
#import "UnitProfile.h"
#import "Army.h"
#import "Sectorial.h"
#import "UnitOptionRosterBuildTableViewCell.h"
#import "Util.h"
#import "SpecOpsBuild.h"
#import "SpecOpsBaseUnitOption.h"
#import "AppDelegate.h"
#import "ExportRosterViewController.h"
#import "UIColor+ColorPalette.h"
#import "UnitAspects.h"
#import "SpecialRule.h"
#import "ArmyViewController.h"
#import "SectorialViewController.h"
#import "ColorCompat.h"

enum RosterViewDisplayMode
{
    eMainRoster,
    eAltRoster
} typedef RosterViewDisplayMode;

@interface RosterViewController ()
{
    Roster* _currentRoster;
    Roster* _altRoster;
}
@property (strong, nonatomic) Roster *roster;
@property (nonatomic) RosterViewDisplayMode rosterViewDisplayMode;
@end


@implementation RosterViewController

-(void)setRosterViewDisplayMode:(RosterViewDisplayMode)rosterViewDisplayMode
{
    _rosterViewDisplayMode = rosterViewDisplayMode;
    switch (self.rosterViewDisplayMode) {
        case eMainRoster:
        self.rosterExchangeButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"mainRoster.png"] style:UIBarButtonItemStylePlain target:self action:@selector(exchangeRoster:)];
        break;
        
        case eAltRoster:
        self.rosterExchangeButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"altRoster.png"] style:UIBarButtonItemStylePlain target:self action:@selector(exchangeRoster:)];
        break;
    }
    NSArray* toolBarItems = [
                             [self.toolbarItems objectsAtIndexes:
                              [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [self.toolbarItems count]-1)]
                              ]
                             arrayByAddingObject:self.rosterExchangeButton
                             ];
    [self setToolbarItems:toolBarItems animated:TRUE];
    [self setDetailViewControllerDisplayedRoster];
    [self invalidateGroupMember:nil];
    [self configureView];
}

-(void)setRoster:(Roster *)roster
{
    switch(self.rosterViewDisplayMode)
    {
        case eMainRoster:
        _currentRoster = roster;
        [Roster setCurrentRoster:_currentRoster];
        break;
        
        case eAltRoster:
        _altRoster = roster;
        [Roster setAlternateRoster:_altRoster];
        break;
    }
    [self setDetailViewControllerDisplayedRoster];
    [self invalidateGroupMember:nil];
    [self configureView];

}

-(void)setDetailViewControllerDisplayedRoster
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    MiddleNavigationController* con = delegate.containerViewController.middleViewController;
    [con setDisplayedRoster:self.roster];
}

-(Roster*)roster
{
    switch(self.rosterViewDisplayMode)
    {
        case eMainRoster:
        return _currentRoster;
        break;
        
        case eAltRoster:
        return _altRoster;
        break;
    }
    return nil;
}

-(void)calculateRosterColors
{
    if(self.roster.sectorial)
    {
        backgroundColor = [self.roster.sectorial backgroundColor];
        primaryColor = [self.roster.sectorial primaryColor];
        secondaryColor = [self.roster.sectorial secondaryColor];
    }
    else
    {
        backgroundColor = [self.roster.army backgroundColor];
        primaryColor = [self.roster.army primaryColor];
        secondaryColor = [self.roster.army secondaryColor];
    }
}
    
    
#pragma mark Init
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    tableViewEditting = FALSE;
    if(self.roster.isPlaying == nil)
    {
        self.roster.isPlayingValue = FALSE;
    }
    _currentRoster = [Roster currentRoster];
    _altRoster = [Roster alternateRoster];

    [self setDetailViewControllerDisplayedRoster];
    [self.roster updateValidation];
    [self configureView];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(middleViewControllerInitialization:)
     name:@"MiddleViewControllerInitialization"
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(newRoster:)
     name:@"NewRoster"
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(rosterSelection:)
     name:@"RosterSelection"
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(rosterDeletion:)
     name:@"RosterDeletion"
     object:nil];
    
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(unitOptionAddition:)
     name:@"UnitOptionAddition"
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(unitOptionChange:)
     name:@"UnitOptionChange"
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(specOpsAddition:)
     name:@"SpecOpsBuildAddition"
     object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:FALSE animated:TRUE];
    [self setMercenariesInRoster];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"ShowExportRoster"])
    {
        ExportRosterViewController *con = [segue destinationViewController];
        con.delegate = self;
        [con setRoster:self.roster];
    }
    else if([segue.identifier isEqualToString:@"ShowNotation"])
    {

    }
}

-(void) refreshLeft
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.containerViewController refreshLeft];
}

-(void) refresh
{
    if(self.roster)
    {
        [self.roster updateValidation];
    }
    [self configureView];
}

-(void)clearSelection
{
    [self.unitTableView deselectRowAtIndexPath:[self.unitTableView indexPathForSelectedRow] animated:TRUE];
}

#pragma mark View Configuration
- (void)configureView
{
    [self setPlayEnabled: self.roster.isPlayingValue];
    [self configureViewControls];
    [self.unitTableView reloadData];
    [self refreshLeft];
    
    [self calculateRosterColors];
    if(self.roster != nil)
    {
        [self.navigationController.navigationBar setBarTintColor:backgroundColor];
        [self.navigationController.navigationBar setTintColor:secondaryColor];
        
        [self.navigationController.navigationBar setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:
          primaryColor,
          NSForegroundColorAttributeName,nil]];
    }
    else
    {
        [self.navigationController.navigationBar setBarTintColor:[UIColor barColor]];
        [self.navigationController.navigationBar setTintColor:[UIColor barItemColor]];
        
        [self.navigationController.navigationBar setTitleTextAttributes:
         [NSDictionary dictionaryWithObjectsAndKeys:
          [UIColor titleColor],
          NSForegroundColorAttributeName,nil]];
    }
}

-(void) configureViewControls
{
    if(self.roster != nil && (self.roster.army || self.roster.sectorial))
    {

        [self setEnabledRosterControlViews:TRUE];
    }
    else
    {
        [self setEnabledRosterControlViews:FALSE];
    }
}

-(void) setEnabledRosterControlViews:(BOOL)enabled
{
    [self.goToButton setEnabled:enabled];
    [self.renameButton setEnabled:enabled && !self.roster.isPlayingValue];
    [self.resetButton setEnabled:enabled];
    [self.deleteSaveButton setEnabled:enabled && !self.roster.isPlayingValue];
    [self.exportRosterBarButtonItem setEnabled:enabled];
    [self.infoLabel setHidden:enabled];
    [self.rosterExtendedInfo setHidden:!enabled];
    [self.navigationItem.rightBarButtonItem setEnabled:enabled];
    [self.editButton setEnabled:enabled];
    [self.rosterDuplicationButton setEnabled:enabled && !self.roster.isPlayingValue];
    
    if(enabled)
    {
        [self setTitle:self.roster.name];
        self.factionIconImageView.image = [self rosterImage];
        [self.unitTableView reloadData];
        
        if(!self.roster.isPlayingValue)
        {
            [self.rosterCostButton setTitle:
            [NSString stringWithFormat:@"%ld / %ld",
             (long)[self.roster totalPoints],
             (long)[self.roster.pointcap integerValue]
             ]
            forState:UIControlStateNormal];
            
            if([self.roster totalPoints] > [self.roster.pointcap integerValue])
            {
                [self.rosterCostButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            }
            else
            {
                [self.rosterCostButton setTitleColor:labelColor() forState:UIControlStateNormal];
            }
        }
        else
        {
            [self.rosterCostButton setTitle:
             [NSString stringWithFormat:@"%ld / %ld",
             (long)[self.roster totalPlayingPoints] - [self.roster totalDeadPoints],
             (long)[self.roster totalPlayingPointCap]
             ]
            forState:UIControlStateNormal];
            
            [self.rosterCostButton setTitleColor:labelColor() forState:UIControlStateNormal];
        }
        
        if(!self.roster.isPlayingValue)
        {
            self.rosterSWCLabel.text =
            [NSString stringWithFormat:@"%.1f / %.1f SWC",
             [self.roster totalSWC],
             floor([self.roster.pointcap doubleValue]/50.0)
             ];
            
            if([self.roster totalSWC] > [self.roster maxSWC])
            {
                self.rosterSWCLabel.textColor = [UIColor redColor];
            }
            else
            {
                self.rosterSWCLabel.textColor = labelColor();
            }
            
            
            int ptsPerOrder = 0;
            if([self.roster numberOfOrders] != 0)
            {
                ptsPerOrder = (int)round((double)[self.roster totalPoints]/(double)[self.roster numberOfOrders]);
            }
            [self.rosterExtendedInfo setText:[NSString stringWithFormat:@"Orders: %ld | Order Cost: %dpts",(long)[self.roster numberOfOrders], ptsPerOrder]];
        }
        else
        {
            double ratio = 100.0 * ([self.roster totalPlayingPoints] - [self.roster totalDeadPoints]) / [self.roster totalPlayingPointCap];
            self.rosterSWCLabel.text =
            [NSString stringWithFormat:@"(%.1f%%)",ratio];
            
            long totalOrdersLeft = [self.roster numberOfOrdersCountingDead:FALSE];
            long ptsToRetreat = [self.roster pointsToRetreat];

            if(ptsToRetreat <= 0)
            {
                self.rosterSWCLabel.textColor = [UIColor redColor];
            }
            else
            {
                self.rosterSWCLabel.textColor = labelColor();
            }
            
            NSString* retreatString = ptsToRetreat > 0 ? [NSString stringWithFormat:@"To Retreat: %ldpts",ptsToRetreat] : @"In Retreat!";
            [self.rosterExtendedInfo setText:[NSString stringWithFormat:@"Orders Left: %ld | %@",totalOrdersLeft,retreatString]];
        }
    }
    else
    {
        self.factionIconImageView.image = nil;
        [self.rosterCostButton setTitle: @"" forState:UIControlStateNormal];
        self.rosterSWCLabel.text = @"";
        [self setTitle:@"No Roster"];
    }
}

-(UIImage*) rosterImage
{
    NSString* imageTitle;
    if(self.roster.sectorial)
    {
        imageTitle = [self.roster.sectorial imageTitle];
    }
    else if(self.roster.army)
    {
        imageTitle = [self.roster.army imageTitle];
    }
    
    [self setTitle:self.roster.name];
    return [UIImage imageNamed:imageTitle];
}

-(void) configureSectionView:(UIView*)sectionView inSection:(NSInteger)section
{
    [sectionView setBackgroundColor:systemBackgroundColor()];
    if([self showFailedValidationSection] && section == 0)
    {
        UILabel* title = [[UILabel alloc] initWithFrame:CGRectMake(10,0,160,40)];
        [title setFont:[UIFont boldSystemFontOfSize:17]];
        title.text = [NSString stringWithFormat:@"%lu Validation Error%@",(unsigned long)[self.roster.failedValidators count],[self.roster.failedValidators count] == 1 ? @"" : @"s"];
        [sectionView addSubview:title];
        return;
    }
    CombatGroup* combatGroup = [self combatGroupAtSection:section];

    NSInteger nImpetuous = [combatGroup numberOfImpetuousOrdersCountingDead:!self.roster.isPlayingValue];
    NSInteger nIrregulars = [combatGroup numberOfIrregularOrdersCountingDead:!self.roster.isPlayingValue];
    NSInteger nRegulars = [combatGroup numberOfRegularOrdersCountingDead:!self.roster.isPlayingValue];
    NSInteger nFigures = [combatGroup numberOfFiguresCountingDead:!self.roster.isPlayingValue];
    
    BOOL showRemoveButton = ((section != [self.roster failedValidation]) && ([combatGroup numberOfOrdersCountingDead:TRUE] == 0 || [self.unitTableView isEditing]));
    
    if([self.roster isPlaying] && [self.roster isPlayingValue])
    {
        showRemoveButton = FALSE;
    }
    if(showRemoveButton)
    {
        UIButton* removeCombatGroup = [UIButton buttonWithType:UIButtonTypeSystem];
        removeCombatGroup.tag = section;
        [removeCombatGroup setFrame: CGRectMake(320-60,0,60,40)];
        [removeCombatGroup addTarget:self action:@selector(removeCombatGroup:) forControlEvents:UIControlEventTouchUpInside];
        
        [removeCombatGroup setTitle: @"Remove" forState:UIControlStateNormal];
        [sectionView addSubview:removeCombatGroup];
    }
    
    UILabel* title = [[UILabel alloc] initWithFrame:CGRectMake(10,0,160,40)];
    [title setFont:[UIFont boldSystemFontOfSize:17]];
    title.text = [NSString stringWithFormat:@"%@#%ld", showRemoveButton ? @"" : @"Group ", (long)(section+1-([self showFailedValidationSection] ? 1 : 0))];
    [sectionView addSubview:title];
    
    int deltaX = 30;
    
    if(showRemoveButton)
    {
        deltaX = 85;
    }
    
    if(nImpetuous > 0)
    {
        UIImageView* regularIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"images/impetuous_icon.png"]];
        regularIcon.frame = CGRectMake(320-deltaX, 10, 20, 20);
        [sectionView addSubview:regularIcon];
        deltaX +=  25; //10 pont margins;
        
        title = [[UILabel alloc] initWithFrame:CGRectMake(320-deltaX,0,20,40)];
        [title setFont:[UIFont boldSystemFontOfSize:17]];
        [title setTextAlignment:NSTextAlignmentCenter];
        title.text = [NSString stringWithFormat:@"%ld",(long)nImpetuous];
        [sectionView addSubview:title];
        deltaX += 25; //10 pont margins;
    }

    if(nIrregulars > 0)
    {
        UIImageView* regularIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"images/irregular_icon.png"]];
        regularIcon.frame = CGRectMake(320-deltaX, 10, 20, 20);
        [sectionView addSubview:regularIcon];
        deltaX +=  25; //10 pont margins;
        
        title = [[UILabel alloc] initWithFrame:CGRectMake(320-deltaX,0,20,40)];
        [title setFont:[UIFont boldSystemFontOfSize:17]];
        [title setTextAlignment:NSTextAlignmentCenter];
        title.text = [NSString stringWithFormat:@"%ld",(long)nIrregulars];
        [sectionView addSubview:title];
        deltaX +=  25; //10 pont margins;
    }

    if(nRegulars > 0)
    {
        UIImageView* regularIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"images/regular_icon.png"]];
        regularIcon.frame = CGRectMake(320-deltaX, 10, 20, 20);
        [sectionView addSubview:regularIcon];
        deltaX += 25; //10 pont margins;
        
        title = [[UILabel alloc] initWithFrame:CGRectMake(320-deltaX,0,25,40)];
        [title setFont:[UIFont boldSystemFontOfSize:17]];
        [title setTextAlignment:NSTextAlignmentCenter];
        title.text = [NSString stringWithFormat:@"%ld",(long)nRegulars];
        [sectionView addSubview:title];
        deltaX +=  25; //10 pont margins;
    }
    
    //Set combat group count

    title = [[UILabel alloc] initWithFrame:CGRectMake(320-deltaX-40,0,65,40)];
    [title setFont:[UIFont boldSystemFontOfSize:17]];
    [title setTextAlignment:NSTextAlignmentCenter];
    if(nFigures > 10)
    {
        [title setTextColor:[UIColor redColor]];
    }
    title.text = [NSString stringWithFormat:@"[%ld/10]",(long)nFigures];
    [sectionView addSubview:title];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    if([self showFailedValidationSection] && indexPath.section == 0)
    {
        RosterValidator* validator = [self.roster.failedValidators objectAtIndex:indexPath.row];
        cell.textLabel.text = validator.name;
        cell.imageView.image = [[UIImage imageNamed:@"791-warning.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.imageView setTintColor:[UIColor redColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return;
    }
    CombatGroup* combatGroup = [self combatGroupAtSection:indexPath.section];
    GroupMember* groupMember = [combatGroup.groupMembers objectAtIndex:indexPath.row];
    
    UnitOptionRosterBuildTableViewCell* buildCell = (UnitOptionRosterBuildTableViewCell*)cell;
    
    buildCell.unitNameLabel.textColor = labelColor();
    
    [buildCell setBackgroundColor:systemBackgroundColor()];
    
    buildCell.unitOptionCodeLabel.textColor = labelColor();
    buildCell.unitAvaLabel.textColor = labelColor();
    buildCell.unitOptionCostLabel.textColor = labelColor();
    buildCell.unitAvaLabel.textColor = labelColor();
    [buildCell.unitOptionSWCLabel setHidden:FALSE];
    [buildCell.costSeperatorLabel setHidden:FALSE];

    
    NSNumber* cost = nil;
    if(groupMember.specops != nil)
    {
        Unit* unit;
        UnitOption* unitOption;
        unitOption = groupMember.specops.baseUnitOption.unitOption;
        
        buildCell.unitOptionCodeLabel.text = [NSString stringWithFormat:@"%ld XP", (long)[groupMember.specops getCost]];
        
        buildCell.unitOptionCostLabel.text = [NSString stringWithFormat:@"%d", [[unitOption cost] intValue]];
        
        buildCell.unitOptionSWCLabel.text = [NSString stringWithFormat:@"%g", [[unitOption swc] doubleValue]];
        
        cost = [unitOption cost];
        
        unit = groupMember.specops.unit;
        
        buildCell.unitNameLabel.text = unit.name;
        
        NSString* imageTitle = [unit imageTitle];
        buildCell.unitIconImageView.image = [UIImage imageNamed:imageTitle];
        
        if(!self.roster.isPlayingValue)
        {
            if([unitOption isLt] && [self.roster getLtCountUptoGroupMember:groupMember] != 0)
            {
                buildCell.unitOptionCodeLabel.textColor = [UIColor redColor];
            }
            
            long avaCount = [self.roster getAVACountUptoGroupMember:groupMember];
            Unit* avaUnit = unit;
            if (avaUnit.sharedAvaId) {
                if (avaUnit.sectorial) {
                    avaUnit = [avaUnit.sectorial unitWithID:avaUnit.sharedAvaIdValue];
                } else {
                    avaUnit = [avaUnit.army unitWithID:avaUnit.sharedAvaIdValue];
                }
            }
            
            buildCell.unitAvaLabel.text = [NSString stringWithFormat:@"%ld/%@", avaCount+1, (avaUnit.ava == nil) ? @"T" : [avaUnit.ava stringValue]];
            
            if(avaUnit.ava != nil)
            {
                if([avaUnit.ava integerValue] < avaCount + 1)
                {
                    buildCell.unitNameLabel.textColor = [UIColor redColor];
                    buildCell.unitAvaLabel.textColor = [UIColor redColor];
                }
            }
            [buildCell.costView setHidden:FALSE];
        }
        
    }
    else if(groupMember.unitOption)
    {
        Unit* unit;
        UnitOption* unitOption;
        unitOption = groupMember.unitOption;
        
        buildCell.unitOptionCodeLabel.text = unitOption.code;
        
        unit = [self.roster getUnitFrom:unitOption];
        
        if(unitOption.isIndependentValue)
        {
            buildCell.unitOptionCostLabel.text = [NSString stringWithFormat:@"%d", [[unitOption independentCost] intValue]];
            
            buildCell.unitOptionSWCLabel.text = [NSString stringWithFormat:@"%g", [[unitOption independentSWC] doubleValue]];
            
            cost = [unitOption independentCost];
        }
        else
        {
            buildCell.unitOptionCostLabel.text = [NSString stringWithFormat:@"%d", [[unitOption cost] intValue]];
        
            buildCell.unitOptionSWCLabel.text = [NSString stringWithFormat:@"%g", [[unitOption swc] doubleValue]];
            
            cost = [unitOption cost];
        }
        
        long maxAva = unit.avaValue;
        long avaCount = [self.roster getAVACountUptoGroupMember:groupMember];
        if(unit.mercAva && avaCount >= maxAva){
            buildCell.unitNameLabel.text = [NSString stringWithFormat:@"%@ (Mercenary)", unit.name];
        } else {
            buildCell.unitNameLabel.text = unit.name;
        }
        
        if(!self.roster.isPlayingValue)
        {
            if([unitOption isLt] && [self.roster getLtCountUptoGroupMember:groupMember] != 0)
            {
                buildCell.unitOptionCodeLabel.textColor = [UIColor redColor];
            }
            
            // If mercs mode is currently enabled, and a non AVA T unit has additional merc AVA, count it
            if(unit.ava && self.roster.isSoldiersOfFortuneValue && unit.mercAva){
                maxAva += unit.mercAvaValue;
            }
            
            if (unit.isMercValue && !self.roster.isSoldiersOfFortuneValue) {
                maxAva = 0;
            }
            
            buildCell.unitAvaLabel.text = [NSString stringWithFormat:@"%ld/%@",avaCount+1, (unit.ava == NULL) ? @"T" : [[NSNumber numberWithLong:maxAva] stringValue]];
            
            if(unit.ava)
            {
                if(maxAva < avaCount+1)
                {
                    buildCell.unitNameLabel.textColor = [UIColor redColor];
                    buildCell.unitAvaLabel.textColor = [UIColor redColor];
                }
            }
            [buildCell.costView setHidden:FALSE];
        }
        
        NSString* imageTitle = [unit imageTitle];
        buildCell.unitIconImageView.image = [UIImage imageNamed:imageTitle];
    }
    else if(groupMember.unitProfile)
    {
        UnitProfile* unitProfile;
        unitProfile = groupMember.unitProfile;
        buildCell.unitOptionCodeLabel.text = @"";
        
        buildCell.unitNameLabel.text = unitProfile.name;
        
        // Show G: Sync units with their cost in parenthesis
        SpecialRule *gSync = [SpecialRule specialRuleWithName:@"G: Synchronized"];
        if([unitProfile.aspects.specialRules containsObject:gSync])
        {
            buildCell.unitOptionCostLabel.text = [NSString stringWithFormat:@"(%d)", [[unitProfile independentCost] intValue]];
        }
        else
        {
            buildCell.unitOptionCostLabel.text = [NSString stringWithFormat:@"%d", [[unitProfile independentCost] intValue]];
        }
        
        buildCell.unitOptionSWCLabel.text = [NSString stringWithFormat:@"%g", [[unitProfile independentSWC] doubleValue]];
        
        cost = [unitProfile independentCost];
        
        buildCell.unitAvaLabel.text = @"N/A";
        
        NSString* imageTitle = [unitProfile imageTitle];
        buildCell.unitIconImageView.image = [UIImage imageNamed:imageTitle];
    }
    
    if(self.roster.isPlayingValue)
    {
        buildCell.unitOptionCodeLabel.textColor = labelColor();
        [buildCell.unitOptionSWCLabel setHidden:TRUE];
        [buildCell.costSeperatorLabel setHidden:TRUE];
        buildCell.unitAvaLabel.textColor = [UIColor lightGrayColor];
        if(cost && [cost integerValue] != 0)
        {
            [buildCell.unitAvaLabel setText:[NSString stringWithFormat:@"%d%%",(int)([cost doubleValue]/(double)[self.roster totalPoints]*100)]];
        }
        else
        {
            [buildCell.unitAvaLabel setText:@""];
        }
        if(groupMember.isDeadValue)
        {
            [buildCell setBackgroundColor:deadUnitBackgroundColor()];
            [buildCell.unitAvaLabel setTextColor:[UIColor colorWithRed:1 green:.3 blue:.3 alpha:1]];
        }
        else
        {
            [buildCell setBackgroundColor:systemBackgroundColor()];
        }
    }
    

}


-(void) prepareRosterForUnit:(Unit*)unit unitOption:(UnitOption*)unitOption
{
    if(self.roster == nil)
    {
        if(unit.sectorial != nil)
        {
            self.roster = [Roster createNewRosterWithSectorial:unit.sectorial name:[Roster newRosterTitle] pointCap:300];
        }
        else if(unit.army != nil)
        {
            self.roster = [Roster createNewRosterWithArmy:unit.army name:[Roster newRosterTitle] pointCap:300];
        }
        [Roster setCurrentRoster:self.roster];
    }
    else if([self.roster numberOfModels] == 0 && ![self.roster testUnit:unit unitOption:unitOption])
    {
        [self.roster.managedObjectContext deleteObject:self.roster];
        
        if(unit.sectorial != nil)
        {
            self.roster = [Roster createNewRosterWithSectorial:unit.sectorial name:[Roster newRosterTitle] pointCap:300];
        }
        else if(unit.army != nil)
        {
            self.roster = [Roster createNewRosterWithArmy:unit.army name:[Roster newRosterTitle] pointCap:300];
        }
        [Roster setCurrentRoster:self.roster];
    }
}

-(void)invalidateGroupMember:(GroupMember*)groupMember
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    MiddleNavigationController* con = delegate.containerViewController.middleViewController;
    [con invalidateGroupMember:groupMember];
}


#pragma mark NSNotifcation Response

-(void) middleViewControllerInitialization:(NSNotification*) notification
{
    [self setDetailViewControllerDisplayedRoster];
}

-(void) newRoster:(NSNotification*) notification
{
    NSDictionary* dict = [notification object];
    Sectorial* sectorial = [dict objectForKey:@"sectorial"];
    Army* army = [dict objectForKey:@"army"];
    if(sectorial != nil)
    {
        self.roster = [Roster createNewRosterWithSectorial:sectorial name:[Roster newRosterTitle] pointCap:300];
    }
    else if(army != nil)
    {
        self.roster = [Roster createNewRosterWithArmy:army name:[Roster newRosterTitle] pointCap:300];
    }
    if(self.rosterViewDisplayMode == eMainRoster)
    {
        [Roster setCurrentRoster:self.roster];
    }
    else
    {
        [Roster setAlternateRoster:self.roster];
    }
    [self configureView];
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [delegate.containerViewController displayRightViewController:TRUE animationCompletionBlock:nil];
    if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [delegate.containerViewController displayLeftViewController:FALSE animationCompletionBlock:nil];
    }
}
-(void) rosterSelection:(NSNotification*) notification
{
    Roster* newRoster = (Roster*)[notification object];
    
    BOOL displayModeSwitch = FALSE;
    switch (self.rosterViewDisplayMode)
    {
        case eMainRoster:
        if(newRoster == _altRoster)
        {
            self.rosterViewDisplayMode = eAltRoster;
            displayModeSwitch = TRUE;
        }
        break;
        
        case eAltRoster:
        if(newRoster == _currentRoster)
        {
            self.rosterViewDisplayMode = eMainRoster;
            displayModeSwitch = TRUE;
        }
        break;
    }
    if(!displayModeSwitch)
    {
        if(self.roster != newRoster)
        {
            self.roster = newRoster;
            [self refresh];
        }
    }
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.containerViewController displayRightViewController:TRUE animationCompletionBlock:nil];
    if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [delegate.containerViewController displayLeftViewController:FALSE animationCompletionBlock:nil];
    }
}

-(void) rosterDeletion:(NSNotification*) notification
{
    Roster* newRoster = (Roster*)[notification object];
    if(self.roster == newRoster)
    {
        self.roster = nil;
    }
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.containerViewController displayRightViewController:TRUE animationCompletionBlock:nil];
    if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [delegate.containerViewController displayLeftViewController:FALSE animationCompletionBlock:nil];
    }
}

-(void) unitOptionAddition:(NSNotification*) notification
{
    NSDictionary* dict = [notification object];
    if(self.roster == nil || (self.roster && !self.roster.isPlayingValue))
        [self unitAddition:[dict objectForKey:@"unit"] unitOption:[dict objectForKey:@"unitOption"]];
}

-(void) unitOptionChange:(NSNotification*) notification
{
    NSDictionary* dict = [notification object];
    if(self.roster && !self.roster.isPlayingValue)
    {
        [self unitOptionChange:[dict objectForKey:@"unitOption"] groupMember:[dict objectForKey:@"groupMember"]];
    }
}

-(void) unitAddition:(Unit*)unit unitOption:(UnitOption*) unitOption
{
    [self prepareRosterForUnit:unit unitOption:unitOption];
    [self.roster addUnit:unit unitOption:unitOption];
    [self configureView];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowRightPane" object:nil];
    [self scrollToBottom];
}

-(void) unitOptionChange:(UnitOption*) unitOption groupMember:(GroupMember*)groupMember
{
    [self.roster changeGroupMember:groupMember toOption:unitOption];
    [self.roster updateValidation];
    [self configureView];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowRightPane" object:nil];
}



-(void) specOpsAddition:(NSNotification*) notification
{
    NSDictionary* dict = [notification object];
    if(self.roster == nil || (self.roster && !self.roster.isPlayingValue))
        [self specOpsBuildAddition:[dict objectForKey:@"specOpsBuild"]];
}
-(void) specOpsBuildAddition:(SpecOpsBuild*)specOpsBuild
{
    [self prepareRosterForUnit:specOpsBuild.unit unitOption:specOpsBuild.baseUnitOption.unitOption ];
    [self.roster addSpecOpsBuild:specOpsBuild];
    [self configureView];
    [self scrollToBottom];
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    MiddleNavigationController* middleCon = delegate.containerViewController.middleViewController;
    [middleCon showSpecOpsRebuild:specOpsBuild];
}

- (void)scrollToBottom
{
    CGPoint bottomOffset = CGPointMake(0, self.unitTableView.contentSize.height - self.unitTableView.bounds.size.height);
    if ( bottomOffset.y > 0 ) {
        [self.unitTableView setContentOffset:bottomOffset animated:YES];
    }
}

#pragma mark Action Selectors
-(IBAction)addCombatGroup:(UIButton*)sender
{
    CombatGroup* combatGroup = [CombatGroup insertInManagedObjectContext:self.roster.managedObjectContext];
    [self.roster addCombatGroupsObject:combatGroup];
    [self.roster.managedObjectContext save:nil];
    [self.unitTableView reloadData];
}

-(IBAction)removeCombatGroup:(UIButton*)sender
{
    CombatGroup* combatGroup = [self combatGroupAtSection:sender.tag];
    
    if([combatGroup.groupMembers count] != 0)
    {
        deleteCombatGroupTag = sender.tag;
        deleteCombatGroupActionSheet = [[UIActionSheet alloc] initWithTitle:@"Are you sure?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete" otherButtonTitles: nil];
        [deleteCombatGroupActionSheet showFromRect:sender.frame inView:sender.superview animated:TRUE];
    }
    else
    {
        [self deleteCombatGroup:sender.tag];
    }
}

- (IBAction)editRoster:(UIBarButtonItem *)sender {
    if([self isActionSheetShown]) return;
    tableViewEditting = !tableViewEditting;
    [self.unitTableView setEditing:tableViewEditting animated:TRUE];
    [self.unitTableView reloadData];
}

- (IBAction)resetRoster:(id)sender {
    if([self isActionSheetShown]) return;
    resetActionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Are you sure you want to reset %@?", self.roster.name] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Reset" otherButtonTitles: nil];
    [resetActionSheet showFromBarButtonItem:sender animated:TRUE];
}

-(BOOL) isActionSheetShown
{
    return deleteActionSheet || resetActionSheet || endPlayModeActionSheet || deleteCombatGroupActionSheet || duplicateActionSheet;
}

-(void)resetRoster
{
    if(self.roster.isPlayingValue)
    {
        [self.roster resetDead];
        [self configureView];
        return;
    }
    [self.roster reset];
    [self configureView];
    [self invalidateGroupMember:nil];


    
}


- (void)refreshTable {
    [self.unitTableView reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self saveNameTextField];
    return NO;
}

- (IBAction)saveRosterName:(id)sender {
    [self saveNameTextField];
}

-(void)saveNameTextField
{
    self.roster.name = self.nameTextField.text;
    [self.roster.managedObjectContext save:nil];
    [self setTitle:self.roster.name];
    [self toggleToolBarForRenameEnabled:FALSE];
    [self.nameTextField resignFirstResponder];
    [self.roster clearRosterCache];
    [self refreshLeft];
}

- (IBAction)renameRoster:(id)sender {
    if([self isActionSheetShown]) return;
    [self toggleToolBarForRenameEnabled:TRUE];
    [self.nameTextField setText:self.roster.name];
    [self.nameTextField becomeFirstResponder];
    [self.nameTextField setSelectedTextRange:[self.nameTextField textRangeFromPosition:self.nameTextField.beginningOfDocument toPosition:self.nameTextField.endOfDocument]];
}

-(void) toggleToolBarForRenameEnabled:(BOOL)renameEnabled
{
    [self.nameTextField setHidden:!renameEnabled];
    
    UIBarButtonItem* seperator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    if(renameEnabled)
    {
        UIBarButtonItem* saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveRosterName:)];
        [self.toolbar setItems:@[seperator, saveButton]];
    }
    else
    {
        [self.toolbar setItems:@[self.goToButton, seperator, self.renameButton, seperator, self.resetButton, seperator, self.deleteSaveButton]];
    }
    

}
- (IBAction)deleteRoster:(id)sender {
    if([self isActionSheetShown]) return;
    deleteActionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Are you sure you want to delete %@?", self.roster.name] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Delete" otherButtonTitles: nil];
    [deleteActionSheet showFromBarButtonItem:sender animated:TRUE];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(actionSheet == deleteActionSheet && buttonIndex == 0)
    {
        [self deleteRoster];
        deleteActionSheet = nil;
    }
    else if(actionSheet == resetActionSheet && buttonIndex == 0)
    {
        [self resetRoster];
        resetActionSheet = nil;
        
    }
    else if(actionSheet == endPlayModeActionSheet && buttonIndex == 0)
    {
        [self endPlayMode];
        endPlayModeActionSheet = nil;
    }
    else if(actionSheet == deleteCombatGroupActionSheet && buttonIndex == 0)
    {
        [self deleteCombatGroup:deleteCombatGroupTag];
        deleteCombatGroupActionSheet = nil;
        deleteCombatGroupTag = NSNotFound;
    }
    else if(actionSheet == duplicateActionSheet && buttonIndex == 0)
    {
        [self duplicateRoster];
        duplicateActionSheet = nil;
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    deleteActionSheet = nil;
    resetActionSheet = nil;
    endPlayModeActionSheet = nil;
    deleteCombatGroupActionSheet = nil;
    duplicateActionSheet = nil;
}

-(void)actionSheetCancel:(UIActionSheet *)actionSheet
{
    deleteActionSheet = nil;
    resetActionSheet = nil;
    endPlayModeActionSheet = nil;
    deleteCombatGroupActionSheet = nil;
}

-(void)deleteRoster
{
    NSManagedObjectContext* moc = self.roster.managedObjectContext;
    [self.roster.managedObjectContext deleteObject:self.roster];
    [moc save:nil];
    self.roster = nil;
    [self invalidateGroupMember:nil];
    [self configureView];
}
    
-(void) deleteCombatGroup:(NSInteger)section
{
    CombatGroup* combatGroup = [self combatGroupAtSection:section];
    for (GroupMember* groupMember in combatGroup.groupMembers) {
        [self invalidateGroupMember:groupMember];
    }
    [self.roster.managedObjectContext deleteObject:combatGroup];
    [self.roster.managedObjectContext save:nil];
    [self configureView];
}

#pragma mark Table View Methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self showFailedValidationSection] && indexPath.section == 0)
    {
        return;
    }
    
    CombatGroup* combatGroup = [self combatGroupAtSection:indexPath.section];
    GroupMember* groupMember = [combatGroup.groupMembers objectAtIndex:indexPath.row];
    
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    MiddleNavigationController* middleCon = delegate.containerViewController.middleViewController;
    
    if(groupMember.specops != nil)
    {
        if(!self.roster.isPlayingValue)
        {
            [middleCon showSpecOpsRebuild:groupMember.specops];
        }
        else
        {
            [middleCon showSpecOpsBuild:groupMember.specops];
        }
        [delegate.containerViewController clearSelectionLeft];
    }
    else if(groupMember.unitOption)
    {
        UnitOption* unitOption = groupMember.unitOption;
        
        Unit* unit = [self.roster getUnitFrom:unitOption];
        
        if(!self.roster.isPlayingValue)
        {
            [middleCon showUnit:unit groupMember: groupMember];
        }
        else
        {
            [middleCon showUnitDetail:unit groupMember: groupMember];
        }
    }
    else if(groupMember.unitProfile)
    {
        UnitOption* unitOption = groupMember.parentGroupMember.unitOption;
        
        Unit* unit = [self.roster getUnitFrom:unitOption];
        
        if(!self.roster.isPlayingValue)
        {
            [middleCon showUnit:unit groupMember: groupMember];
        }
        else
        {
            [middleCon showUnitDetail:unit groupMember: groupMember];
        }
    }
}
    
-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    if([self showFailedValidationSection] && indexPath.section == 0)
    {
        return;
    }
    
    CombatGroup* combatGroup = [self combatGroupAtSection:indexPath.section];
    GroupMember* groupMember = [combatGroup.groupMembers objectAtIndex:indexPath.row];
    
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    MiddleNavigationController* middleCon = delegate.containerViewController.middleViewController;
    
    if(groupMember.specops != nil)
    {
        [middleCon showSpecOpsBuild:groupMember.specops];
    }
    if(!self.roster.isPlayingValue)
    {
        if(groupMember.unitOption)
        {
            UnitOption* unitOption = groupMember.unitOption;
            
            Unit* unit = [self.roster getUnitFrom:unitOption];
            

            [middleCon showUnitDetail:unit groupMember: groupMember];
        }
        else if(groupMember.unitProfile)
        {
            UnitOption* unitOption = groupMember.parentGroupMember.unitOption;
            
            Unit* unit = [self.roster getUnitFrom:unitOption];
            
            [middleCon showUnitDetail:unit groupMember: groupMember];
        }
    }
    else
    {
        UINavigationController *nav = [self.storyboard instantiateViewControllerWithIdentifier:@"NotationController"];
        NotationViewController *con = [nav.viewControllers lastObject];
        Unit* unit = [groupMember unitWithinRoster:self.roster];
        [con setTitle:unit.isc];
        con.delegate = self;
        [con setGroupMember:groupMember];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        notationPopover = [[UIPopoverController alloc] initWithContentViewController:nav];
        [notationPopover presentPopoverFromRect:cell.bounds inView:cell.contentView permittedArrowDirections:UIPopoverArrowDirectionAny animated:TRUE];
    }
    [tableView selectRowAtIndexPath:indexPath animated:FALSE scrollPosition:UITableViewScrollPositionNone];
}

// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.roster isPlaying] && self.roster.isPlayingValue)
    {
        return NO;
    }
    if([self showFailedValidationSection] && indexPath.section == 0)
    {
        return NO;
    }
    CombatGroup* combatGroup = [self combatGroupAtSection:indexPath.section];
    
    GroupMember* groupMember = [combatGroup.groupMembers objectAtIndex:indexPath.row];
    
    if(groupMember.unitProfile != nil && groupMember.parentGroupMember != nil)
    {
        return NO;
    }
    
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self showFailedValidationSection] && indexPath.section == 0)
    {
        return NO;
    }
    
    CombatGroup* combatGroup = [self combatGroupAtSection:indexPath.section];
    
    GroupMember* groupMember = [combatGroup.groupMembers objectAtIndex:indexPath.row];
    
    if(groupMember.unitProfile && groupMember.parentGroupMember && !self.roster.isPlayingValue)
    {
        return NO;
    }
    
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}

-(NSString*) tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.roster.isPlayingValue)
    {
        CombatGroup* combatGroup = [self combatGroupAtSection:indexPath.section];
        GroupMember* groupMember = [combatGroup.groupMembers objectAtIndex:indexPath.row];
        if(!groupMember.isDeadValue)
        {
            return @"Dead";
        }
        else
        {
            return @"Alive";
        }
    }
    else
    {
        return @"Delete";
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
    CombatGroup* sourceCombatGroup = [self combatGroupAtSection:sourceIndexPath.section];
    GroupMember* groupMember = [sourceCombatGroup.groupMembers objectAtIndex:sourceIndexPath.row];

    if(proposedDestinationIndexPath.section == 0 && [self showFailedValidationSection]){
        // Don't let them drag into the validation header
        return sourceIndexPath;
    }
    else if(proposedDestinationIndexPath.section - ([self showFailedValidationSection] ? 1 : 0) < [self.roster.combatGroups count])
    {
        CombatGroup* proposedCombatGroup = [self combatGroupAtSection:proposedDestinationIndexPath.section];
        GroupMember* replacingGroupMember = nil;
        
        //NSLog(@"from row %d/%d to row %d/%d", sourceIndexPath.section, sourceIndexPath.row, proposedDestinationIndexPath.section, proposedDestinationIndexPath.row);


        if(sourceIndexPath.section != proposedDestinationIndexPath.section){
            if(proposedDestinationIndexPath.row > [proposedCombatGroup.groupMembers count])
            {
                return proposedDestinationIndexPath;
            }
            
            // Always accept heads or tails of the list
            if((proposedDestinationIndexPath.row == 0) || [proposedCombatGroup.groupMembers count] == proposedDestinationIndexPath.row)
            {
                return proposedDestinationIndexPath;
            }

            // Index 0 is before the first member of the group
            replacingGroupMember = [proposedCombatGroup.groupMembers objectAtIndex:proposedDestinationIndexPath.row - 1];
        }else{
            // Last row is always ok
            if(proposedDestinationIndexPath.row + 1 >= [proposedCombatGroup.groupMembers count])
            {
                return proposedDestinationIndexPath;
            }
            
            // Make sure groupmember is not trying to go under own child
            if((proposedDestinationIndexPath.row > sourceIndexPath.row) && (proposedDestinationIndexPath.row - sourceIndexPath.row <= [groupMember.childrenGroupMembers count]))
            {
                return sourceIndexPath;
            }
            
            // Always accept heads or tails of the list
            if((proposedDestinationIndexPath.row == 0) || [proposedCombatGroup.groupMembers count] == proposedDestinationIndexPath.row + 1)
            {
                return proposedDestinationIndexPath;
            }

            replacingGroupMember = [proposedCombatGroup.groupMembers objectAtIndex:proposedDestinationIndexPath.row];
        }

        // Keep grouped units together
        if((sourceIndexPath.section != proposedDestinationIndexPath.section || sourceIndexPath.row < proposedDestinationIndexPath.row))
        {
            // Put unit after all children:
            // - If in the same group and coming from above
            // - If coming from another group

            if([replacingGroupMember.childrenGroupMembers count])
            {
                NSInteger row = proposedDestinationIndexPath.row + [replacingGroupMember.childrenGroupMembers count];
                //NSLog(@"Replacing a model with children. row %d becomes %d", proposedDestinationIndexPath.row, row);
                return [NSIndexPath indexPathForRow: row inSection:proposedDestinationIndexPath.section];
            }
            else if(replacingGroupMember.parentGroupMember)
            {
                NSInteger row = proposedDestinationIndexPath.row + [replacingGroupMember.parentGroupMember.childrenGroupMembers count] - [replacingGroupMember.parentGroupMember.childrenGroupMembers indexOfObject:replacingGroupMember] - 1;
                //NSLog(@"Replacing a model with a parent. row %d becomes %d", proposedDestinationIndexPath.row, row);
                return [NSIndexPath indexPathForRow: row inSection:proposedDestinationIndexPath.section];
            }

        }
        else
        {
            // Put unit before all children AND the parent:
            // - If in the same group and coming from below

            if(replacingGroupMember.parentGroupMember)
            {
                NSInteger row = proposedDestinationIndexPath.row - [replacingGroupMember.parentGroupMember.childrenGroupMembers indexOfObject:replacingGroupMember] - 1;
                //NSLog(@"Replacing a model with a parent. row %d becomes %d", proposedDestinationIndexPath.row, row);
                return [NSIndexPath indexPathForRow: row inSection:proposedDestinationIndexPath.section];
            }
        }
    }
    return proposedDestinationIndexPath;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath == nil)
        return;
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        // Delete the managed object for the given index path
        
        CombatGroup* combatGroup = [self combatGroupAtSection:indexPath.section];
        GroupMember* groupMember = [combatGroup.groupMembers objectAtIndex:indexPath.row];
        
        if(self.roster.isPlayingValue)
        {
            groupMember.isDeadValue = !groupMember.isDeadValue;
            lastDeletedIndexPath = indexPath;
            [tableView beginUpdates];
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView endUpdates];
            // Save changed roster state
            [self.roster onRosterChange];
        }
        else
        {
            [combatGroup.groupMembersSet removeObject:groupMember];
            
            NSMutableArray* indices = [[NSMutableArray alloc] init];
            [indices addObject:indexPath];
            //Children must be below parent in order
            for(int n = 0; n < [groupMember.childrenGroupMembers count]; n++)
            {
                GroupMember* child = groupMember.childrenGroupMembers[n];
                [indices addObject:[NSIndexPath indexPathForRow:indexPath.row + 1 + n inSection:indexPath.section]];
                [combatGroup.groupMembersSet removeObject:child];
            }
            [tableView beginUpdates];
            [tableView deleteRowsAtIndexPaths:indices withRowAnimation:UITableViewRowAnimationAutomatic];

            lastDeletedIndexPath = indexPath;
            
            // Notice if the validation section was added, removed, or changed.
            NSInteger start_sections = [self numberOfSectionsInTableView:tableView];
            NSInteger start_validate_errors = [self.roster.failedValidators count];
            
            [self invalidateGroupMember:groupMember];
            [self.roster deleteGroupMember:groupMember];            

            NSInteger end_sections = [self numberOfSectionsInTableView:tableView];
            NSInteger end_validate_errors = [self.roster.failedValidators count];

            if (end_sections > start_sections) {
                // Validator was added
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
                [tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            } else if (end_sections < start_sections) {
                // Validator was removed
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
                [tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            
            // If we're showing any validation errors at all, check the counts
            if (end_validate_errors && end_validate_errors != start_validate_errors) {
                NSMutableArray* indices = [[NSMutableArray alloc] init];
                // Validation count changed
                // The animations would be more correct if I carefully inserted rows in the correct spaces based on the actual contents of the validation string arrays.  However, this is easier and is good enough.
                if (end_validate_errors > start_validate_errors) {
                    for (NSInteger n = start_validate_errors; n < end_validate_errors; n++) {
                        [indices addObject:[NSIndexPath indexPathForRow:n inSection:0]];
                    }
                    [tableView insertRowsAtIndexPaths:indices withRowAnimation:UITableViewRowAnimationAutomatic];
                } else {
                    for (NSInteger n = end_validate_errors; n < start_validate_errors; n++) {
                        [indices addObject:[NSIndexPath indexPathForRow:n inSection:0]];
                    }
                    [tableView deleteRowsAtIndexPaths:indices withRowAnimation:UITableViewRowAnimationAutomatic];
                }
            }
            
            [tableView reloadData];
            [tableView endUpdates];
        }
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(lastDeletedIndexPath)
    {
        [self configureView];
        lastDeletedIndexPath = nil;
    }

}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    CombatGroup* fromCombatGroup = [self combatGroupAtSection:fromIndexPath.section];
    GroupMember* fromGroupMember = [fromCombatGroup.groupMembers objectAtIndex:fromIndexPath.row];
    CombatGroup* toCombatGroup = fromCombatGroup;
    NSInteger row = toIndexPath.row;
    
    //If seperate Combat Groups
    if(fromIndexPath.section != toIndexPath.section)
    {
        if(tableViewEditting && toIndexPath.section - ([self showFailedValidationSection] ? 1 : 0) > [self.roster.combatGroups count]-1)
        {
            toCombatGroup = [CombatGroup insertInManagedObjectContext:delegate.managedObjectContext];
            [self.roster addCombatGroupsObject:toCombatGroup];
        }
        else
        {
            toCombatGroup = [self combatGroupAtSection:toIndexPath.section];
        }
    }
    else
    {
        // If moving down within same group, do not count indexes of own children
        if(fromIndexPath.row < toIndexPath.row)
        {
            row -= [fromGroupMember.childrenGroupMembers count];
        }

    }
    
    [fromCombatGroup removeGroupMember:fromGroupMember withChildren:TRUE];
    [toCombatGroup addGroupMember:fromGroupMember atIndex:row withChildren:TRUE];

    [self.roster updateValidation];
    [delegate saveContext];
    [self configureView];
    //[NSTimer scheduledTimerWithTimeInterval:(0.35) target:self selector:@selector(refreshTable) userInfo:nil repeats:NO];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"Cell";
    
    if([self showFailedValidationSection] && indexPath.section == 0)
    {
       CellIdentifier = @"ValidationErrorCell";
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self configureCell: cell atIndexPath:indexPath];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* sectionView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320, 40)];
    
    UIView* seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0,39,320, 1)];
    [seperatorView setBackgroundColor:[UIColor lightGrayColor]];
    [sectionView addSubview:seperatorView];
    
    seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320, 1)];
    [seperatorView setBackgroundColor:[UIColor lightGrayColor]];
    [sectionView addSubview:seperatorView];
    
    [sectionView setBackgroundColor:[UIColor colorWithWhite:1 alpha:1]];
    
    //Detect "Add Combat Group Section"
    BOOL lastSection = section == [self.roster.combatGroups count]+([self showFailedValidationSection] ? 1 : 0);
    if(tableViewEditting &&  lastSection && !(self.roster.isPlaying && self.roster.isPlayingValue))
    {
        UIButton* addCombatGroup = [UIButton buttonWithType:UIButtonTypeSystem];
        addCombatGroup.tag = section;
        [addCombatGroup setFrame: CGRectMake(10,0,140,40)];
        [addCombatGroup addTarget:self action:@selector(addCombatGroup:) forControlEvents:UIControlEventTouchUpInside];
        
        [addCombatGroup setTitle: @"Add Combat Group" forState:UIControlStateNormal];
        [sectionView addSubview:addCombatGroup];
    }
    else
    {
        [self configureSectionView:sectionView inSection:section];
    }
    return sectionView;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger nSections = [self.roster.combatGroups count];
    if([self showFailedValidationSection])
    {
        nSections++;
    }
    if(tableViewEditting && nSections != 0 && !(self.roster.isPlaying && self.roster.isPlayingValue))
    {
        nSections++;
    }
    return nSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([self showFailedValidationSection] && section == 0)
    {
        return [self.roster.failedValidators count];
    }
    //No rows for "Add Combat Group" Section
    if(tableViewEditting && section-([self showFailedValidationSection] ? 1 : 0)  > [self.roster.combatGroups count]-1)
    {
        return 0;
    }
    CombatGroup* combatGroup = [self combatGroupAtSection:section];
    return [combatGroup.groupMembers count];
}


-(void) setPlayEnabled:(BOOL)playEnabled
{
    if(playEnabled)
    {
        self.navigationItem.rightBarButtonItem  = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"867-swords.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onTogglePlay:)];

        
    }
    else
    {
        [self.roster resetDead];
        self.navigationItem.rightBarButtonItem  = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPlay target: self action: @selector(onTogglePlay:)];
    }
}

- (IBAction)onGoTo:(id)sender
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if(self.roster.sectorial != nil)
    {
        [delegate.containerViewController.leftViewController goToSectorial:self.roster.sectorial];
    }
    else
    {
        [delegate.containerViewController.leftViewController goToArmy:self.roster.army];
    }
}
    
- (IBAction)exchangeRoster:(id)sender
{
    switch (self.rosterViewDisplayMode) {
        case eMainRoster:
            self.rosterViewDisplayMode = eAltRoster;
        break;
        
        case eAltRoster:
            self.rosterViewDisplayMode = eMainRoster;
    }
    [self setMercenariesInRoster];
}

- (IBAction)onDuplicateRoster:(id)sender
{
    if([self isActionSheetShown]) return;
   duplicateActionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat:@"Are you sure you want to duplicate %@?", self.roster.name] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Duplicate" otherButtonTitles: nil];
    [duplicateActionSheet showFromBarButtonItem:sender animated:TRUE];
}

-(void)duplicateRoster
{
    Roster* duplicate = [self.roster duplicate];
    self.roster = duplicate;
}

- (IBAction)onTogglePlay:(id)sender {
    if(self.roster.isPlayingValue)
    {
        if([self isActionSheetShown]) return;
        endPlayModeActionSheet = [[UIActionSheet alloc] initWithTitle:@"Are you sure?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"End Play" otherButtonTitles: nil];
        [endPlayModeActionSheet showFromBarButtonItem:sender animated:TRUE];
    }
    else
    {
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        self.roster.isPlayingValue = TRUE;
        [delegate saveContext];
        [self setPlayEnabled:self.roster.isPlayingValue];
        [self configureView];
    }
}
-(void) endPlayMode
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.roster.isPlayingValue = FALSE;
    [delegate saveContext];
    [self setPlayEnabled:self.roster.isPlayingValue];
    [self configureView];
}
- (IBAction)rosterCostCycle:(id)sender {
    if(self.roster.isPlayingValue)
    {
        return;
    }
    [self.roster cyclePointCap];
    [self configureViewControls];
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.containerViewController refreshMiddle];
    [delegate saveContext];
}

- (IBAction)onMercenaryToggle:(id)sender {
    self.roster.isSoldiersOfFortuneValue = !self.roster.isSoldiersOfFortuneValue;
    [self.roster.managedObjectContext save:nil];
    [self setMercenariesInRoster];
}

-(void) setMercenariesInRoster
{
    self.mercenaryButton.tintColor = self.roster.isSoldiersOfFortuneValue ? nil : [UIColor lightGrayColor];

    // Update the currently selected faction
    id faction = self.roster.army;
    if (self.roster.sectorial) {
        faction = self.roster.sectorial;
    }

    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    // Update the left view, if it's displaying a faction
    [faction setShowMercsValue: self.roster.isSoldiersOfFortuneValue];
    if ([delegate.containerViewController.leftViewController.topViewController isKindOfClass:[FactionViewController class]]) {
        [(FactionViewController*)delegate.containerViewController.leftViewController.topViewController refreshUnits];
    }
    
    // Refresh middle, in case current display is a merc unit
    MiddleNavigationController* middle = delegate.containerViewController.middleViewController;
    [middle refresh];
    
    [self refresh];
}

-(void) onNotationDone
{
    [notationPopover dismissPopoverAnimated:TRUE];
}

-(void) onExportRosterDone
{
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
    
-(BOOL) showFailedValidationSection
{
    if(self.roster.isPlaying != nil && self.roster.isPlayingValue)
    {
        return FALSE;
    }
    return [self.roster failedValidation];
}
-(CombatGroup*) combatGroupAtSection:(NSUInteger)section
{
    return [self.roster.combatGroups objectAtIndex:[self showFailedValidationSection] ? section-1 : section];
}
@end
