#import "Migration.h"
#import "AppDelegate.h"

@interface Migration ()

// Private interface goes here.

@end


@implementation Migration


+(BOOL)canMigrate:(AppDelegate*)delegate
{
    NSEntityDescription *entity = [Migration entityInManagedObjectContext:delegate.managedObjectContext];
    return entity != nil;
}

+(NSArray*)migrations:(AppDelegate*)delegate
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [Migration entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];

    
    NSError* error;
    NSArray* migrations = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil)
    {
        return migrations;
    }
    return nil;
}

+(BOOL)migrationExists:(AppDelegate*)delegate withName:(NSString*)name
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [Migration entityInManagedObjectContext:delegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"name = %@", name];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    NSArray* migrations = [delegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [migrations count] >= 1)
    {
        return TRUE;
    }
    return FALSE;
}

@end
