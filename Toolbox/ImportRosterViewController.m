//
//  ImportRosterViewController.m
//  Toolbox
//
//  Created by Paul on 12/4/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "ImportRosterViewController.h"
#import "Util.h"


@interface ImportRosterViewController ()

@end

@implementation ImportRosterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    NSString* html = [self.roster toLocalHTML];
    html = [self htmlWithBody:html];
    [self.previewWebView loadHTMLString:html baseURL:[[NSBundle mainBundle] bundleURL]];
    self.previewWebView.delegate = self;
    [super viewWillAppear:animated];
}

- (BOOL)webView:(UIWebView*)webView
shouldStartLoadWithRequest:(NSURLRequest*)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    return navigationType == UIWebViewNavigationTypeOther;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*) htmlWithBody:(NSString*)body
{
    NSString* path = [[NSBundle mainBundle] pathForResource:@"previewTemplate" ofType:@"html"];
    NSString* linkableTemplate = [NSString stringWithContentsOfFile:path
                                                           encoding:NSUTF8StringEncoding
                                                              error:NULL];
    
    NSString* html = [linkableTemplate stringByReplacingOccurrencesOfString:@"${body}" withString:body];
    return html;
}

- (IBAction)onImport:(id)sender
{
    [self.delegate onImportRosterDone:TRUE];
}
- (IBAction)onCancel:(id)sender
{
    [self.delegate onImportRosterDone:FALSE];
}
@end
