#import "_Unit.h"
#import "AppDelegate.h"

@interface Unit : _Unit {}

-(Unit*) clone;
-(void) copyFrom:(Unit*)source;

-(NSArray*) getUnitOptions;
-(BOOL) hasSpecialRule:(NSString*)specialRuleName;

+(Unit*)masterUnitWithID:(int)id withDelegate:(AppDelegate*)delegate;
+(NSArray*)allUnitsWithID:(int)id;
-(UnitOption*)optionWithID:(int)id;

-(NSString*)imageTitle;
-(NSString*)webImageTitle;

-(void)deleteOption:(UnitOption*)option;

+(NSArray*)allUnitsWithLegacyISC:(NSString*)isc;
-(UnitOption*)optionWithLegacyCode:(NSString*)code;

-(void)delete;

// DEBUG
-(NSString*)dumpDebug;

@end
