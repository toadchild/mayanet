//
//  RosterValidator.m
//  Toolbox
//
//  Created by Paul on 11/27/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "RosterValidator.h"
#import "Roster.h"
#import "Sectorial.h"
#import "Unit.h"
#import "UnitOption.h"
#import "GroupMember.h"
#import "SpecialRule.h"
#import "Army.h"
#import "Sectorial.h"
#import "CombatGroup.h"
#import "UnitType.h"
#import "UnitAspects.h"
#import "SpecOpsBuild.h"
#import "SpecOpsAspects.h"
#import "SpecOpsSpecialRule.h"
#import "SpecOpsEquipment.h"
#import "UnitProfile.h"
#import "HackingDevice.h"


static NSMutableArray* validators = nil;
@implementation RosterValidator

+(void) initialize
{
    if(!validators)
    {
        validators = [[NSMutableArray alloc] init];
    }
    
    //Test for Lt.
    RosterValidator* validator = [[RosterValidator alloc] initWithName:@"No Lieutenant" rosterValidationBlock:^BOOL(Roster* roster) {
        for(UnitOption* unitOption in [roster unitOptions])
        {
            for(SpecialRule* specialRule in unitOption.specialRules)
            {
                if([specialRule.name isEqualToString: @"Lieutenant"] || [specialRule.name isEqualToString: @"Lieutenant L2"])
                {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }];
    [RosterValidator addValidator:validator];
    
    //Test for One Spec Ops.
    validator = [[RosterValidator alloc] initWithName:@"Only One Spec-Ops Allowed"
                 
    rosterValidationBlock:^BOOL(Roster* roster) {
        return [[roster specOps] count] <= 1;
    }];
    [RosterValidator addValidator:validator];
    
    
    //Must include Celestial Guard with Kuang Shi Control Device in order to field Kuang Shi. (Same Combat groups)
    validator = [[RosterValidator alloc] initWithName:@"No Kuang Shi Controller"
                 
    rosterValidationBlock:^BOOL(Roster *roster)
    {
        for(CombatGroup* combatGroup in roster.combatGroups)
        {
            //Test for Kuang Shi
            BOOL bKuangShiFound = FALSE;
            BOOL bControlDeviceFound = FALSE;
            for(UnitOption* unitOption in [combatGroup unitOptions])
            {
                if(!bKuangShiFound)
                {
                    Unit* unit = [roster getUnitFrom:unitOption];
                    if(unit.idValue == 2027)
                    {
                        bKuangShiFound = TRUE;
                    }
                }
                
                if(!bControlDeviceFound)
                {
                    for(SpecialRule* specialRule in unitOption.specialRules)
                    {
                        if([specialRule.name compare:@"Kuang Shi Control Device"] == NSOrderedSame)
                        {
                            bControlDeviceFound = TRUE;
                        }
                    }
                }
            }
            if((bKuangShiFound && !bControlDeviceFound))
            {
                return FALSE;
            }
        }
        return TRUE;
        
    }
    armyApplicability:^BOOL(Army *army) {
        if(army.idValue == eArmyYuJing)
        {
            return TRUE;
        }
        return FALSE;
    }];
    [RosterValidator addValidator:validator];
    
    //Must have at least two different Post Humans body (same combat groups)
    validator = [[RosterValidator alloc] initWithName:@"Must have 2 or 3 Proxies"
                 
    rosterValidationBlock:^BOOL(Roster *roster) {
        int postHumanModels = 0;
        for(CombatGroup* combatGroup in roster.combatGroups)
        {
            for(UnitOption* unitOption in [combatGroup unitOptions])
            {
                Unit* unit = [roster getUnitFrom:unitOption];
                if([unit hasSpecialRule:@"G: Jumper L1"])
                {
                    postHumanModels++;
                }
            }
            //If you have post humans, must have at least 2 different models
            if(postHumanModels != 0 && (postHumanModels < 2 || postHumanModels > 3))
            {
                return FALSE;
            }
        }
        return TRUE;
    }
    armyApplicability:^BOOL(Army *army) {
        if(army.idValue == eArmyAleph)
        {
            return TRUE;
        }
        return FALSE;
    }];
    [RosterValidator addValidator:validator];
    
    //Must include Traktor Mul Control Device in order to field Traktor Mul. (Same Combat groups)
    validator = [[RosterValidator alloc] initWithName:@"No Traktor Mul Controller"
                 
    rosterValidationBlock:^BOOL(Roster *roster)
    {
        for(CombatGroup* combatGroup in roster.combatGroups)
        {
            //Test for Tractor Mul
            BOOL bTraktorMulFound = FALSE;
            BOOL bControlDeviceFound = FALSE;
            for(UnitOption* unitOption in [combatGroup unitOptions])
            {
                if(!bTraktorMulFound)
                {
                    Unit* unit = [roster getUnitFrom:unitOption];
                    if(unit.idValue == 3028 || unit.idValue == 3055)
                    {
                        bTraktorMulFound = TRUE;
                    }
                }
                
                if(!bControlDeviceFound)
                {
                    for(SpecialRule* specialRule in unitOption.specialRules)
                    {
                        if([specialRule.name compare:@"Traktor Mul Control Device"] == NSOrderedSame)
                        {
                            bControlDeviceFound = TRUE;
                        }
                    }
                }
            }
            if((bTraktorMulFound && !bControlDeviceFound))
            {
                return FALSE;
            }
        }
        return TRUE;
        
    }
    armyApplicability:^BOOL(Army *army) {
        if(army.idValue == eArmyAriadna)
        {
            return TRUE;
        }
        if(army.idValue == eArmyMercs)
        {
            return TRUE;
        }
        return FALSE;
    }];
    [RosterValidator addValidator:validator];
    
    //Must include Hacker or TAG in order to field REM. (Same Combat groups). Remotes with G: Sync are exempt.
    validator = [[RosterValidator alloc] initWithName:@"Remote Controller Required"
                 
    rosterValidationBlock:^BOOL(Roster *roster)
    {
        //Test for Remote
        BOOL bRemoteFound = FALSE;
        BOOL bGAutotoolRemoteFound = FALSE;
        BOOL bHackerOrTAGFound = FALSE;
        BOOL bGMnemonicaFound = FALSE;
        
        for(UnitOption* unitOption in [roster unitOptions])
        {
            Unit* unit = [roster getUnitFrom:unitOption];
            NSArray* specialRules = [[unitOption.specialRules array] arrayByAddingObjectsFromArray:[unit.aspects.specialRules array]];
            
            if([unit.type.code compare:@"REM"] == NSOrderedSame)
            {
                if(!bRemoteFound)
                {
                    //Remotes with G: Sync, G: Servant, G: Marionette are exempt
                    // G: Autotool is handled in a separate block
                    BOOL bGExempt = FALSE;
                    for(SpecialRule* specialRule in specialRules)
                    {
                        if([specialRule.name compare:@"G: Synchronized"] == NSOrderedSame ||
                           [specialRule.name compare:@"G: Servant"] == NSOrderedSame ||
                           [specialRule.name compare:@"G: Marionette"] == NSOrderedSame ||
                           [specialRule.name compare:@"G: Autotool"] == NSOrderedSame)
                        {
                            bGExempt = TRUE;
                            break;
                        }
                    }
                    // Traktor Muls have special requirements
                    if (unit.idValue == 3028 || unit.idValue == 3055) {
                        bGExempt = TRUE;
                    }
                    if(!bGExempt)
                    {
                        bRemoteFound = TRUE;
                    }
                }
                
                if(!bGAutotoolRemoteFound)
                {
                    for(SpecialRule* specialRule in specialRules)
                    {
                        if([specialRule.name compare:@"G: Autotool"] == NSOrderedSame)
                        {
                            bGAutotoolRemoteFound = TRUE;
                            break;
                        }
                    }
                }
            }

            if(!bHackerOrTAGFound)
            {
                if([unit.type.code compare:@"TAG"] == NSOrderedSame)
                {
                    bHackerOrTAGFound = TRUE;
                }
            }
            if(!bHackerOrTAGFound)
            {
                
                for(SpecialRule* specialRule in specialRules)
                {
                    if([HackingDevice deviceWithName:specialRule.name])
                    {
                        bHackerOrTAGFound = TRUE;
                        break;
                    }
                }
            }
            if(!bGMnemonicaFound)
            {
                for(SpecialRule* specialRule in specialRules)
                {
                    if([specialRule.name compare:@"G: Mnemonica"] == NSOrderedSame)
                    {
                        bGMnemonicaFound = TRUE;
                        break;
                    }
                }
            }
        }
        if(!bHackerOrTAGFound) {
            for(SpecOpsBuild* specOps in [roster specOps]) {
                for(SpecOpsEquipment* equipment in specOps.equipment) {
                    if([HackingDevice deviceWithName:equipment.specialRule.name]) {
                        bHackerOrTAGFound = TRUE;
                        break;
                    }
                }
            }
        }
        
        // Remotes require Hacker or TAG
        if((bRemoteFound && !bHackerOrTAGFound))
        {
            return FALSE;
        }
        
        // G: Autotool requires Hacker, TAG, or G: Mnemonica
        if(bGAutotoolRemoteFound && !(bGMnemonicaFound || bHackerOrTAGFound))
        {
            return FALSE;
        }

        return TRUE;
    }];
    [RosterValidator addValidator:validator];
    
    //Must have at least one doctor or engineer for each G: Servant
    validator = [[RosterValidator alloc] initWithName:@"G: Servant Controller Required"
                 
    rosterValidationBlock:^BOOL(Roster *roster)
    {
        //Test for Remote
        int nGServantRemoteFound = 0;
        int nDoctorOrEngineerFound = 0;
        
        for(UnitOption* unitOption in [roster unitOptions])
        {
            Unit* unit = [roster getUnitFrom:unitOption];
            NSArray* specialRules = [[unitOption.specialRules array] arrayByAddingObjectsFromArray:[unit.aspects.specialRules array]];

            for(SpecialRule* specialRule in specialRules)
            {
                if([specialRule.name compare:@"G: Servant"] == NSOrderedSame)
                {
                    nGServantRemoteFound++;
                    break;
                }
            }
            
        
            for(SpecialRule* specialRule in specialRules)
            {
                if([specialRule.name compare:@"Doctor"] == NSOrderedSame ||
                   [specialRule.name compare:@"Engineer"] == NSOrderedSame ||
                   [specialRule.name compare:@"Doctor Plus"] == NSOrderedSame ||
                   [specialRule.name compare:@"Akbar Doctor"] == NSOrderedSame)
                {
                    nDoctorOrEngineerFound++;
                    break;
                }
            }
            
        }
        // Search for special skills in independent child profiles (Cordelia)
        for(UnitProfile* unitProfile in [roster unitProfiles])
        {
            for(SpecialRule* specialRule in unitProfile.aspects.specialRules)
            {
                if([specialRule.name compare:@"Doctor"] == NSOrderedSame ||
                   [specialRule.name compare:@"Engineer"] == NSOrderedSame ||
                   [specialRule.name compare:@"Doctor Plus"] == NSOrderedSame ||
                   [specialRule.name compare:@"Akbar Doctor"] == NSOrderedSame)
                {
                    nDoctorOrEngineerFound++;
                    break;
                }
            }
            
        }
        //Test Spec Ops for special Rules
        for(SpecOpsBuild* specOps in [roster specOps])
        {
            for(SpecOpsSpecialRule* specOpsSpecialRule in specOps.specialRules)
            {
                SpecialRule* specialRule = specOpsSpecialRule.specialRule;
                if([specialRule.name compare:@"Doctor"] == NSOrderedSame ||
                   [specialRule.name compare:@"Engineer"] == NSOrderedSame)
                {
                    nDoctorOrEngineerFound++;
                    break;
                }
            }
        }
        if(nGServantRemoteFound != 0)
        {
            return nDoctorOrEngineerFound != 0;
        }
        else
        {
            return TRUE;
        }
    }];
    [RosterValidator addValidator:validator];

    //Must include Chimera in order to field Pupniks. (Same Combat groups)
    validator = [[RosterValidator alloc] initWithName:@"Too Many Pupniks"

    rosterValidationBlock:^BOOL(Roster *roster)
    {
        for(CombatGroup* combatGroup in roster.combatGroups)
        {
            //Test for Pupniks
            int pupniksFound = 0;
            int chimeraFound = 0;
            for(UnitOption* unitOption in [combatGroup unitOptions])
            {
                Unit* unit = [roster getUnitFrom:unitOption];
                if(unit.idValue == 5037) {
                    chimeraFound++;
                } else if (unit.idValue == 5038) {
                    pupniksFound++;
                }
            
            }
            if(pupniksFound > chimeraFound * 3)
            {
                return FALSE;
            }
        }
        return TRUE;
    }
                 
    armyApplicability:^BOOL(Army *army) {
        if(army.idValue == eArmyNomads)
        {
            return TRUE;
        }
        if(army.idValue == eArmyMercs)
        {
            return TRUE;
        }
        return FALSE;
    }];
    [RosterValidator addValidator:validator];
    
    //Must include Arjuna in order to field Kiranbots. (Same Combat groups)
    validator = [[RosterValidator alloc] initWithName:@"Too Many Kiranbots"
                 
    rosterValidationBlock:^BOOL(Roster *roster) {
        for(CombatGroup* combatGroup in roster.combatGroups) {
            //Test for Pupniks
            int kiranbots = 0;
            int arjunas = 0;
            for(UnitOption* unitOption in [combatGroup unitOptions]) {
                Unit* unit = [roster getUnitFrom:unitOption];
                if(unit.idValue == 7052) {
                    arjunas++;
                } else if(unit.idValue == 7053) {
                    kiranbots++;
                }
            }
            if(kiranbots > arjunas * 3) {
                return FALSE;
            }
        }
        return TRUE;
    }
                 
    armyApplicability:^BOOL(Army *army) {
        if(army.idValue == eArmyAleph) {
            return TRUE;
        }
        return FALSE;
    }];
    [RosterValidator addValidator:validator];
    
    
    //Dog-Warrior/Cameronian AVA
    validator = [[RosterValidator alloc] initWithName:@"Dog-Warrior AVA 2 Exceeded"

    rosterValidationBlock:^BOOL(Roster *roster)
    {
        int cameronianCount = 0;
        
        for(UnitOption* unitOption in [roster unitOptions])
        {
            Unit* unit = [roster getUnitFrom:unitOption];
            if(unit.idValue == 3018 ||  // Cameronian
               unit.idValue == 3039 ||  // Dog-Warrior
               unit.idValue == 3043)    // Devil Dog
            {
                cameronianCount++;
            }
        }
        return cameronianCount <= 2;
    }
    armyApplicability:^BOOL(Army *army) {
        if(army.idValue == eArmyAriadna)
        {
            return TRUE;
        }
        return FALSE;
    }
    sectorialApplicability:^BOOL(Sectorial *sectorial) {
        if(!sectorial)
        {
            return TRUE;
        }
        return FALSE;
    }];
    [RosterValidator addValidator:validator];

    // MAF Hungries Control Device
    validator = [[RosterValidator alloc] initWithName:@"No Hungries Control Device"
                 
    rosterValidationBlock:^BOOL(Roster *roster)
    {
        for(CombatGroup* combatGroup in roster.combatGroups)
        {
            //Test for Hungries
            BOOL bHungriesFound = FALSE;
            BOOL bControlDeviceFound = FALSE;
            for(UnitOption* unitOption in [combatGroup unitOptions])
            {
                if(!bHungriesFound)
                {
                    Unit* unit = [roster getUnitFrom:unitOption];
                    // Gakis or Pretas
                    if(unit.idValue == 6014 || unit.idValue == 6015)
                    {
                        bHungriesFound = TRUE;
                    }
                }
                
                if(!bControlDeviceFound)
                {
                    for(SpecialRule* specialRule in [unitOption getUnit].aspects.specialRules)
                    {
                        if([specialRule.name compare:@"Hungries Control Device"] == NSOrderedSame)
                        {
                            bControlDeviceFound = TRUE;
                        }
                    }
                }
            }
            if((bHungriesFound && !bControlDeviceFound))
            {
                return FALSE;
            }
        }
        return TRUE;
    }
    armyApplicability:^BOOL(Army *army) {
        if(army.idValue == eArmyCombined)
        {
            return TRUE;
        }
        return FALSE;
    }
    sectorialApplicability:^BOOL(Sectorial *sectorial) {
        if(sectorial.idValue == eSectorialMorat)
        {
            return TRUE;
        }
        return FALSE;
    }];
    [RosterValidator addValidator:validator];
    
    // ISS Ashcroft Hunting Party
    validator = [[RosterValidator alloc] initWithName:@"Ashcroft Hunting Party incomplete"
                 
    rosterValidationBlock:^BOOL(Roster *roster)
    {
        for(CombatGroup* combatGroup in roster.combatGroups)
        {
            //Test for Miranda & ABH
            BOOL bMirandaFound = FALSE;
            BOOL bABHFound = FALSE;
            for(UnitOption* unitOption in [combatGroup unitOptions])
            {
                if(!bMirandaFound)
                {
                    Unit* unit = [roster getUnitFrom:unitOption];
                    if(unit.idValue == 2051)
                    {
                        bMirandaFound = TRUE;
                    }
                }
                 
                if(!bABHFound)
                {
                    Unit* unit = [roster getUnitFrom:unitOption];
                    if(unit.idValue == 2052)
                    {
                        bABHFound = TRUE;
                    }
                }
            }
            if(bMirandaFound != bABHFound)
            {
                return FALSE;
            }
        }
        return TRUE;
    }
    armyApplicability:^BOOL(Army *army) {
        if(army.idValue == eArmyYuJing || army.idValue == eArmyNomads)
        {
            return TRUE;
        }
        return FALSE;
    }
    sectorialApplicability:^BOOL(Sectorial *sectorial) {
       if(sectorial.idValue == eSectorialImperialService || sectorial.idValue == eSectorialTunguska)
       {
           return TRUE;
       }
       return FALSE;
    }];
    [RosterValidator addValidator:validator];

    //Must include Puppet Master in order to field Puppetbots. (Same Combat groups)
    validator = [[RosterValidator alloc] initWithName:@"No Puppet Master"
                 
    rosterValidationBlock:^BOOL(Roster *roster)
    {
        for(CombatGroup* combatGroup in roster.combatGroups)
        {
            BOOL puppets = FALSE;
            BOOL master = FALSE;
            for(UnitOption* unitOption in [combatGroup unitOptions])
            {
                Unit* unit = [roster getUnitFrom:unitOption];

                if(unit.idValue == 5050)
                {
                    master = TRUE;
                }
                
                if(unit.idValue == 5051)
                {
                    puppets = TRUE;
                }
            }
            if((puppets && !master))
            {
                return FALSE;
            }
        }
        return TRUE;
    }
                 
    armyApplicability:^BOOL(Army *army) {
        if(army.idValue == eArmyNomads)
        {
            return TRUE;
        }
        return FALSE;
    }];
    [RosterValidator addValidator:validator];
    
    //Must include Puppet Master in order to field Puppetbots. (Same Combat groups)
    validator = [[RosterValidator alloc] initWithName:@"Too Many Puppetbots"
                 
    rosterValidationBlock:^BOOL(Roster *roster)
    {
        for(CombatGroup* combatGroup in roster.combatGroups)
        {
            int puppets = 0;
            int masters = 0;
            for(UnitOption* unitOption in [combatGroup unitOptions])
            {
                Unit* unit = [roster getUnitFrom:unitOption];
                if(unit.idValue == 5050)
                {
                    masters++;
                }
                
                if(unit.idValue == 5051)
                {
                    puppets++;
                }
            
            }
            if(masters != 0 && puppets > masters*3)
            {
                return FALSE;
            }
        }
        return TRUE;
    }
                 
    armyApplicability:^BOOL(Army *army) {
        if(army.idValue == eArmyNomads)
        {
            return TRUE;
        }
        return FALSE;
    }];
    [RosterValidator addValidator:validator];

    //Soldiers of Fortune only allows 75 points of mercs
    validator = [[RosterValidator alloc] initWithName:@"Merc points value exceeds 75"
                 
    rosterValidationBlock:^BOOL(Roster *roster)
    {
        if (!roster.isSoldiersOfFortuneValue) {
            return TRUE;
        }
        
        int mercPoints = 0;
        for(CombatGroup* combatGroup in roster.combatGroups)
        {
            for (GroupMember* member in [combatGroup groupMembers])
            {
                UnitOption* unitOption = [member getUnitOption];
                Unit* unit = [roster getUnitFrom:unitOption];
                if (unit.isMercValue) {
                    mercPoints += unitOption.costValue;
                }
                
                // Also count units that are using mercAva
                if (unit.mercAvaValue && [roster getAVACountUptoGroupMember:member] >= unit.avaValue) {
                    mercPoints += unitOption.costValue;
                }
            }
        }
        return mercPoints <= 75;
    }
                 
    armyApplicability:^BOOL(Army *army) {
        return TRUE;
    }];
    [RosterValidator addValidator:validator];

    //Max of one Tri-Core per roster
    validator = [[RosterValidator alloc] initWithName:@"Too many Tri-Cores"
                 
    rosterValidationBlock:^BOOL(Roster *roster) {
        int tricores = 0;
        SpecialRule* tricore = [SpecialRule specialRuleWithName:@"Tri-Core"];
        for (CombatGroup* combatGroup in roster.combatGroups) {
            for (GroupMember* member in [combatGroup groupMembers]) {
                UnitOption* unitOption = [member getUnitOption];
                if ([unitOption.specialRulesSet containsObject:tricore]) {
                    tricores++;
                }
            }
        }
        return tricores <= 1;
    }
                 
    sectorialApplicability:^BOOL(Sectorial *sectorial) {
        if(sectorial.idValue == eSectorialSpiral) {
            return TRUE;
        }
        return FALSE;
    }];
    [RosterValidator addValidator:validator];

    //Max of three spec-ops weapons
    validator = [[RosterValidator alloc] initWithName:@"Max of 3 Spec-Ops Weapons"
    rosterValidationBlock:^BOOL(Roster *roster) {
        for (CombatGroup* combatGroup in roster.combatGroups) {
            for (GroupMember* member in [combatGroup groupMembers]) {
                if (member.specops) {
                    if ([member.specops.weapons count] > 3) {
                        return FALSE;
                    }
                }
            }
        }
        return TRUE;
    }];
    [RosterValidator addValidator:validator];

    //Max of three spec-ops skills
    validator = [[RosterValidator alloc] initWithName:@"Max of 3 Spec-Ops Skills"
    rosterValidationBlock:^BOOL(Roster *roster) {
        for (CombatGroup* combatGroup in roster.combatGroups) {
            for (GroupMember* member in [combatGroup groupMembers]) {
                if (member.specops) {
                    if ([member.specops.specialRules count] > 3) {
                        return FALSE;
                    }
                }
            }
        }
        return TRUE;
    }];
    [RosterValidator addValidator:validator];

    //Max of three spec-ops equipment
    validator = [[RosterValidator alloc] initWithName:@"Max of 3 Spec-Ops Equipment"
    rosterValidationBlock:^BOOL(Roster *roster) {
        for (CombatGroup* combatGroup in roster.combatGroups) {
            for (GroupMember* member in [combatGroup groupMembers]) {
                if (member.specops) {
                    if ([member.specops.equipment count] > 3) {
                        return FALSE;
                    }
                }
            }
        }
        return TRUE;
    }];
    [RosterValidator addValidator:validator];

    // Jaan Staar counts as a Kiiutan Imposter, but also has his own AVA of 1.
    validator = [[RosterValidator alloc] initWithName:@"Too many Kiiutan Imposters"
                 
    rosterValidationBlock:^BOOL(Roster *roster) {
        const int jaan_staar_id = 8033;
        const int kiiutan_id = 8031;
        int kiiutan = 0;
        for (CombatGroup* combatGroup in roster.combatGroups) {
            for (GroupMember* member in [combatGroup groupMembers]) {
                UnitOption* unitOption = [member getUnitOption];
                Unit* unit = [roster getUnitFrom:unitOption];
                if(unit.idValue == jaan_staar_id || unit.idValue == kiiutan_id) {
                    kiiutan++;
                }
            }
        }
        
        Unit* kiiutan_unit = nil;
        if (roster.sectorial) {
            kiiutan_unit = [roster.sectorial unitWithID:kiiutan_id];
        } else {
            kiiutan_unit = [roster.army unitWithID:kiiutan_id];
        }
        
        return kiiutan <= kiiutan_unit.avaValue;
    }

    armyApplicability:^BOOL(Army *army) {
        if (army.idValue == eArmyTohaa) {
            return TRUE;
        }
        if (army.idValue == eArmyMercs) {
            return TRUE;
        }
        return FALSE;
    }
    
    sectorialApplicability:^BOOL(Sectorial *sectorial) {
        if (sectorial == nil) {
            return TRUE;
        }
        if (sectorial.idValue == eSectorialSpiral) {
            return TRUE;
        }
        return FALSE;
    }];
    [RosterValidator addValidator:validator];
}

-(id)initWithName:(NSString*)name rosterValidationBlock:(RosterValidationBlock)theRosterValidationBlock
{
    self = [super init];
    if(self)
    {
        _name = name;
        rosterValidationBlock = theRosterValidationBlock;
        armyApplicabilityBlock = nil;
        sectorialApplicabilityBlock = nil;
    }
    return self;
}
-(id)initWithName:(NSString*)name rosterValidationBlock:(RosterValidationBlock)theRosterValidationBlock armyApplicability:(ArmyApplicabilityBlock)theArmyApplicabiltyBlock
{
    self = [super init];
    if(self)
    {
        _name = name;
        rosterValidationBlock = theRosterValidationBlock;
        armyApplicabilityBlock = theArmyApplicabiltyBlock;
        sectorialApplicabilityBlock = nil;
    }
    return self;
}
-(id)initWithName:(NSString*)name rosterValidationBlock:(RosterValidationBlock)theRosterValidationBlock sectorialApplicability:(SectorialApplicabilityBlock)theSectorialApplicabiltyBlock
{
    self = [super init];
    if(self)
    {
        _name = name;
        rosterValidationBlock = theRosterValidationBlock;
        armyApplicabilityBlock = nil;
        sectorialApplicabilityBlock = theSectorialApplicabiltyBlock;
    }
    return self;
}
                 
-(id)initWithName:(NSString*)name rosterValidationBlock:(RosterValidationBlock)theRosterValidationBlock
        armyApplicability:(ArmyApplicabilityBlock)theArmyApplicabiltyBlock
        sectorialApplicability:(SectorialApplicabilityBlock)theSectorialApplicabiltyBlock
{
self = [super init];
if(self)
{
    _name = name;
    rosterValidationBlock = theRosterValidationBlock;
    armyApplicabilityBlock = theArmyApplicabiltyBlock;
    sectorialApplicabilityBlock = theSectorialApplicabiltyBlock;
}
return self;
}


+(void)addValidator:(RosterValidator*)validator
{
    [validators addObject:validator];
}
    
+(NSArray*)validate:(Roster*)roster;
{
    NSMutableArray* failedValidation = [[NSMutableArray alloc] init];
    for(RosterValidator* validator in validators)
    {
        //Test for army applicability
        if(validator->armyApplicabilityBlock && !validator->armyApplicabilityBlock(roster.sectorial ? roster.sectorial.army : roster.army))
        {
            continue;
        }
        
        //Test for sectorial applicability
        if(validator->sectorialApplicabilityBlock && !validator->sectorialApplicabilityBlock(roster.sectorial))
        {
            continue;
        }

        //Test Roster
        if(validator->rosterValidationBlock && !validator->rosterValidationBlock(roster))
        {
            [failedValidation addObject:validator];
        }
    }
    return failedValidation;
}
    
@end
