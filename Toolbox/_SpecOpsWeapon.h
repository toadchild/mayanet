// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SpecOpsWeapon.h instead.

#import <CoreData/CoreData.h>

extern const struct SpecOpsWeaponAttributes {
	__unsafe_unretained NSString *cost;
} SpecOpsWeaponAttributes;

extern const struct SpecOpsWeaponRelationships {
	__unsafe_unretained NSString *army;
	__unsafe_unretained NSString *specops;
	__unsafe_unretained NSString *weapon;
} SpecOpsWeaponRelationships;

@class Army;
@class SpecOpsBuild;
@class Weapon;

@interface SpecOpsWeaponID : NSManagedObjectID {}
@end

@interface _SpecOpsWeapon : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SpecOpsWeaponID* objectID;

@property (nonatomic, strong) NSNumber* cost;

@property (atomic) int16_t costValue;
- (int16_t)costValue;
- (void)setCostValue:(int16_t)value_;

//- (BOOL)validateCost:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *army;

- (NSMutableSet*)armySet;

@property (nonatomic, strong) NSSet *specops;

- (NSMutableSet*)specopsSet;

@property (nonatomic, strong) Weapon *weapon;

//- (BOOL)validateWeapon:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newArmyFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecopsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _SpecOpsWeapon (ArmyCoreDataGeneratedAccessors)
- (void)addArmy:(NSSet*)value_;
- (void)removeArmy:(NSSet*)value_;
- (void)addArmyObject:(Army*)value_;
- (void)removeArmyObject:(Army*)value_;

@end

@interface _SpecOpsWeapon (SpecopsCoreDataGeneratedAccessors)
- (void)addSpecops:(NSSet*)value_;
- (void)removeSpecops:(NSSet*)value_;
- (void)addSpecopsObject:(SpecOpsBuild*)value_;
- (void)removeSpecopsObject:(SpecOpsBuild*)value_;

@end

@interface _SpecOpsWeapon (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveCost;
- (void)setPrimitiveCost:(NSNumber*)value;

- (int16_t)primitiveCostValue;
- (void)setPrimitiveCostValue:(int16_t)value_;

- (NSMutableSet*)primitiveArmy;
- (void)setPrimitiveArmy:(NSMutableSet*)value;

- (NSMutableSet*)primitiveSpecops;
- (void)setPrimitiveSpecops:(NSMutableSet*)value;

- (Weapon*)primitiveWeapon;
- (void)setPrimitiveWeapon:(Weapon*)value;

@end
