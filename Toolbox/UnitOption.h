#import "_UnitOption.h"

@class Army;

@interface UnitOption : _UnitOption {}

-(BOOL) isLt;
-(Unit*) getUnit;

-(NSString*) weaponDescription:(Unit*)unit;
-(NSString*) weaponDescription:(Unit*)unit seperator:(NSString*)seperator;
-(NSString*) specialRulesDescription;

-(UnitOption*) clone;
-(void) copyFrom:(UnitOption*)source;

@end
