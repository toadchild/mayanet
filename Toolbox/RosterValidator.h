//
//  RosterValidator.h
//  Toolbox
//
//  Created by Paul on 11/27/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Roster,Army,Sectorial,Unit;

typedef BOOL (^ RosterValidationBlock)(Roster* roster);
typedef BOOL (^ ArmyApplicabilityBlock)(Army* army);
typedef BOOL (^ SectorialApplicabilityBlock)(Sectorial* sectorial);

@interface RosterValidator : NSObject
{
@protected
    RosterValidationBlock rosterValidationBlock;
    ArmyApplicabilityBlock armyApplicabilityBlock;
    SectorialApplicabilityBlock sectorialApplicabilityBlock;
}
    
-(id)initWithName:(NSString*)name rosterValidationBlock:(RosterValidationBlock)theRosterValidationBlock;
    
-(id)initWithName:(NSString*)name rosterValidationBlock:(RosterValidationBlock)theRosterValidationBlock
    armyApplicability:(ArmyApplicabilityBlock)theArmyApplicabiltyBlock;
    
-(id)initWithName:(NSString*)name rosterValidationBlock:(RosterValidationBlock)theRosterValidationBlock
    sectorialApplicability:(SectorialApplicabilityBlock)theSectorialApplicabiltyBlock;
    
-(id)initWithName:(NSString*)name rosterValidationBlock:(RosterValidationBlock)theRosterValidationBlock
    armyApplicability:(ArmyApplicabilityBlock)theArmyApplicabiltyBlock
    sectorialApplicability:(SectorialApplicabilityBlock)theSectorialApplicabiltyBlock;

@property (nonatomic, strong, readonly) NSString* name;
@property (nonatomic, strong, readonly) NSString* failedDescription;
@property (nonatomic, readonly) CGFloat tableViewCellHeight;
    
+(NSArray*)validate:(Roster*)roster;
+(void)addValidator:(RosterValidator*)validator;
@end
