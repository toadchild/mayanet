// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to HackingDevice.m instead.

#import "_HackingDevice.h"

const struct HackingDeviceAttributes HackingDeviceAttributes = {
	.name = @"name",
};

const struct HackingDeviceRelationships HackingDeviceRelationships = {
	.groups = @"groups",
	.upgrades = @"upgrades",
};

@implementation HackingDeviceID
@end

@implementation _HackingDevice

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"HackingDevice" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"HackingDevice";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"HackingDevice" inManagedObjectContext:moc_];
}

- (HackingDeviceID*)objectID {
	return (HackingDeviceID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic name;

@dynamic groups;

- (NSMutableSet*)groupsSet {
	[self willAccessValueForKey:@"groups"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"groups"];

	[self didAccessValueForKey:@"groups"];
	return result;
}

@dynamic upgrades;

- (NSMutableSet*)upgradesSet {
	[self willAccessValueForKey:@"upgrades"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"upgrades"];

	[self didAccessValueForKey:@"upgrades"];
	return result;
}

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newGroupsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"HackingProgramGroup" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"devices CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newUpgradesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"HackingProgram" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"upgrades CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

