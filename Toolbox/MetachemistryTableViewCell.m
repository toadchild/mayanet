//
//  MetachemistryTableViewCell.m
//  Toolbox
//
//  Created by Paul on 12/6/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "MetachemistryTableViewCell.h"
#import "Metachemistry.h"
#import "MetachemistryNote.h"
#import "MetachemistryNotation.h"
#import "SpecialRule.h"

@implementation MetachemistryTableViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    if(_note)
    {
        [self setMetachemistry:self.metachemistryNote.metachemistry];
    }
    else
    {
        if([self.notation type] == eMetachemistryNotation)
        {
            [self.metachemistryL2Button removeFromSuperview];
        }
    }
}

- (IBAction)onMetachemistryL1Roll:(id)sender
{
    Metachemistry* metachemistry = [Metachemistry rollWithLevelTwo:FALSE];
    
    [self setMetachemistry:metachemistry];
    [self onModified];
}

- (IBAction)onMetachemistryL2Roll:(id)sender
{
    Metachemistry* metachemistry = [Metachemistry rollWithLevelTwo:TRUE];
    
    [self setMetachemistry:metachemistry];
    [self onModified];
}

-(void)setMetachemistry:(Metachemistry*)metachemistry
{
    self.metachemistryNote.metachemistry = metachemistry;
    
    NSMutableArray *skillNames = [[NSMutableArray alloc] init];
    for (SpecialRule *rule in metachemistry.specialRule) {
        [skillNames addObject:rule.name];
    }
    NSString* outcome = [skillNames componentsJoinedByString:@", "];
    
    self.metachemistryOutcome.text = [NSString stringWithFormat:@"Metachemistry: %@", outcome];
    [self.metachemistryOutcome setHidden:FALSE];
    [self.metachemistryL1Button removeFromSuperview];
    [self.metachemistryL2Button removeFromSuperview];
}


-(MetachemistryNote*)metachemistryNote
{
    return (MetachemistryNote*)self.note;
}

-(Note*)note
{
    if(!_note)
    {
        _note = [MetachemistryNote insertInManagedObjectContext:self.groupMember.managedObjectContext];
        _note.groupMember = self.groupMember;
        _note.notation = self.notation;
        
        // The MetachemistryNote needs to have the same levelTwo flag as the MetachemistryNotation
        MetachemistryNote *metachemistryNote = (MetachemistryNote*)_note;
        MetachemistryNotation *metachemistryNotation = (MetachemistryNotation*)self.notation;
        metachemistryNote.levelTwoValue = metachemistryNotation.levelTwoValue;
}
    return _note;
}

@end
