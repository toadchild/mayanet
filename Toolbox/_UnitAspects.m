// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UnitAspects.m instead.

#import "_UnitAspects.h"

const struct UnitAspectsAttributes UnitAspectsAttributes = {
	.arm = @"arm",
	.bs = @"bs",
	.bts = @"bts",
	.cc = @"cc",
	.cube = @"cube",
	.fury = @"fury",
	.hackable = @"hackable",
	.linkable = @"linkable",
	.mov = @"mov",
	.ph = @"ph",
	.regular = @"regular",
	.silhouette = @"silhouette",
	.wip = @"wip",
	.wounds = @"wounds",
};

const struct UnitAspectsRelationships UnitAspectsRelationships = {
	.classification = @"classification",
	.profile = @"profile",
	.specialRules = @"specialRules",
	.unit = @"unit",
	.weapons = @"weapons",
};

@implementation UnitAspectsID
@end

@implementation _UnitAspects

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"UnitAspects" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"UnitAspects";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"UnitAspects" inManagedObjectContext:moc_];
}

- (UnitAspectsID*)objectID {
	return (UnitAspectsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"armValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"arm"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"bsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"bs"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"btsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"bts"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"ccValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cc"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"cubeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"cube"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"furyValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"fury"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"hackableValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"hackable"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"linkableValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"linkable"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"phValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"ph"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"regularValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"regular"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"silhouetteValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"silhouette"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"wipValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"wip"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"woundsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"wounds"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic arm;

- (int16_t)armValue {
	NSNumber *result = [self arm];
	return [result shortValue];
}

- (void)setArmValue:(int16_t)value_ {
	[self setArm:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveArmValue {
	NSNumber *result = [self primitiveArm];
	return [result shortValue];
}

- (void)setPrimitiveArmValue:(int16_t)value_ {
	[self setPrimitiveArm:[NSNumber numberWithShort:value_]];
}

@dynamic bs;

- (int16_t)bsValue {
	NSNumber *result = [self bs];
	return [result shortValue];
}

- (void)setBsValue:(int16_t)value_ {
	[self setBs:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveBsValue {
	NSNumber *result = [self primitiveBs];
	return [result shortValue];
}

- (void)setPrimitiveBsValue:(int16_t)value_ {
	[self setPrimitiveBs:[NSNumber numberWithShort:value_]];
}

@dynamic bts;

- (int16_t)btsValue {
	NSNumber *result = [self bts];
	return [result shortValue];
}

- (void)setBtsValue:(int16_t)value_ {
	[self setBts:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveBtsValue {
	NSNumber *result = [self primitiveBts];
	return [result shortValue];
}

- (void)setPrimitiveBtsValue:(int16_t)value_ {
	[self setPrimitiveBts:[NSNumber numberWithShort:value_]];
}

@dynamic cc;

- (int16_t)ccValue {
	NSNumber *result = [self cc];
	return [result shortValue];
}

- (void)setCcValue:(int16_t)value_ {
	[self setCc:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCcValue {
	NSNumber *result = [self primitiveCc];
	return [result shortValue];
}

- (void)setPrimitiveCcValue:(int16_t)value_ {
	[self setPrimitiveCc:[NSNumber numberWithShort:value_]];
}

@dynamic cube;

- (int16_t)cubeValue {
	NSNumber *result = [self cube];
	return [result shortValue];
}

- (void)setCubeValue:(int16_t)value_ {
	[self setCube:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveCubeValue {
	NSNumber *result = [self primitiveCube];
	return [result shortValue];
}

- (void)setPrimitiveCubeValue:(int16_t)value_ {
	[self setPrimitiveCube:[NSNumber numberWithShort:value_]];
}

@dynamic fury;

- (int16_t)furyValue {
	NSNumber *result = [self fury];
	return [result shortValue];
}

- (void)setFuryValue:(int16_t)value_ {
	[self setFury:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveFuryValue {
	NSNumber *result = [self primitiveFury];
	return [result shortValue];
}

- (void)setPrimitiveFuryValue:(int16_t)value_ {
	[self setPrimitiveFury:[NSNumber numberWithShort:value_]];
}

@dynamic hackable;

- (BOOL)hackableValue {
	NSNumber *result = [self hackable];
	return [result boolValue];
}

- (void)setHackableValue:(BOOL)value_ {
	[self setHackable:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveHackableValue {
	NSNumber *result = [self primitiveHackable];
	return [result boolValue];
}

- (void)setPrimitiveHackableValue:(BOOL)value_ {
	[self setPrimitiveHackable:[NSNumber numberWithBool:value_]];
}

@dynamic linkable;

- (BOOL)linkableValue {
	NSNumber *result = [self linkable];
	return [result boolValue];
}

- (void)setLinkableValue:(BOOL)value_ {
	[self setLinkable:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveLinkableValue {
	NSNumber *result = [self primitiveLinkable];
	return [result boolValue];
}

- (void)setPrimitiveLinkableValue:(BOOL)value_ {
	[self setPrimitiveLinkable:[NSNumber numberWithBool:value_]];
}

@dynamic mov;

@dynamic ph;

- (int16_t)phValue {
	NSNumber *result = [self ph];
	return [result shortValue];
}

- (void)setPhValue:(int16_t)value_ {
	[self setPh:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePhValue {
	NSNumber *result = [self primitivePh];
	return [result shortValue];
}

- (void)setPrimitivePhValue:(int16_t)value_ {
	[self setPrimitivePh:[NSNumber numberWithShort:value_]];
}

@dynamic regular;

- (BOOL)regularValue {
	NSNumber *result = [self regular];
	return [result boolValue];
}

- (void)setRegularValue:(BOOL)value_ {
	[self setRegular:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveRegularValue {
	NSNumber *result = [self primitiveRegular];
	return [result boolValue];
}

- (void)setPrimitiveRegularValue:(BOOL)value_ {
	[self setPrimitiveRegular:[NSNumber numberWithBool:value_]];
}

@dynamic silhouette;

- (int16_t)silhouetteValue {
	NSNumber *result = [self silhouette];
	return [result shortValue];
}

- (void)setSilhouetteValue:(int16_t)value_ {
	[self setSilhouette:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveSilhouetteValue {
	NSNumber *result = [self primitiveSilhouette];
	return [result shortValue];
}

- (void)setPrimitiveSilhouetteValue:(int16_t)value_ {
	[self setPrimitiveSilhouette:[NSNumber numberWithShort:value_]];
}

@dynamic wip;

- (int16_t)wipValue {
	NSNumber *result = [self wip];
	return [result shortValue];
}

- (void)setWipValue:(int16_t)value_ {
	[self setWip:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveWipValue {
	NSNumber *result = [self primitiveWip];
	return [result shortValue];
}

- (void)setPrimitiveWipValue:(int16_t)value_ {
	[self setPrimitiveWip:[NSNumber numberWithShort:value_]];
}

@dynamic wounds;

- (int16_t)woundsValue {
	NSNumber *result = [self wounds];
	return [result shortValue];
}

- (void)setWoundsValue:(int16_t)value_ {
	[self setWounds:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveWoundsValue {
	NSNumber *result = [self primitiveWounds];
	return [result shortValue];
}

- (void)setPrimitiveWoundsValue:(int16_t)value_ {
	[self setPrimitiveWounds:[NSNumber numberWithShort:value_]];
}

@dynamic classification;

@dynamic profile;

@dynamic specialRules;

- (NSMutableOrderedSet*)specialRulesSet {
	[self willAccessValueForKey:@"specialRules"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"specialRules"];

	[self didAccessValueForKey:@"specialRules"];
	return result;
}

@dynamic unit;

@dynamic weapons;

- (NSMutableOrderedSet*)weaponsSet {
	[self willAccessValueForKey:@"weapons"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"weapons"];

	[self didAccessValueForKey:@"weapons"];
	return result;
}

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newSpecialRulesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecialRule" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"units CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newWeaponsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Weapon" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"units CONTAINS %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

@implementation _UnitAspects (SpecialRulesCoreDataGeneratedAccessors)
- (void)addSpecialRules:(NSOrderedSet*)value_ {
	[self.specialRulesSet unionOrderedSet:value_];
}
- (void)removeSpecialRules:(NSOrderedSet*)value_ {
	[self.specialRulesSet minusOrderedSet:value_];
}
- (void)addSpecialRulesObject:(SpecialRule*)value_ {
	[self.specialRulesSet addObject:value_];
}
- (void)removeSpecialRulesObject:(SpecialRule*)value_ {
	[self.specialRulesSet removeObject:value_];
}
- (void)insertObject:(SpecialRule*)value inSpecialRulesAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"specialRules"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specialRules]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specialRules"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"specialRules"];
}
- (void)removeObjectFromSpecialRulesAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"specialRules"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specialRules]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specialRules"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"specialRules"];
}
- (void)insertSpecialRules:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"specialRules"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specialRules]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specialRules"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"specialRules"];
}
- (void)removeSpecialRulesAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"specialRules"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specialRules]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specialRules"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"specialRules"];
}
- (void)replaceObjectInSpecialRulesAtIndex:(NSUInteger)idx withObject:(SpecialRule*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"specialRules"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specialRules]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specialRules"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"specialRules"];
}
- (void)replaceSpecialRulesAtIndexes:(NSIndexSet *)indexes withSpecialRules:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"specialRules"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self specialRules]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"specialRules"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"specialRules"];
}
@end

@implementation _UnitAspects (WeaponsCoreDataGeneratedAccessors)
- (void)addWeapons:(NSOrderedSet*)value_ {
	[self.weaponsSet unionOrderedSet:value_];
}
- (void)removeWeapons:(NSOrderedSet*)value_ {
	[self.weaponsSet minusOrderedSet:value_];
}
- (void)addWeaponsObject:(Weapon*)value_ {
	[self.weaponsSet addObject:value_];
}
- (void)removeWeaponsObject:(Weapon*)value_ {
	[self.weaponsSet removeObject:value_];
}
- (void)insertObject:(Weapon*)value inWeaponsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"weapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self weapons]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"weapons"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"weapons"];
}
- (void)removeObjectFromWeaponsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"weapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self weapons]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"weapons"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"weapons"];
}
- (void)insertWeapons:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"weapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self weapons]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"weapons"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"weapons"];
}
- (void)removeWeaponsAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"weapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self weapons]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"weapons"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"weapons"];
}
- (void)replaceObjectInWeaponsAtIndex:(NSUInteger)idx withObject:(Weapon*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"weapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self weapons]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"weapons"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"weapons"];
}
- (void)replaceWeaponsAtIndexes:(NSIndexSet *)indexes withWeapons:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"weapons"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self weapons]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"weapons"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"weapons"];
}
@end

