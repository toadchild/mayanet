// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to HackingProgram.h instead.

#import <CoreData/CoreData.h>

extern const struct HackingProgramAttributes {
	__unsafe_unretained NSString *ammo;
	__unsafe_unretained NSString *attMod;
	__unsafe_unretained NSString *burst;
	__unsafe_unretained NSString *damage;
	__unsafe_unretained NSString *effect;
	__unsafe_unretained NSString *hide_mods;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *oppMod;
	__unsafe_unretained NSString *range;
	__unsafe_unretained NSString *skill;
	__unsafe_unretained NSString *special;
	__unsafe_unretained NSString *target;
} HackingProgramAttributes;

extern const struct HackingProgramRelationships {
	__unsafe_unretained NSString *group;
	__unsafe_unretained NSString *upgrades;
	__unsafe_unretained NSString *wikiEntry;
} HackingProgramRelationships;

@class HackingProgramGroup;
@class HackingDevice;
@class WikiEntry;

@interface HackingProgramID : NSManagedObjectID {}
@end

@interface _HackingProgram : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) HackingProgramID* objectID;

@property (nonatomic, strong) NSString* ammo;

//- (BOOL)validateAmmo:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* attMod;

//- (BOOL)validateAttMod:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* burst;

//- (BOOL)validateBurst:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* damage;

//- (BOOL)validateDamage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* effect;

//- (BOOL)validateEffect:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* hide_mods;

@property (atomic) BOOL hide_modsValue;
- (BOOL)hide_modsValue;
- (void)setHide_modsValue:(BOOL)value_;

//- (BOOL)validateHide_mods:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* oppMod;

//- (BOOL)validateOppMod:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* range;

//- (BOOL)validateRange:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* skill;

//- (BOOL)validateSkill:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* special;

//- (BOOL)validateSpecial:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* target;

//- (BOOL)validateTarget:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) HackingProgramGroup *group;

//- (BOOL)validateGroup:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *upgrades;

- (NSMutableSet*)upgradesSet;

@property (nonatomic, strong) WikiEntry *wikiEntry;

//- (BOOL)validateWikiEntry:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newUpgradesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _HackingProgram (UpgradesCoreDataGeneratedAccessors)
- (void)addUpgrades:(NSSet*)value_;
- (void)removeUpgrades:(NSSet*)value_;
- (void)addUpgradesObject:(HackingDevice*)value_;
- (void)removeUpgradesObject:(HackingDevice*)value_;

@end

@interface _HackingProgram (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAmmo;
- (void)setPrimitiveAmmo:(NSString*)value;

- (NSString*)primitiveAttMod;
- (void)setPrimitiveAttMod:(NSString*)value;

- (NSString*)primitiveBurst;
- (void)setPrimitiveBurst:(NSString*)value;

- (NSString*)primitiveDamage;
- (void)setPrimitiveDamage:(NSString*)value;

- (NSString*)primitiveEffect;
- (void)setPrimitiveEffect:(NSString*)value;

- (NSNumber*)primitiveHide_mods;
- (void)setPrimitiveHide_mods:(NSNumber*)value;

- (BOOL)primitiveHide_modsValue;
- (void)setPrimitiveHide_modsValue:(BOOL)value_;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveOppMod;
- (void)setPrimitiveOppMod:(NSString*)value;

- (NSString*)primitiveRange;
- (void)setPrimitiveRange:(NSString*)value;

- (NSString*)primitiveSkill;
- (void)setPrimitiveSkill:(NSString*)value;

- (NSString*)primitiveSpecial;
- (void)setPrimitiveSpecial:(NSString*)value;

- (NSString*)primitiveTarget;
- (void)setPrimitiveTarget:(NSString*)value;

- (HackingProgramGroup*)primitiveGroup;
- (void)setPrimitiveGroup:(HackingProgramGroup*)value;

- (NSMutableSet*)primitiveUpgrades;
- (void)setPrimitiveUpgrades:(NSMutableSet*)value;

- (WikiEntry*)primitiveWikiEntry;
- (void)setPrimitiveWikiEntry:(WikiEntry*)value;

@end
