//
//  ArmyViewController.h
//  Toolbox
//
//  Created by Paul on 10/20/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "Containable.h"
#import "FactionViewController.h"



@class Army;

@interface ArmyViewController : FactionViewController <Containable>
{
    NSArray* sectorials;
}

@property (nonatomic, assign) Army* army;

-(void)refreshUnits;

@end
