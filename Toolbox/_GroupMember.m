// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to GroupMember.m instead.

#import "_GroupMember.h"

const struct GroupMemberAttributes GroupMemberAttributes = {
	.isDead = @"isDead",
};

const struct GroupMemberRelationships GroupMemberRelationships = {
	.childrenGroupMembers = @"childrenGroupMembers",
	.combatGroup = @"combatGroup",
	.notes = @"notes",
	.parentGroupMember = @"parentGroupMember",
	.specops = @"specops",
	.unitOption = @"unitOption",
	.unitProfile = @"unitProfile",
};

@implementation GroupMemberID
@end

@implementation _GroupMember

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"GroupMember" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"GroupMember";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"GroupMember" inManagedObjectContext:moc_];
}

- (GroupMemberID*)objectID {
	return (GroupMemberID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"isDeadValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"isDead"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic isDead;

- (BOOL)isDeadValue {
	NSNumber *result = [self isDead];
	return [result boolValue];
}

- (void)setIsDeadValue:(BOOL)value_ {
	[self setIsDead:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveIsDeadValue {
	NSNumber *result = [self primitiveIsDead];
	return [result boolValue];
}

- (void)setPrimitiveIsDeadValue:(BOOL)value_ {
	[self setPrimitiveIsDead:[NSNumber numberWithBool:value_]];
}

@dynamic childrenGroupMembers;

- (NSMutableOrderedSet*)childrenGroupMembersSet {
	[self willAccessValueForKey:@"childrenGroupMembers"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"childrenGroupMembers"];

	[self didAccessValueForKey:@"childrenGroupMembers"];
	return result;
}

@dynamic combatGroup;

@dynamic notes;

- (NSMutableSet*)notesSet {
	[self willAccessValueForKey:@"notes"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"notes"];

	[self didAccessValueForKey:@"notes"];
	return result;
}

@dynamic parentGroupMember;

@dynamic specops;

@dynamic unitOption;

@dynamic unitProfile;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newChildrenGroupMembersFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"GroupMember" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"parentGroupMember == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

- (NSFetchedResultsController*)newNotesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"Note" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"groupMember == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

@implementation _GroupMember (ChildrenGroupMembersCoreDataGeneratedAccessors)
- (void)addChildrenGroupMembers:(NSOrderedSet*)value_ {
	[self.childrenGroupMembersSet unionOrderedSet:value_];
}
- (void)removeChildrenGroupMembers:(NSOrderedSet*)value_ {
	[self.childrenGroupMembersSet minusOrderedSet:value_];
}
- (void)addChildrenGroupMembersObject:(GroupMember*)value_ {
	[self.childrenGroupMembersSet addObject:value_];
}
- (void)removeChildrenGroupMembersObject:(GroupMember*)value_ {
	[self.childrenGroupMembersSet removeObject:value_];
}
- (void)insertObject:(GroupMember*)value inChildrenGroupMembersAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"childrenGroupMembers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self childrenGroupMembers]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"childrenGroupMembers"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"childrenGroupMembers"];
}
- (void)removeObjectFromChildrenGroupMembersAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"childrenGroupMembers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self childrenGroupMembers]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"childrenGroupMembers"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"childrenGroupMembers"];
}
- (void)insertChildrenGroupMembers:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"childrenGroupMembers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self childrenGroupMembers]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"childrenGroupMembers"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"childrenGroupMembers"];
}
- (void)removeChildrenGroupMembersAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"childrenGroupMembers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self childrenGroupMembers]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"childrenGroupMembers"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"childrenGroupMembers"];
}
- (void)replaceObjectInChildrenGroupMembersAtIndex:(NSUInteger)idx withObject:(GroupMember*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"childrenGroupMembers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self childrenGroupMembers]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"childrenGroupMembers"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"childrenGroupMembers"];
}
- (void)replaceChildrenGroupMembersAtIndexes:(NSIndexSet *)indexes withChildrenGroupMembers:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"childrenGroupMembers"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self childrenGroupMembers]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"childrenGroupMembers"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"childrenGroupMembers"];
}
@end

