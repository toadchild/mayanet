//
//  DetailViewController.m
//  Toolbox
//
//  Created by Paul on 10/20/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "DetailViewController.h"
#import "Unit.h"
#import "UnitType.h"
#import "UnitClassification.h"
#import "UnitAspects.h"
#import "SpecialRule.h"
#import "Army.h"
#import "Sectorial.h"
#import "Util.h"
#import "UnitOption.h"
#import "UnitOptionTableViewCell.h"
#import "Weapon.h"
#import "Roster.h"
#import "AppDelegate.h"
#import "ContainerViewController.h"
#import "WeaponTableViewCell.h"
#import "HackingProgramTableViewCell.h"
#import "PherowareTacticTableViewCell.h"
#import "CCSkillTableViewCell.h"
#import "SkillNotesTableViewCell.h"
#import "SpecOps.h"
#import "SpecOpsBaseUnitOption.h"
#import "SpecOpsWeapon.h"
#import "SpecOpsSpecialRule.h"
#import "SpecOpsEquipment.h"
#import "SpecOpsBuild.h"
#import "SpecOpsAspects.h"
#import "WikiWebViewController.h"
#import "AspectSegmentedControl.h"
#import "GroupMember.h"
#import "UnitProfile.h"
#import "CombatGroup.h"
#import "UIColor+ColorPalette.h"
#import "HackingProgram.h"
#import "HackingProgramGroup.h"
#import "HackingDevice.h"
#import "CCSkill.h"
#import "WikiEntry.h"
#import "UnitNote.h"
#import "ColorCompat.h"

@interface DetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@end

@implementation DetailViewController

#pragma mark - Initialization

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        cellHeightCache = [[NSMutableDictionary alloc] init];
        self.unitOption = nil;
        self.unit = nil;
        self.groupMember = nil;
        weapons = [[NSMutableOrderedSet alloc] init];
        templateWeapons = [[NSMutableOrderedSet alloc] init];
        hacking = [[NSMutableOrderedSet alloc] init];
        pheroware = [[NSMutableOrderedSet alloc] init];
        ccskill = [[NSMutableOrderedSet alloc] init];
        ccweapons = [[NSMutableOrderedSet alloc] init];
        deployables = [[NSMutableOrderedSet alloc] init];
        miscskill = [[NSMutableOrderedSet alloc] init];
        notes = [[NSMutableOrderedSet alloc] init];

        NSString* path = [[NSBundle mainBundle] pathForResource:@"linkableTemplate" ofType:@"html"];
        linkableTemplate = [NSString stringWithContentsOfFile:path
                                                      encoding:NSUTF8StringEncoding
                                                         error:NULL];
        path = [[NSBundle mainBundle] pathForResource:@"linkableHeaderTemplate" ofType:@"html"];
        linkableHeaderTemplate = [NSString stringWithContentsOfFile:path
                                                     encoding:NSUTF8StringEncoding
                                                        error:NULL];
        viewWidth = 0;
        
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        self.managedObjectContext = [delegate managedObjectContext];
        
        currentDisplayMode = eNoDisplayMode;
        previousDisplayMode = eNoDisplayMode;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
    
    [self.unitOptionsTableView setAlwaysBounceVertical:FALSE];
    //[self.navigationController.navigationBar setBarTintColor:[UIColor redColor]];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self configureNavigationColor];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MiddleViewControllerInitialization" object:nil];
    [super viewWillAppear:animated];
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    MiddleNavigationController* parent = delegate.containerViewController.middleViewController;
    CGFloat parentWidth = parent.view.superview.frame.size.width;
    

    if(viewWidth != 0)
    {
        if(viewWidth != parentWidth)
        {
            viewWidth = parentWidth;
            [cellHeightCache removeAllObjects];
            [self.unitOptionsTableView reloadData];
        }
    }
    else
    {
        viewWidth = parentWidth;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"ViewWiki"])
    {
        //WikiWebViewController* con = [segue destinationViewController];
        //con.wikiEntry = sender;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Managing the detail item

-(void) unitSelection:(Unit*)unit
{
    //If there is a spec ops, then initialize specOpsBuildMode
    if(unit.specops)
    {
        currentDisplayMode = eBrowseSpecOpsBuildMode;
    }
    //Starting Browse Display mode for unit
    else
    {
        currentDisplayMode = eBrowseUnitDisplayMode;
    }
    
    self.specOpsBuild = nil;
    self.groupMember = nil;
    self.unit = unit;
    
    [self postUnitSelection];
}

-(void) unitDetailSelection:(Unit*)unit
{
    //If there is a spec ops, then initialize specOpsBuildMode
    if(unit.specops)
    {
        currentDisplayMode = eBrowseSpecOpsBuildMode;
    }
    else
    {
        currentDisplayMode = eBrowseUnitWeaponDisplayMode;
    }
    
    self.specOpsBuild = nil;
    self.groupMember = nil;
    self.unit = unit;
    
    [self postUnitSelection];
}

-(void) unitSelection:(Unit*)unit groupMember:(GroupMember*)groupMember
{

    currentDisplayMode = eRosterUnitDisplayMode;
    self.specOpsBuild = nil;
    self.unit = unit;
    self.unitOption = groupMember.unitOption;
    self.groupMember = groupMember;
    
    [self postUnitSelection];

}
    
-(void) unitDetailSelection:(Unit*)unit groupMember:(GroupMember*)groupMember
{
    currentDisplayMode = eRosterUnitOptionDisplayMode;
    self.specOpsBuild = nil;
    self.unit = unit;
    self.unitOption = groupMember.unitOption;
    self.groupMember = groupMember;
    
    [self postUnitSelection];
}
    
-(void) specOpsBuildSelection:(SpecOpsBuild*)specOpsBuild
{
    
    currentDisplayMode = eSpecOpsDisplayMode;
    
    self.specOpsBuild = specOpsBuild;
    self.groupMember = specOpsBuild.groupMember;
    self.unit = self.specOpsBuild.unit;
    self.unitOption = nil;
    
    [self postUnitSelection];
}

-(void) specOpsRebuild:(SpecOpsBuild*)specOpsBuild
{
    
    currentDisplayMode = eSpecOpsRebuildMode;
    
    self.specOpsBuild = specOpsBuild;
    self.groupMember = specOpsBuild.groupMember;
    self.unit = self.specOpsBuild.unit;
    self.unitOption = nil;
    
    [self postUnitSelection];
}



//If groupMember is nil, then we don't filter, we just remove groupMember specific browse
-(void)invalidateGroupMember:(GroupMember*)groupMember
{
    if((self.groupMember == groupMember || groupMember == nil) && !groupMember.combatGroup.roster.isPlayingValue)
    {
        if(self.specOpsBuild)
        {
            currentDisplayMode = eBrowseSpecOpsBuildMode;
            self.specOpsBuild = nil;
        }
        else
        {
            currentDisplayMode = eBrowseUnitDisplayMode;
        }
        self.groupMember = nil;

    }
    [self postUnitSelection];
}

-(void)postUnitSelection
{
    //Hide Browse if selecting from roster in Portrait
    if(UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        if([delegate.containerViewController isRightViewControllerVisible])
            [delegate.containerViewController displayLeftViewController:FALSE animationCompletionBlock:nil];
    }
    
    // Update the view.
    [self configureView];
    [self.unitOptionsTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

// Returns the currently selected alt profile
- (UnitProfile*)selectedAltProfile
{
    // First index is reserved for the unit itself, not a profile.
    if ([self.profileSelectionSegmentedControl selectedSegmentIndex] == 0)
    {
        return nil;
    }
    
    if([availableProfiles count]) {
        return availableProfiles[[self.profileSelectionSegmentedControl selectedSegmentIndex] - 1];
    }
    
    return nil;
}

// Returns whether the list the currently selected unit is from is currently in play mode
- (BOOL)isPlaying
{
    if(self.groupMember)
    {
        return self.groupMember.combatGroup.roster.isPlayingValue;
    }
    return FALSE;
}

#pragma mark - View Configuration
-(void)clearView
{
    tappedRow = -1;
    self.specOpsCost.text = @"";
    [self.specOpsAddButton setHidden:TRUE];
    [self.specOpsResetButton setHidden:TRUE];
    [weapons removeAllObjects];
    [templateWeapons removeAllObjects];
    [hacking removeAllObjects];
    [pheroware removeAllObjects];
    [ccskill removeAllObjects];
    [ccweapons removeAllObjects];
    [deployables removeAllObjects];
    [miscskill removeAllObjects];
    [notes removeAllObjects];
    [cellHeightCache removeAllObjects];
}
- (void)configureView
{
    [self viewWillLayoutSubviews];
    [self clearView];
    [self getSpecOpsBuild];
    [self configureNavigationColor];
    
    if(currentDisplayMode == eBrowseSpecOpsBuildMode || currentDisplayMode == eSpecOpsRebuildMode)
    {
        [self configureSpecOpsView];
    }
    
    if(currentDisplayMode == eBrowseUnitDisplayMode ||
       currentDisplayMode == eRosterUnitDisplayMode)
    {
        [self populateUnitOptions];
    }
    
    [self loadUnit];
    [self onProfileSelection:nil];
}

-(void)configureNavigationColor
{
    UIColor* backgroundColor;
    UIColor* primaryColor;
    UIColor* secondaryColor;
    
    if(self.unit)
    {
        if(self.unit.sectorial)
        {
            backgroundColor = [self.unit.sectorial backgroundColor];
            primaryColor = [self.unit.sectorial primaryColor];
            secondaryColor = [self.unit.sectorial secondaryColor];
        }
        else
        {
            backgroundColor = [self.unit.army backgroundColor];
            primaryColor = [self.unit.army primaryColor];
            secondaryColor = [self.unit.army secondaryColor];
        }
    }
    else
    {
        backgroundColor = [UIColor barColor];
        primaryColor = [UIColor titleColor];
        secondaryColor = [UIColor barItemColor];
    }
    
    [self.navigationController.navigationBar setBarTintColor:backgroundColor];
    [self.navigationController.navigationBar setTintColor:secondaryColor];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      primaryColor,
      NSForegroundColorAttributeName,nil]];
    
}

-(void)loadUnit
{
    if (self.unit) {
        Unit* unit = nil;
        if(self.specOpsBuild != nil)
        {
            // The selected item is a Spec-Op
            unit = [self.specOpsBuild.baseUnitOption.unitOption getUnit];
        }
        else if(self.groupMember.unitProfile)
        {
            // The selected item is an independent child profile (Duroc, Auxbot, etc)
        }
        else
        {
            // The selected item is a normal unit
            unit = self.unit;
        }

        //Filter avialable profiles based on unit option
        availableProfiles = [[NSMutableArray alloc] init];
        
        if(unit)
        {
            for(UnitProfile* unitProfile in unit.profiles)
            {
                // Skip independent profiles
                if(unitProfile.isIndependentValue && (currentDisplayMode == eRosterUnitOptionDisplayMode || currentDisplayMode == eRosterUnitDisplayMode))
                {
                    continue;
                }

                // Skip non-relevant per-option profiles
                if(currentDisplayMode == eRosterUnitOptionDisplayMode && unitProfile.optionSpecificValue && ![self.unitOption.enabledUnitProfile containsObject:unitProfile])
                {
                    continue;
                }
                
                [availableProfiles addObject:unitProfile];
            }
        }

        if([availableProfiles count] >= 1)
        {
            [self.profileSelectionSegmentedControl setHidden:FALSE];
            [self.profileSelectionSegmentedControl removeConstraint:self.profileSegmentedControlHeightConstraint];
            [self.profileSelectionSegmentedControl removeAllSegments];
            // Add base unit at first spot
            [self.profileSelectionSegmentedControl insertSegmentWithTitle:unit.name atIndex:0 animated:FALSE];
            NSUInteger index = 1;
            for(UnitProfile* unitProfile in availableProfiles)
            {
                [self.profileSelectionSegmentedControl insertSegmentWithTitle:unitProfile.name atIndex:index++ animated:FALSE];
            }
            [self.profileSelectionSegmentedControl setSelectedSegmentIndex:0];
        }
        else
        {
            [self.profileSelectionSegmentedControl setHidden:TRUE];
            [self.profileSelectionSegmentedControl addConstraint:self.profileSegmentedControlHeightConstraint];
        }
        
    }
}

-(void) populateStatBlock
{
    UnitAspects* unitAspects;
    SpecOpsAspects* specOpsAspects = nil;
    Unit* unit = nil;
    UnitProfile *unitProfile = [self selectedAltProfile];
    UnitType* unitType = nil;
    if(self.specOpsBuild != nil)
    {
        // The selected item is a Spec-Op
        specOpsAspects = [self getSpecOpsBuild].aspects;
        unit = [self.specOpsBuild.baseUnitOption.unitOption getUnit];
        unitAspects = unit.aspects;
        unitType = unit.specops.type;
    }
    else if(self.groupMember.unitProfile)
    {
        return [self populateStatBlockFromProfile:self.groupMember.unitProfile];
    }
    else if(unitProfile)
    {
        return [self populateStatBlockFromProfile:unitProfile];
    }
    else
    {
        // The selected item is a normal unit
        unit = self.unit;
        unitAspects = unit.aspects;
        unitType = unit.type;
    }

    self.unitNameLabel.text = [unit name];
    self.unitTypeLabel.text = unitType.code;
    if (unitAspects.classification) {
        self.unitClassificationLabel.text = unitAspects.classification.displayName;
    } else {
        self.unitClassificationLabel.text = @"";
    }

    if (unit.imageTitle) {
        self.unitIconImageView.image = [UIImage imageNamed:[self.unit imageTitle]];
    }
    
    //Detect if either army, sectorial, or roster is Mercs, so as to show Merc AVA
    BOOL showMercAVA = FALSE;
    if(self.unit.army)
    {
        if(self.unit.army.showMercsValue)
            showMercAVA = TRUE;
    }
    else if(self.unit.sectorial)
    {
        if(self.unit.sectorial.showMercsValue)
        {
            showMercAVA = TRUE;
        }
    }
    
    if(self.unit.ava == nil)
    {
        self.unitAVALabel.text = @"T";
    }
    else if(showMercAVA && !self.unit.isMercValue && self.unit.mercAva != nil)
    {
        self.unitAVALabel.text = [NSString stringWithFormat:@"%@ (%d)", self.unit.ava, [self.unit.ava intValue] + [self.unit.mercAva intValue]];
    }
    else if (!showMercAVA && self.unit.isMercValue)
    {
        self.unitAVALabel.text = @"0";
    }
    else
    {
        self.unitAVALabel.text =  [NSString stringWithFormat:@"%d", [unit.ava intValue]];
    }

    //Modifiy Wound header to compensate for Tags or remotes
    if(([unit.type isStructure] || unit.strOverrideValue) &&!unit.wOverrideValue)
    {
        self.woundsHeaderLabel.text = @"STR";
    }
    else
    {
        self.woundsHeaderLabel.text = @"W";
    }
    
    [self populateStatBlockFromAspects:unitAspects SpecOpsAspects:specOpsAspects];
}

-(void) populateStatBlockFromProfile:(UnitProfile*) profile
{
    // The They have selected a sub-profile of this unit
    self.unitNameLabel.text = profile.name;
    NSString* imageTitle = [profile imageTitle];
    self.unitIconImageView.image = [UIImage imageNamed:imageTitle];
    
    //Modifiy Wound header to compensate for Tags or remotes
    if(([profile.type isStructure] || profile.strOverrideValue) &&!profile.wOverrideValue)
    {
        self.woundsHeaderLabel.text = @"STR";
    }
    else
    {
        self.woundsHeaderLabel.text = @"W";
    }
    
    UnitAspects* profileAspects = profile.aspects;
    
    self.unitTypeLabel.text = profile.type.code;
    if (profileAspects.classification) {
        self.unitClassificationLabel.text = profileAspects.classification.displayName;
    } else {
        self.unitClassificationLabel.text = @"";
    }
    
    // Perimeter weapons
    SpecialRule* perimeter = [SpecialRule specialRuleWithName:@"Perimeter"];
    if([profile.aspects.specialRules containsObject:perimeter])
    {
        return [self populatePerimeterStatBlock:profile];
    }

    [self populateStatBlockFromAspects:profileAspects SpecOpsAspects:nil];
}

-(void) populatePerimeterStatBlock:(UnitProfile*) profile
{
    UnitAspects* profileAspects = profile.aspects;
    self.unitMOVLabel.text = [profileAspects convertedMov];
    self.unitAVALabel.text = @"-";
    self.unitTypeLabel.text = @"";
    self.unitCCLabel.text = self.unitBSLabel.text = self.unitPHLabel.text = self.unitWIPLabel.text = @"--";
    self.unitARMLabel.text =  [NSString stringWithFormat:@"%d", [profileAspects.arm intValue]];
    self.unitBTSLabel.text =  [NSString stringWithFormat:@"%d", [profileAspects.bts intValue]];
    self.unitWLabel.text =  [NSString stringWithFormat:@"%d", [profileAspects.wounds intValue]];
    self.unitSLabel.text =  [NSString stringWithFormat:@"%d", [profileAspects.silhouette intValue]];
    self.woundsHeaderLabel.text = @"STR";
    self.unitImpetuousImageView.image = nil;
    self.unitRegularImageView.image = nil;
    self.unitCubeImageView.image = nil;
    self.unitHackableImageView.image = nil;
    self.unitIconImageView.image = nil;
}

-(void)populateStatBlockFromAspects:(UnitAspects*)unitAspects SpecOpsAspects:(SpecOpsAspects*)specOpsAspects
{
    self.unitMOVLabel.text = [unitAspects convertedMov];
    self.unitCCLabel.text =  [NSString stringWithFormat:@"%d", [unitAspects.cc intValue] + ((specOpsAspects) ? specOpsAspects.ccIncreaseValue : 0)];
    self.unitBSLabel.text =  [NSString stringWithFormat:@"%d", [unitAspects.bs intValue] + ((specOpsAspects) ? specOpsAspects.bsIncreaseValue : 0)];
    if(specOpsAspects){
        self.unitPHLabel.text =  [NSString stringWithFormat:@"%d", MIN([unitAspects.ph intValue] + specOpsAspects.phIncreaseValue, 14)]; //Max PH of 14
    }
    else
    {
        self.unitPHLabel.text =  [NSString stringWithFormat:@"%d", [unitAspects.ph intValue]];
    }
    self.unitARMLabel.text =  [NSString stringWithFormat:@"%d", [unitAspects.arm intValue] + ((specOpsAspects) ? specOpsAspects.armIncreaseValue : 0)];
    self.unitBTSLabel.text =  [NSString stringWithFormat:@"%d", [unitAspects.bts intValue] + ((specOpsAspects) ? abs(specOpsAspects.btsIncreaseValue) : 0)];
    self.unitWLabel.text =  [NSString stringWithFormat:@"%d", [unitAspects.wounds intValue] + ((specOpsAspects) ? specOpsAspects.wIncreaseValue : 0)];
    self.unitSLabel.text =  [NSString stringWithFormat:@"%d", [unitAspects.silhouette intValue]];
    
    if(specOpsAspects)
    {
        self.unitWIPLabel.text =  [NSString stringWithFormat:@"%d", MIN([unitAspects.wip intValue] + specOpsAspects.wipIncreaseValue, 15)]; //Max WIP of 15;
    }
    else
    {
        self.unitWIPLabel.text =  [NSString stringWithFormat:@"%d", [unitAspects.wip intValue]];
    }
    
    self.unitRegularImageView.image = [UIImage imageNamed:[unitAspects.regular boolValue] ? @"images/regular_icon.png" : @"images/irregular_icon.png" ];
    if(unitAspects.furyValue == FURY_IMPETUOUS)
    {
        self.unitImpetuousImageView.image = [UIImage imageNamed: @"images/impetuous_icon.png"];
        self.cubeHorizontalSpacingConstraint.constant = 42;
        self.hackableHorizontalSpacingConstraint.constant = 42;
    }
    else if(unitAspects.furyValue == FURY_EXTREME_IMPETUOUS)
    {
        self.unitImpetuousImageView.image = [UIImage imageNamed: @"images/extreme_impetuous_icon.png"];
        self.cubeHorizontalSpacingConstraint.constant = 42;
        self.hackableHorizontalSpacingConstraint.constant = 42;
    }
    else if(unitAspects.furyValue == FURY_FRENZY)
    {
        self.unitImpetuousImageView.image = [UIImage imageNamed: @"images/frenzy_icon.png"];
        self.cubeHorizontalSpacingConstraint.constant = 42;
        self.hackableHorizontalSpacingConstraint.constant = 42;
    }
    else
    {
        self.unitImpetuousImageView.image = NULL;
        self.cubeHorizontalSpacingConstraint.constant = 9;
        self.hackableHorizontalSpacingConstraint.constant = 9;
    }
    
    if(unitAspects.cubeValue == 2)
    {
        self.unitCubeImageView.image = [UIImage imageNamed: @"images/cube2_icon.png"];
        self.hackableHorizontalSpacingConstraint.constant += 25 + 9;
    }
    else if(unitAspects.cubeValue == 1)
    {
        self.unitCubeImageView.image = [UIImage imageNamed: @"images/cube_icon.png"];
        self.hackableHorizontalSpacingConstraint.constant += 25 + 9;
    }
    else
    {
        self.unitCubeImageView.image = NULL;
    }
    
    if(unitAspects.hackableValue)
    {
        self.unitHackableImageView.image = [UIImage imageNamed: @"images/hackable_icon.png"];
    }
    else
    {
        self.unitHackableImageView.image = NULL;
        
    }
}

-(void) populateSpecialRulesLabel
{
    //Special Rule Label Population
    NSArray* unitSpecialRules;
    NSArray* unitOptionSpecialRules = @[];
    NSArray* _specOpsSpecialRules = @[];
    
    
    if(self.specOpsBuild != nil)
    {
        unitSpecialRules = [[self.specOpsBuild.baseUnitOption.unitOption getUnit].aspects.specialRules array];
        unitOptionSpecialRules = [self.specOpsBuild.baseUnitOption.unitOption.specialRules array];
        _specOpsSpecialRules = [self.specOpsBuild getSpecialRulesArray];
        
    }
    else if(self.groupMember.unitProfile)
    {
        unitSpecialRules = [self.groupMember.unitProfile.aspects.specialRules array];
    }
    else if(currentDisplayMode == eRosterUnitOptionDisplayMode)
    {
        UnitProfile *unitProfile = [self selectedAltProfile];
        if(unitProfile)
        {
            unitSpecialRules = [unitProfile.aspects.specialRules array];
            if([self unitProfileInheritsBase:unitProfile])
            {
                unitOptionSpecialRules = [self.unitOption.specialRules array];
            }
        }
        else
        {
            unitSpecialRules = [self.unit.aspects.specialRules array];
            unitOptionSpecialRules = [self.unitOption.specialRules array];
        }
    }
    else
    {
        UnitProfile *unitProfile = [self selectedAltProfile];
        if(unitProfile)
        {
            unitSpecialRules = [unitProfile.aspects.specialRules array];
        }
        else
        {
            unitSpecialRules = [self.unit.aspects.specialRules array];
        }
    }
    
    NSArray* specialRules = [unitOptionSpecialRules arrayByAddingObjectsFromArray:unitSpecialRules];
    specialRules = [specialRules arrayByAddingObjectsFromArray:_specOpsSpecialRules];
    
    if([specialRules count] != 0)
    {
        NSString* html = [self createLinkableHTMLWithBody: [self GetLinksFromSpecialRulesArray:specialRules]];
        self.unitSpecialRulesWebView.scrollView.scrollEnabled = TRUE;
        [self.unitSpecialRulesWebView loadHTMLString: html baseURL:nil];
    }
    else
    {
        self.specialRulesHeightConstraint.constant = 0;
    }

}

-(NSString*) createLinkableHTMLWithBody:(NSString*)body
{
    NSString* html = [linkableTemplate stringByReplacingOccurrencesOfString:@"${body}" withString:body];
    return html;
}

-(NSString*) createLinkableHeaderHTMLWithBody:(NSString*)body
{
    NSString* html = [linkableHeaderTemplate stringByReplacingOccurrencesOfString:@"${body}" withString:body];
    return html;
}

-(void) populateWeaponsLabel
{
    //Weapons Label Population
    
    NSArray* theWeapons;
    if(self.specOpsBuild)
    {
        NSArray* unitOptionWeapons = [self.specOpsBuild.baseUnitOption.unitOption.weapons array];
        NSArray* unitWeapons = [[self.specOpsBuild.baseUnitOption.unitOption getUnit].aspects.weapons array];
        NSArray* buildWeapons = [self.specOpsBuild getWeaponsArrayWithModes:FALSE];
        
        UnitProfile *unitProfile = [self selectedAltProfile];
        if(unitProfile)
        {
            theWeapons = [unitProfile.aspects.weapons array];
            if([self unitProfileInheritsBase:unitProfile])
            {
                theWeapons = [theWeapons arrayByAddingObjectsFromArray:unitOptionWeapons];
                theWeapons = [theWeapons arrayByAddingObjectsFromArray:buildWeapons];
            }
        }
        else
        {
            theWeapons = [unitOptionWeapons arrayByAddingObjectsFromArray:unitWeapons];
            theWeapons = [theWeapons arrayByAddingObjectsFromArray:buildWeapons];
        }
    }
    else if(currentDisplayMode == eRosterUnitDisplayMode)
    {
        if (self.groupMember.unitProfile) {
            theWeapons = [self.groupMember.unitProfile.aspects.weapons array];
        } else {
            UnitProfile *unitProfile = [self selectedAltProfile];
            if(unitProfile)
            {
                theWeapons = [unitProfile.aspects.weapons array];
            }
        }
    }
    else if(currentDisplayMode == eBrowseUnitDisplayMode)
    {
        // If a secondary profile is selected, show its weapons in the weapons list view.
        // But don't show them for the base unit, since the table cells below do that.
        UnitProfile *unitProfile = [self selectedAltProfile];
        if(unitProfile)
        {
            theWeapons = [unitProfile.aspects.weapons array];
        }
    }
    else if (currentDisplayMode == eRosterUnitOptionDisplayMode) {
        NSArray* unitOptionWeapons = nil;
        NSArray* unitWeapons = nil;

        UnitProfile *unitProfile = [self selectedAltProfile];
        if(unitProfile) {
            unitWeapons = [unitProfile.aspects.weapons array];
            if([self unitProfileInheritsBase:unitProfile]) {
                unitOptionWeapons = [self.unitOption.weapons array];
            }
        } else if(self.groupMember.unitProfile) {
            unitWeapons = [self.groupMember.unitProfile.aspects.weapons array];
        } else {
            unitWeapons = [self.unit.aspects.weapons array];
            unitOptionWeapons = [self.unitOption.weapons array];
        }
        theWeapons = [unitWeapons arrayByAddingObjectsFromArray:unitOptionWeapons];
    }
    
    if([theWeapons count] != 0)
    {
        NSString* html = [self createLinkableHTMLWithBody: [self GetLinksFromWeaponsArray:theWeapons]];
        self.unitWeaponsWebView.scrollView.scrollEnabled = TRUE;
        [self.unitWeaponsWebView loadHTMLString: html baseURL:nil];
    }
    else
    {
        self.weaponsHeightConstraint.constant = 0;
    }
    
}

-(void) addWeaponsFromOrderedSet:(NSOrderedSet*) source toMutableArray:(NSMutableArray*) dest
{
    for(Weapon* weapon in source)
    {
        [dest addObject: weapon];
        
        Weapon *altWeapon = weapon;
        while(altWeapon.altProfile && ![altWeapon.altProfile isEqualToString:@""])
        {
            altWeapon = [Weapon weaponWithName:altWeapon.altProfile];
            [dest addObject: altWeapon];
        }
    }
}

-(void) populateWeaponsArray
{
    //Remove all weapons for refresh
    NSArray* profileSpecialRules = @[];
    NSArray* unitSpecialRules = @[];
    NSArray* unitOptionSpecialRules = @[];
    NSMutableArray* profileWeapons = [[NSMutableArray alloc] init];
    NSMutableArray* unitWeapons = [[NSMutableArray alloc] init];
    NSMutableArray* unitOptionWeapons = [[NSMutableArray alloc] init];
    NSArray* _specOpsSpecialRules = @[];
    NSArray* _specOpsWeapons = @[];
    BOOL skipDefaultSkills = FALSE;
    bool displayOptions = false;
    
    // This contains either an independent child profile, or the selected sub-profile of a unit.
    UnitProfile *unitProfile = self.groupMember.unitProfile;
    if (!unitProfile)
    {
        unitProfile = [self selectedAltProfile];
    }
    
    [weapons removeAllObjects];
    [templateWeapons removeAllObjects];
    [hacking removeAllObjects];
    [pheroware removeAllObjects];
    [ccskill removeAllObjects];
    [ccweapons removeAllObjects];
    [deployables removeAllObjects];
    [miscskill removeAllObjects];
    [notes removeAllObjects];

    //Show the weapons for a unit option
    if(currentDisplayMode == eRosterUnitOptionDisplayMode)
    {
        if(unitProfile){
            profileSpecialRules = [unitProfile.aspects.specialRules array];
            [self addWeaponsFromOrderedSet:unitProfile.aspects.weapons toMutableArray:profileWeapons];
            
            // Non-independent profiles usually get the option-specific equipment
            if([self unitProfileInheritsBase:unitProfile])
            {
                unitOptionSpecialRules = [self.unitOption.specialRules array];
                [self addWeaponsFromOrderedSet:self.unitOption.weapons toMutableArray:unitOptionWeapons];
            }
        }
        else
        {
            unitSpecialRules = [self.unit.aspects.specialRules array];
            unitOptionSpecialRules = [self.unitOption.specialRules array];

            [self addWeaponsFromOrderedSet:self.unit.aspects.weapons toMutableArray:unitWeapons];
            [self addWeaponsFromOrderedSet:self.unitOption.weapons toMutableArray:unitOptionWeapons];
        }
    }
    //Show weapons that the spec ops can take
    else if(currentDisplayMode ==  eSpecOpsDisplayMode)
    {
        if(unitProfile)
        {
            profileSpecialRules = [unitProfile.aspects.specialRules array];
            [self addWeaponsFromOrderedSet:unitProfile.aspects.weapons toMutableArray:profileWeapons];
            
            // Non-independent profiles usually get the option-specific equipment
            if([self unitProfileInheritsBase:unitProfile])
            {
                unitOptionSpecialRules = [self.specOpsBuild.baseUnitOption.unitOption.specialRules array];
                [self addWeaponsFromOrderedSet:self.specOpsBuild.baseUnitOption.unitOption.weapons toMutableArray:unitOptionWeapons];
            }
        }
        else
        {
            unitSpecialRules = [[self.specOpsBuild.baseUnitOption.unitOption getUnit].aspects.specialRules array];
            unitOptionSpecialRules = [self.specOpsBuild.baseUnitOption.unitOption.specialRules array];
            
            [self addWeaponsFromOrderedSet:[self.specOpsBuild.baseUnitOption.unitOption getUnit].aspects.weapons toMutableArray:unitWeapons];

            [self addWeaponsFromOrderedSet:self.specOpsBuild.baseUnitOption.unitOption.weapons toMutableArray:unitOptionWeapons];

            _specOpsSpecialRules = [self.specOpsBuild getSpecialRulesArray];
            _specOpsWeapons = [self.specOpsBuild getWeaponsArrayWithModes:TRUE];
        }
    }
    //Show all weapons for the unit
    else if(currentDisplayMode == eBrowseUnitWeaponDisplayMode)
    {
        bool include_options = false;
        
        if(unitProfile){
            profileSpecialRules = [unitProfile.aspects.specialRules array];
            [self addWeaponsFromOrderedSet:unitProfile.aspects.weapons toMutableArray:profileWeapons];
            
            // Non-independent profiles usually get the option-specific equipment
            if([self unitProfileInheritsBase:unitProfile])
            {
                include_options = true;
            }
        }
        else
        {
            unitSpecialRules = [self.unit.aspects.specialRules array];
            [self addWeaponsFromOrderedSet:self.unit.aspects.weapons toMutableArray:unitWeapons];
            include_options = true;
        }

        if(include_options)
        {
            NSMutableSet* unitOptionSpecialRulesSet = [[NSMutableSet alloc] init];
            for(UnitOption* unitOption in self.unit.options)
            {
                [self addWeaponsFromOrderedSet:unitOption.weapons toMutableArray:unitOptionWeapons];
                for(SpecialRule* specialRule in unitOption.specialRules)
                {
                    [unitOptionSpecialRulesSet addObject:specialRule];
                }
            }
            unitOptionSpecialRules = [unitOptionSpecialRulesSet allObjects];
        }
    }
    else if (currentDisplayMode == eBrowseUnitDisplayMode) {
        // For unit listing, only show the selector to choose between unit options and notes
        skipDefaultSkills = TRUE;
        displayOptions = true;
    }
    else if (currentDisplayMode == eRosterUnitDisplayMode) {
        // For unit listing, only show the selector to choose between unit options and notes
        skipDefaultSkills = TRUE;
        // Only show options if it's not an independent profile
        if (!unitProfile.independentCostValue) {
            displayOptions = true;
        }
    }
    else
    {
        [self.skillSelectionSegmentedControl setHidden:TRUE];
        [self.skillSelectionSegmentedControl addConstraint:self.skillSegmentedControlHeightConstraint];
        return;
    }
    
    NSArray* specialRules = [unitOptionSpecialRules arrayByAddingObjectsFromArray:unitSpecialRules];
    specialRules = [specialRules arrayByAddingObjectsFromArray:profileSpecialRules];
    specialRules = [specialRules arrayByAddingObjectsFromArray:_specOpsSpecialRules];
    
    NSArray* _weapons = [unitOptionWeapons arrayByAddingObjectsFromArray:unitWeapons];
    _weapons = [_weapons arrayByAddingObjectsFromArray:profileWeapons];
    _weapons = [_weapons arrayByAddingObjectsFromArray:_specOpsWeapons];
    
    [self populateWeaponsArraysfromArray:_weapons withSpecialRules:specialRules andSpecOpsRules: _specOpsSpecialRules skippingDefaultSkills:skipDefaultSkills];
    
    // Also add hacking programs
    [self addHackingProgramsFromSpecialRules:specialRules];
    
    // Pheroware Tactics
    [self addPherowareTacticsFromSpecialRules:specialRules];
    
    // Add CC skills
    [self addCCSkillsFromSpecialRules:specialRules];
    
    // Unit notes
    [notes addObjectsFromArray:[self.unit.notes array]];
    
    if([weapons count] || [templateWeapons count] || [ccweapons count] || [ccskill count] || [hacking count] || [notes count])
    {
        [self.skillSelectionSegmentedControl setHidden:FALSE];
        [self.skillSelectionSegmentedControl removeConstraint:self.skillSegmentedControlHeightConstraint];
        [self.skillSelectionSegmentedControl removeAllSegments];
    }
    else
    {
        // Things like Seed-Embryos may have no skills at all
        [self.skillSelectionSegmentedControl setHidden:TRUE];
        [self.skillSelectionSegmentedControl addConstraint:self.skillSegmentedControlHeightConstraint];
        selectedSkill = eSkillOptions;
        return;
    }
    
    int index = 0;
    if([weapons count])
    {
        [self.skillSelectionSegmentedControl insertSegmentWithTitle:@"BS" atIndex:index++ animated:FALSE];
    }
    
    if([templateWeapons count])
    {
        [self.skillSelectionSegmentedControl insertSegmentWithTitle:@"DTW" atIndex:index++ animated:FALSE];
    }
    
    if([ccweapons count] || [ccskill count])
    {
        [self.skillSelectionSegmentedControl insertSegmentWithTitle:@"CC" atIndex:index++ animated:FALSE];
    }

    if([deployables count])
    {
        [self.skillSelectionSegmentedControl insertSegmentWithTitle:@"Deployable" atIndex:index++ animated:FALSE];
    }
    
    if([miscskill count])
    {
        [self.skillSelectionSegmentedControl insertSegmentWithTitle:@"Misc" atIndex:index++ animated:FALSE];
    }

    if([hacking count])
    {
        [self.skillSelectionSegmentedControl insertSegmentWithTitle:@"Hacking" atIndex:index++ animated:FALSE];
    }
    
    if([pheroware count])
    {
        [self.skillSelectionSegmentedControl insertSegmentWithTitle:@"Pheroware" atIndex:index++ animated:FALSE];
    }
    
    if([notes count])
    {
        // If this is a unit display view mode, we may have both unit options and notes.
        if (displayOptions) {
            [self.skillSelectionSegmentedControl insertSegmentWithTitle:@"Options" atIndex:index++ animated:FALSE];
        }
        [self.skillSelectionSegmentedControl insertSegmentWithTitle:@"Notes" atIndex:index++ animated:FALSE];
    }
    
    // Initialize selected skill
    [self.skillSelectionSegmentedControl setSelectedSegmentIndex:0];
    [self onSkillSelection:nil];
}

-(void) populateWeaponsArraysfromArray:(NSArray*)source withSpecialRules:(NSArray*) specialRules andSpecOpsRules:(NSArray*) specOpsRules skippingDefaultSkills:(BOOL) skipDefaultSkills
{
    NSMutableArray *allWeapons = [source mutableCopy];
    for(SpecialRule* specialRule in specialRules)
    {
        Weapon* weapon = [Weapon weaponWithName:specialRule.name];
        if(weapon)
        {
            [allWeapons addObject:weapon];
        }
        if([specialRule.name compare:@"Forward Observer"] == NSOrderedSame)
        {
            Weapon* weapon = [Weapon weaponWithName:@"Flash Pulse"];
            [allWeapons addObject:weapon];
        }
        if([specialRule.name compare:@"Engineer"] == NSOrderedSame)
        {
            Weapon* weapon = [Weapon weaponWithName:@"Deactivator"];
            [allWeapons addObject:weapon];
        }
    }
    
    for(SpecialRule* specialRule in specOpsRules)
    {
        if([specialRule.name isEqualToString:@"Minelayer"])
        {
            Weapon* weapon = [Weapon weaponWithName:@"Antipersonnel Mines"];
            [allWeapons addObject:weapon];
        }
    }
    
    BOOL suppressive = FALSE;
    for(Weapon* weapon in allWeapons)
    {
        BOOL added = false;
        BOOL isTemplate = ![weapon.templateType isEqualToString: @"No"];
        BOOL isDeployable = [weapon.note containsString:@"Deployable"];
        BOOL isGuided = [weapon.note containsString:@"Guided"];
        BOOL isSpecialDodge = [weapon.note containsString:@"Special Dodge"];
        BOOL isAdhesive = ([weapon.ammo caseInsensitiveCompare:@"Adhesive"] == NSOrderedSame);
        if(weapon.shortDistance == nil && isTemplate && !isDeployable && !isGuided)
        {
            [templateWeapons addObject:weapon];
            added = true;
        }
        if([weapon.closeCombat boolValue])
        {
            [ccweapons addObject:weapon];
            added = true;
        }
        if(isDeployable)
        {
            [deployables addObject:weapon];
            added = true;
        }
        if((weapon.shortDistance != nil && (weapon.damage != nil || isAdhesive)) || isGuided || isSpecialDodge)
        {
            [weapons addObject:weapon];
            added = true;
            
            if(weapon.suppressiveValue)
            {
                suppressive = TRUE;
            }
        }
        if(!added)
        {
            // Fallback case for things that did not fit elsewhere
            [miscskill addObject:weapon];
        }
    }
    
    // Everyone except seed-embryos, AI Beacons, and CrazyKoalas can do these things.
    SpecialRule *seed = [SpecialRule specialRuleWithName:@"Seed-Embryo"];
    SpecialRule *beacon = [SpecialRule specialRuleWithName:@"AI Beacon"];
    SpecialRule *perimeter = [SpecialRule specialRuleWithName:@"Perimeter"];
    if(![specialRules containsObject:seed] && ![specialRules containsObject:beacon] && ![specialRules containsObject:perimeter] &&!skipDefaultSkills)
    {
        [ccweapons addObject:[Weapon weaponWithName:@"Bare-Handed"]];
        [miscskill addObject:[Weapon weaponWithName:@"Discover"]];
    }

    // Sort BS weapons
    [weapons sortUsingComparator:^(Weapon *obj1, Weapon *obj2) {
        return [obj1.name compare:obj2.name];
    }];

    // Sort CC weapons
    [ccweapons sortUsingComparator:^(Weapon *obj1, Weapon *obj2) {
        return [obj1.name compare:obj2.name];
    }];
    
    // Sort misc skills
    [miscskill sortUsingComparator:^(Weapon *obj1, Weapon *obj2) {
        return [obj1.name compare:obj2.name];
    }];
    
    // Placed last, after all BS weapons
    if(suppressive)
    {
        [weapons addObject:[Weapon weaponWithName:@"Suppressive Fire"]];
    }
}

-(void) addHackingProgramsFromSpecialRules:(NSArray*) specialRules
{
    for(SpecialRule *rule in specialRules)
    {
        HackingDevice *device = [HackingDevice deviceWithName:rule.name];
        if(device){
            for(HackingProgramGroup *group in device.groups)
            {
                for(HackingProgram *program in group.programs)
                {
                    [hacking addObject:program];
                }
            }
            for(HackingProgram *program in device.upgrades)
            {
                [hacking addObject:program];
            }
        }
    }
    [hacking sortUsingComparator:^(HackingProgram *obj1, HackingProgram *obj2) {
        if(obj1.group && obj2.group)
        {
            NSComparisonResult c = [obj1.group.name compare:obj2.group.name];
            if(c == NSOrderedSame)
            {
                return [obj1.name compare:obj2.name];
            }
            return c;
        }
        else if(obj1.group)
        {
            // Groups before upgrades
            return NSOrderedAscending;
        }
        else if(obj2.group)
        {
            // Ugrades after groups
            return NSOrderedDescending;
        }
        return [obj1.name compare:obj2.name];
    }];
}

-(void) addPherowareTacticsFromSpecialRules:(NSArray*) specialRules
{
    for(SpecialRule *rule in specialRules)
    {
        // Should be handled more generically, but...
        if([rule.name isEqualToString:@"SymbioBomb (2)"]){
            // Symbiobomb provides all tactics
            [pheroware addObjectsFromArray:[PherowareTactic allTactics]];
        } else if([rule.name containsString:@"Pheroware Tactics: "]) {
            // Some units have specific skills
            NSString* tactic = [rule.name substringFromIndex:19];
            [pheroware addObject:[PherowareTactic tacticWithName:tactic]];
        }
    }

    [pheroware sortUsingComparator:^(PherowareTactic *obj1, PherowareTactic *obj2) {
        NSComparisonResult c = [obj1.tacticType compare:obj2.tacticType];
        if(c == NSOrderedSame)
        {
            return [obj1.name compare:obj2.name];
        }
        return c;
    }];
}

-(void) addCCSkillsFromSpecialRules:(NSArray*) specialRules
{
    int ma_level = 0;
    int berserk_level = 0;
    int protheion_level = 0;
    int guard_level = 0;
    BOOL nbw = FALSE;
    for(SpecialRule *rule in specialRules)
    {
        if([rule.name isEqualToString:@"Martial Arts L5"])
        {
            ma_level = 5;
        }
        else if([rule.name isEqualToString:@"Martial Arts L4"])
        {
            ma_level = 4;
        }
        else if([rule.name isEqualToString:@"Martial Arts L3"])
        {
            ma_level = 3;
        }
        else if([rule.name isEqualToString:@"Martial Arts L2"])
        {
            ma_level = 2;
        }
        else if([rule.name isEqualToString:@"Martial Arts L1"])
        {
            ma_level = 1;
        }
        
        if([rule.name isEqualToString:@"Berserk"])
        {
            berserk_level = 2;
        }
        else if([rule.name isEqualToString:@"Assault"])
        {
            berserk_level = 1;
        }
        
        if([rule.name isEqualToString:@"Protheion L5"])
        {
            protheion_level = 5;
        }
        else if([rule.name isEqualToString:@"Protheion L4"])
        {
            protheion_level = 4;
        }
        else if([rule.name isEqualToString:@"Protheion L3"])
        {
            protheion_level = 3;
        }
        else if([rule.name isEqualToString:@"Protheion L2"])
        {
            protheion_level = 2;
        }
        else if([rule.name isEqualToString:@"Protheion L1"])
        {
            protheion_level = 1;
        }

        if([rule.name isEqualToString:@"Guard L4"])
        {
            guard_level = 4;
        }
        else if([rule.name isEqualToString:@"Guard L3"])
        {
            guard_level = 3;
        }
        else if([rule.name isEqualToString:@"Guard L2"])
        {
            guard_level = 2;
        }
        else if([rule.name isEqualToString:@"Guard L1"])
        {
            guard_level = 1;
        }
        else if([rule.name isEqualToString:@"Natural Born Warrior"])
        {
            nbw = TRUE;
        }
    }
    
    while(ma_level > 0)
    {
        NSString *skill_name = [NSString stringWithFormat:@"Martial Arts L%d", ma_level];
        CCSkill *skill = [CCSkill skillWithName:skill_name];
        [ccskill addObject: skill];
        ma_level--;
    }
    
    while(protheion_level > 0)
    {
        NSString *skill_name = [NSString stringWithFormat:@"Protheion L%d", protheion_level];
        CCSkill *skill = [CCSkill skillWithName:skill_name];
        [ccskill addObject: skill];
        protheion_level--;
    }

    while(guard_level > 0)
    {
        NSString *skill_name = [NSString stringWithFormat:@"Guard L%d", guard_level];
        CCSkill *skill = [CCSkill skillWithName:skill_name];
        [ccskill addObject: skill];
        guard_level--;
    }

    if(berserk_level == 2)
    {
        CCSkill *skill = [CCSkill skillWithName:@"Berserk"];
        [ccskill addObject: skill];
    }
    
    if(berserk_level >= 1)
    {
        CCSkill *skill = [CCSkill skillWithName:@"Assault"];
        [ccskill addObject: skill];
    }
    
    if(nbw)
    {
        CCSkill *skill = [CCSkill skillWithName:@"Natural Born Warrior Mode A"];
        [ccskill addObject: skill];

        skill = [CCSkill skillWithName:@"Natural Born Warrior Mode B"];
        [ccskill addObject: skill];
    }
}

-(void) populateUnitOptions
{
    // Do not show options if this is an sub-profile
    if(self.groupMember.unitProfile)
    {
        unitOptions = @[];
        return;
    }
    
    if(!self.unit)
    {
        unitOptions = @[];
        return;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [UnitOption entityInManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setIncludesSubentities:TRUE];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"cost" ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"swc" ascending:YES];
    NSSortDescriptor *sortDescriptor3 = [[NSSortDescriptor alloc] initWithKey:@"code" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor,sortDescriptor2,sortDescriptor3];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    [fetchRequest setRelationshipKeyPathsForPrefetching:@[@"weapons",@"specialRules"]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"ANY units == %@", self.unit];
    [fetchRequest setPredicate:predicate];
    
    
    NSError* error;
    unitOptions = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
}

-(void)configureSpecOpsView
{
    if(self.unit.army != nil)
    {
        specOpsWeapons = [self.unit.army.specopWeapons array];
        specOpsSpecialRules = [self.unit.army.specopSkills allObjects];
        specOpsEquipment = [self.unit.army.specopEquipment allObjects];
    }
    else if(self.unit.sectorial.army != nil)
    {
        specOpsWeapons = [self.unit.sectorial.army.specopWeaponsSet array];
        specOpsSpecialRules = [self.unit.sectorial.army.specopSkills allObjects];
        specOpsEquipment = [self.unit.sectorial.army.specopEquipment allObjects];
    }
    
    specOpsWeapons = [specOpsWeapons sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        SpecOpsWeapon *sw1 = obj1, *sw2 = obj2;
        NSComparisonResult ret = [sw1.cost compare:sw2.cost];
        if(ret == NSOrderedSame){
            ret = [sw1.weapon.name compare:sw2.weapon.name];
        }
        
        return ret;
    }];
    
    specOpsSpecialRules = [specOpsSpecialRules sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        SpecOpsSpecialRule *sr1 = obj1, *sr2 = obj2;
        NSComparisonResult ret = [sr1.cost compare:sr2.cost];
        if(ret == NSOrderedSame){
            ret = [sr1.specialRule.name compare:sr2.specialRule.name];
        }
        
        return ret;
    }];
    
    specOpsEquipment = [specOpsEquipment sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        SpecOpsEquipment *se1 = obj1, *se2 = obj2;
        NSComparisonResult ret = [se1.cost compare:se2.cost];
        if(ret == NSOrderedSame){
            ret = [se1.specialRule.name compare:se2.specialRule.name];
        }
        
        return ret;
    }];
    
    [self getSpecOpsBuild];
    
    self.specOpsCost.text = [NSString stringWithFormat:@"%ld XP   %ld | %.1f SWC",
                             (long)[self.specOpsBuild getCost],
                             (long)self.specOpsBuild.baseUnitOption.unitOption.costValue,
                             [self.specOpsBuild.baseUnitOption.unitOption.swc doubleValue]
                             ];
    if(currentDisplayMode == eBrowseSpecOpsBuildMode)
    {
        [self.specOpsAddButton setHidden:FALSE];
    }
    else if(currentDisplayMode == eSpecOpsRebuildMode)
    {
        [self.specOpsAddButton setHidden:TRUE];
    }
    [self.specOpsResetButton setHidden:FALSE];
}

-(bool)unitProfileInheritsBase:(UnitProfile*)unitProfile
{
    // CrazyKoala is equipment that inherits nothing.
    SpecialRule* perimeter = [SpecialRule specialRuleWithName:@"Perimeter"];
    if([unitProfile.aspects.specialRules containsObject:perimeter])
    {
        return false;
    }
    
    // Independent models do not inherit anything.
    if(unitProfile.independentCostValue)
    {
        return false;
    }

    // A small number of profiles are special and do not inherit abilities from the parent unit option.

    SpecialRule *pilot = [SpecialRule specialRuleWithName:@"Pilot"];
    if([unitProfile.aspects.specialRulesSet containsObject:pilot])
    {
        return false;
    }

    SpecialRule *operator = [SpecialRule specialRuleWithName:@"Operator"];
    if([unitProfile.aspects.specialRulesSet containsObject:operator])
    {
        return false;
    }

    SpecialRule *rem_pilot = [SpecialRule specialRuleWithName:@"Remote Pilot"];
    if([unitProfile.aspects.specialRulesSet containsObject:rem_pilot])
    {
        return false;
    }
    
    SpecialRule *seed = [SpecialRule specialRuleWithName:@"Seed-Embryo"];
    if([unitProfile.aspects.specialRulesSet containsObject:seed])
    {
        return false;
    }

    return true;
}






#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(currentDisplayMode == eBrowseSpecOpsBuildMode || currentDisplayMode == eSpecOpsRebuildMode)
    {
        return 4;
    }
    return 1;
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(currentDisplayMode != eBrowseSpecOpsBuildMode && currentDisplayMode != eSpecOpsRebuildMode)
    {
        return nil;
    }
    switch(section)
    {
        case 0:
            return @"Attributes";
        case 1:
            return @"Weapons";
        case 2:
            return @"Skills";
        case 3:
            return @"Equipment";
        default:
            return nil;
            
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(currentDisplayMode == eBrowseSpecOpsBuildMode  || currentDisplayMode == eSpecOpsRebuildMode)
    {
        switch(section)
        {
            case 0:
                return 7;
            case 1:
                return [specOpsWeapons count];
            case 2:
                return [specOpsSpecialRules count];
            case 3:
                return [specOpsEquipment count];
            default:
                return 0;
        }
    }
    
    switch(selectedSkill)
    {
        case eSkillBS:
            return [weapons count];
            break;
        case eSkillDTW:
            return [templateWeapons count];
            break;
        case eSkillCC:
            return [ccskill count] + [ccweapons count];
            break;
        case eSkillDeployable:
            return [deployables count];
            break;
        case eSkillMisc:
            return [miscskill count];
            break;
        case eSkillHacking:
            return [hacking count];
            break;
        case eSkillPheroware:
            return [pheroware count];
            break;
        case eSkillNotes:
            return [notes count];
            break;
        case eSkillOptions:
            return [unitOptions count];
            break;
    }
}

#define kTableViewCellRightMargin 150

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"UnitOption";
    
    if(currentDisplayMode == eBrowseSpecOpsBuildMode  || currentDisplayMode == eSpecOpsRebuildMode)
    {
        CellIdentifier = @"SpecOps";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [self configureSpecOpsCell:cell atIndexPath:indexPath];
        return cell;
    }

    switch(selectedSkill)
    {
        case eSkillBS:
        case eSkillDTW:
        case eSkillDeployable:
        case eSkillMisc:
        {
            CellIdentifier = @"Weapon";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            [self configureWeaponCell:(WeaponTableViewCell*)cell atIndexPath:indexPath];
            return cell;
        }
            break;
        case eSkillCC:
        {
            // Mixed CC weapons and CC skills
            UITableViewCell *cell;
            if(indexPath.row < [ccweapons count])
            {
                CellIdentifier = @"Weapon";
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
                [self configureWeaponCell:(WeaponTableViewCell*)cell atIndexPath:indexPath];
            }
            else
            {
                CellIdentifier = @"CCSkill";
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
                [self configureCCSkillCell:(CCSkillTableViewCell*)cell atIndexPath:indexPath];
            }
            return cell;
        }
            break;
        case eSkillHacking:
        {
            CellIdentifier = @"Hacking";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            [self configureHackingCell:(HackingProgramTableViewCell*)cell atIndexPath:indexPath];
            return cell;
        }
            break;
        case eSkillPheroware:
        {
            CellIdentifier = @"Pheroware";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            [self configurePherowareCell:(PherowareTacticTableViewCell*)cell atIndexPath:indexPath];
            return cell;
        }
            break;
        case eSkillNotes:
        {
            CellIdentifier = @"Notes";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            [self configureNotesCell:(SkillNotesTableViewCell*)cell atIndexPath:indexPath];
            return cell;
        }
            break;
        case eSkillOptions:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            UnitOptionTableViewCell* unitOptionCell = (UnitOptionTableViewCell*)cell;
            
            unitOptionCell.unitOptionSpec.preferredMaxLayoutWidth = viewWidth-kTableViewCellRightMargin;
            
            [self configureOptionsCell:unitOptionCell atIndexPath:indexPath];
            
            [cell setNeedsUpdateConstraints];
            return cell;
        }
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(currentDisplayMode == eBrowseSpecOpsBuildMode || currentDisplayMode == eSpecOpsRebuildMode)
    {
        return 44;
    }
    
    switch(selectedSkill)
    {
        case eSkillBS:
        {
            Weapon *weapon = [weapons objectAtIndex:indexPath.row];
            return [WeaponTableViewCell cellHeightForWeapon:weapon];
        }
            break;
        case eSkillDTW:
        {
            Weapon *weapon = [templateWeapons objectAtIndex:indexPath.row];
            return [WeaponTableViewCell cellHeightForWeapon:weapon];
        }
            break;
        case eSkillCC:
        {
            // Mixed CC weapons and CC skills
            if(indexPath.row < [ccweapons count])
            {
                Weapon *weapon = [ccweapons objectAtIndex:indexPath.row];
                return [WeaponTableViewCell cellHeightForWeapon:weapon];
            }
            else
            {
                unsigned long skillIndex = indexPath.row - [ccweapons count];
                CCSkill *skill = ccskill[skillIndex];
                return [CCSkillTableViewCell cellHeightForCCSkill:skill];
            }
        }
            break;
        case eSkillDeployable:
        {
            Weapon *weapon = [deployables objectAtIndex:indexPath.row];
            return [WeaponTableViewCell cellHeightForWeapon:weapon];
        }
            break;
        case eSkillMisc:
        {
            Weapon *weapon = [miscskill objectAtIndex:indexPath.row];
            return [WeaponTableViewCell cellHeightForWeapon:weapon];
        }
            break;
        case eSkillHacking:
        {
            if(heightCachedHackingProgramTableViewCell == nil)
            {
            heightCachedHackingProgramTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"Hacking"];
            }
            
            // Effect cell is not wrapping lines correctly without this.
            heightCachedHackingProgramTableViewCell.effect.preferredMaxLayoutWidth = 368;
            
            [self configureHackingCell:heightCachedHackingProgramTableViewCell atIndexPath:indexPath];
            
            [heightCachedHackingProgramTableViewCell setNeedsUpdateConstraints];
            [heightCachedHackingProgramTableViewCell updateConstraintsIfNeeded];
            [heightCachedHackingProgramTableViewCell setNeedsLayout];
            [heightCachedHackingProgramTableViewCell layoutIfNeeded];
            CGFloat height = [heightCachedHackingProgramTableViewCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
            
            height += 2;
            
            return height;
        }
            break;
        case eSkillPheroware:
        {
            if(heightCachedPherowareTacticTableViewCell == nil)
            {
                heightCachedPherowareTacticTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"Pheroware"];
            }
            
            // Effect cell is not wrapping lines correctly without this.
            heightCachedPherowareTacticTableViewCell.effect.preferredMaxLayoutWidth = 368;
            
            [self configurePherowareCell:heightCachedPherowareTacticTableViewCell atIndexPath:indexPath];
            
            [heightCachedPherowareTacticTableViewCell setNeedsUpdateConstraints];
            [heightCachedPherowareTacticTableViewCell updateConstraintsIfNeeded];
            [heightCachedPherowareTacticTableViewCell setNeedsLayout];
            [heightCachedPherowareTacticTableViewCell layoutIfNeeded];
            CGFloat height = [heightCachedPherowareTacticTableViewCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
            
            height += 2;
            
            return height;
        }
            break;
        case eSkillNotes:
        {
            if(heightCachedSkillNotesTableViewCell == nil)
            {
                heightCachedSkillNotesTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"Notes"];
            }
            
            // Effect cell is not wrapping lines correctly without this.
            heightCachedSkillNotesTableViewCell.title.preferredMaxLayoutWidth = 368;
            heightCachedSkillNotesTableViewCell.note.preferredMaxLayoutWidth = 368;

            [self configureNotesCell:heightCachedSkillNotesTableViewCell atIndexPath:indexPath];
            
            [heightCachedSkillNotesTableViewCell setNeedsUpdateConstraints];
            [heightCachedSkillNotesTableViewCell updateConstraintsIfNeeded];
            [heightCachedSkillNotesTableViewCell setNeedsLayout];
            [heightCachedSkillNotesTableViewCell layoutIfNeeded];
            CGSize size = [heightCachedSkillNotesTableViewCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
            
            return size.height + 2;
        }
            break;
        case eSkillOptions:
        {
            NSNumber* cachedHeight = [cellHeightCache objectForKey:indexPath];
            if(cachedHeight != nil){
                CGFloat height = [cachedHeight floatValue];
                return height;
            }
            
            if(heightCachedUnitOptionTableViewCell == nil)
            {
                heightCachedUnitOptionTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"UnitOption"];
            }
            UnitOptionTableViewCell* unitOptionCell = heightCachedUnitOptionTableViewCell;
            
            unitOptionCell.unitOptionSpec.preferredMaxLayoutWidth = viewWidth-kTableViewCellRightMargin;
            
            [self configureOptionsCell:unitOptionCell atIndexPath:indexPath];
            
            [unitOptionCell setNeedsUpdateConstraints];
            [unitOptionCell updateConstraintsIfNeeded];
            [unitOptionCell setNeedsLayout];
            [unitOptionCell layoutIfNeeded];
            CGFloat height = [unitOptionCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
            
            height = MAX(45.0, height+2);
            
            [cellHeightCache setObject:[NSNumber numberWithFloat:height] forKey:indexPath];
            
            return height;
        }
            break;
    }
}

#pragma mark - Table View Cell Selection
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(currentDisplayMode == eRosterUnitOptionDisplayMode || currentDisplayMode == eBrowseUnitWeaponDisplayMode)
        return;
    
    if(currentDisplayMode == eRosterUnitDisplayMode && selectedSkill == eSkillOptions)
    {
        UnitOption* unitOption = [unitOptions objectAtIndex:indexPath.row];
        [self unitOptionSelected: unitOption];
        return;
    }
    
    if(currentDisplayMode == eBrowseSpecOpsBuildMode || currentDisplayMode == eSpecOpsRebuildMode)
    {
        [self tableView:tableView didSelectSpecOpsRowAtIndexPath:indexPath];
        return;
    }
}

-(IBAction)onAddUnitOption:(UIButton*)sender
{
    long row = sender.tag;
    UnitOption* unitOption = [unitOptions objectAtIndex:row];
    [self unitOptionSelected: unitOption];
    [self configureView];
}

-(void)tableView:(UITableView *)tableView didSelectSpecOpsRowAtIndexPath:(NSIndexPath *)indexPath
{
    SpecOpsBaseUnitOption* baseUnitOption;
    SpecOpsWeapon* specOpsWeapon;
    SpecOpsSpecialRule* specOpsRule;
    SpecOpsEquipment* specOpsEquip;
    SpecOpsBuild* specops;
    switch(indexPath.section)
    {
        case 1:
            specOpsWeapon = [specOpsWeapons objectAtIndex:indexPath.row];
            specops = [self getSpecOpsBuild];
            [specops toggleSpecOpsWeapon: specOpsWeapon];
            
            break;
        case 2:
            specOpsRule = [specOpsSpecialRules objectAtIndex:indexPath.row];
            specops = [self getSpecOpsBuild];
            [specops toggleSpecOpsSpecialRule: specOpsRule];
            break;
        case 3:
            specOpsEquip = [specOpsEquipment objectAtIndex:indexPath.row];
            specops = [self getSpecOpsBuild];
            [specops toggleSpecOpsEquipment: specOpsEquip];
            break;
    }
    [self configureView];
    if(currentDisplayMode == eSpecOpsRebuildMode)
    {
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [delegate saveContext];
        [self refreshRight];
        
    }
}

#pragma mark - Table View Cell Configuration
- (void)configureOptionsCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    UnitOptionTableViewCell* unitOptionCell = (UnitOptionTableViewCell*)cell;
    unitOptionCell.accessoryType = UITableViewCellAccessoryNone;
    unitOptionCell.accessoryView = nil;
    
    UnitOption* unitOption = [unitOptions objectAtIndex:indexPath.row];

    unitOptionCell.unitOptionCode.text = unitOption.code;
    unitOptionCell.unitOptionCost.text = [NSString stringWithFormat:@"%d", [[unitOption cost] intValue]];
    if([[unitOption swc] doubleValue] == 0)
    {
        unitOptionCell.unitOptionSWC.text = @"0";
    }
    else
    {
        unitOptionCell.unitOptionSWC.text = [NSString stringWithFormat:@"%g", [[unitOption swc] doubleValue]];
    }
    unitOptionCell.unitOptionSpec.text = [self GetUnitOptionCellDescription:unitOption];
    
    Roster* roster = displayedRoster;
    
    
    long addedCost = 0;
    double addedSWC = 0;
    BOOL showCostofModelAddedToDescription = FALSE;
    //If we have a roster, and want to change the unitOption
    if(currentDisplayMode == eRosterUnitDisplayMode)
    {
        showCostofModelAddedToDescription = TRUE;
        if(self.unitOption == unitOption)
        {
            unitOptionCell.accessoryType = UITableViewCellAccessoryCheckmark;
            addedCost = [roster totalPoints];
            addedSWC = [roster totalSWC];
        }
        else
        {
            unitOptionCell.accessoryType = UITableViewCellAccessoryNone;
            
            addedCost = (unitOption.costValue - self.unitOption.costValue) +[roster totalPoints];
            addedSWC = ([unitOption.swc doubleValue] - [self.unitOption.swc doubleValue])+[roster totalSWC];
        }
    }
    else if(currentDisplayMode == eBrowseUnitDisplayMode)
    {
        //We allow a add button when there is no roster, or if we can "create a new roster"
        if(
           (
            roster &&
                (
                 [roster testUnit:self.unit unitOption:self.unitOption] ||
                 [roster numberOfModels] == 0
                )
           )
           || !roster)
        {
            if(roster)
            {
                addedCost = unitOption.costValue + [roster totalPoints];
                addedSWC =  [unitOption.swc doubleValue] + [roster totalSWC];
                showCostofModelAddedToDescription = TRUE;
            }
            UIButton *button = [UIButton buttonWithType:UIButtonTypeContactAdd];
            button.tag = indexPath.row;
            [button addTarget:self action:@selector(onAddUnitOption:) forControlEvents:UIControlEventTouchUpInside];
            unitOptionCell.accessoryView = button;
        }
        
    }
    
    //If we need to show "Cost if adding model roster"
    if(showCostofModelAddedToDescription)
    {
    
        int pointCap = [roster pointcapValue];
        double maxSWC = [roster maxSWC];
        unitOptionCell.rosterAdditionCost.text = [NSString stringWithFormat:@"%g/%g | %ld/%d",addedSWC, maxSWC,addedCost,pointCap];
        if(addedCost > pointCap || addedSWC > maxSWC) {
            unitOptionCell.rosterAdditionCost.textColor = [UIColor redColor];
        } else {
            unitOptionCell.rosterAdditionCost.textColor = labelColor();
        }
    }
    else
    {
        unitOptionCell.rosterAdditionCost.text = @"";
    }
}

-(void) configureHackingCell:(HackingProgramTableViewCell*)cell atIndexPath:(NSIndexPath *)indexPath
{
    unsigned long hackingIndex = indexPath.row;
    HackingProgram *program = hacking[hackingIndex];
    cell.program = program;
    
    NSMutableAttributedString *name;
    if(program.group)
    {
        name = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@ (%@)", program.name, program.group.name]];
    }
    else
    {
        name = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@ (UPGRADE)", program.name]];
    }
    
    // Make it look like a link
    NSRange linkRange = NSMakeRange(0, [program.name length]);
    NSDictionary *linkAttributes = @{ NSForegroundColorAttributeName : self.view.tintColor,
                                      NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                      @"wiki" : program.wikiEntry.name,
                                      };
    [name setAttributes:linkAttributes range:linkRange];
    
    // make it tappable
    [cell.programName addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnLabel:)]];
    
    cell.programName.attributedText = name;

    cell.skill.text = program.skill;
    cell.range.text = program.range;
    cell.attMod.text = program.attMod;
    cell.oppMod.text = program.oppMod;
    cell.burst.text = program.burst;
    cell.damage.text = program.damage;
    cell.ammo.text = program.ammo;
    cell.target.text = program.target;
    cell.effect.text = program.effect;
    cell.special.text = program.special;

    [cell setNeedsUpdateConstraints];
}

-(void) configurePherowareCell:(PherowareTacticTableViewCell*)cell atIndexPath:(NSIndexPath *)indexPath
{
    unsigned long pherowareIndex = indexPath.row;
    PherowareTactic *tactic = pheroware[pherowareIndex];
    cell.tactic = tactic;

    NSMutableAttributedString *name;
    name = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@ (%@)", tactic.name, tactic.tacticType]];
    
    // Make it look like a link
    NSRange linkRange = NSMakeRange(0, [tactic.name length]);
    NSDictionary *linkAttributes = @{ NSForegroundColorAttributeName : self.view.tintColor,
                                      NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                      @"wiki" : tactic.wikiEntry.name,
                                      };
    [name setAttributes:linkAttributes range:linkRange];
    
    // make it tappable
    [cell.tacticName addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnLabel:)]];
    
    cell.tacticName.attributedText = name;
    
    cell.skill.text = tactic.skill;
    cell.range.text = tactic.range;
    cell.attMod.text = tactic.attMod;
    cell.oppMod.text = tactic.oppMod;
    cell.burst.text = tactic.burst;
    cell.damage.text = tactic.damage;
    cell.ammo.text = tactic.ammo;
    cell.target.text = tactic.target;
    cell.effect.text = tactic.effect;
    cell.special.text = tactic.special;
    
    [cell setNeedsUpdateConstraints];
}

-(void) configureNotesCell:(SkillNotesTableViewCell*)cell atIndexPath:(NSIndexPath *)indexPath
{
    unsigned long noteIndex = indexPath.row;
    UnitNote *note = notes[noteIndex];
    cell.title.text = note.title;
    cell.note.text = note.note;
    
    [cell setNeedsUpdateConstraints];
}

-(void) configureCCSkillCell:(CCSkillTableViewCell*)cell atIndexPath:(NSIndexPath *)indexPath
{
    unsigned long skillIndex = indexPath.row - [ccweapons count];
    CCSkill *skill = ccskill[skillIndex];

    cell.name.text = skill.name;
    cell.attMod.text = skill.attMod;
    cell.oppMod.text = skill.oppMod;
    cell.damageMod.text = skill.damageMod;
    cell.burstMod.text = skill.burstMod;
    cell.special.text = skill.special;
    
    [cell setNeedsUpdateConstraints];
}

-(void) configureWeaponCell:(WeaponTableViewCell*)cell atIndexPath:(NSIndexPath *)indexPath
{
    Weapon *weapon;
    UnitProfile *altProfile = [self selectedAltProfile];
    switch(selectedSkill)
    {
        case eSkillBS:
            weapon = [weapons objectAtIndex:indexPath.row];
            break;
        case eSkillDTW:
            weapon = [templateWeapons objectAtIndex:indexPath.row];
            break;
        case eSkillDeployable:
            weapon = [deployables objectAtIndex:indexPath.row];
            break;
        case eSkillMisc:
            weapon = [miscskill objectAtIndex:indexPath.row];
            break;
        case eSkillCC:
            weapon = [ccweapons objectAtIndex:indexPath.row];
            break;
        default:
            NSAssert(false, @"This skill should not have weapon cells!");
            break;
    }
    
    //HackingProgram *program = hacking[hackingIndex];
    //cell.program = program;

    NSMutableAttributedString *name;
    if(weapon.mode)
    {
        name = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@ %@", weapon.name, weapon.mode]];
    }
    else
    {
        name = [[NSMutableAttributedString alloc] initWithString: weapon.name];
    }
    
    if(weapon.wikiEntry) {
        // Make it look like a link
        NSRange linkRange = NSMakeRange(0, [name length]);
        NSDictionary *linkAttributes = @{ NSForegroundColorAttributeName : self.view.tintColor,
                                          NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle),
                                          @"wiki" : weapon.wikiEntry.name,
                                          };
        [name setAttributes:linkAttributes range:linkRange];
        
        // make it tappable
        [cell.weaponName addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapOnLabel:)]];
    }
    
    cell.weaponName.attributedText = name;
    
    // Calculate a table with modifiers for 8/16/24/32/40/48/96.
    NSNumber* rangeMod[numRangeBands];
    
    int band = 0;
    int nextRange;
    
    nextRange = [weapon.shortDistance intValue];
    if(nextRange){
        while(band < numRangeBands && [WeaponTableViewCell rangeBandDistance:band] <= nextRange)
        {
            rangeMod[band] = weapon.shortModifier;
            band++;
        }
    }
    
    nextRange = [weapon.mediumDistance intValue];
    if(nextRange){
        while(band < numRangeBands && [WeaponTableViewCell rangeBandDistance:band] <= nextRange)
        {
            rangeMod[band] = weapon.mediumModifier;
            band++;
        }
    }
    
    nextRange = [weapon.longDistance intValue];
    if(nextRange){
        while(band < numRangeBands && [WeaponTableViewCell rangeBandDistance:band] <= nextRange)
        {
            rangeMod[band] = weapon.longModifier;
            band++;
        }
    }
    
    nextRange = [weapon.maxDistance intValue];
    if(nextRange){
        while(band < numRangeBands && [WeaponTableViewCell rangeBandDistance:band] <= nextRange)
        {
            rangeMod[band] = weapon.maxModifier;
            band++;
        }
    }

    // Put the values in the cells

    cell.range8Distance.text = [Weapon rangeFromNSNumber:@8];
    cell.range8Modifier.text = [Weapon modFromNSNumber:rangeMod[range8]];
    [self configureWeaponModifier:rangeMod[range8] label:cell.range8Modifier];
    
    cell.range16Distance.text = [Weapon rangeFromNSNumber:@16];
    cell.range16Modifier.text = [Weapon modFromNSNumber:rangeMod[range16]];
    [self configureWeaponModifier:rangeMod[range16] label:cell.range16Modifier];
    
    cell.range24Distance.text = [Weapon rangeFromNSNumber:@24];
    cell.range24Modifier.text = [Weapon modFromNSNumber:rangeMod[range24]];
    [self configureWeaponModifier:rangeMod[range24] label:cell.range24Modifier];
    
    cell.range32Distance.text = [Weapon rangeFromNSNumber:@32];
    cell.range32Modifier.text = [Weapon modFromNSNumber:rangeMod[range32]];
    [self configureWeaponModifier:rangeMod[range32] label:cell.range32Modifier];
    
    cell.range40Distance.text = [Weapon rangeFromNSNumber:@40];
    cell.range40Modifier.text = [Weapon modFromNSNumber:rangeMod[range40]];
    [self configureWeaponModifier:rangeMod[range40] label:cell.range40Modifier];
    
    cell.range48Distance.text = [Weapon rangeFromNSNumber:@48];
    cell.range48Modifier.text = [Weapon modFromNSNumber:rangeMod[range48]];
    [self configureWeaponModifier:rangeMod[range48] label:cell.range48Modifier];
    
    cell.range96Distance.text = [Weapon rangeFromNSNumber:@96];
    cell.range96Modifier.text = [Weapon modFromNSNumber:rangeMod[range96]];
    [self configureWeaponModifier:rangeMod[range96] label:cell.range96Modifier];
    

    
    cell.weaponExtras.text = [weapon getExtrasString];
    
    cell.burstValue.text = (weapon.burst != nil) ? weapon.burst : @"--";
    if(altProfile)
    {
        // If they have multiple alternate profiles, show the currently selected one
        cell.damageValue.text = [weapon damageDescriptorWithUnit:altProfile.aspects];
    }
    else if(self.groupMember.unitProfile)
    {
        cell.damageValue.text = [weapon damageDescriptorWithUnit:self.groupMember.unitProfile.aspects];
    }
    else
    {
        cell.damageValue.text = [weapon damageDescriptorWithUnit:self.unit.aspects];
    }
    cell.ammoValue.text = weapon.ammo;
}

-(void)configureWeaponModifier:(NSNumber*)modifier label:(UILabel*)label
{
    UIColor* modifierColor = [Weapon weaponDescriptorColorFromNSNumber:modifier];
    if([modifier integerValue] == -6) {
        label.textColor = [UIColor whiteColor];
    } else {
        label.textColor = [UIColor blackColor];
    }
    label.backgroundColor = modifierColor;
}

-(void) configureSpecOpsCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.accessoryType = UITableViewCellAccessoryNone;
    for(UIView* view in cell.contentView.subviews)
    {
        if([view isKindOfClass:[UISegmentedControl class]])
            [view removeFromSuperview];
    }
    SpecOpsBaseUnitOption* baseUnitOption;
    SpecOpsWeapon* specOpsWeapon;
    SpecOpsSpecialRule* specOpsRule;
    SpecOpsEquipment* specOpsEquip;

    switch(indexPath.section)
    {
        case 0:
            [self configureSpecOpsAspectsCell:cell atIndexPath:indexPath];
            break;
        case 1:
            specOpsWeapon = [specOpsWeapons objectAtIndex:indexPath.row];
            cell.textLabel.text = [NSString stringWithFormat:@"%@ (%ld XP)", specOpsWeapon.weapon.name, [specOpsWeapon.cost longValue]];
            cell.accessoryType = [[self getSpecOpsBuild] hasWeapon:specOpsWeapon] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
            break;
        case 2:
            specOpsRule = [specOpsSpecialRules objectAtIndex:indexPath.row];
            cell.textLabel.text = [NSString stringWithFormat:@"%@ (%ld XP)", specOpsRule.specialRule.name, [specOpsRule.cost longValue]];
            cell.accessoryType = [[self getSpecOpsBuild] hasSpecialRule:specOpsRule] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
            break;
        case 3:
            specOpsEquip = [specOpsEquipment objectAtIndex:indexPath.row];
            cell.textLabel.text = [NSString stringWithFormat:@"%@ (%ld XP)", specOpsEquip.specialRule.name, [specOpsEquip.cost longValue]];
            cell.accessoryType = [[self getSpecOpsBuild] hasEquipment:specOpsEquip] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
            break;
    }
}


-(void) configureSpecOpsAspectsCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSArray* segmentedChoices;
    NSInteger selectedIndex = 0;
    SpecOpsAspects* specOpsAspects = [self getSpecOpsBuild].aspects;
    switch(indexPath.row)
    {
        case 0:
            cell.textLabel.text = @"CC";
            segmentedChoices = @[@"+2 (2 XP)",@"+5 (5 XP)",@"+10 (10 XP)"];
            switch (specOpsAspects.ccIncreaseValue) {
                case 2:
                    selectedIndex = 1;
                    break;
                case 5:
                    selectedIndex = 2;
                    break;
                case 10:
                    selectedIndex = 3;
                    break;
                default:
                    break;
            }
            break;
        case 1:
            cell.textLabel.text = @"BS";
            segmentedChoices = @[@"+1 (2 XP)",@"+2 (5 XP)",@"+3 (10 XP)"];
            switch (specOpsAspects.bsIncreaseValue) {
                case 1:
                    selectedIndex = 1;
                    break;
                case 2:
                    selectedIndex = 2;
                    break;
                case 3:
                    selectedIndex = 3;
                    break;
                default:
                    break;
            }
            break;
        case 2:
            cell.textLabel.text = @"PH";
            segmentedChoices = @[@"+1 (2 XP)",@"+3 (5 XP)"];
            switch (specOpsAspects.phIncreaseValue) {
                case 1:
                    selectedIndex = 1;
                    break;
                case 3:
                    selectedIndex = 2;
                    break;
                default:
                    break;
            }
            break;
        case 3:
            cell.textLabel.text = @"WIP";
            segmentedChoices = @[@"+1 (2 XP)",@"+3 (5 XP)"];
            switch (specOpsAspects.wipIncreaseValue) {
                case 1:
                    selectedIndex = 1;
                    break;
                case 3:
                    selectedIndex = 2;
                    break;
                default:
                    break;
            }
            break;
        case 4:
            cell.textLabel.text = @"ARM";
            segmentedChoices = @[@"+1 (5 XP)",@"+3 (10 XP)"];
            switch (specOpsAspects.armIncreaseValue) {
                case 1:
                    selectedIndex = 1;
                    break;
                case 3:
                    selectedIndex = 2;
                    break;
                default:
                    break;
            }
            break;
        case 5:
            cell.textLabel.text = @"BTS";
            segmentedChoices = @[@"+3 (2 XP)",@"+6 (5 XP)",@"+9 (10 XP)"];
            switch (abs(specOpsAspects.btsIncreaseValue)) {
                case 3:
                    selectedIndex = 1;
                    break;
                case 6:
                    selectedIndex = 2;
                    break;
                case 9:
                    selectedIndex =3;
                    break;
                default:
                    break;
            }
            break;
        case 6:
            cell.textLabel.text = @"W";
            segmentedChoices = @[@"+1 (10 XP)"];
            switch (specOpsAspects.wIncreaseValue) {
                case 1:
                    selectedIndex = 1;
                    break;
                default:
                    break;
            }
            break;
            break;
    }
    segmentedChoices = [@[@"0" ] arrayByAddingObjectsFromArray: segmentedChoices];
    UISegmentedControl* segmentedControl = [[UISegmentedControl alloc] initWithItems:segmentedChoices];
    [segmentedControl setWidth:50 forSegmentAtIndex:0];
    segmentedControl.tag = indexPath.row;
    [segmentedControl setSelectedSegmentIndex:selectedIndex];
    
    NSInteger remainingWidth = 300 - 50;
    
    for(int x = 1; x < [segmentedChoices count]; x++)
    {
        [segmentedControl setWidth: remainingWidth/([segmentedChoices count]-1) forSegmentAtIndex:x];
    }
    segmentedControl.frame = CGRectMake(374 - segmentedControl.frame.size.width, 8, segmentedControl.frame.size.width, segmentedControl.frame.size.height);
    [segmentedControl addTarget:self action:@selector(aspectSegmentControlChange:) forControlEvents:UIControlEventValueChanged];
    [cell.contentView addSubview:segmentedControl];
}



#pragma mark - Misc
-(IBAction)aspectSegmentControlChange:(AspectSegmentedControl*)sender
{
    SpecOpsAspects* aspects = [self getSpecOpsBuild].aspects;
    NSInteger selectedSegmentIndex = [sender selectedSegmentIndex];
    switch(sender.tag)
    {
        case 0:
            switch(selectedSegmentIndex)
            {
                case 1:
                    aspects.ccIncreaseValue = 2;
                    break;
                case 2:
                    aspects.ccIncreaseValue = 5;
                    break;
                case 3:
                    aspects.ccIncreaseValue = 10;
                    break;
                case 0:
                    aspects.ccIncreaseValue = 0;
                    break;
            }
            //segmentedChoices = @[@"+2 (2 XP)",@"+5 (5 XP)",@"+10 (10 XP)"];
            break;
        case 1:
            switch(selectedSegmentIndex)
            {
                case 1:
                    aspects.bsIncreaseValue = 1;
                    break;
                case 2:
                    aspects.bsIncreaseValue = 2;
                    break;
                case 3:
                    aspects.bsIncreaseValue = 3;
                    break;
                case 0:
                    aspects.bsIncreaseValue = 0;
                    break;
            }
            //segmentedChoices = @[@"+1 (2 XP)",@"+2 (5 XP)",@"+3 (10 XP)"];
            break;
        case 2:
            switch(selectedSegmentIndex)
            {
                case 1:
                    aspects.phIncreaseValue = 1;
                    break;
                case 2:
                    aspects.phIncreaseValue = 3;
                    break;
                case 0:
                    aspects.phIncreaseValue = 0;
                    break;
            }
            //cell.textLabel.text = @"PH";
            //segmentedChoices = @[@"+1 (2 XP)",@"+3 (5 XP)"];
            break;
        case 3:
            switch(selectedSegmentIndex)
            {
                case 1:
                    aspects.wipIncreaseValue = 1;
                    break;
                case 2:
                    aspects.wipIncreaseValue = 3;
                    break;
                case 3:
                    aspects.wipIncreaseValue = 6;
                    break;
                case 0:
                    aspects.wipIncreaseValue = 0;
                    break;
            }
            //cell.textLabel.text = @"WIP";
            //segmentedChoices = @[@"+1 (2 XP)",@"+3 (5 XP)",@"+6 (10 XP)"];
            break;
        case 4:
            switch(selectedSegmentIndex)
            {
                case 1:
                    aspects.armIncreaseValue = 1;
                    break;
                case 2:
                    aspects.armIncreaseValue = 3;
                    break;
                case 0:
                    aspects.armIncreaseValue = 0;
                    break;
            }
            //cell.textLabel.text = @"ARM";
            //segmentedChoices = @[@"+1 (5 XP)",@"+3 (10 XP)"];
            break;
        case 5:
            switch(selectedSegmentIndex)
            {
                case 1:
                    aspects.btsIncreaseValue = 3;
                    break;
                case 2:
                    aspects.btsIncreaseValue = 6;
                    break;
                case 3:
                    aspects.btsIncreaseValue = 9;
                    break;
                case 0:
                    aspects.btsIncreaseValue = 0;
                    break;
            }
            //cell.textLabel.text = @"BTS";
            //segmentedChoices = @[@"+-3 (2 XP)",@"+-6 (5 XP)",@"+-9 (10 XP)"];
            break;
        case 6:
            switch(selectedSegmentIndex)
            {
                case 1:
                    aspects.wIncreaseValue = 1;
                    break;
                case 0:
                    aspects.wIncreaseValue = 0;
                    break;
            }
            //cell.textLabel.text = @"W";
            //segmentedChoices = @[@"+1 (10 XP)"];
            break;
    }
    [self configureView];
    if(currentDisplayMode == eSpecOpsRebuildMode)
    {
        AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [delegate saveContext];
        [self refreshRight];
        
    }
}

-(SpecOpsBuild*) getSpecOpsBuild
{
    if(self.specOpsBuild == nil && self.unit && self.unit.specops)
    {
        self.specOpsBuild = [SpecOpsBuild insertInManagedObjectContext:self.unit.managedObjectContext];
        self.specOpsBuild.name = self.unit.name;
        self.specOpsBuild.unit = self.unit;
        self.specOpsBuild.aspects = [SpecOpsAspects insertInManagedObjectContext:self.unit.managedObjectContext];
        if([self.unit.specops.baseUnitOptions count] == 1)
        {
            self.specOpsBuild.baseUnitOption = [self.unit.specops.baseUnitOptions firstObject];
        }
    }
    return self.specOpsBuild;
}


-(void) unitOptionSelected:(UnitOption*) unitOption
{
    if(currentDisplayMode == eRosterUnitDisplayMode)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UnitOptionChange" object:@{@"unit":self.unit, @"unitOption":unitOption, @"groupMember":self.groupMember}];
        [self unitSelection:self.unit groupMember:self.groupMember];
    }
    else if(currentDisplayMode == eBrowseUnitDisplayMode)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UnitOptionAddition" object:@{@"unit":self.unit, @"unitOption":unitOption}];
    }
}

- (IBAction)onSubmitFeedback:(id)sender
{
    FeedbackFormViewController* feedbackView = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedbackForm"];
    feedbackView.delegate = self;
    [self presentViewController:feedbackView animated:TRUE completion:nil];
}

-(void)onFeedbackFormComplete
{
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)onResetSpecOpsButtonPressed:(id)sender
{
    if(currentDisplayMode == eBrowseSpecOpsBuildMode)
    {
        self.specOpsBuild = nil;
    }
    else
    {
        if(currentDisplayMode == eSpecOpsRebuildMode)
        {
            [self.specOpsBuild.aspects reset];
            [self.specOpsBuild.weaponsSet removeAllObjects];
            [self.specOpsBuild.specialRulesSet removeAllObjects];
            AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            [delegate saveContext];
            [self refreshRight];
        }
    }
    [self configureView];
}

- (IBAction)onAddSpecOpsButtonPressed:(id)sender
{
    if(self.specOpsBuild != nil && self.specOpsBuild.baseUnitOption != nil)
    {
        SpecOpsBuild* addingSpecOpsBuild = [SpecOpsBuild insertInManagedObjectContext:self.unit.managedObjectContext];
        addingSpecOpsBuild.unit = self.unit;
        addingSpecOpsBuild.specialRules = self.specOpsBuild.specialRules;
        addingSpecOpsBuild.weapons = self.specOpsBuild.weapons;
        addingSpecOpsBuild.aspects = [self.specOpsBuild.aspects createCopy];
        addingSpecOpsBuild.baseUnitOption = self.specOpsBuild.baseUnitOption;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SpecOpsBuildAddition" object:@{@"specOpsBuild":addingSpecOpsBuild}];
    }
}

#pragma mark - String Constructors
-(NSString*)GetUnitOptionCellDescription:(UnitOption*)unitOption
{
    NSString* theWeapons = [unitOption weaponDescription:self.unit];
    NSString* specialRules = [unitOption specialRulesDescription];
    
    return [Util conditionalConcatArray:@[specialRules, theWeapons] seperator:@"\n"];
}

-(NSString*) GetLinksFromSpecialRulesArray:(NSArray*)specialRulesArray {
    NSMutableArray* stringArray = [[NSMutableArray alloc] init];
    
    for(SpecialRule* specialRule in specialRulesArray) {
        if([specialRule.choices count] != 0) {
            NSString* choicesLinksString = specialRule.name;
            for(SpecialRule* specialRuleChoice in specialRule.choices) {
                if(specialRuleChoice.wikiEntry) {
                    NSString* link = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>",
                                      specialRuleChoice.wikiEntry.urlEscapedName,
                                      specialRuleChoice.name
                                      ];
                    
                    choicesLinksString = [choicesLinksString stringByReplacingOccurrencesOfString:specialRuleChoice.name withString:link];
                }
            }
            [stringArray addObject:choicesLinksString];
        } else {
            if(specialRule.wikiEntry) {
                NSString* link = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>",
                                  specialRule.wikiEntry.urlEscapedName,
                                  specialRule.name
                                  ];
                [stringArray addObject:link];
            } else {
                [stringArray addObject:specialRule.name];
            }
        }
    }
    return [Util concatArray:stringArray];
}

-(NSString*) GetLinksFromWeaponsArray:(NSArray*)weaponsArray
{
    NSMutableArray* stringArray = [[NSMutableArray alloc] init];
    
    
    for(Weapon* weapon in weaponsArray)
    {
        if(weapon.wikiEntry)
        {
            NSString* link = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>",
                              weapon.wikiEntry.urlEscapedName,
                              weapon.name
                              ];
            [stringArray addObject:link];
        }
        else
        {
            [stringArray addObject:weapon.name];
        }
    }
    return [Util concatArray:stringArray];
}



-(void)showWikiEntry:(WikiEntry*)wikiEntry
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.containerViewController.middleViewController showWikiEntry:wikiEntry sender:self];
}

#pragma mark - UIWebView Delegate

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    // Minimal size to fit the web content
    CGRect frame = webView.frame;
    frame.size.height = 1;
    webView.frame = frame;
    frame.size.height = webView.scrollView.contentSize.height;
    webView.frame = frame;

    if(webView == self.unitSpecialRulesWebView)
    {
        self.specialRulesHeightConstraint.constant = webView.scrollView.contentSize.height - 10;
    }
    else if(webView == self.unitWeaponsWebView)
    {
        self.weaponsHeightConstraint.constant = webView.scrollView.contentSize.height - 10;
    }
    webView.scrollView.scrollEnabled = FALSE;
}

- (BOOL)webView:(UIWebView*)webView
shouldStartLoadWithRequest:(NSURLRequest*)request
 navigationType:(UIWebViewNavigationType)navigationType {
    
    if(navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSURL *url = request.URL;
        NSString* path = [url.path stringByReplacingOccurrencesOfString:@"/" withString:@""];
       [self showWikiEntry:[WikiEntry wikiEntryWithName:path]];
    }

    return YES;
}

- (void)handleTapOnLabel:(UITapGestureRecognizer *)tapGesture
{
    UILabel *label = (UILabel*)tapGesture.view;
    CGSize labelSize = label.bounds.size;
    // create instances of NSLayoutManager, NSTextContainer and NSTextStorage
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeZero];
    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:label.attributedText];
    
    // configure layoutManager and textStorage
    [layoutManager addTextContainer:textContainer];
    [textStorage addLayoutManager:layoutManager];
    [textStorage addAttribute:NSFontAttributeName value:label.font range:NSMakeRange(0, [textStorage length])];

    // configure textContainer for the label
    textContainer.lineFragmentPadding = 0.0;
    textContainer.lineBreakMode = label.lineBreakMode;
    textContainer.maximumNumberOfLines = label.numberOfLines;
    textContainer.size = labelSize;
    
    float alignment = 0;
    if (label.textAlignment == NSTextAlignmentCenter) {
        alignment = 0.5;
    }
    
    // find the tapped character location and compare it to the specified range
    CGPoint locationOfTouchInLabel = [tapGesture locationInView:label];
    CGRect textBoundingBox = [layoutManager usedRectForTextContainer:textContainer];
    CGPoint textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * alignment - textBoundingBox.origin.x,
                                              (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
    CGPoint locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
                                                         locationOfTouchInLabel.y - textContainerOffset.y);
    // See if point is within a glyph
    CGFloat glyphFraction;
    [layoutManager glyphIndexForPoint:locationOfTouchInTextContainer inTextContainer:textContainer fractionOfDistanceThroughGlyph:&glyphFraction];
    if (glyphFraction == 0 || glyphFraction == 1) {
        return;
    }

    NSInteger indexOfCharacter = [layoutManager characterIndexForPoint:locationOfTouchInTextContainer
                                                       inTextContainer:textContainer
                              fractionOfDistanceBetweenInsertionPoints:nil];
    
    NSDictionary* attributes = [label.attributedText attributesAtIndex:indexOfCharacter effectiveRange:NULL];
    NSString *wikiName = attributes[@"wiki"];
    if (wikiName) {
        [self showWikiEntry:[WikiEntry wikiEntryWithName:wikiName]];
    }
}

- (IBAction)onProfileSelection:(id)sender {
    [self populateStatBlock];
    [self populateWeaponsArray];
    [self populateSpecialRulesLabel];
    [self populateWeaponsLabel];
    [self.unitOptionsTableView reloadData];
}

- (IBAction)onSkillSelection:(id)sender {
    // Determine selected segment by examining the title string.
    // The API does not give us any way to attach arbitrary data to the segments as they are inserted.
    NSString *title = [self.skillSelectionSegmentedControl titleForSegmentAtIndex:self.skillSelectionSegmentedControl.selectedSegmentIndex];
    if([title isEqualToString:@"BS"])
    {
        selectedSkill = eSkillBS;
    }
    else if([title isEqualToString:@"DTW"])
    {
        selectedSkill = eSkillDTW;
    }
    else if([title isEqualToString:@"CC"])
    {
        selectedSkill = eSkillCC;
    }
    else if([title isEqualToString:@"Deployable" ])
    {
        selectedSkill = eSkillDeployable;
    }
    else if([title isEqualToString:@"Misc" ])
    {
        selectedSkill = eSkillMisc;
    }
    else if([title isEqualToString:@"Hacking" ])
    {
        selectedSkill = eSkillHacking;
    }
    else if([title isEqualToString:@"Pheroware" ])
    {
        selectedSkill = eSkillPheroware;
    }
    else if([title isEqualToString:@"Notes" ])
    {
        selectedSkill = eSkillNotes;
    }
    else if([title isEqualToString:@"Options" ])
    {
        selectedSkill = eSkillOptions;
    }

    // Load the correct set of skills and scroll to the top.
    [self.unitOptionsTableView reloadData];
    NSIndexPath *top = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.unitOptionsTableView scrollToRowAtIndexPath:top atScrollPosition:UITableViewScrollPositionTop animated:false];
}

-(void) refreshRight
{
    AppDelegate* delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate.containerViewController refreshRight];
}

-(void)refresh
{
    [self configureView];
}

-(void)setDisplayedRoster:(Roster*)roster
{
    displayedRoster = roster;
}

@end
