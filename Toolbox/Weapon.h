#import "_Weapon.h"
#import "AppDelegate.h"

@class Unit;
@interface Weapon : _Weapon {}
// Custom logic goes here.
+(Weapon*) weaponWithName:(NSString*)name;
+(Weapon*) weaponWithName:(NSString*)name andDelegate:(AppDelegate*)delegate;

+(NSString*) descriptionFromWeaponsArray:(NSArray*)theWeapons;
+(NSString*) descriptionFromWeaponsArray:(NSArray*)theWeapons seperator:(NSString*)seperator;
-(NSString*) damageDescriptorWithUnit:(UnitAspects*)unit;
-(NSString*) getExtrasString;

+(NSString*) rangeFromNSNumber:(NSNumber*)number;
+(NSString*) modFromNSNumber:(NSNumber*)number;
+(UIColor*)weaponDescriptorColorFromNSNumber:(NSNumber*)number;

-(Weapon*) createCopyWithDelegate:(AppDelegate*)delegate;
-(void) copyFromWeapon:(Weapon*)weapon;

#define WEAPON_DAM_PH    0
#define WEAPON_DAM_PH_1 -1
#define WEAPON_DAM_PH_2 -2
#define WEAPON_DAM_WIP  -3
#define WEAPON_DAM_STAR -4
#define WEAPON_DAM_MARK -5

@end
