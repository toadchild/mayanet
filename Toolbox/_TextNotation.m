// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TextNotation.m instead.

#import "_TextNotation.h"

@implementation TextNotationID
@end

@implementation _TextNotation

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TextNotation" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TextNotation";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TextNotation" inManagedObjectContext:moc_];
}

- (TextNotationID*)objectID {
	return (TextNotationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

#if TARGET_OS_IPHONE

#endif

@end

