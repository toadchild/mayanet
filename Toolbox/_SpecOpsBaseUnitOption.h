// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SpecOpsBaseUnitOption.h instead.

#import <CoreData/CoreData.h>

extern const struct SpecOpsBaseUnitOptionRelationships {
	__unsafe_unretained NSString *army;
	__unsafe_unretained NSString *specopBuilds;
	__unsafe_unretained NSString *specops;
	__unsafe_unretained NSString *unitOption;
} SpecOpsBaseUnitOptionRelationships;

@class Army;
@class SpecOpsBuild;
@class SpecOps;
@class UnitOption;

@interface SpecOpsBaseUnitOptionID : NSManagedObjectID {}
@end

@interface _SpecOpsBaseUnitOption : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SpecOpsBaseUnitOptionID* objectID;

@property (nonatomic, strong) Army *army;

//- (BOOL)validateArmy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *specopBuilds;

- (NSMutableSet*)specopBuildsSet;

@property (nonatomic, strong) SpecOps *specops;

//- (BOOL)validateSpecops:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) UnitOption *unitOption;

//- (BOOL)validateUnitOption:(id*)value_ error:(NSError**)error_;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newSpecopBuildsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _SpecOpsBaseUnitOption (SpecopBuildsCoreDataGeneratedAccessors)
- (void)addSpecopBuilds:(NSSet*)value_;
- (void)removeSpecopBuilds:(NSSet*)value_;
- (void)addSpecopBuildsObject:(SpecOpsBuild*)value_;
- (void)removeSpecopBuildsObject:(SpecOpsBuild*)value_;

@end

@interface _SpecOpsBaseUnitOption (CoreDataGeneratedPrimitiveAccessors)

- (Army*)primitiveArmy;
- (void)setPrimitiveArmy:(Army*)value;

- (NSMutableSet*)primitiveSpecopBuilds;
- (void)setPrimitiveSpecopBuilds:(NSMutableSet*)value;

- (SpecOps*)primitiveSpecops;
- (void)setPrimitiveSpecops:(SpecOps*)value;

- (UnitOption*)primitiveUnitOption;
- (void)setPrimitiveUnitOption:(UnitOption*)value;

@end
