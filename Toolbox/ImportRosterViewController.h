//
//  ImportRosterViewController.h
//  Toolbox
//
//  Created by Paul on 12/4/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Roster.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@protocol ImportRosterDelegate <NSObject>

-(void) onImportRosterDone:(BOOL)import;

@end

@interface ImportRosterViewController : UIViewController <MFMailComposeViewControllerDelegate, UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelBarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *importButton;
@property (strong, nonatomic) IBOutlet UIWebView *previewWebView;
@property (strong, nonatomic) Roster* roster;
@property (strong, nonatomic) id<ImportRosterDelegate> delegate;

- (IBAction)onCancel:(id)sender;
- (IBAction)onImport:(id)sender;

@end