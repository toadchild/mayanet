// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SpecOpsBuild.h instead.

#import <CoreData/CoreData.h>

extern const struct SpecOpsBuildAttributes {
	__unsafe_unretained NSString *name;
} SpecOpsBuildAttributes;

extern const struct SpecOpsBuildRelationships {
	__unsafe_unretained NSString *aspects;
	__unsafe_unretained NSString *baseUnitOption;
	__unsafe_unretained NSString *equipment;
	__unsafe_unretained NSString *groupMember;
	__unsafe_unretained NSString *specialRules;
	__unsafe_unretained NSString *unit;
	__unsafe_unretained NSString *weapons;
} SpecOpsBuildRelationships;

@class SpecOpsAspects;
@class SpecOpsBaseUnitOption;
@class SpecOpsEquipment;
@class GroupMember;
@class SpecOpsSpecialRule;
@class Unit;
@class SpecOpsWeapon;

@interface SpecOpsBuildID : NSManagedObjectID {}
@end

@interface _SpecOpsBuild : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SpecOpsBuildID* objectID;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) SpecOpsAspects *aspects;

//- (BOOL)validateAspects:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) SpecOpsBaseUnitOption *baseUnitOption;

//- (BOOL)validateBaseUnitOption:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *equipment;

- (NSMutableSet*)equipmentSet;

@property (nonatomic, strong) GroupMember *groupMember;

//- (BOOL)validateGroupMember:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *specialRules;

- (NSMutableSet*)specialRulesSet;

@property (nonatomic, strong) Unit *unit;

//- (BOOL)validateUnit:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *weapons;

- (NSMutableSet*)weaponsSet;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newEquipmentFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecialRulesFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newWeaponsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _SpecOpsBuild (EquipmentCoreDataGeneratedAccessors)
- (void)addEquipment:(NSSet*)value_;
- (void)removeEquipment:(NSSet*)value_;
- (void)addEquipmentObject:(SpecOpsEquipment*)value_;
- (void)removeEquipmentObject:(SpecOpsEquipment*)value_;

@end

@interface _SpecOpsBuild (SpecialRulesCoreDataGeneratedAccessors)
- (void)addSpecialRules:(NSSet*)value_;
- (void)removeSpecialRules:(NSSet*)value_;
- (void)addSpecialRulesObject:(SpecOpsSpecialRule*)value_;
- (void)removeSpecialRulesObject:(SpecOpsSpecialRule*)value_;

@end

@interface _SpecOpsBuild (WeaponsCoreDataGeneratedAccessors)
- (void)addWeapons:(NSSet*)value_;
- (void)removeWeapons:(NSSet*)value_;
- (void)addWeaponsObject:(SpecOpsWeapon*)value_;
- (void)removeWeaponsObject:(SpecOpsWeapon*)value_;

@end

@interface _SpecOpsBuild (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (SpecOpsAspects*)primitiveAspects;
- (void)setPrimitiveAspects:(SpecOpsAspects*)value;

- (SpecOpsBaseUnitOption*)primitiveBaseUnitOption;
- (void)setPrimitiveBaseUnitOption:(SpecOpsBaseUnitOption*)value;

- (NSMutableSet*)primitiveEquipment;
- (void)setPrimitiveEquipment:(NSMutableSet*)value;

- (GroupMember*)primitiveGroupMember;
- (void)setPrimitiveGroupMember:(GroupMember*)value;

- (NSMutableSet*)primitiveSpecialRules;
- (void)setPrimitiveSpecialRules:(NSMutableSet*)value;

- (Unit*)primitiveUnit;
- (void)setPrimitiveUnit:(Unit*)value;

- (NSMutableSet*)primitiveWeapons;
- (void)setPrimitiveWeapons:(NSMutableSet*)value;

@end
