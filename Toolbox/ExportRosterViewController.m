//
//  ExportRosterViewController.m
//  Toolbox
//
//  Created by Paul on 12/4/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "ExportRosterViewController.h"
#import "Util.h"


@interface ExportRosterViewController ()

@end

@implementation ExportRosterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.emailBarButton.enabled = [MFMailComposeViewController canSendMail];
}

-(void)viewWillAppear:(BOOL)animated
{
    NSString* html = [self.roster toLocalHTML];
    html = [self htmlWithBody:html];
    [self.previewWebView loadHTMLString:html baseURL:[[NSBundle mainBundle] bundleURL]];
    self.previewWebView.delegate = self;
    [super viewWillAppear:animated];
}

- (BOOL)webView:(UIWebView*)webView
shouldStartLoadWithRequest:(NSURLRequest*)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    return navigationType == UIWebViewNavigationTypeOther;
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*) htmlWithBody:(NSString*)body
{
    NSString* path = [[NSBundle mainBundle] pathForResource:@"previewTemplate" ofType:@"html"];
    NSString* linkableTemplate = [NSString stringWithContentsOfFile:path
                                                           encoding:NSUTF8StringEncoding
                                                              error:NULL];
    
    NSString* html = [linkableTemplate stringByReplacingOccurrencesOfString:@"${body}" withString:body];
    return html;
}

- (IBAction)onEmail:(id)sender
{
    MFMailComposeViewController* mailCon = [[MFMailComposeViewController alloc] init];
    if(mailCon != nil){
        mailCon.mailComposeDelegate = self;
        [mailCon setSubject:[self.roster emailHeader]];
        [mailCon setMessageBody:[self.roster toEmailHTML] isHTML:TRUE];
        [self presentViewController:mailCon animated:TRUE completion:nil];
    }
}

- (IBAction)onClipboard:(id)sender
{
    [[UIPasteboard generalPasteboard] setString:[self.roster toBBCode]];
    exportAlertView = [[UIAlertView alloc] initWithTitle:@"Copy Roster" message:@"The Roster has been copied to your clipboard." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [exportAlertView show];
}

- (IBAction)onLink:(id)sender
{
    [[UIPasteboard generalPasteboard] setString:[self.roster urlWithWebLink:TRUE]];
    exportAlertView = [[UIAlertView alloc] initWithTitle:@"Roster Link" message:@"The Roster link has been copied to your clipboard." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [exportAlertView show];
}

/*
- (IBAction)onLink:(id)sender
{
    
}
*/
- (IBAction)onDone:(id)sender
{
    [self.delegate onExportRosterDone];
}
@end
