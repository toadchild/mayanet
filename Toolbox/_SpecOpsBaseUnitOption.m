// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SpecOpsBaseUnitOption.m instead.

#import "_SpecOpsBaseUnitOption.h"

const struct SpecOpsBaseUnitOptionRelationships SpecOpsBaseUnitOptionRelationships = {
	.army = @"army",
	.specopBuilds = @"specopBuilds",
	.specops = @"specops",
	.unitOption = @"unitOption",
};

@implementation SpecOpsBaseUnitOptionID
@end

@implementation _SpecOpsBaseUnitOption

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SpecOpsBaseUnitOption" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SpecOpsBaseUnitOption";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SpecOpsBaseUnitOption" inManagedObjectContext:moc_];
}

- (SpecOpsBaseUnitOptionID*)objectID {
	return (SpecOpsBaseUnitOptionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic army;

@dynamic specopBuilds;

- (NSMutableSet*)specopBuildsSet {
	[self willAccessValueForKey:@"specopBuilds"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"specopBuilds"];

	[self didAccessValueForKey:@"specopBuilds"];
	return result;
}

@dynamic specops;

@dynamic unitOption;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newSpecopBuildsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors {
	NSFetchRequest *fetchRequest = [NSFetchRequest new];

	fetchRequest.entity = [NSEntityDescription entityForName:@"SpecOpsBuild" inManagedObjectContext:self.managedObjectContext];
	fetchRequest.predicate = [NSPredicate predicateWithFormat:@"baseUnitOption == %@", self];
	fetchRequest.sortDescriptors = sortDescriptors;

	return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
											   managedObjectContext:self.managedObjectContext
												 sectionNameKeyPath:nil
														  cacheName:nil];
}

#endif

@end

