// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CamoNotation.m instead.

#import "_CamoNotation.h"

@implementation CamoNotationID
@end

@implementation _CamoNotation

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"CamoNotation" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"CamoNotation";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"CamoNotation" inManagedObjectContext:moc_];
}

- (CamoNotationID*)objectID {
	return (CamoNotationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

#if TARGET_OS_IPHONE

#endif

@end

