//
//  TOCamoMarkerTableViewCell.h
//  Toolbox
//
//  Created by Paul on 12/6/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "CamoMarkerTableViewCell.h"

@class TOCamoNote;

@interface TOCamoMarkerTableViewCell : CamoMarkerTableViewCell

-(NSArray*)markers;

@end
