//
//  DataParser.m
//  Toolbox
//
//  Created by Paul on 10/20/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "DataParser.h"
#import "Weapon.h"
#import "Army.h"
#import "Sectorial.h"
#import "Unit.h"
#import "UnitAspects.h"
#import "UnitOption.h"
#import "UnitType.h"
#import "UnitClassification.h"
#import "SpecialRule.h"
#import "AppDelegate.h"
#import "SpecOps.h"
#import "SpecOpsAspects.h"
#import "SpecOpsBaseUnitOption.h"
#import "SpecOpsSpecialRule.h"
#import "SpecOpsWeapon.h"
#import "SpecOpsSpecialRule.h"
#import "SpecOpsEquipment.h"
#import "SpecOpsBuild.h"
#import "Wiki.h"
#import "WikiEntry.h"
#import "UnitProfile.h"
#import "TOCamoNotation.h"
#import "CamoNotation.h"
#import "BootyNotation.h"
#import "Booty.h"
#import "Migration.h"
#import "GroupMember.h"
#import "CombatGroup.h"
#import "Roster.h"
#import "Metachemistry.h"
#import "MetachemistryNotation.h"
#import "HackingProgram.h"
#import "HackingProgramGroup.h"
#import "HackingDevice.h"
#import "PherowareTactic.h"
#import "CCSkill.h"
#import "Note.h"
#import "UnitNote.h"

@implementation DataParser

-(DataParser*)initWithAppDelegate:(AppDelegate*)delegate andProgressLabel:(UILabel*)label
{
    self = [super init];
    if (self) {
        weaponsDictionary = [[NSMutableDictionary alloc] init];
        specialRulesDictionary = [[NSMutableDictionary alloc] init];
        typesDictionary = [[NSMutableDictionary alloc] init];
        specOpsWeaponsDictionary = [[NSMutableDictionary alloc] init];
        appDelegate = delegate;
        wikiNameDictionary = [[NSMutableDictionary alloc] init];
        hackingProgramDictionary = [[NSMutableDictionary alloc] init];
        hackingProgramGroupDictionary = [[NSMutableDictionary alloc] init];
        hackingDeviceDictionary = [[NSMutableDictionary alloc] init];
        pherowareTacticDictionary = [[NSMutableDictionary alloc] init];
        ccSkillDictionary = [[NSMutableDictionary alloc] init];
        progressMessageLabel = label;
    }
    return self;
}

-(void) populateTypes
{
    NSMutableDictionary* typesDic = [[NSMutableDictionary alloc] init];
    [typesDic setObject:@{@"name" : @"No Type", @"sortOrder": [NSNumber numberWithInt:0]} forKey:@"N/A"];
    [typesDic setObject:@{@"name" : @"Light Infantry", @"sortOrder": [NSNumber numberWithInt:1]} forKey:@"LI"];
    [typesDic setObject:@{@"name" : @"Medium Infantry", @"sortOrder": [NSNumber numberWithInt:2]} forKey:@"MI"];
    [typesDic setObject:@{@"name" : @"Heavy Infantry", @"sortOrder": [NSNumber numberWithInt:3]} forKey:@"HI"];
    [typesDic setObject:@{@"name" : @"T.A.G.", @"sortOrder": [NSNumber numberWithInt:4]} forKey:@"TAG"];
    [typesDic setObject:@{@"name" : @"Remotes", @"sortOrder": [NSNumber numberWithInt:5]} forKey:@"REM"];
    [typesDic setObject:@{@"name" : @"Skirmishers", @"sortOrder": [NSNumber numberWithInt:6]} forKey:@"SK"];
    [typesDic setObject:@{@"name" : @"Warbands", @"sortOrder": [NSNumber numberWithInt:7]} forKey:@"WB"];
    [typesDic setObject:@{@"name" : @"Spec-Ops", @"sortOrder": [NSNumber numberWithInt:8]} forKey:@"SPEC-OPS"];
    for(NSString* code in typesDic)
    {
        UnitType* unitType = [self getType:code];
        if(!unitType)
        {
            unitType = [UnitType insertInManagedObjectContext:[appDelegate managedObjectContext]];
            unitType.code = code;
            [typesDictionary setObject:unitType forKey:code];
        }
        NSDictionary* type = [typesDic objectForKey:code];
        unitType.name = [type objectForKey:@"name"];
        unitType.sortOrder = [type objectForKey:@"sortOrder"];
    }
}

-(void)setProgressMessage:(NSString*)string
{
    // Execute the UI update event on the main thread.
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->progressMessageLabel setText:string];
    });
}

-(void) parse
{
    if([self needToParse])
    {
        [self setProgressMessage:@"Parsing base data"];

        [self populateMetachemistry];
        [self populateMetachemistryLevelTwo];
        [self populateNotation];
    }

    [appDelegate saveContext];
}

// Cleanup function to get rid of old data during upgrade
-(void) deleteAllUnitData
{
    NSEntityDescription* entityDescription;
    NSFetchRequest* request;
    NSError* error;
    
    // Delete Rosters
    // Should cascade CombatGroup, GroupMember, SpecOpsBuild
    {
        entityDescription = [Roster entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* rosterList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        NSAssert(error == nil, @"Could not load rosters for deletion: %@", error);
        
        for (Roster *roster in rosterList) {
            [appDelegate.managedObjectContext deleteObject:roster];
        }
    }

    // Delete Roster Notes
    {
        entityDescription = [Note entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* notesList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        NSAssert(error == nil, @"Could not load notes for deletion: %@", error);
        
        for (Note *note in notesList) {
            [appDelegate.managedObjectContext deleteObject:note];
        }
    }
    
    // Delete Units
    {
        entityDescription = [Unit entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* unitList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        NSAssert(error == nil, @"Could not load units for deletion: %@", error);
        
        for (Unit *unit in unitList) {
            [appDelegate.managedObjectContext deleteObject:unit];
        }
    }

    // Delete UnitAspects
    {
        entityDescription = [UnitAspects entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* aspectList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        NSAssert(error == nil, @"Could not load unit aspects for deletion: %@", error);
        
        for (UnitAspects *aspect in aspectList) {
            [appDelegate.managedObjectContext deleteObject:aspect];
        }
    }

    // Delete UnitOptions
    {
        entityDescription = [UnitOption entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* optionList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        NSAssert(error == nil, @"Could not load unit options for deletion: %@", error);
        
        for (UnitOption *option in optionList) {
            [appDelegate.managedObjectContext deleteObject:option];
        }
    }

    // Delete UnitProfiles
    {
        entityDescription = [UnitProfile entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* profileList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        NSAssert(error == nil, @"Could not load unit profiles for deletion: %@", error);
        
        for (UnitProfile *profile in profileList) {
            [appDelegate.managedObjectContext deleteObject:profile];
        }
    }

    // Delete SpecOps
    {
        entityDescription = [SpecOps entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* specOpsList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        NSAssert(error == nil, @"Could not load spec ops for deletion: %@", error);
        
        for (SpecOps *specOps in specOpsList) {
            [appDelegate.managedObjectContext deleteObject:specOps];
        }
    }

    // Delete SpecOpsBaseUnitOption
    {
        entityDescription = [SpecOpsBaseUnitOption entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* specOpsBaseList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        NSAssert(error == nil, @"Could not load spec ops base unit option for deletion: %@", error);
        
        for (SpecOpsBaseUnitOption *base in specOpsBaseList) {
            [appDelegate.managedObjectContext deleteObject:base];
        }
    }
    
    // Delete SpecOpsBuild
    {
        entityDescription = [SpecOpsBuild entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* specOpsBuildList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        NSAssert(error == nil, @"Could not load spec ops build for deletion: %@", error);
        
        for (SpecOpsBuild *build in specOpsBuildList) {
            [appDelegate.managedObjectContext deleteObject:build];
        }
    }
    
    // Delete SpecOpsAspects
    {
        entityDescription = [SpecOpsAspects entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* specOpsAspectsList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        NSAssert(error == nil, @"Could not load spec ops aspects for deletion: %@", error);
        
        for (SpecOpsAspects *aspects in specOpsAspectsList) {
            [appDelegate.managedObjectContext deleteObject:aspects];
        }
    }
    
    // Delete SpecOpsSpecialRule
    {
        entityDescription = [SpecOpsSpecialRule entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* specOpsSpecialList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        NSAssert(error == nil, @"Could not load spec ops special rules for deletion: %@", error);
        
        for (SpecOpsSpecialRule *special in specOpsSpecialList) {
            [appDelegate.managedObjectContext deleteObject:special];
        }
    }
    
    // Delete SpecOpsEquipment
    {
        entityDescription = [SpecOpsEquipment entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* specOpsEquipmentList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        NSAssert(error == nil, @"Could not load spec ops equipment for deletion: %@", error);
        
        for (SpecOpsEquipment *equip in specOpsEquipmentList) {
            [appDelegate.managedObjectContext deleteObject:equip];
        }
    }
    
    // Delete SpecOpsWeapon
    {
        entityDescription = [SpecOpsWeapon entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* specOpsWeaponList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        NSAssert(error == nil, @"Could not load spec ops base unit option for deletion: %@", error);
        
        for (SpecOpsWeapon *weapon in specOpsWeaponList) {
            [appDelegate.managedObjectContext deleteObject:weapon];
        }
    }
}

#pragma mark Migrations

-(void) migrate
{
    if(![Migration canMigrate:appDelegate])
    {
        NSLog(@"Warning: Migrations cannot occur!");
        return;
    }
    NSArray* migrations = [Migration migrations:appDelegate];
    for(Migration* migration in migrations)
    {
        NSLog(@"%@",migration.name);
    }
    
    ///////////////////////////
    // Weapons and Equipment //
    ///////////////////////////
    
    // Load any/all new weapons (reusable migration)
    [self migrateUpdateWeapons: @"weapons_version_4.3.0"];

    // Load any/all new hacking data (reusable migration)
    [self migrateUpdateHacking: @"hacking_version_4.5.0"];

    // Load any/all new pheroware data (reusable migration)
    [self migrateUpdatePheroware: @"pheroware_version_2.9.0"];
    
    // Load any/all new cc skill data (reusable migration)
    [self migrateUpdateCCSkills: @"cc_skill_version_4.0.2"];
    
    // Load any/add new unit types (reusable migration)
    [self migrateUpdateUnitTypes: @"unit_types_version_3.0.0"];

    // Load any/add new unit classifications (reusable migration)
    [self migrateUpdateUnitClassifications: @"unit_classifications_version_2.6.2"];
    
    ////////////////////////
    // Units and profiles //
    ////////////////////////
    
    // New Units
    [self migrateUpdateUnits: @"units_version_4.5.1"];

    //////////
    // Wiki //
    //////////

    // When a new import is required, just increment the migration name
    [self migrateUpdateWiki: @"wiki_version_4.5.0"];
    
    ///////////////
    // Notations //
    ///////////////
    
    [self migrateAmbushCamoNotation];
    [self migrateAddMetachemistryL2];
    [self migrateAddMetachemistryL1];

    // Clean up remenants of Yu Jing's JSA
    [self migrateRemoveLegacyJSA];

    [self setProgressMessage:@""];
}

-(void) migrateAmbushCamoNotation
{
    NSString* migrationID = @"ambush_camo_notation";
    if(![Migration migrationExists:appDelegate withName:migrationID])
    {
        [self setProgressMessage:@"Updating Ambush Camo"];
        
        SpecialRule* ambushCamouflageSpecialRule = [self getSpecialRule:@"CH: Ambush Camouflage"];
        if(ambushCamouflageSpecialRule && ambushCamouflageSpecialRule.notation == NULL)
        {
            TOCamoNotation* notation = [CamoNotation insertInManagedObjectContext:appDelegate.managedObjectContext];
            notation.specialRule = ambushCamouflageSpecialRule;
        }

        Migration* migration = [Migration insertInManagedObjectContext:[appDelegate managedObjectContext]];
        migration.name = migrationID;
        [appDelegate saveContext];
    }
}

-(void)migrateAddMetachemistryL2
{
    NSString* migrationID = @"add_metachemistry_l2";
    if(![Migration migrationExists:appDelegate withName:migrationID])
    {
        [self setProgressMessage:@"Updating Metachemistry L2"];
        // Check if any Metachemistry L2 objects already exist
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [Metachemistry entityInManagedObjectContext:appDelegate.managedObjectContext];
        [fetchRequest setEntity:entity];
        NSPredicate *predicate = [NSPredicate predicateWithFormat: @"levelTwo != 0"];
        [fetchRequest setPredicate:predicate];
        
        NSError* error;
        NSUInteger count = [appDelegate.managedObjectContext countForFetchRequest:fetchRequest error:&error];
        if(error == nil && count == 0)
        {
            [self populateNotation];
            [self populateMetachemistryLevelTwo];
        }
        
        Migration* migration =  [Migration insertInManagedObjectContext:[appDelegate managedObjectContext]];
        migration.name = migrationID;
        [appDelegate saveContext];
    }
}

// Updating name from "Metachemistry" to "Metachemistry L1"
-(void)migrateAddMetachemistryL1
{
    NSString* migrationID = @"add_metachemistry_l1";
    if(![Migration migrationExists:appDelegate withName:migrationID])
    {
        [self setProgressMessage:@"Updating Metachemistry L1"];
        [self populateNotation];

        Migration* migration =  [Migration insertInManagedObjectContext:[appDelegate managedObjectContext]];
        migration.name = migrationID;
        [appDelegate saveContext];
    }
}
    
-(void)migrateRemoveLegacyJSA
{
    NSString* migrationID = @"remove_legacy_jsa";
    if(![Migration migrationExists:appDelegate withName:migrationID])
    {
        NSEntityDescription* entityDescription;
        NSFetchRequest* request;
        NSError* error;
        
        entityDescription = [Sectorial entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* sectorialList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        NSAssert(error == nil, @"Could not load sectorials for deletion: %@", error);
        
        for (Sectorial *sectorial in sectorialList) {
            if (sectorial.idValue == eSectorialLegacyJSA) {
                for (Roster* roster in sectorial.rosters) {
                    [appDelegate.managedObjectContext deleteObject:roster];
                }
                [appDelegate.managedObjectContext deleteObject:sectorial];
                break;
            }
        }

        Migration* migration =  [Migration insertInManagedObjectContext:[appDelegate managedObjectContext]];
        migration.name = migrationID;
        [appDelegate saveContext];
    }
}

#pragma mark Reusable Migrations

-(void)migrateUpdateWiki: (NSString*)migrationID
{
    if(![Migration migrationExists:appDelegate withName:migrationID])
    {
        [self setProgressMessage:@"Updating wiki"];
        NSEntityDescription *entityDescription;
        NSFetchRequest *request;
        NSError* error;

        // Erase all wiki data
        entityDescription = [Wiki entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* wikiList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        if ((error != nil) || (wikiList == nil)) {
            NSLog(@"Could not load wikis for deletion");
        }
        
        for (Wiki *wiki in wikiList) {
            [appDelegate.managedObjectContext deleteObject:wiki];
        }
        
        // Erase all wiki pages
        entityDescription = [WikiEntry entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* wikiEntryList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        if ((error != nil) || (wikiEntryList == nil)) {
            NSLog(@"Could not load wiki pages for deletion");
        }
        
        for (WikiEntry *wikiEntry in wikiEntryList) {
            [appDelegate.managedObjectContext deleteObject:wikiEntry];
        }
        
        // populate the rules structure that the importer uses
        entityDescription = [SpecialRule entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];

        NSArray* specialRuleList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        if ((error != nil) || (specialRuleList == nil)) {
            NSLog(@"Could not load list of special rules");
        }
        
        for (SpecialRule *specialRule in specialRuleList) {
            [specialRulesDictionary setObject:specialRule forKey:specialRule.name];
        }

        // populate the weapons structure that the importer uses
        entityDescription = [Weapon entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* weaponList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        if ((error != nil) || (weaponList == nil)) {
            NSLog(@"Could not load list of weapons");
        }
        
        for (Weapon *weapon in weaponList) {
            [weaponsDictionary setObject:weapon forKey:weapon.name];
        }
        
        // populate the hacking program structure that the importer uses
        entityDescription = [HackingProgram entityInManagedObjectContext:appDelegate.managedObjectContext];
        request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSArray* hackingProgramList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        if ((error != nil) || (hackingProgramList == nil)) {
            NSLog(@"Could not load list of hacking programs");
        }
        
        for (HackingProgram *program in hackingProgramList) {
            [hackingProgramDictionary setObject:program forKey:program.name];
        }
        
        // populate the pheroware tactic structure that the importer uses
        for (PherowareTactic *tactic in [PherowareTactic allTactics]) {
            [pherowareTacticDictionary setObject:tactic forKey:tactic.name];
        }
        
        // Re-import
        [self parseWiki];
        [self parseChoiceSpecialRules];
        [self matchSpecialRulesToWikiEntries];
        [self matchWeaponsToWikiEntries];
        [self matchHackingProgramsToWikiEntries];
        [self matchPherowareTacticsToWikiEntries];
        
        Migration* migration =  [Migration insertInManagedObjectContext:[appDelegate managedObjectContext]];
        migration.name = migrationID;
        [appDelegate saveContext];
    }
}

-(void)migrateUpdateWeapons: (NSString*)migrationID
{
    if(![Migration migrationExists:appDelegate withName:migrationID])
    {
        [self setProgressMessage:@"Updating weapons"];
        
        NSDictionary* weapons = [DataParser getFile:@"infinitydata/weapons"];
        [self parseWeapons: weapons];

        // populate the wiki page structure that the matcher uses
        NSEntityDescription* entityDescription = [WikiEntry entityInManagedObjectContext:appDelegate.managedObjectContext];
        NSFetchRequest* request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSError* error;
        NSArray* wikiList = [appDelegate.managedObjectContext executeFetchRequest:request error:&error];
        if ((error != nil) || (wikiList == nil)) {
            NSLog(@"Could not load list of wiki pages");
        }
        
        for (WikiEntry *wiki in wikiList) {
            [wikiNameDictionary setObject:wiki forKey:wiki.name];
        }
        
        // Update wiki page mappings
        [self matchWeaponsToWikiEntries];
        
        // Find all dual weapons and regenerate them
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [Weapon entityInManagedObjectContext:appDelegate.managedObjectContext];
        [fetchRequest setEntity:entity];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name ENDSWITH '(2)'"];
        [fetchRequest setPredicate:predicate];
        
        NSArray* dualWeapons = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        if(error == nil)
        {
            for(Weapon *weapon in dualWeapons)
            {
                [self createDualWeapon:weapon.name];
            }
        }
        
        // Populate booty tables; must be called after weapons are loaded.
        [self populateBooty];
        
        Migration* migration =  [Migration insertInManagedObjectContext:[appDelegate managedObjectContext]];
        migration.name = migrationID;
        [appDelegate saveContext];
    }
}

-(void)migrateUpdateHacking: (NSString*)migrationID
{
    if(![Migration migrationExists:appDelegate withName:migrationID])
    {
        [self setProgressMessage:@"Updating hacking"];
        
        [self parseHackingData];
        [self matchHackingProgramsToWikiEntries];
        
        Migration* migration =  [Migration insertInManagedObjectContext:[appDelegate managedObjectContext]];
        migration.name = migrationID;
        [appDelegate saveContext];
    }
}

-(void)migrateUpdatePheroware: (NSString*)migrationID
{
    if(![Migration migrationExists:appDelegate withName:migrationID])
    {
        [self setProgressMessage:@"Updating pheroware"];
        
        [self parsePherowareData];
        [self matchPherowareTacticsToWikiEntries];
        
        Migration* migration =  [Migration insertInManagedObjectContext:[appDelegate managedObjectContext]];
        migration.name = migrationID;
        [appDelegate saveContext];
    }
}

-(void)migrateUpdateUnits: (NSString*)migrationID
{
    if(![Migration migrationExists:appDelegate withName:migrationID])
    {
        [self setProgressMessage:@"Updating unit data"];
        NSArray* rosters = [Roster exportAllWithDelegate:appDelegate];
        [self deleteAllUnitData];
        [self parseMasterUnits];
        [self parseArmiesAndSectorials];
        [self copyMercs];
        [self parseArmyColors];
        [self parseSpecOpsUnits];

        [Roster importAllFromArray:rosters withDelegate:appDelegate];
        
        Migration* migration =  [Migration insertInManagedObjectContext:[appDelegate managedObjectContext]];
        migration.name = migrationID;
        [appDelegate saveContext];
    }
}

-(void)migrateUpdateCCSkills: (NSString*)migrationID
{
    if(![Migration migrationExists:appDelegate withName:migrationID])
    {
        [self setProgressMessage:@"Updating CC skills"];
        
        [self parseCCSkillData];
        
        Migration* migration =  [Migration insertInManagedObjectContext:[appDelegate managedObjectContext]];
        migration.name = migrationID;
        [appDelegate saveContext];
    }
}

-(void)migrateUpdateUnitTypes: (NSString*)migrationID
{
    if(![Migration migrationExists:appDelegate withName:migrationID])
    {
        [self populateTypes];
        
        Migration* migration =  [Migration insertInManagedObjectContext:[appDelegate managedObjectContext]];
        migration.name = migrationID;
        [appDelegate saveContext];
    }
}

-(void)migrateUpdateUnitClassifications: (NSString*)migrationID
{
    if(![Migration migrationExists:appDelegate withName:migrationID])
    {
        NSArray* classifications = @[
            @{@"code": @"Garrison", @"displayName": @"Garrison Troops"},
            @{@"code": @"Line", @"displayName": @"Line Troops"},
            @{@"code": @"Specially Trained", @"displayName": @"Spec. Trained Troops"},
            @{@"code": @"Veteran", @"displayName": @"Veteran Troops"},
            @{@"code": @"Elite", @"displayName": @"Elite Troops"},
            @{@"code": @"Headquarters", @"displayName": @"Headquarters Troops"},
            @{@"code": @"Mechanized", @"displayName": @"Mechanized Troops"},
            @{@"code": @"HQ/Mechanized", @"displayName": @"HQ / Mechanized Troops"},
            @{@"code": @"Support", @"displayName": @"Support Troops"},
            @{@"code": @"Mercenary", @"displayName": @"Mercenary Troops"},
            @{@"code": @"Character", @"displayName": @"Character"},
        ];
        
        for(NSDictionary* classDict in classifications)
        {
            UnitClassification* classification = [UnitClassification classificationWithCode:classDict[@"code"]];
            if(!classification)
            {
                classification = [UnitClassification newClassificationWithCode:classDict[@"code"]];
            }
            classification.displayName = classDict[@"displayName"];
        }
        
        Migration* migration =  [Migration insertInManagedObjectContext:[appDelegate managedObjectContext]];
        migration.name = migrationID;
        [appDelegate saveContext];
    }
}

#pragma mark Initial Population

-(BOOL) needToParse
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [Army entityInManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setIncludesSubentities:FALSE];
    
    [fetchRequest setFetchLimit:1];
    
    NSError* error;
    NSArray* results = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(error == nil && [results count] == 1)
    {
        return FALSE;
    }
    return TRUE;

}

-(void) parseArmyColors
{
    NSDictionary* colors = [DataParser getFile:@"colors"];
    
    NSDictionary* armies = [colors objectForKey:@"armies"];
    NSDictionary* sectorials = [colors objectForKey:@"sectorials"];
    
    
    for(NSDictionary* sectorial_record in sectorials)
    {
        NSNumber* sectorialID = [sectorial_record valueForKey:@"id"];
        Sectorial* sectorial = [Sectorial sectorialWithID: [sectorialID intValue] withDelegate:appDelegate];
        NSAssert(sectorial != nil, @"Unknown sectorial %@", sectorialID);
        sectorial.rgbBackground = sectorial_record[@"backgroundColor"];
        sectorial.rgbPrimary = sectorial_record[@"primaryColor"];
        sectorial.rgbSecondary = sectorial_record[@"secondaryColor"];
    }
    for(NSDictionary* army_record in armies)
    {
        NSNumber* armyID = [army_record valueForKey:@"id"];
        Army* army = [Army armyWithID: [armyID intValue] withDelegate:appDelegate];
        NSAssert(army != nil, @"Unknown army %@", armyID);
        army.rgbBackground = army_record[@"backgroundColor"];
        army.rgbPrimary = army_record[@"primaryColor"];
        army.rgbSecondary = army_record[@"secondaryColor"];
    }
}

-(NSArray*) armyFileData
{
    NSMutableArray* armies = [[NSMutableArray alloc] init];
    [armies addObject:[DataParser getFile:@"infinitydata/alep_army"]];
    [armies addObject:[DataParser getFile:@"infinitydata/aria_army"]];
    [armies addObject:[DataParser getFile:@"infinitydata/comb_army"]];
    [armies addObject:[DataParser getFile:@"infinitydata/haqq_army"]];
    [armies addObject:[DataParser getFile:@"infinitydata/noma_army"]];
    [armies addObject:[DataParser getFile:@"infinitydata/pano_army"]];
    [armies addObject:[DataParser getFile:@"infinitydata/toha_army"]];
    [armies addObject:[DataParser getFile:@"infinitydata/yuji_army"]];
    [armies addObject:[DataParser getFile:@"infinitydata/merc_army"]];
    [armies addObject:[DataParser getFile:@"infinitydata/o12_army"]];

    return armies;
}

-(NSArray*) sectorialFileData
{
    NSMutableArray* sectorials = [[NSMutableArray alloc] init];
    [sectorials addObject:[DataParser getFile:@"infinitydata/alep_phalanx_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/alep_operations_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/aria_caledonian_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/aria_mrrf_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/aria_usa_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/aria_tartary_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/comb_morat_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/comb_shasvastii_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/comb_onyx_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/haqq_hassassin_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/haqq_qk_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/haqq_ramah_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/noma_bakunin_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/noma_corregidor_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/noma_tunguska_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/pano_acontecimento_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/pano_militaryorders_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/pano_noeterran_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/pano_varuna_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/yuji_imperialservice_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/yuji_invincible_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/merc_jsa_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/merc_bayram_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/merc_ikari_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/merc_starco_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/merc_foreign_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/merc_dahshat_army"]];
    [sectorials addObject:[DataParser getFile:@"infinitydata/merc_spiral_army"]];

    return sectorials;
}

// Copies Master units into a all Armies and Sectorials
// Also creates an empty army/sectorial if needed.
-(void) parseArmiesAndSectorials
{
    // Create armies
    NSArray* armies = [self armyFileData];
    for(NSArray* wrapped_army_record in armies)
    {
        NSDictionary* army_record = wrapped_army_record[0];
        Army* army = [Army armyWithIDCreatingNew:[[army_record valueForKey:@"id"] intValue] withDelegate:appDelegate];
        NSAssert(army != nil, @"Unable to create army %@", [army_record valueForKey:@"id"]);
        army.name = [army_record valueForKey:@"name"];
        NSLog(@"Processed army %@ (%@)", [army_record valueForKey:@"id"], [army_record valueForKey:@"name"]);
    }
    
    // create sectorials
    NSArray* sectorials = [self sectorialFileData];
    for(NSArray* wrapped_sectorial_record in sectorials)
    {
        NSDictionary* sectorial_record = wrapped_sectorial_record[0];
        Sectorial* sectorial = [Sectorial sectorialWithIDCreatingNew:[[sectorial_record valueForKey:@"id"] intValue] withDelegate:appDelegate];
        NSAssert(sectorial != nil, @"Unable to create sectorial %@", [sectorial_record valueForKey:@"id"]);
        sectorial.name = [sectorial_record valueForKey:@"name"];
        sectorial.army = [Army armyWithID:[[sectorial_record valueForKey:@"army_id"] intValue] withDelegate:appDelegate];
        sectorial.abbr = [sectorial_record valueForKey:@"abbr"];
        NSLog(@"Processed sectorial %@ (%@)", [sectorial_record valueForKey:@"id"], [sectorial_record valueForKey:@"name"]);
    }
    
    // populate units into armies and sectorials
    NSMutableArray* factions = [armies mutableCopy];
    [factions addObjectsFromArray:sectorials];
    for(NSArray* wrapped_faction_record in factions)
    {
        NSDictionary* faction_record = wrapped_faction_record[0];
        int factionId = [[faction_record valueForKey:@"id"] intValue];
        NSDictionary* units = [faction_record valueForKey:@"units"];

        [self setProgressMessage:[NSString stringWithFormat:@"Updating units for %@", [faction_record valueForKey:@"name"]]];
        
        // Only one of these will find a match.
        Sectorial *sectorial = [Sectorial sectorialWithID:factionId withDelegate:appDelegate];
        Army *army = [Army armyWithID:factionId withDelegate:appDelegate];
        NSAssert(army == nil || sectorial == nil, @"Found both an army and a sectorial with id %d", factionId);
        NSAssert(army != nil || sectorial != nil, @"Unable to find an army or a sectorial with id %d", factionId);

        for(NSDictionary* unit_record in units)
        {
            // Update if the unit already exists in this faction
            int id = [[unit_record valueForKey:@"id"] intValue];
            Unit* baseUnit = [Unit masterUnitWithID:id withDelegate:appDelegate];
            NSAssert(baseUnit != nil, @"Base Unit Not Found: %d (%@)", id, [unit_record valueForKey:@"comment"]);
            
            Unit* factionUnit;
            if(sectorial)
            {
                factionUnit = [sectorial unitWithID:id];
            }
            else
            {
                factionUnit = [army unitWithID:id];
            }

            if(!factionUnit)
            {
                // Create new if unit does not already exist
                factionUnit = [baseUnit clone];
                factionUnit.army = army;
                factionUnit.sectorial = sectorial;
            }
            else
            {
                // If the unit was previously only available as a non-ITS merc unit, clear that flag and copy the AVA.
                if(factionUnit.isMercValue)
                {
                    factionUnit.mercAvaValue = factionUnit.avaValue;
                    factionUnit.isMercValue = FALSE;
                }
                // Copy any updated profile data into the factional unit
                [factionUnit copyFrom:baseUnit];
            }

            NSString* note = [unit_record valueForKey:@"note"];
            if(note)
            {
                factionUnit.note = note;
            }
            
            NSArray* notes = unit_record[@"notes"];
            if (notes) {
                for(NSDictionary* noteDict in notes) {
                    UnitNote* note = [UnitNote insertInManagedObjectContext:[appDelegate managedObjectContext]];
                    note.title = noteDict[@"title"];
                    note.note = noteDict[@"note"];
                    [factionUnit addNotesObject:note];
                }
            }
            
            // Army lists use the AVA from the master unit
            NSString* avaString = [unit_record valueForKey:@"ava"];
            if(avaString)
            {
                if([avaString isEqualToString:@"T"])
                {
                    [factionUnit setAva:nil];
                }
                else
                {
                    factionUnit.avaValue = [[unit_record valueForKey:@"ava"] intValue];
                }
            }
            
            // Some units have joint AVA.
            if (unit_record[@"sharedAva"]) {
                int avaId = [unit_record[@"sharedAva"] intValue];
                factionUnit.sharedAvaIdValue = avaId;
            }
            
            // Update the unit skills if there were any changes
            NSArray* skills = [unit_record valueForKey:@"skills"];
            for(NSDictionary* skill in skills)
            {
                SpecialRule* rule = [SpecialRule specialRuleWithName:skill[@"name"]];
                NSAssert(rule, @"Unknown skill '%@'", skill[@"name"]);
                
                NSMutableOrderedSet* rulesSet = nil;
                if (skill[@"profile"]) {
                    int profileIndex = [skill[@"profile"] intValue];
                    for (UnitProfile* profile in factionUnit.profiles) {
                        if (profile.idValue == profileIndex) {
                            rulesSet = profile.aspects.specialRulesSet;
                            break;
                        }
                    }
                    NSAssert(rulesSet, @"Unknown profile id %d", profileIndex);
                } else if (skill[@"option"]) {
                    int optionIndex = [skill[@"option"] intValue];
                    UnitOption* option = [factionUnit optionWithID:optionIndex];
                    NSAssert(option, @"Unknown option id %d", optionIndex);
                    rulesSet = option.specialRulesSet;
                } else {
                    rulesSet = factionUnit.aspects.specialRulesSet;
                }
                
                if([skill[@"hide"] boolValue]) {
                    NSAssert([rulesSet containsObject:rule], @"Unit/profile does not have skill '%@'", skill[@"name"]);
                    [rulesSet removeObject:rule];
                } else if ([skill[@"add"] boolValue]) {
                    NSAssert(![rulesSet containsObject:rule], @"Unit/profile already has skill '%@'", skill[@"name"]);
                    [rulesSet addObject:rule];
                } else {
                    NSAssert(FALSE, @"Unknown or missing skill operation!");
                }
            }
            
            // Update the unit options if there were any changes
            NSArray* childs = [unit_record valueForKey:@"childs"];
            for(NSDictionary* child in childs)
            {
                UnitOption* option = [factionUnit optionWithID:[child[@"id"] intValue]];
                NSAssert(option != nil, @"Unknown option id %@", child[@"id"]);

                // Delete options from option set if they are marked as hidden
                if([child[@"hide"] boolValue])
                {
                    [factionUnit deleteOption:option];
                }
                
                // SWC overrides
                if(child[@"swc"] != nil)
                {
                    option.swc = [NSDecimalNumber decimalNumberWithString:child[@"swc"]];
                }
            }
            
            // Update the unit classification if changed
            NSString* classification = [unit_record valueForKey:@"classification"];
            if (classification) {
                factionUnit.aspects.classification = [UnitClassification classificationWithCode:classification];
                NSAssert(factionUnit.aspects.classification != nil, @"Unknown unit classification: %@", classification);
            }
        }
    }
}

// Loads all units, creates master enties that are not in any army
-(void) parseMasterUnits
{
    NSMutableArray* units = [[NSMutableArray alloc] init];
    
    [units addObject:[DataParser getFile:@"infinitydata/alep_units"]];
    [units addObject:[DataParser getFile:@"infinitydata/aria_units"]];
    [units addObject:[DataParser getFile:@"infinitydata/comb_units"]];
    [units addObject:[DataParser getFile:@"infinitydata/haqq_units"]];
    [units addObject:[DataParser getFile:@"infinitydata/noma_units"]];
    [units addObject:[DataParser getFile:@"infinitydata/pano_units"]];
    [units addObject:[DataParser getFile:@"infinitydata/toha_units"]];
    [units addObject:[DataParser getFile:@"infinitydata/yuji_units"]];
    [units addObject:[DataParser getFile:@"infinitydata/merc_units"]];
    [units addObject:[DataParser getFile:@"infinitydata/o12_units"]];
    for(NSDictionary* unit_set in units)
    {
        [self parseUnits:unit_set];
    }
}

-(void) parseUnits:(NSDictionary*)unitDictionary
{
    for(NSDictionary* unit_record in unitDictionary)
    {
        Unit* unit = [Unit masterUnitWithID:[[unit_record valueForKey:@"id"] intValue] withDelegate:appDelegate];
        if(!unit) {
            unit = [Unit insertInManagedObjectContext:[appDelegate managedObjectContext]];
            unit.idValue = [[unit_record valueForKey:@"id"] intValue];
        }
        
        NSArray* profiles = [self getUnitProfileAspects:unit_record];
        
        UnitProfile* mainProfile = [profiles firstObject];
        unit.aspects = mainProfile.aspects;
        
        //Type off of main profile, if able.
        [unit setType:mainProfile.type];
        
        unit.strOverride = mainProfile.strOverride;
        unit.wOverride = mainProfile.wOverride;
        
        //Save the rest of the profiles
        for(int x = 1; x < [profiles count]; x++)
        {
            UnitProfile* profile = [profiles objectAtIndex:x];
            // Update existing profile if it exists.
            if([unit.profilesSet count] >= x)
            {
                [unit.profilesSet[x-1] copyFrom: profile];
                [appDelegate.managedObjectContext deleteObject:profile];
            }
            else
            {
                [unit.profilesSet addObject:profile];
            }
        }

        // Do not currently have code to handle a reduction in the number of profiles.
        NSAssert([unit.profilesSet count] == [profiles count] - 1, @"Mismatched profile count for unit %@ (%@)", [unit_record valueForKey: @"id"], [unit_record valueForKey:@"isc"]);
        
        //Name
        [unit setName:[unit_record valueForKey:@"name"]];
        
        //ISC
        [unit setIsc:[unit_record valueForKey:@"isc"]];
        
        // Legacy ISC (used for importing old rosters)
        unit.legacyIsc = unit_record[@"legacy_isc"];
        
        //options
        NSDictionary* options = [unit_record valueForKey:@"childs"];
        int optionIndex = 0;
        for(NSDictionary* option in options)
        {
            [self parseOption:option unit:unit withIndex: optionIndex++];
        }

        // Delete any excess options.
        while([unit.optionsSet count] > optionIndex)
        {
            [unit.optionsSet removeObjectAtIndex:optionIndex];
        }
        
        // Set primary profile's independent cost to all options
        if(mainProfile.isIndependentValue)
        {
            for(UnitOption *unitOption in unit.options){
                [unitOption setIsIndependentValue:TRUE];
                unitOption.independentCost = mainProfile.independentCost;
                unitOption.independentSWC = mainProfile.independentSWC;
            }
        }
        
        //We don't need the first UnitProfile object, just aspects, and type
        [appDelegate.managedObjectContext deleteObject:mainProfile];
        
        //AVA
        NSString* avaString = [unit_record valueForKey:@"ava"];
        if([avaString compare:@"T"] != NSOrderedSame)
        {
            [unit setAva:
             [NSNumber numberWithInteger:[
                                      [unit_record valueForKey:@"ava"] integerValue
                                      ]
              ]
             ];
        }
        else
        {
            [unit setAva:nil];
        }

        // Some units have joint AVA.
        if (unit_record[@"sharedAva"]) {
            int avaId = [unit_record[@"sharedAva"] intValue];
            unit.sharedAvaIdValue = avaId;
        }
        
    }
}

-(NSArray*) getUnitProfileAspects:(NSDictionary*)unit_dictionary
{
    NSMutableArray* foundProfiles = [[NSMutableArray alloc] init];
    
    NSMutableArray* unit_records = [[NSMutableArray alloc] init];
    
    //Detect Profiles
    NSArray* profiles = [unit_dictionary objectForKey:@"profiles"];
    if(profiles)
    {
        for(NSDictionary* profile in profiles)
        {
            //All profiles are diffed off of first profile
            NSMutableDictionary* mutableProfile = [[NSMutableDictionary alloc] initWithDictionary:[profiles firstObject]];
            if(profile != [profiles firstObject])
            {
                [mutableProfile removeObjectForKey:@"independent"];
                [mutableProfile removeObjectForKey:@"isc"];
            }
            [mutableProfile setValuesForKeysWithDictionary:profile];
            
            //Unit Special rules source array
            NSArray* specialRulesArray = [unit_dictionary valueForKey:@"spec"];

            //Add profile special rules
            specialRulesArray = [specialRulesArray arrayByAddingObjectsFromArray:[profile valueForKey:@"spec"]];

            [mutableProfile setValue:specialRulesArray forKey:@"spec"];
            [unit_records addObject:mutableProfile];
        }
    }
    else
    {
        [unit_records addObject: unit_dictionary];
    }
    
    for(NSDictionary* unit_record in unit_records)
    {
        
        //BS Weapons
        NSArray* bsWeapons = [unit_record valueForKey:@"bsw"];
        
        //CC Weapons
        NSArray* ccWeapons = [unit_record valueForKey:@"ccw"];
        
        //All Weapons
        NSArray* weaponsArray =[bsWeapons arrayByAddingObjectsFromArray: ccWeapons];
        
        //Special rules source array
        NSArray* specialRulesArray = [unit_record valueForKey:@"spec"];

        
        
        UnitAspects* unitAspects = [UnitAspects insertInManagedObjectContext:[appDelegate managedObjectContext]];

        //Statline
        //Mov
        [unitAspects setMov:[unit_record valueForKey:@"mov"]];
        
        //bs
        [unitAspects setBs:
         [NSNumber numberWithInteger:[
                                  [unit_record valueForKey:@"bs"] integerValue
                                  ]
          ]
         ];
        
        //ph
        [unitAspects setPh:
         [NSNumber numberWithInteger:[
                                  [unit_record valueForKey:@"ph"] integerValue
                                  ]
          ]
         ];
        
        //bts
        [unitAspects setBts:
         [NSNumber numberWithInteger:[
                                  [unit_record valueForKey:@"bts"] integerValue
                                  ]
          ]
         ];
        
        //WIP
        [unitAspects setWip:
         [NSNumber numberWithInteger:[
                                  [unit_record valueForKey:@"wip"] integerValue
                                  ]
          ]
         ];
        
        //cc
        [unitAspects setCc:
         [NSNumber numberWithInteger:[
                                  [unit_record valueForKey:@"cc"] integerValue
                                  ]
          ]
         ];
        
        //arm
        [unitAspects setArm:
         [NSNumber numberWithInteger:[
                                  [unit_record valueForKey:@"arm"] integerValue
                                  ]
          ]
         ];
        
        //wounds
        [unitAspects setWounds:
         [NSNumber numberWithInteger:[
                                  [unit_record valueForKey:@"w"] integerValue
                                  ]
          ]
         ];
        
        //silhouette
        [unitAspects setSilhouette:
         [NSNumber numberWithInteger:[
                                      [unit_record valueForKey:@"s"] integerValue
                                      ]
          ]
         ];
        
        //Regular
        NSString* irr = [unit_record valueForKey:@"irr"];
        [unitAspects setRegular:
         [NSNumber numberWithBool:
          ([irr  compare: @"X"] != NSOrderedSame)
          ]
         ];
        
        //Impetuous
        NSString* imp = [unit_record valueForKey:@"imp"];
        if([imp  compare: @"I"] == NSOrderedSame){
            [unitAspects setFuryValue: FURY_IMPETUOUS];
        }else if([imp  compare: @"X"] == NSOrderedSame){
            [unitAspects setFuryValue: FURY_EXTREME_IMPETUOUS];
        }else if([imp  compare: @"F"] == NSOrderedSame){
            [unitAspects setFuryValue: FURY_FRENZY];
        }else if([imp  compare: @""] == NSOrderedSame){
            [unitAspects setFuryValue: FURY_NOT_IMPETUOUS];
        }

        //Cube
        NSString* cube = [unit_record valueForKey:@"cube"];
        if([cube  compare: @"X"] == NSOrderedSame)
        {
            unitAspects.cubeValue = 1;
        }
        else if([cube  compare: @"2"] == NSOrderedSame)
        {
            unitAspects.cubeValue = 2;
        }
        
        //Hackable
        NSString* hackable = [unit_record valueForKey:@"hackable"];
        if(hackable && ([hackable  compare: @"X"] == NSOrderedSame))
        {
            unitAspects.hackableValue = TRUE;
        }
        else
        {
            unitAspects.hackableValue = FALSE;
        }
        
        // Unit Classification
        NSString* classCode = [unit_record valueForKey:@"classification"];
        if(classCode && [classCode length])
        {
            unitAspects.classification = [UnitClassification classificationWithCode:classCode];
            NSAssert(unitAspects.classification != nil, @"Unknown unit classification: %@", classCode);
        }
        
        NSMutableOrderedSet* weapons = [[NSMutableOrderedSet alloc] init];

        for(NSString* weaponName in weaponsArray)
        {
            Weapon* weapon = [self getWeapon:weaponName];
            NSAssert(weapon != nil, @"Unknown weapon %@", weaponName);
            [weapons addObject:weapon];
        }
        [unitAspects setWeapons:weapons];
        
        //Special Rules
        NSMutableOrderedSet* specialRules = [[NSMutableOrderedSet alloc] init];

        for(NSString* specialRuleName in specialRulesArray)
        {
            SpecialRule* specialRule = [self getSpecialRule: specialRuleName];
            if(specialRule != nil)
            {
                [specialRules addObject:specialRule];
                
                // Check for missing hacking devices
                if ([specialRuleName containsString:@"Hacking Device"]) {
                    NSAssert([self getHackingDevice:specialRuleName] != NULL, @"Missing hacking device %@", specialRuleName);
                }
            }
        }
        [unitAspects setSpecialRules:specialRules];
        
        UnitProfile* newProfile = [UnitProfile insertInManagedObjectContext:appDelegate.managedObjectContext];
        
        newProfile.aspects = unitAspects;
        //Save type
        [newProfile setType:[self getType:[unit_record valueForKey:@"type"]]];
        
        NSString* wtype = [unit_record valueForKey:@"wtype"];
        if(wtype != nil && [wtype compare:@"str"] == NSOrderedSame)
        {
            newProfile.strOverrideValue = TRUE;
        }
        else if(wtype != nil && [wtype compare:@"w"] == NSOrderedSame)
        {
            newProfile.wOverrideValue = TRUE;
        }
        
        NSString* optionSpecific = [unit_record valueForKey:@"optionSpecific"];
        if(optionSpecific != nil && [optionSpecific  compare: @"X"] == NSOrderedSame)
        {
            newProfile.optionSpecificValue = TRUE;
        }
        
        
        // ID
        newProfile.id = [unit_record valueForKey:@"id"];
        
        //Name
        [newProfile setName:[unit_record valueForKey:@"name"]];
        
        //ISC
        [newProfile setIsc:[unit_record valueForKey:@"isc"]];
        
        if([unit_record valueForKey:@"independent"])
        {
            NSDictionary* independentDictionary = [unit_record valueForKey:@"independent"];
            
            [newProfile setIsIndependentValue:TRUE];
            //Cost
            [newProfile setIndependentCost:
             [NSNumber numberWithInteger:[
                                      [independentDictionary valueForKey:@"cost"] integerValue
                                      ]
              ]
             ];
            
            //Swc
            [newProfile setIndependentSWC:
             [NSDecimalNumber decimalNumberWithString:[independentDictionary valueForKey:@"swc"]]
             ];
        }
        
        [foundProfiles addObject:newProfile];
    }
    return foundProfiles;
}

-(void) parseSpecOpsUnits
{
    [self setProgressMessage:@"Updating Spec-Ops"];

    NSDictionary* specops = [DataParser getFile:@"infinitydata/specops"];
    NSDictionary* specOpsIsc = [specops objectForKey:@"specopsisc"];    
    for(NSNumber* armyId in specOpsIsc)
    {
        NSArray* specOpsList = [specOpsIsc objectForKey:armyId];
        
        for(NSDictionary* specops_record in specOpsList)
        {
            int id = [[specops_record objectForKey:@"id"] intValue];
            NSArray* units = [Unit allUnitsWithID:id];
            NSAssert(units != nil, @"Unable to find Spec-Ops unit %d", id);

            UnitType* type = [self getType:[specops_record valueForKey:@"type"]];
            NSAssert(type != nil, @"Unable to look up Spec-Ops type");

            for(Unit* unit in units)
            {
                if(unit.specops == nil)
                {
                    unit.specops = [SpecOps insertInManagedObjectContext:appDelegate.managedObjectContext];
                }

                unit.specops.type = type;

                NSArray* baseModels = [specops_record objectForKey:@"basemodels"];
                
                for(NSDictionary* models in baseModels)
                {
                    int modelId = [[models objectForKey:@"unit_id"] intValue];
                    int optionId = [[models objectForKey:@"child_id"] intValue];
                    Unit* baseUnit = nil;
                    
                    // Get sectorial version of the unit for linkability annotations
                    if(unit.sectorial) {
                        baseUnit = [unit.sectorial unitWithID:modelId];
                        if (!baseUnit) {
                            baseUnit = [unit.sectorial.army unitWithID:modelId];
                        }
                    } else if(unit.army) {
                        baseUnit = [unit.army unitWithID:modelId];
                    } else {
                        baseUnit = [Unit masterUnitWithID:modelId withDelegate:appDelegate];
                    }
                    NSAssert(baseUnit != nil, @"Base unit %d not found!", modelId);

                    UnitOption* unitOption = [baseUnit optionWithID:optionId];
                    NSAssert(unitOption != nil, @"Base unit Option %d not found for unit: %d", optionId, modelId);

                    // See if the spec-ops already references this unit option
                    SpecOpsBaseUnitOption* specOpsBaseUnitOption = nil;
                    for(SpecOpsBaseUnitOption* specBase in unit.specops.baseUnitOptionsSet)
                    {
                        if(specBase.unitOption == unitOption)
                        {
                            specOpsBaseUnitOption = specBase;
                            break;
                        }
                    }
                    
                    // Add if it does not exist
                    if(!specOpsBaseUnitOption)
                    {
                        specOpsBaseUnitOption = [SpecOpsBaseUnitOption insertInManagedObjectContext:appDelegate.managedObjectContext];
                        specOpsBaseUnitOption.unitOption = unitOption;
                        specOpsBaseUnitOption.specops = unit.specops;
                    }
                }
            }
        }
    }
    
    NSDictionary* gear = [specops objectForKey:@"gear"];
    for(NSNumber* armyID in gear)
    {
        Army* army = [Army armyWithID:[armyID intValue] withDelegate:appDelegate];

        NSDictionary* weapons = gear[armyID][@"weapons"];
        for(NSString* weaponName in weapons)
        {
            Weapon* weapon = [self getWeapon: weaponName];
            NSAssert(weapon, @"Unable to find weapon %@", weaponName);
            SpecOpsWeapon* specOpsWeapon = [SpecOpsWeapon insertInManagedObjectContext:appDelegate.managedObjectContext];
            specOpsWeapon.weapon = weapon;
            specOpsWeapon.cost = weapons[weaponName];
            
            [army.specopWeaponsSet addObject:specOpsWeapon];
        }

        NSDictionary* skills = gear[armyID][@"skills"];
        for(NSString* specialRuleName in skills)
        {
            SpecialRule* specialRule = [self getSpecialRule:specialRuleName];
            SpecOpsSpecialRule* specOpsRule = [SpecOpsSpecialRule insertInManagedObjectContext:appDelegate.managedObjectContext];
            specOpsRule.specialRule = specialRule;
            specOpsRule.cost = skills[specialRuleName];
            
            [army.specopSkillsSet addObject:specOpsRule];
        }

        NSDictionary* equipment = gear[armyID][@"equipment"];
        for(NSString* specialRuleName in equipment)
        {
            SpecialRule* specialRule = [self getSpecialRule:specialRuleName];
            SpecOpsEquipment* specOpsEquip = [SpecOpsEquipment insertInManagedObjectContext:appDelegate.managedObjectContext];
            specOpsEquip.specialRule = specialRule;
            specOpsEquip.cost = equipment[specialRuleName];
            
            // Check for missing hacking devices
            if ([specialRuleName containsString:@"Hacking Device"]) {
                NSAssert([self getHackingDevice:specialRuleName] != NULL, @"Missing hacking device %@", specialRuleName);
            }

            [army.specopEquipmentSet addObject:specOpsEquip];
        }
    }
}

-(UnitOption*)parseOption:(NSDictionary*) option_record unit:(Unit*)unit withIndex:(int)optionIndex
{
    // Update existing or create new, based on id
    UnitOption* option = [unit optionWithID:[[option_record valueForKey:@"id"] intValue]];
    if(!option)
    {
        option = [UnitOption insertInManagedObjectContext:[appDelegate managedObjectContext]];
        option.id = [option_record valueForKey:@"id"];
        [unit addOptionsObject:option];
    }
    // Check indexes match
    NSAssert(unit.options[optionIndex] == option, @"Unit %d (%@) has mismatched option %d (%@)", unit.idValue, unit.isc, option.idValue, option.code);
    
    //Code
    option.code = [option_record valueForKey:@"name"];
    
    // Legacy Code (used for importing old rosters)
    option.legacyCode = option_record[@"legacy_code"];
    
    //Cost
    option.cost = [NSNumber numberWithInteger:[[option_record valueForKey:@"cost"] integerValue]];
    //Swc
    option.swc = [NSDecimalNumber decimalNumberWithString:[option_record valueForKey:@"swc"]];
    
    NSMutableOrderedSet* weapons = [[NSMutableOrderedSet alloc] init];
    
    //Special Rules
    NSMutableOrderedSet* specialRules = [[NSMutableOrderedSet alloc] init];
    NSArray* specialRulesArray = [option_record valueForKey:@"spec"];
    
    for(NSString* specialRuleName in specialRulesArray)
    {
        SpecialRule* specialRule = [self getSpecialRule: specialRuleName];
        if(specialRule != nil)
        {
            [specialRules addObject:specialRule];
            
            // Check for missing hacking devices
            if ([specialRuleName containsString:@"Hacking Device"]) {
                NSAssert([self getHackingDevice:specialRuleName] != NULL, @"Missing hacking device %@", specialRuleName);
            }
        }
    }
    [option setSpecialRules:specialRules];
    
    //BS Weapons
    NSArray* bsWeapons = [option_record valueForKey:@"bsw"];
    //CC Weapons
    NSArray* ccWeapons = [option_record valueForKey:@"ccw"];
    
    NSArray* weaponsArray = [bsWeapons arrayByAddingObjectsFromArray: ccWeapons];
    
    for(NSString* weaponName in weaponsArray)
    {
        Weapon* weapon = [self getWeapon:weaponName];
        NSAssert(weapon != nil, @"Unknown weapon %@", weaponName);
        [weapons addObject:weapon];
    }
    [option setWeapons:weapons];

    // Option Specific Profiles
    // Clear and start over
    [option.enabledUnitProfileSet removeAllObjects];

    // Single profile
    if([option_record valueForKey:@"profile"])
    {
        int profileIndex = [[option_record valueForKey:@"profile"] intValue];
        UnitProfile* unitProfile = nil;
        for (UnitProfile* searchProfile in unit.profiles) {
            if (searchProfile.idValue == profileIndex) {
                unitProfile = searchProfile;
                break;
            }
        }
        NSAssert(unitProfile, @"Unknown profile id %d specified", profileIndex);
        NSAssert(unitProfile.optionSpecificValue, @"Non-option-specific profile %d specified", profileIndex);
        [option addEnabledUnitProfileObject:unitProfile];
    }

    // Array of profiles
    if([option_record valueForKey:@"profiles"])
    {
        NSArray* profiles = [option_record valueForKey:@"profiles"];
        for(NSString* profile in profiles){
            int profileIndex = [profile intValue];
            NSAssert(profileIndex-1 < [unit.profiles count], @"Illegal profile id %d specified", profileIndex);
            UnitProfile* unitProfile = [unit.profiles objectAtIndex:profileIndex-1];

            NSAssert(unitProfile.optionSpecificValue, @"Non-option-specific profile %d specified", profileIndex);
            [option addEnabledUnitProfileObject:unitProfile];
        }
    }
    
    if([option_record valueForKey:@"independent"])
    {
        NSDictionary* independentDictionary = [option_record valueForKey:@"independent"];
        
        [option setIsIndependentValue:TRUE];
        //Cost
        [option setIndependentCost:
         [NSNumber numberWithInteger:[
                                      [independentDictionary valueForKey:@"cost"] integerValue
                                      ]
          ]
         ];
        
        //Swc
        [option setIndependentSWC:
         [NSDecimalNumber decimalNumberWithString:[independentDictionary valueForKey:@"swc"]]
         ];
    }
    return option;
}

-(void)copyMercs
{
    NSArray* armies = [Army allWithDelegate:appDelegate];
    Army* merc = [Army armyWithID:eArmyMercs withDelegate:appDelegate];
    NSAssert(merc != NULL, @"Cannot find Mercenary army");
    
    for(Army* army in armies)
    {
        if(army == merc)
        {
            continue;
        }
        
        for(Unit* mercUnit in merc.units)
        {
            Unit* armyUnit = [army unitWithID:mercUnit.idValue];
            
            if(armyUnit)
            {
                if(armyUnit.isMercValue)
                {
                    // If this unit is a merc-only unit, update it.
                    [armyUnit copyFrom:mercUnit];

                    // Prevent units with duplicate names.
                    armyUnit.isc = [mercUnit.isc stringByAppendingString:@" (Mercenary)"];
                    armyUnit.name = [mercUnit.name stringByAppendingString:@" (Mercenary)"];
                }
                else
                {
                    // Just update the extra mercenary AVA; all the rest was done in the main army pass.
                    armyUnit.mercAvaValue = mercUnit.avaValue;
                }
            }
            else
            {
                // Unit is not yet present; clone it.
                armyUnit = [mercUnit clone];
                armyUnit.isMercValue = YES;
                armyUnit.army = army;

                // Prevent units with duplicate names.
                armyUnit.isc = [mercUnit.isc stringByAppendingString:@" (Mercenary)"];
                armyUnit.name = [mercUnit.name stringByAppendingString:@" (Mercenary)"];
            }
        }
    }
    
    for(Sectorial* sectorial in [Sectorial allWithDelegate:appDelegate])
    {
        for(Unit* mercUnit in merc.units)
        {
            Unit* sectorialUnit = [sectorial unitWithID:mercUnit.idValue];
            
            if(sectorialUnit)
            {
                if(sectorialUnit.isMercValue)
                {
                    // If this unit is a merc-only unit, update it.
                    [sectorialUnit copyFrom:mercUnit];
                    
                    // Prevent units with duplicate names.
                    sectorialUnit.isc = [mercUnit.isc stringByAppendingString:@" (Mercenary)"];
                    sectorialUnit.name = [mercUnit.name stringByAppendingString:@" (Mercenary)"];
                }
                else
                {
                    // Just update the extra mercenary AVA; all the rest was done in the main army pass.
                    sectorialUnit.mercAvaValue = mercUnit.avaValue;
                }
            }
            else
            {
                // Unit is not yet present; clone it.
                sectorialUnit = [mercUnit clone];
                sectorialUnit.isMercValue = YES;
                sectorialUnit.sectorial = sectorial;
                
                // Prevent units with duplicate names.
                sectorialUnit.isc = [mercUnit.isc stringByAppendingString:@" (Mercenary)"];
                sectorialUnit.name = [mercUnit.name stringByAppendingString:@" (Mercenary)"];
            }
        }
    }
}

- (void)parseWeapons: (NSDictionary*) weapons
{
    for(NSDictionary* weapon_record in weapons)
    {
        // Update if exists, otherwise create new
        Weapon* weapon = [self getWeapon:[weapon_record valueForKey:@"name"]];
        if(!weapon){
            weapon = [Weapon insertInManagedObjectContext:[appDelegate managedObjectContext]];
        }
        
        //Ammo type
        NSString* ammo = [weapon_record valueForKey:@"ammo"];
        [weapon setAmmo:[ammo uppercaseString]];
        
        //Burst count
        NSString* burst = [weapon_record valueForKey:@"burst"];
        [weapon setBurst:burst];
        
        //CC
        BOOL cc = [[weapon_record valueForKey:@"cc"] boolValue];
        [weapon setCloseCombat:[NSNumber numberWithBool:cc]];
        
        
        //Uses
        if([weapon_record valueForKey:@"uses"])
        {
            [weapon setUses:[NSNumber numberWithInteger:[[weapon_record valueForKey:@"uses"] integerValue]]];
        }
    
        //Damage
        NSString* damageString = [weapon_record valueForKey:@"damage"];
        if([damageString compare:@"PH"] == NSOrderedSame)
        {
            weapon.damage = [NSNumber numberWithInt:WEAPON_DAM_PH];
        }
        else if([damageString compare:@"PH-1"] == NSOrderedSame)
        {
            weapon.damage = [NSNumber numberWithInt:WEAPON_DAM_PH_1];
        }
        else if([damageString compare:@"PH-2"] == NSOrderedSame)
        {
            weapon.damage = [NSNumber numberWithInt:WEAPON_DAM_PH_2];
        }
        else if([damageString compare:@"WIP"] == NSOrderedSame)
        {
            weapon.damage = [NSNumber numberWithInt:WEAPON_DAM_WIP];
        }
        else if([damageString compare:@"*"] == NSOrderedSame)
        {
            weapon.damage = [NSNumber numberWithInt:WEAPON_DAM_STAR];
        }
        else if([damageString compare:@"marking"] == NSOrderedSame)
        {
            weapon.damage = [NSNumber numberWithInt:WEAPON_DAM_MARK];
        }
        else if([damageString compare:@"--"] == NSOrderedSame)
        {
            weapon.damage = nil;
        }
        else
        {
            weapon.damage = [NSNumber numberWithInteger:[damageString integerValue]];
        }
        
        //Name
        [weapon setName:[weapon_record valueForKey:@"name"]];
        
        // Name Suffix for multi-mode weapons
        [weapon setMode:[weapon_record valueForKey:@"mode"]];
        
        //Note
        [weapon setNote:[weapon_record valueForKey:@"note"]];
        
        //Template
        [weapon setTemplateType:[weapon_record valueForKey:@"template"]];
        
        NSString* string;
        NSNumber* number;
        
        //Short Distance
        string = [weapon_record valueForKey:@"short_dist"];
        number = [DataParser parseDashDash: string];
        [weapon setShortDistance:number];

        //Short Modifier
        string = [weapon_record valueForKey:@"short_mod"];
        number = [DataParser parseDashDash: string];
        [weapon setShortModifier:number];
        
        //Medium Distance
        string = [weapon_record valueForKey:@"medium_dist"];
        number = [DataParser parseDashDash: string];
        [weapon setMediumDistance:number];
        
        //Medium Modifier
        string = [weapon_record valueForKey:@"medium_mod"];
        number = [DataParser parseDashDash: string];
        [weapon setMediumModifier:number];
        
        //Long Distance
        string = [weapon_record valueForKey:@"long_dist"];
        number = [DataParser parseDashDash: string];
        [weapon setLongDistance:number];
        
        //Long Modifier
        string = [weapon_record valueForKey:@"long_mod"];
        number = [DataParser parseDashDash: string];
        [weapon setLongModifier:number];
        
        //Max Distance
        string = [weapon_record valueForKey:@"max_dist"];
        number = [DataParser parseDashDash: string];
        [weapon setMaxDistance:number];
        
        //Max Modifier
        string = [weapon_record valueForKey:@"max_mod"];
        number = [DataParser parseDashDash: string];
        [weapon setMaxModifier:number];
        
        // Alternate profile stats
        weapon.altProfile = [weapon_record valueForKey:@"alt_profile"];
        
        // Suppressive Fire
        if([@"Yes" compare: [weapon_record valueForKey:@"suppressive"]] == NSOrderedSame)
        {
            weapon.suppressive = [NSNumber numberWithInteger: 1];
        }

        // Attribute
        weapon.attribute = [weapon_record valueForKey:@"attr"];

        [weaponsDictionary setValue:weapon forKey:[weapon name]];
    }
}

-(void)populateNotation
{
    SpecialRule* camouflageSpecialRule = [self getSpecialRule:@"CH: Camouflage"];
    if(!camouflageSpecialRule.notation)
    {
        CamoNotation* notation = [CamoNotation insertInManagedObjectContext:appDelegate.managedObjectContext];
        notation.specialRule = camouflageSpecialRule;
    }
    
    SpecialRule* toCamouflageSpecialRule = [self getSpecialRule:@"CH: TO Camouflage"];
    if(!toCamouflageSpecialRule.notation)
    {
        TOCamoNotation* notation = [TOCamoNotation insertInManagedObjectContext:appDelegate.managedObjectContext];
        notation.specialRule = toCamouflageSpecialRule;
    }
    
    SpecialRule* limitiedCamouflageSpecialRule = [self getSpecialRule:@"CH: Limited Camouflage"];
    if(!limitiedCamouflageSpecialRule.notation)
    {
        TOCamoNotation* notation = [CamoNotation insertInManagedObjectContext:appDelegate.managedObjectContext];
        notation.specialRule = limitiedCamouflageSpecialRule;
    }
    
    SpecialRule* ambushCamouflageSpecialRule = [self getSpecialRule:@"CH: Ambush Camouflage"];
    if(!ambushCamouflageSpecialRule.notation)
    {
        TOCamoNotation* notation = [CamoNotation insertInManagedObjectContext:appDelegate.managedObjectContext];
        notation.specialRule = ambushCamouflageSpecialRule;
    }
    
    SpecialRule* booty = [self getSpecialRule:@"Booty L1"];
    if(!booty.notation)
    {
        BootyNotation* notation = [BootyNotation insertInManagedObjectContext:appDelegate.managedObjectContext];
        notation.levelTwoValue = FALSE;
        notation.specialRule = booty;
    }
    
    SpecialRule* bootyLevelTwo = [self getSpecialRule:@"Booty L2"];
    if(!bootyLevelTwo.notation)
    {
        BootyNotation* notation = [BootyNotation insertInManagedObjectContext:appDelegate.managedObjectContext];
        notation.levelTwoValue = TRUE;
        notation.specialRule = bootyLevelTwo;
    }
    
    SpecialRule* metachemistry = [self getSpecialRule:@"MetaChemistry L1"];
    if(!metachemistry.notation)
    {
        MetachemistryNotation* notation = [MetachemistryNotation insertInManagedObjectContext:appDelegate.managedObjectContext];
        notation.specialRule = metachemistry;
    }

    SpecialRule* metachemistryLevelTwo = [self getSpecialRule:@"MetaChemistry L2"];
    if(!metachemistryLevelTwo.notation)
    {
        MetachemistryNotation* notation = [MetachemistryNotation insertInManagedObjectContext:appDelegate.managedObjectContext];
        notation.levelTwoValue = TRUE;
        notation.specialRule = metachemistryLevelTwo;
    }
}

-(void)populateBooty
{
    Booty* booty;

    //Booty Lvl1
    if((booty = [Booty getBootyWithRoll:1 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 1;
    booty.highRollRangeValue = 3;
    booty.levelTwoValue = FALSE;
    booty.specialRule = [self getSpecialRule:@"Light Protection (+1 ARM)"];
    
    if((booty = [Booty getBootyWithRoll:4 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 4;
    booty.highRollRangeValue = 4;
    booty.levelTwoValue = FALSE;
    booty.weapon = [self getWeapon:@"EXP CCW"];
    
    if((booty = [Booty getBootyWithRoll:5 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 5;
    booty.highRollRangeValue = 5;
    booty.levelTwoValue = FALSE;
    booty.weapon = [self getWeapon:@"Light Shotgun"];
    
    if((booty = [Booty getBootyWithRoll:6 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 6;
    booty.highRollRangeValue = 6;
    booty.levelTwoValue = FALSE;
    booty.specialRule = [self getSpecialRule:@"Heavy Protection (+4 ARM)"];
    
    if((booty = [Booty getBootyWithRoll:7 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 7;
    booty.highRollRangeValue = 7;
    booty.levelTwoValue = FALSE;
    booty.weapon = [self getWeapon:@"Grenades"];
    
    if((booty = [Booty getBootyWithRoll:8 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 8;
    booty.highRollRangeValue = 8;
    booty.levelTwoValue = FALSE;
    booty.weapon = [self getWeapon:@"Adhesive Launcher"];
    
    if((booty = [Booty getBootyWithRoll:9 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 9;
    booty.highRollRangeValue = 9;
    booty.levelTwoValue = FALSE;
    booty.weapon = [self getWeapon:@"Light Grenade Launcher"];
    
    if((booty = [Booty getBootyWithRoll:10 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 10;
    booty.highRollRangeValue = 10;
    booty.levelTwoValue = FALSE;
    booty.weapon = [self getWeapon:@"Light Flamethrower"];
    
    if((booty = [Booty getBootyWithRoll:11 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 11;
    booty.highRollRangeValue = 11;
    booty.levelTwoValue = FALSE;
    booty.weapon = [self getWeapon:@"Panzerfaust"];
    
    if((booty = [Booty getBootyWithRoll:12 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 12;
    booty.highRollRangeValue = 12;
    booty.levelTwoValue = FALSE;
    booty.weapon = [self getWeapon:@"E/M Grenades"];
    
    if((booty = [Booty getBootyWithRoll:13 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 13;
    booty.highRollRangeValue = 13;
    booty.levelTwoValue = FALSE;
    booty.weapon = [self getWeapon:@"E/M CCW"];
    
    if((booty = [Booty getBootyWithRoll:14 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 14;
    booty.highRollRangeValue = 15;
    booty.levelTwoValue = FALSE;
    booty.specialRule = [self getSpecialRule:@"Light Protection (+2 ARM)"];
    
    if((booty = [Booty getBootyWithRoll:16 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 16;
    booty.highRollRangeValue = 16;
    booty.levelTwoValue = FALSE;
    booty.specialRule = [self getSpecialRule:@"X Visor"];
    
    if((booty = [Booty getBootyWithRoll:17 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 17;
    booty.highRollRangeValue = 17;
    booty.levelTwoValue = FALSE;
    booty.weapon = [self getWeapon:@"Monofilament CCW"];
    
    if((booty = [Booty getBootyWithRoll:18 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 18;
    booty.highRollRangeValue = 18;
    booty.levelTwoValue = FALSE;
    booty.weapon = [self getWeapon:@"Combi Rifle"];
    
    if((booty = [Booty getBootyWithRoll:19 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 19;
    booty.highRollRangeValue = 19;
    booty.levelTwoValue = FALSE;
    booty.weapon = [self getWeapon:@"AP Rifle"];
    
    if((booty = [Booty getBootyWithRoll:20 levelTwo:FALSE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 20;
    booty.highRollRangeValue = 20;
    booty.levelTwoValue = FALSE;
    booty.specialRule = [self getSpecialRule:@"AutoMediKit"];
    
    
    //Booty Lvl2
    if((booty = [Booty getBootyWithRoll:1 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 1;
    booty.highRollRangeValue = 2;
    booty.levelTwoValue = TRUE;
    booty.specialRule = [self getSpecialRule:@"Light Protection (+1 ARM)"];
    
    if((booty = [Booty getBootyWithRoll:3 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 3;
    booty.highRollRangeValue = 3;
    booty.levelTwoValue = TRUE;
    booty.weapon = [self getWeapon:@"Chain Rifle"];
    
    if((booty = [Booty getBootyWithRoll:4 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 4;
    booty.highRollRangeValue = 4;
    booty.levelTwoValue = TRUE;
    booty.specialRule = [self getSpecialRule:@"AutoMediKit"];
    
    if((booty = [Booty getBootyWithRoll:5 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 5;
    booty.highRollRangeValue = 5;
    booty.levelTwoValue = TRUE;
    booty.weapon = [self getWeapon:@"Nanopulser"];
    
    if((booty = [Booty getBootyWithRoll:6 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 6;
    booty.highRollRangeValue = 6;
    booty.levelTwoValue = TRUE;
    booty.weapon = [self getWeapon:@"Panzerfaust"];
    
    if((booty = [Booty getBootyWithRoll:7 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 7;
    booty.highRollRangeValue = 7;
    booty.levelTwoValue = TRUE;
    booty.weapon = [self getWeapon:@"MULTI Sniper Rifle"];
    
    if((booty = [Booty getBootyWithRoll:8 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 8;
    booty.highRollRangeValue = 8;
    booty.levelTwoValue = TRUE;
    booty.weapon = [self getWeapon:@"Smoke Grenades"];
    
    if((booty = [Booty getBootyWithRoll:9 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 9;
    booty.highRollRangeValue = 9;
    booty.levelTwoValue = TRUE;
    booty.specialRule = [self getSpecialRule:@"CH: Mimetism"];
    
    if((booty = [Booty getBootyWithRoll:10 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 10;
    booty.highRollRangeValue = 10;
    booty.levelTwoValue = TRUE;
    booty.specialRule = [self getSpecialRule:@"Light Protection (+2 ARM)"];
    
    if((booty = [Booty getBootyWithRoll:11 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 11;
    booty.highRollRangeValue = 11;
    booty.levelTwoValue = TRUE;
    booty.weapon = [self getWeapon:@"MULTI Rifle"];
    
    if((booty = [Booty getBootyWithRoll:12 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 12;
    booty.highRollRangeValue = 12;
    booty.levelTwoValue = TRUE;
    booty.specialRule = [self getSpecialRule:@"Multispectral Visor L1"];
    
    if((booty = [Booty getBootyWithRoll:13 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 13;
    booty.highRollRangeValue = 13;
    booty.levelTwoValue = TRUE;
    booty.weapon = [self getWeapon:@"Breaker Rifle"];
    
    if((booty = [Booty getBootyWithRoll:14 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 14;
    booty.highRollRangeValue = 15;
    booty.levelTwoValue = TRUE;
    booty.weapon = [self getWeapon:@"Adhesive Launcher"];
    
    if((booty = [Booty getBootyWithRoll:16 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 16;
    booty.highRollRangeValue = 16;
    booty.levelTwoValue = TRUE;
    booty.specialRule = [self getSpecialRule:@"Medium Protection (+3 ARM)"];
    
    if((booty = [Booty getBootyWithRoll:17 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 17;
    booty.highRollRangeValue = 17;
    booty.levelTwoValue = TRUE;
    booty.weapon = [self getWeapon:@"Flash Pulse"];
    
    if((booty = [Booty getBootyWithRoll:18 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 18;
    booty.highRollRangeValue = 18;
    booty.levelTwoValue = TRUE;
    booty.specialRule = [self getSpecialRule:@"Motorcycle (MOV 8-6)"];
    
    if((booty = [Booty getBootyWithRoll:19 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 19;
    booty.highRollRangeValue = 19;
    booty.levelTwoValue = TRUE;
    booty.specialRule = [self getSpecialRule:@"ODD: Optical Disruptor"];
    
    if((booty = [Booty getBootyWithRoll:20 levelTwo:TRUE]) == nil)
    {
        booty = [Booty insertInManagedObjectContext:appDelegate.managedObjectContext];
    }
    booty.lowRollRangeValue = 20;
    booty.highRollRangeValue = 20;
    booty.levelTwoValue = TRUE;
    booty.weapon = [self getWeapon:@"HMG"];
    
    [appDelegate saveContext];
}

-(void)populateMetachemistry
{
    Metachemistry* metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 1;
    metachemistry.highRollRangeValue = 3;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Natural Armor (+1 ARM)"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 4;
    metachemistry.highRollRangeValue = 5;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"V: Dogged"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 6;
    metachemistry.highRollRangeValue = 6;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Bioimmunity"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 7;
    metachemistry.highRollRangeValue = 8;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Enhanced Mobility (MOV: 8-4)"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 9;
    metachemistry.highRollRangeValue = 9;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Reinforced Biotech (+6 BTS)"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 10;
    metachemistry.highRollRangeValue = 11;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Enhanced Physique (+3 PH)"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 12;
    metachemistry.highRollRangeValue = 13;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"V: No Wound Incapacitation"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 14;
    metachemistry.highRollRangeValue = 14;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Sixth Sense L2"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 15;
    metachemistry.highRollRangeValue = 16;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Regeneration"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 17;
    metachemistry.highRollRangeValue = 18;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Super-Jump"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 19;
    metachemistry.highRollRangeValue = 19;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Climbing Plus"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 20;
    metachemistry.highRollRangeValue = 20;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Total Immunity"]];
    
    [appDelegate saveContext];
}

-(void)populateMetachemistryLevelTwo
{
    Metachemistry* metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 1;
    metachemistry.highRollRangeValue = 4;
    metachemistry.levelTwoValue = TRUE;
    [metachemistry.specialRuleSet addObject: [self getSpecialRule:@"Natural Armor (+1 ARM)"]];
    [metachemistry.specialRuleSet addObject: [self getSpecialRule:@"Bioimmunity"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 5;
    metachemistry.highRollRangeValue = 8;
    metachemistry.levelTwoValue = TRUE;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"V: Dogged"]];
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Total Immunity"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 9;
    metachemistry.highRollRangeValue = 12;
    metachemistry.levelTwoValue = TRUE;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Superior Mobility (MOV: 6-4)"]];
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Super-Jump"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 13;
    metachemistry.highRollRangeValue = 16;
    metachemistry.levelTwoValue = TRUE;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Superior Mobility (MOV: 6-4)"]];
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Climbing Plus"]];
    
    metachemistry = [Metachemistry insertInManagedObjectContext:appDelegate.managedObjectContext];
    metachemistry.lowRollRangeValue = 17;
    metachemistry.highRollRangeValue = 20;
    metachemistry.levelTwoValue = TRUE;
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Super-Physique (+3 PH)"]];
    [metachemistry.specialRuleSet addObject:[self getSpecialRule:@"Regeneration"]];
    
    [appDelegate saveContext];
}

-(UnitType*) getType:(NSString*)typeName
{
    UnitType* type = [typesDictionary valueForKey:typeName];
    if(type)
    {
        return type;
    }
    type = [UnitType unitTypeWithCode:typeName];
    if(type)
    {
        return type;
    }
    
    return nil;
}

-(SpecialRule*) getSpecialRule:(NSString*)specialRuleName
{
    SpecialRule* specialRule = [specialRulesDictionary valueForKey:specialRuleName];
    if(specialRule)
    {
        return specialRule;
    }
    
    specialRule = [SpecialRule specialRuleWithName:specialRuleName andDelegate:appDelegate];
    if(specialRule)
    {
        return specialRule;
    }
    
    specialRule = [SpecialRule insertInManagedObjectContext:[appDelegate managedObjectContext]];
    [specialRule setName:specialRuleName];
    [specialRulesDictionary setObject:specialRule forKey:specialRuleName];

    return specialRule;
}

-(Weapon*) getWeapon:(NSString*) weaponName
{
    Weapon* weapon = [weaponsDictionary valueForKey:weaponName];
    if(weapon)
    {
        return weapon;
    }
    
    weapon = [Weapon weaponWithName:weaponName andDelegate:appDelegate];
    if(weapon)
    {
        [weaponsDictionary setValue:weapon forKey:[weapon name]];
        return weapon;
    }
    
    //Check for (2) quantity suffix
    NSString* quantity = [weaponName substringFromIndex:[weaponName length]-3];
    if([quantity compare:@"(2)"] == NSOrderedSame)
    {
        return [self createDualWeapon:weaponName];
    }
    NSLog(@"Could not find weapon record for %@",weaponName);
    return nil;
}

-(Weapon*) createDualWeapon:(NSString*) weaponName
{
    Weapon* baseWeapon = [Weapon weaponWithName:[weaponName substringToIndex:[weaponName length]-4]];
    // Modify existing, or create new
    Weapon* newWeapon = [Weapon weaponWithName:weaponName];
    if(!newWeapon)
    {
        newWeapon = [baseWeapon createCopyWithDelegate:appDelegate];
    }
    else
    {
        [newWeapon copyFromWeapon:baseWeapon];
    }
    
    // New name
    newWeapon.name = weaponName;
    
    //Burst count
    newWeapon.burst = [NSString stringWithFormat:@"%d", [baseWeapon.burst intValue] + 1 ];
    
    // Double the use count for a disposable weapon
    newWeapon.usesValue *= 2;
    
    // add to cache
    [weaponsDictionary setObject:newWeapon forKey:weaponName];
    
    // Alternate firing modes
    Weapon *altWeapon = newWeapon;
    while(altWeapon.altProfile){
        // Get the base weapon
        NSString *baseName = baseWeapon.altProfile;
        baseWeapon = [Weapon weaponWithName:baseName];
        
        // Insert the quantity string in between the base name and the mode name
        // "Missile Launcher (Antitank Mode)" -> "Missile Launcher (2) (Antitank Mode)"
        NSRange modeRange = [baseName rangeOfString:@"("];
        NSString *altName = [NSString stringWithFormat:@"%@ %@", weaponName, [baseName substringFromIndex: modeRange.location]];
        
        // Make the previous weapon point to the one we are about to create
        altWeapon.altProfile = altName;
        
        // Then clone the base version of the new alt profile
        altWeapon = [Weapon weaponWithName:altName];
        if(!altWeapon)
        {
            altWeapon = [baseWeapon createCopyWithDelegate:appDelegate];
        }
        else
        {
            [altWeapon copyFromWeapon:baseWeapon];
        }

        altWeapon.name = altName;
        
        // Burst increase does not apply to CC weapons.
        if(!altWeapon.closeCombatValue)
        {
            altWeapon.burst = [NSString stringWithFormat:@"%d", [altWeapon.burst intValue] + 1 ];
        }
        
        altWeapon.usesValue *= 2;
        
        [weaponsDictionary setObject:altWeapon forKey:altName];
    }
    
    return newWeapon;
}

-(HackingProgram*) getHackingProgram:(NSString*) programName
{
    HackingProgram* program = [hackingProgramDictionary valueForKey:programName];
    if(program)
    {
        return program;
    }
    
    program = [HackingProgram programWithName:programName];
    if(program)
    {
        [hackingProgramDictionary setValue:program forKey:programName];
        return program;
    }
    
    return nil;
}

-(HackingProgramGroup*) getHackingProgramGroup:(NSString*) groupName
{
    HackingProgramGroup* group = [hackingProgramGroupDictionary valueForKey:groupName];
    if(group)
    {
        return group;
    }
    
    group = [HackingProgramGroup groupWithName:groupName];
    if(group)
    {
        [hackingProgramGroupDictionary setValue:group forKey:groupName];
        return group;
    }
    
    return nil;
}

-(HackingDevice*) getHackingDevice:(NSString*) deviceName
{
    HackingDevice* device = [hackingDeviceDictionary valueForKey:deviceName];
    if(device)
    {
        return device;
    }
    
    device = [HackingDevice deviceWithName:deviceName];
    if(device)
    {
        [hackingDeviceDictionary setValue:device forKey:deviceName];
        return device;
    }
    
    return nil;
}

-(void) parseHackingData
{
    NSDictionary *hacking = [DataParser getFile:@"infinitydata/hacking"];
    
    // Hacking Programs
    for(NSDictionary* program_record in [hacking valueForKey:@"Hacking Programs"])
    {
        // Update if exists, otherwise create new
        HackingProgram* program = [self getHackingProgram:[program_record valueForKey:@"name"]];
        if(!program){
            program = [HackingProgram insertInManagedObjectContext:[appDelegate managedObjectContext]];
        }

        program.name = [program_record valueForKey:@"name"];
        program.range = [program_record valueForKey:@"range"];
        program.attMod = [program_record valueForKey:@"att mod"];
        program.oppMod = [program_record valueForKey:@"opp mod"];
        program.damage = [program_record valueForKey:@"damage"];
        program.burst = [program_record valueForKey:@"burst"];
        program.ammo = [program_record valueForKey:@"ammo"];
        program.target = [program_record valueForKey:@"target"];
        program.effect = [program_record valueForKey:@"effect"];
        program.skill = [program_record valueForKey:@"skill"];
        program.special = [program_record valueForKey:@"special"];
        program.hide_mods = [program_record valueForKey:@"hide_mods"];
        
        [hackingProgramDictionary setValue:program forKey:program.name];
    }
    
    // Hacking Program Groups
    for(NSDictionary* group_record in [hacking valueForKey:@"Hacking Program Groups"])
    {
        // Update if exists, otherwise create new
        HackingProgramGroup* group = [self getHackingProgramGroup:[group_record valueForKey:@"name"]];
        if(!group){
            group = [HackingProgramGroup insertInManagedObjectContext:[appDelegate managedObjectContext]];
        }
        group.name = [group_record valueForKey:@"name"];
        [hackingProgramGroupDictionary setValue:group forKey:group.name];

        // clear all programs and re-add
        [group.programsSet removeAllObjects];
        for(NSString* programName in [group_record valueForKey:@"programs"]){
            [group.programsSet addObject: [self getHackingProgram:programName]];
        }
    }
    
    // Hacking Devices
    for(NSDictionary* device_record in [hacking valueForKey:@"Hacking Devices"])
    {
        // Update if exists, otherwise create new
        HackingDevice* device = [self getHackingDevice:[device_record valueForKey:@"name"]];
        if(!device){
            device = [HackingDevice insertInManagedObjectContext:[appDelegate managedObjectContext]];
        }
        device.name = [device_record valueForKey:@"name"];
        [hackingDeviceDictionary setValue:device forKey:device.name];
        
        // clear all groups and re-add
        [device.groupsSet removeAllObjects];
        for(NSString* groupName in [device_record valueForKey:@"groups"]){
            [device.groupsSet addObject: [self getHackingProgramGroup:groupName]];
        }

        // clear all upgrades and re-add
        [device.upgradesSet removeAllObjects];
        for(NSString* programName in [device_record valueForKey:@"upgrades"]){
            [device.upgradesSet addObject: [self getHackingProgram:programName]];
        }
    }
}

-(PherowareTactic*) getPherowareTactic:(NSString*) tacticName
{
    PherowareTactic* tactic = [pherowareTacticDictionary valueForKey:tacticName];
    if(tactic)
    {
        return tactic;
    }
    
    tactic = [PherowareTactic tacticWithName:tacticName];
    if(tactic)
    {
        [pherowareTacticDictionary setValue:tactic forKey:tacticName];
        return tactic;
    }
    
    return nil;
}

-(void) parsePherowareData
{
    NSDictionary *pheroware = [DataParser getFile:@"infinitydata/pheroware"];
    
    // Pheroware Tactics
    for(NSDictionary* tactic_record in pheroware)
    {
        // Update if exists, otherwise create new
        PherowareTactic* tactic = [self getPherowareTactic:[tactic_record valueForKey:@"name"]];
        if(!tactic){
            tactic = [PherowareTactic insertInManagedObjectContext:[appDelegate managedObjectContext]];
        }
        
        tactic.name = tactic_record[@"name"];
        tactic.tacticType = tactic_record[@"type"];
        tactic.range = tactic_record[@"range"];
        tactic.attMod = tactic_record[@"att mod"];
        tactic.oppMod = tactic_record[@"opp mod"];
        tactic.damage = tactic_record[@"damage"];
        tactic.burst = tactic_record[@"burst"];
        tactic.ammo = tactic_record[@"ammo"];
        tactic.target = tactic_record[@"target"];
        tactic.effect = tactic_record[@"effect"];
        tactic.skill = tactic_record[@"skill"];
        tactic.special = tactic_record[@"special"];
        
        [pherowareTacticDictionary setValue:tactic forKey:tactic.name];
    }
}

-(CCSkill*) getCCSkill:(NSString*) skillName
{
    CCSkill* skill = [ccSkillDictionary valueForKey:skillName];
    if(skill)
    {
        return skill;
    }
    
    skill = [CCSkill skillWithName:skillName];
    if(skill)
    {
        [ccSkillDictionary setValue:skill forKey:skillName];
        return skill;
    }
    
    return nil;
}

-(void) parseCCSkillData
{
    NSDictionary *cc_skills = [DataParser getFile:@"infinitydata/cc_skills"];
    
    // CC Skills Programs
    for(NSDictionary* skill_record in cc_skills)
    {
        // Update if exists, otherwise create new
        CCSkill* skill = [self getCCSkill:[skill_record valueForKey:@"name"]];
        if(!skill){
            skill = [CCSkill insertInManagedObjectContext:[appDelegate managedObjectContext]];
        }
        
        skill.name = [skill_record valueForKey:@"name"];
        skill.attMod = [skill_record valueForKey:@"attack"];
        skill.oppMod = [skill_record valueForKey:@"opponent"];
        skill.damageMod = [skill_record valueForKey:@"damage"];
        skill.burstMod = [skill_record valueForKey:@"burst"];
        skill.special = [skill_record valueForKey:@"special"];
        
        [ccSkillDictionary setValue:skill forKey:skill.name];
    }
}

#pragma mark - Wiki
-(void)parseWiki
{
    NSArray* filter = @[
                        @"CC Attack CSS",
                        @"CC Attack CSS templates",
                        @"FAQ test",
                        @"FAQs",
                        @"Guns, Lots of Guns",
                        @"Hedgehog Weapon",
                        @"Icestorm Expanded",
                        @"Labels Reverse List",
                        @"Main Page",
                        @"Table color test",
                        @"Tabletest",
                        @"Templates of Doom",
                        @"WeaponTest",
                        @"WeaponTest2",
                        @"WeaponTestCSS",
                        @"X-2 Visor",
                        ];
    
    Wiki* wiki_en = [Wiki insertInManagedObjectContext:appDelegate.managedObjectContext];
    NSDictionary* wikis = [DataParser getFile:@"wiki_en"];
    for(NSDictionary* page in [wikis objectForKey:@"pages"])
    {
        NSString* filename = [page objectForKey:@"filename"];
        
        NSString* pagename = [page objectForKey:@"pagename"];
        NSString* cleanPageName = [pagename stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        cleanPageName = [cleanPageName stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[cleanPageName substringToIndex:1] capitalizedString]];
        
        if([filter indexOfObject:pagename] != NSNotFound)
        {
            continue;
        }

        //About 10 or so wiki names have trailing commas, filter out
        BOOL trailingCommaInName = [[pagename substringFromIndex:[pagename length] -1] isEqualToString:@","];
        if(trailingCommaInName) {
            continue;
        }

        NSArray* exceptionsForTrailingPeriods = @[@"D.E.P.", @"(Choosing Deployment)."];
        
        //Some wiki names have trailing periods filter out
        BOOL trailingPeriods = [[pagename substringFromIndex:[pagename length] -1] isEqualToString:@"."];
        if(trailingPeriods && [exceptionsForTrailingPeriods indexOfObject:pagename] == NSNotFound) {
            continue;
        }

        NSDictionary* forcedRenames = @{};
        NSString* rename;
        if((rename  = [forcedRenames objectForKey:cleanPageName]) != nil)
        {
            cleanPageName = rename;
        }
        
        WikiEntry* wikiEntry = [WikiEntry insertInManagedObjectContext:appDelegate.managedObjectContext];
        wikiEntry.url = filename;
        wikiEntry.pageName = pagename;
        wikiEntry.name = cleanPageName;
        [wikiNameDictionary setObject:wikiEntry forKey:wikiEntry.name];
        [wiki_en addEntriesObject:wikiEntry];
    }
}


-(void) parseChoiceSpecialRules
{
    // Iterate over the returned list of keys in the specialRulesDictionary
    // This allows us to safely add new rules while iterating.
    for (NSString* specialRuleName in [specialRulesDictionary allKeys]) {
        SpecialRule* specialRule = [specialRulesDictionary objectForKey:specialRuleName];
        [specialRule.choicesSet removeAllObjects];
        
        if ([specialRule.name compare:@"Veteran L2 (V: No Wound Incapacitation, Sixth Sense L2)"] == NSOrderedSame) {
            [specialRule.choicesSet addObject:[self getSpecialRule:@"Veteran"]];
            [specialRule.choicesSet addObject:[self getSpecialRule:@"V: No Wound Incapacitation"]];
            [specialRule.choicesSet addObject:[self getSpecialRule:@"Sixth Sense L2"]];
        } else if ([specialRule.name compare:@"Multiterrain (Jungle Terrain / Mountain Terrain)"] == NSOrderedSame) {
            [specialRule.choicesSet addObject:[self getSpecialRule:@"Multiterrain"]];
            [specialRule.choicesSet addObject:[self getSpecialRule:@"Jungle Terrain"]];
            [specialRule.choicesSet addObject:[self getSpecialRule:@"Mountain Terrain"]];
        } else if ([specialRule.name rangeOfString:@" / "].location != NSNotFound) {
            NSString* remainingRule = specialRule.name;
            while ([remainingRule rangeOfString:@" / "].location != NSNotFound) {
                NSUInteger loc = [remainingRule rangeOfString:@" / "].location;
                NSUInteger length = [remainingRule rangeOfString:@" / "].length;
                NSString* firstChoice = [remainingRule substringWithRange:NSMakeRange(0, loc)];
                remainingRule = [remainingRule substringWithRange:NSMakeRange(loc+length, [remainingRule length] - (loc+length))];
                [specialRule.choicesSet addObject:[self getSpecialRule:firstChoice]];
            }
            [specialRule.choicesSet addObject:[self getSpecialRule:remainingRule]];
        }
    }
}

-(void) matchSpecialRulesToWikiEntries
{
    NSDictionary* forcedMatches = @{
                                    @"AD" : @"Airborne Deployment (AD)",
                                    @"CH" : @"Camouflage and Hiding (CH)",
                                    @"G" : @"Ghost (G)",
                                    @"V" : @"Valor (V)",

                                    @"Cube" : @"Back-up",

                                    @"Akbar Doctor" : @"Doctor",
                                    @"Doctor Plus" : @"Doctor",

                                    @"Jungle Terrain" : @"Terrain",
                                    @"Desert Terrain" : @"Terrain",
                                    @"Mountain Terrain" : @"Terrain",
                                    @"Aquatic Terrain" : @"Terrain",
                                    @"Zero-G": @"Terrain",

                                    @"Shock Immunity" : @"Immunity",
                                    @"Total Immunity" : @"Immunity",

                                    @"Not Hackable" : @"Non-Hackable",

                                    @"Lo-Tech A" : @"Lo-Tech",
                                    
                                    @"Basic Impersonation: Yu Jing" : @"Impersonation",
                                    @"Basic Impersonation" : @"Impersonation",
                                    @"Impersonation Plus" : @"Impersonation",
                                    @"Inferior Impersonation" : @"Impersonation",

                                    @"360° Visor" : @"360 Visor",
                                    @"Aerocam" : @"360 Visor",

                                    @"Heavy Protection (+4 ARM)" : @"Attributes",
                                    @"Motorcycle (MOV 8-6)" : @"Motorcycle",
                                    @"Light Protection (+2 ARM)" : @"Attributes",
                                    @"Medium Protection (+3 ARM)" : @"Attributes",
                                    @"Natural Armor (+1 ARM)" : @"Attributes",
                                    @"Reinforced Biotech (+6 BTS)" : @"Attributes",
                                    @"Enhanced Mobility (MOV: 8-4)" : @"Attributes",
                                    @"Enhanced Physique (+3 PH)" : @"Attributes",
                                    @"Light Protection (+1 ARM)" : @"Attributes",
                                    @"Super-Physique (+3 PH)" : @"Attributes",
                                    
                                    @"ODD: Optical Disruptor" : @"ODD: Optical Disruption Device",

                                    @"Fireteam: Duo" : @"Fireteam: Duo (Skill)",
                                    @"Fireteam: Haris" : @"Fireteam: Haris (Skill)",
                                    @"Fireteam: Triad" : @"Fireteam: Triad (Skill)",

                                    @"Infiltration" : @"Infiltrate",
                                    @"Superior Infiltration" : @"Infiltrate",
                                    @"Inferior Infiltration" : @"Infiltrate",

                                    @"X Visor" : @"X-Visor",

                                    @"Hungries Control Device" : @"Control Device",
                                    @"Traktor Mul Control Device" : @"Control Device",
                                    @"Kuang Shi Control Device": @"Control Device",

                                    @"Advanced ECM" : @"ECM",

                                    @"ED: Escape System" : @"Evacuation Device (ED)",
                                    @"ED: Ejection System" : @"Evacuation Device (ED)",
                                    @"Operator" : @"Evacuation Device (ED)",
                                    
                                    @"Fireteam: Special Triad" : @"Fireteam: Triad",
                                    };
    for(NSString* specialRuleName in specialRulesDictionary)
    {
        SpecialRule* specialRule = specialRulesDictionary[specialRuleName];
        
        if([specialRule.choices count] != 0)
        {
            continue;
        }
        
        // Names we will search the wiki for; will automatically generate variants.
        NSMutableArray* wikiNames = [[NSMutableArray alloc] init];
        [wikiNames addObject:specialRuleName];

        // Add skills from parenthetical expressions
        if ([specialRule.name rangeOfString:@"("].location != NSNotFound) {
            // Match parenthetical skills to their base
            NSUInteger startLoc = [specialRule.name rangeOfString:@" ("].location;
            NSString* baseSkill = [specialRule.name substringWithRange:NSMakeRange(0, startLoc)];
            [wikiNames addObject: baseSkill];

            // No match on the first string; try the second.
            startLoc += 2;
            NSUInteger endLoc = [specialRule.name rangeOfString:@")"].location;
            if (endLoc != NSNotFound) {
                NSString* parenSkill = [specialRule.name substringWithRange:NSMakeRange(startLoc, endLoc - startLoc)];
                [wikiNames addObject: parenSkill];
            }
        }
        
        // Name mutators follow; they operate on all skill names that have been pulled out so far.
        NSMutableArray* mutatedNames = [[NSMutableArray alloc] init];

        // Derive base name of leveled skills
        for (NSString* wikiName in wikiNames) {
            if (wikiName.length > 3 && [wikiName characterAtIndex:wikiName.length - 2] == 'L') {
                NSString* baseSkill = [wikiName substringToIndex:wikiName.length - 3];
                [mutatedNames addObject:baseSkill];
            }
        }

        // Look for colon-delimited prefixes (will only look for the first colon)
        for (NSString* wikiName in wikiNames) {
            if ([wikiName rangeOfString:@":"].location != NSNotFound) {
                NSUInteger loc = [wikiName rangeOfString:@":"].location;
                NSString* prefixSkill = [wikiName substringWithRange:NSMakeRange(0, loc)];
                [mutatedNames addObject:prefixSkill];
            }
        }
        
        // There are way too many different hacking devices to list them all
        if ([specialRule.name rangeOfString:@"Hacking Device"].location != NSNotFound) {
            [mutatedNames addObject:@"Hacking: Equipment"];
        }

        [wikiNames addObjectsFromArray:mutatedNames];
        
        WikiEntry* wikiEntry;
        for (NSString* wikiName in wikiNames) {
            NSString* wikiEntryForcedMatch = [forcedMatches objectForKey:wikiName];
            if (wikiEntryForcedMatch) {
                wikiEntry = [self getWikiEntriesWithName:wikiEntryForcedMatch];
                NSAssert(wikiEntry != nil, @"Special rule forced match %@ does not map to a wiki page", wikiEntryForcedMatch);
                break;
            } else {
                wikiEntry = [self getWikiEntriesWithName:wikiName];
                if (wikiEntry) {
                    break;
                }
            }
        }

        if(wikiEntry) {
            specialRule.wikiEntry = wikiEntry;
        } else {
            NSLog(@"Wiki Entry not found for Special Rule: %@", specialRule.name);
        }
    }
}

-(void) matchWeaponsToWikiEntries
{
    NSDictionary* forcedMatches = @{
                                    @"Heavy Rocket Launcher" : @"Rocket Launcher (RL)",
                                    @"Smart Heavy Rocket Launcher" : @"Rocket Launcher (RL)",
                                    @"Light Rocket Launcher" : @"Rocket Launcher (RL)",

                                    @"Smoke Light Grenade Launcher" : @"Grenade Launcher (GL)",
                                    @"E/M Light Grenade Launcher" : @"Grenade Launcher (GL)",
                                    @"Light Nimbus Grenade Launcher" : @"Grenade Launcher (GL)",
                                    @"Light Nimbus Plus Grenade Launcher" : @"Grenade Launcher (GL)",
                                    @"Eclipse Light Grenade Launcher" : @"Grenade Launcher (GL)",
                                    @"Stun Light Grenade Launcher" : @"Grenade Launcher (GL)",
                                    @"Light Grenade Launcher" : @"Grenade Launcher (GL)",
                                    @"Heavy Grenade Launcher" : @"Grenade Launcher (GL)",

                                    @"Light Shotgun" : @"Shotgun",
                                    @"Heavy Shotgun" : @"Shotgun",
                                    @"T2 Boarding Shotgun" : @"Shotgun",
                                    @"Boarding Shotgun" : @"Shotgun",
                                    @"Vulkan Shotgun" : @"Shotgun",

                                    @"Antipersonnel Mines" : @"Mines",
                                    @"Antipersonnel Mines (1)" : @"Mines",
                                    @"Monofilament Mines" : @"Mines",
                                    @"Viral Mines" : @"Mines",
                                    @"Cybermines" : @"Mines",

                                    @"Combi Rifle" : @"Rifle",
                                    @"MULTI Rifle" : @"Rifle",
                                    @"K1 Combi Rifle" : @"Rifle",
                                    @"Viral Rifle" : @"Rifle",
                                    @"Viral Combi Rifle" : @"Rifle",
                                    @"Plasma Rifle" : @"Rifle",
                                    @"Breaker Combi Rifle" : @"Rifle",
                                    @"Breaker Rifle" : @"Rifle",
                                    @"T2 Rifle" : @"Rifle",

                                    @"Stun Grenades" : @"Grenades",
                                    @"Smoke Grenades" : @"Grenades",
                                    @"Eclipse Grenades" : @"Grenades",
                                    @"Nimbus Plus Grenades" : @"Grenades",
                                    @"Nimbus Grenades" : @"Grenades",
                                    @"E/M Grenades" : @"Grenades",

                                    @"Sepsitor Plus" : @"Sepsitor",

                                    @"AP HMG" : @"Heavy Machine Gun (HMG)",
                                    @"MULTI HMG" : @"Heavy Machine Gun (HMG)",
                                    @"HMG" : @"Heavy Machine Gun (HMG)",

                                    @"Breaker Pistol" : @"Pistol",
                                    @"Assault Pistol" : @"Pistol",
                                    @"Heavy Pistol" : @"Pistol",
                                    @"Stun Pistol" : @"Pistol",
                                    @"AP Heavy Pistol" : @"Pistol",
                                    @"Viral Pistol" : @"Pistol",
                                    @"MULTI Pistol" : @"Pistol",

                                    @"Plasma Sniper Rifle" : @"Sniper Rifle",
                                    @"K1 Sniper Rifle" : @"Sniper Rifle",
                                    @"Viral Sniper Rifle" : @"Sniper Rifle",
                                    @"AP Sniper Rifle" : @"Sniper Rifle",
                                    @"Smart MULTI Sniper Rifle" : @"Sniper Rifle",
                                    @"T2 Sniper Rifle" : @"Sniper Rifle",
                                    @"MULTI Sniper Rifle" : @"Sniper Rifle",

                                    @"Smart Missile Launcher" : @"Missile Launcher (ML)",
                                    @"Missile Launcher" : @"Missile Launcher (ML)",

                                    @"HMC" : @"Hyper-Rapid Magnetic Cannon (HMC)",

                                    @"CCW" : @"CC Weapon",
                                    @"Monofilament CCW" : @"CC Weapon",
                                    @"Vorpal CCW" : @"CC Weapon",
                                    @"EXP CCW" : @"CC Weapon",
                                    @"DA CCW" : @"CC Weapon",
                                    @"AP CCW" : @"CC Weapon",
                                    @"Viral CCW" : @"CC Weapon",
                                    @"Shock CCW" : @"CC Weapon",
                                    @"E/M CCW" : @"CC Weapon",
                                    @"E/M2 CCW" : @"CC Weapon",
                                    @"AP+Shock CCW" : @"CC Weapon",
                                    @"Templar CCW (AP+Shock)" : @"CC Weapon",
                                    @"Templar CCW (AP+DA)" : @"CC Weapon",

                                    @"AP Marksman Rifle" : @"Marksman Rifle",
                                    @"Shock Marksman Rifle" : @"Marksman Rifle",
                                    @"MULTI Marksman Rifle" : @"Marksman Rifle",

                                    @"Heavy Flamethrower" : @"Flamethrower (FT)",
                                    @"Light Flamethrower" : @"Flamethrower (FT)",

                                    @"Autocannon" : @"Portable Autocannon",

                                    @"AP Spitfire" : @"Spitfire",

                                    @"CrazyKoalas" : @"CrazyKoala",
                                    @"MadTraps" : @"MadTrap",
                                    @"SymbioBugs" : @"SymbioBug",
                                    
                                    @"Adhesive Launcher" : @"Adhesive Launcher (ADHL)",
                                    };
    for(NSString* weaponName in weaponsDictionary)
    {
        Weapon* weapon = [weaponsDictionary objectForKey:weaponName];
        NSString* weaponName;
        if([weapon.name hasSuffix:@"(2)"])
        {
            weaponName = [weapon.name substringWithRange:NSMakeRange(0, weapon.name.length-4)];
        }
        else
        {
            weaponName = weapon.name;
        }
        
        NSString* wikiEntryForcedMatch = [forcedMatches objectForKey:weaponName];
        if(wikiEntryForcedMatch)
        {
            weaponName = wikiEntryForcedMatch;
        }

        WikiEntry* wikiEntry = [self getWikiEntriesWithName:weaponName];
        if(wikiEntry)
        {
            weapon.wikiEntry = wikiEntry;

            // Also give each additional profile the same wiki pointer.
            while(weapon.altProfile)
            {
                weapon = [weaponsDictionary objectForKey:weapon.altProfile];
                weapon.wikiEntry = wikiEntry;
            }
        }
    }
}

-(void) matchHackingProgramsToWikiEntries
{
    for(NSString* programName in hackingProgramDictionary)
    {
        HackingProgram* program = [hackingProgramDictionary objectForKey:programName];
        
        WikiEntry* wikiEntry = [self getWikiEntriesWithName:program.name];
        if(wikiEntry)
        {
            program.wikiEntry = wikiEntry;
        }
        else
        {
            NSLog(@"Wiki Entry not found for Hacking Program: %@", program.name);
        }
    }
    
}

-(void) matchPherowareTacticsToWikiEntries
{
    for(NSString* tacticName in pherowareTacticDictionary)
    {
        PherowareTactic* tactic = [pherowareTacticDictionary objectForKey:tacticName];
        
        WikiEntry* wikiEntry = [self getWikiEntriesWithName:tactic.name];
        if(wikiEntry)
        {
            tactic.wikiEntry = wikiEntry;
        }
        else
        {
            NSLog(@"Wiki Entry not found for Pheroware Tactic: %@", tactic.name);
        }
    }
    
}

-(WikiEntry*) getWikiEntriesWithName:(NSString*)name
{
    return [wikiNameDictionary objectForKey:name];
}

+ (NSNumber*)parseDashDash: (NSString*) subject;
{
    //Burst count
    if([subject compare:@"--"] == NSOrderedSame)
    {
        return nil;
    }
    return [NSNumber numberWithInteger:[subject integerValue]];
}
+(id) getFile: (NSString*) filename
{
    NSBundle* mainBundle = [NSBundle mainBundle];
    NSString* file = [mainBundle pathForResource: filename ofType: @"json"];
    NSData* data = [[NSFileManager defaultManager] contentsAtPath:file];
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSAssert(error == nil, @"%@", error);
    return json;
}
@end
