//
//  NotationViewController.h
//  Toolbox
//
//  Created by Paul on 12/10/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GroupMember;

@protocol NotationDelegate <NSObject>

-(void) onNotationDone;

@end

@interface NotationViewController : UITableViewController
- (IBAction)onClose:(id)sender;

@property (nonatomic, strong) GroupMember* groupMember;
@property (nonatomic, weak) id<NotationDelegate> delegate;
@end
