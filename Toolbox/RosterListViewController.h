//
//  RosterListViewController.h
//  Toolbox
//
//  Created by Paul on 11/3/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Army, Sectorial;

@interface RosterListViewController : UITableViewController
{
    BOOL tableViewEditting;
    NSOrderedSet* allRosters;
}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *organizeButton;
- (IBAction)addRoster:(id)sender;
@property (nonatomic, assign) Army* army;
@property (nonatomic, assign) Sectorial* sectorial;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
