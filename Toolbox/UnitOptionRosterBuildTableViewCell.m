//
//  UnitOptionRosterBuildTableViewCell.m
//  Toolbox
//
//  Created by Paul on 11/1/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "UnitOptionRosterBuildTableViewCell.h"

@implementation UnitOptionRosterBuildTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
