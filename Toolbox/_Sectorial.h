// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Sectorial.h instead.

#import <CoreData/CoreData.h>

extern const struct SectorialAttributes {
	__unsafe_unretained NSString *abbr;
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *legacyName;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *rgbBackground;
	__unsafe_unretained NSString *rgbPrimary;
	__unsafe_unretained NSString *rgbSecondary;
	__unsafe_unretained NSString *showMercs;
} SectorialAttributes;

extern const struct SectorialRelationships {
	__unsafe_unretained NSString *army;
	__unsafe_unretained NSString *rosters;
	__unsafe_unretained NSString *units;
} SectorialRelationships;

@class Army;
@class Roster;
@class Unit;

@interface SectorialID : NSManagedObjectID {}
@end

@interface _Sectorial : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SectorialID* objectID;

@property (nonatomic, strong) NSString* abbr;

//- (BOOL)validateAbbr:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* id;

@property (atomic) int32_t idValue;
- (int32_t)idValue;
- (void)setIdValue:(int32_t)value_;

//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* legacyName;

//- (BOOL)validateLegacyName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* rgbBackground;

//- (BOOL)validateRgbBackground:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* rgbPrimary;

//- (BOOL)validateRgbPrimary:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* rgbSecondary;

//- (BOOL)validateRgbSecondary:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* showMercs;

@property (atomic) BOOL showMercsValue;
- (BOOL)showMercsValue;
- (void)setShowMercsValue:(BOOL)value_;

//- (BOOL)validateShowMercs:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Army *army;

//- (BOOL)validateArmy:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSOrderedSet *rosters;

- (NSMutableOrderedSet*)rostersSet;

@property (nonatomic, strong) NSSet *units;

- (NSMutableSet*)unitsSet;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newRostersFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newUnitsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _Sectorial (RostersCoreDataGeneratedAccessors)
- (void)addRosters:(NSOrderedSet*)value_;
- (void)removeRosters:(NSOrderedSet*)value_;
- (void)addRostersObject:(Roster*)value_;
- (void)removeRostersObject:(Roster*)value_;

- (void)insertObject:(Roster*)value inRostersAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRostersAtIndex:(NSUInteger)idx;
- (void)insertRosters:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeRostersAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInRostersAtIndex:(NSUInteger)idx withObject:(Roster*)value;
- (void)replaceRostersAtIndexes:(NSIndexSet *)indexes withRosters:(NSArray *)values;

@end

@interface _Sectorial (UnitsCoreDataGeneratedAccessors)
- (void)addUnits:(NSSet*)value_;
- (void)removeUnits:(NSSet*)value_;
- (void)addUnitsObject:(Unit*)value_;
- (void)removeUnitsObject:(Unit*)value_;

@end

@interface _Sectorial (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAbbr;
- (void)setPrimitiveAbbr:(NSString*)value;

- (NSNumber*)primitiveId;
- (void)setPrimitiveId:(NSNumber*)value;

- (int32_t)primitiveIdValue;
- (void)setPrimitiveIdValue:(int32_t)value_;

- (NSString*)primitiveLegacyName;
- (void)setPrimitiveLegacyName:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveRgbBackground;
- (void)setPrimitiveRgbBackground:(NSString*)value;

- (NSString*)primitiveRgbPrimary;
- (void)setPrimitiveRgbPrimary:(NSString*)value;

- (NSString*)primitiveRgbSecondary;
- (void)setPrimitiveRgbSecondary:(NSString*)value;

- (NSNumber*)primitiveShowMercs;
- (void)setPrimitiveShowMercs:(NSNumber*)value;

- (BOOL)primitiveShowMercsValue;
- (void)setPrimitiveShowMercsValue:(BOOL)value_;

- (Army*)primitiveArmy;
- (void)setPrimitiveArmy:(Army*)value;

- (NSMutableOrderedSet*)primitiveRosters;
- (void)setPrimitiveRosters:(NSMutableOrderedSet*)value;

- (NSMutableSet*)primitiveUnits;
- (void)setPrimitiveUnits:(NSMutableSet*)value;

@end
