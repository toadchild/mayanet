//
//  HackingProgramTableViewCell.m
//  Toolbox
//
//  Created by Jonathan Polley on 9/29/15.
//  Copyright (c) 2015 Paul. All rights reserved.
//

#import "HackingProgramTableViewCell.h"

@implementation HackingProgramTableViewCell

-(void) updateConstraints
{
    /* Field order:
         Name
         Skill Type
         Target (optional)
         Range
         Modifiers (optional)
         Effect
         Special (optional)
     */
    
    if(_program.target && [_program.target length])
    {
        self.targetHeight.constant = 18;
        self.rangeTopConstraint.constant = 3;
    }
    else
    {
        self.targetHeight.constant = 0;
        self.rangeTopConstraint.constant = 0;
    }

    if(!_program.hide_modsValue)
    {
        self.modifiers.hidden = false;
        self.modifiersHeightConstraint.constant = 59.5;
    }
    else
    {
        self.modifiers.hidden = true;
        self.modifiersHeightConstraint.constant = 0;
    }

    if(_program.special && [_program.special length])
    {
        self.specialHeightConstraint.constant = 18;
        self.specialTopConstraint.constant = 3;
    }
    else
    {
        self.specialHeightConstraint.constant = 0;
        self.specialTopConstraint.constant = 0;
    }
    
    [super updateConstraints];
}

@end
