// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Army.h instead.

#import <CoreData/CoreData.h>

extern const struct ArmyAttributes {
	__unsafe_unretained NSString *id;
	__unsafe_unretained NSString *legacyName;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *rgbBackground;
	__unsafe_unretained NSString *rgbPrimary;
	__unsafe_unretained NSString *rgbSecondary;
	__unsafe_unretained NSString *showMercs;
} ArmyAttributes;

extern const struct ArmyRelationships {
	__unsafe_unretained NSString *rosters;
	__unsafe_unretained NSString *sectorials;
	__unsafe_unretained NSString *specopBaseUnitOption;
	__unsafe_unretained NSString *specopEquipment;
	__unsafe_unretained NSString *specopSkills;
	__unsafe_unretained NSString *specopWeapons;
	__unsafe_unretained NSString *units;
} ArmyRelationships;

@class Roster;
@class Sectorial;
@class SpecOpsBaseUnitOption;
@class SpecOpsEquipment;
@class SpecOpsSpecialRule;
@class SpecOpsWeapon;
@class Unit;

@interface ArmyID : NSManagedObjectID {}
@end

@interface _Army : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ArmyID* objectID;

@property (nonatomic, strong) NSNumber* id;

@property (atomic) int32_t idValue;
- (int32_t)idValue;
- (void)setIdValue:(int32_t)value_;

//- (BOOL)validateId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* legacyName;

//- (BOOL)validateLegacyName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* rgbBackground;

//- (BOOL)validateRgbBackground:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* rgbPrimary;

//- (BOOL)validateRgbPrimary:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* rgbSecondary;

//- (BOOL)validateRgbSecondary:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* showMercs;

@property (atomic) BOOL showMercsValue;
- (BOOL)showMercsValue;
- (void)setShowMercsValue:(BOOL)value_;

//- (BOOL)validateShowMercs:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSOrderedSet *rosters;

- (NSMutableOrderedSet*)rostersSet;

@property (nonatomic, strong) NSSet *sectorials;

- (NSMutableSet*)sectorialsSet;

@property (nonatomic, strong) NSSet *specopBaseUnitOption;

- (NSMutableSet*)specopBaseUnitOptionSet;

@property (nonatomic, strong) NSSet *specopEquipment;

- (NSMutableSet*)specopEquipmentSet;

@property (nonatomic, strong) NSSet *specopSkills;

- (NSMutableSet*)specopSkillsSet;

@property (nonatomic, strong) NSOrderedSet *specopWeapons;

- (NSMutableOrderedSet*)specopWeaponsSet;

@property (nonatomic, strong) NSSet *units;

- (NSMutableSet*)unitsSet;

#if TARGET_OS_IPHONE

- (NSFetchedResultsController*)newRostersFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSectorialsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecopBaseUnitOptionFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecopEquipmentFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecopSkillsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newSpecopWeaponsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

- (NSFetchedResultsController*)newUnitsFetchedResultsControllerWithSortDescriptors:(NSArray*)sortDescriptors;

#endif

@end

@interface _Army (RostersCoreDataGeneratedAccessors)
- (void)addRosters:(NSOrderedSet*)value_;
- (void)removeRosters:(NSOrderedSet*)value_;
- (void)addRostersObject:(Roster*)value_;
- (void)removeRostersObject:(Roster*)value_;

- (void)insertObject:(Roster*)value inRostersAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRostersAtIndex:(NSUInteger)idx;
- (void)insertRosters:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeRostersAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInRostersAtIndex:(NSUInteger)idx withObject:(Roster*)value;
- (void)replaceRostersAtIndexes:(NSIndexSet *)indexes withRosters:(NSArray *)values;

@end

@interface _Army (SectorialsCoreDataGeneratedAccessors)
- (void)addSectorials:(NSSet*)value_;
- (void)removeSectorials:(NSSet*)value_;
- (void)addSectorialsObject:(Sectorial*)value_;
- (void)removeSectorialsObject:(Sectorial*)value_;

@end

@interface _Army (SpecopBaseUnitOptionCoreDataGeneratedAccessors)
- (void)addSpecopBaseUnitOption:(NSSet*)value_;
- (void)removeSpecopBaseUnitOption:(NSSet*)value_;
- (void)addSpecopBaseUnitOptionObject:(SpecOpsBaseUnitOption*)value_;
- (void)removeSpecopBaseUnitOptionObject:(SpecOpsBaseUnitOption*)value_;

@end

@interface _Army (SpecopEquipmentCoreDataGeneratedAccessors)
- (void)addSpecopEquipment:(NSSet*)value_;
- (void)removeSpecopEquipment:(NSSet*)value_;
- (void)addSpecopEquipmentObject:(SpecOpsEquipment*)value_;
- (void)removeSpecopEquipmentObject:(SpecOpsEquipment*)value_;

@end

@interface _Army (SpecopSkillsCoreDataGeneratedAccessors)
- (void)addSpecopSkills:(NSSet*)value_;
- (void)removeSpecopSkills:(NSSet*)value_;
- (void)addSpecopSkillsObject:(SpecOpsSpecialRule*)value_;
- (void)removeSpecopSkillsObject:(SpecOpsSpecialRule*)value_;

@end

@interface _Army (SpecopWeaponsCoreDataGeneratedAccessors)
- (void)addSpecopWeapons:(NSOrderedSet*)value_;
- (void)removeSpecopWeapons:(NSOrderedSet*)value_;
- (void)addSpecopWeaponsObject:(SpecOpsWeapon*)value_;
- (void)removeSpecopWeaponsObject:(SpecOpsWeapon*)value_;

- (void)insertObject:(SpecOpsWeapon*)value inSpecopWeaponsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromSpecopWeaponsAtIndex:(NSUInteger)idx;
- (void)insertSpecopWeapons:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeSpecopWeaponsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInSpecopWeaponsAtIndex:(NSUInteger)idx withObject:(SpecOpsWeapon*)value;
- (void)replaceSpecopWeaponsAtIndexes:(NSIndexSet *)indexes withSpecopWeapons:(NSArray *)values;

@end

@interface _Army (UnitsCoreDataGeneratedAccessors)
- (void)addUnits:(NSSet*)value_;
- (void)removeUnits:(NSSet*)value_;
- (void)addUnitsObject:(Unit*)value_;
- (void)removeUnitsObject:(Unit*)value_;

@end

@interface _Army (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveId;
- (void)setPrimitiveId:(NSNumber*)value;

- (int32_t)primitiveIdValue;
- (void)setPrimitiveIdValue:(int32_t)value_;

- (NSString*)primitiveLegacyName;
- (void)setPrimitiveLegacyName:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveRgbBackground;
- (void)setPrimitiveRgbBackground:(NSString*)value;

- (NSString*)primitiveRgbPrimary;
- (void)setPrimitiveRgbPrimary:(NSString*)value;

- (NSString*)primitiveRgbSecondary;
- (void)setPrimitiveRgbSecondary:(NSString*)value;

- (NSNumber*)primitiveShowMercs;
- (void)setPrimitiveShowMercs:(NSNumber*)value;

- (BOOL)primitiveShowMercsValue;
- (void)setPrimitiveShowMercsValue:(BOOL)value_;

- (NSMutableOrderedSet*)primitiveRosters;
- (void)setPrimitiveRosters:(NSMutableOrderedSet*)value;

- (NSMutableSet*)primitiveSectorials;
- (void)setPrimitiveSectorials:(NSMutableSet*)value;

- (NSMutableSet*)primitiveSpecopBaseUnitOption;
- (void)setPrimitiveSpecopBaseUnitOption:(NSMutableSet*)value;

- (NSMutableSet*)primitiveSpecopEquipment;
- (void)setPrimitiveSpecopEquipment:(NSMutableSet*)value;

- (NSMutableSet*)primitiveSpecopSkills;
- (void)setPrimitiveSpecopSkills:(NSMutableSet*)value;

- (NSMutableOrderedSet*)primitiveSpecopWeapons;
- (void)setPrimitiveSpecopWeapons:(NSMutableOrderedSet*)value;

- (NSMutableSet*)primitiveUnits;
- (void)setPrimitiveUnits:(NSMutableSet*)value;

@end
