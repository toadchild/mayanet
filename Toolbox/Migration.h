#import "_Migration.h"
#import "AppDelegate.h"

@interface Migration : _Migration {}
// Custom logic goes here.
+(BOOL)canMigrate:(AppDelegate*)delegate;
+(NSArray*)migrations:(AppDelegate*)delegate;
+(BOOL)migrationExists:(AppDelegate*)delegate withName:(NSString*)name;
@end
