//
//  UnitOptionTableViewCell.h
//  Toolbox
//
//  Created by Paul on 10/28/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UnitOptionTableViewCell : UITableViewCell <UIGestureRecognizerDelegate>
@property (strong, nonatomic) IBOutlet UILabel *rosterAdditionCost;
@property (weak, nonatomic) IBOutlet UILabel *unitOptionCode;
@property (weak, nonatomic) IBOutlet UILabel *unitOptionCost;
@property (weak, nonatomic) IBOutlet UILabel *unitOptionSWC;
@property (weak, nonatomic) IBOutlet UILabel *unitOptionSpec;
@property (weak, nonatomic) IBOutlet UILabel *unitOptionBSW;
@end
