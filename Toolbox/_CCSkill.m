// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CCSkill.m instead.

#import "_CCSkill.h"

const struct CCSkillAttributes CCSkillAttributes = {
	.attMod = @"attMod",
	.burstMod = @"burstMod",
	.damageMod = @"damageMod",
	.name = @"name",
	.oppMod = @"oppMod",
	.special = @"special",
};

@implementation CCSkillID
@end

@implementation _CCSkill

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"CCSkill" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"CCSkill";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"CCSkill" inManagedObjectContext:moc_];
}

- (CCSkillID*)objectID {
	return (CCSkillID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic attMod;

@dynamic burstMod;

@dynamic damageMod;

@dynamic name;

@dynamic oppMod;

@dynamic special;

#if TARGET_OS_IPHONE

#endif

@end

