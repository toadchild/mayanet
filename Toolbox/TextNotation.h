#import "_TextNotation.h"

@interface TextNotation : _TextNotation {}
// Custom logic goes here.
-(NotationType) type;

+(TextNotation*)getTextNotation;

@end
