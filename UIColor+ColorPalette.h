//
//  UIColor+ColorPalette.h
//  Toolbox
//
//  Created by Paul on 12/17/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorPalette)

+(UIColor*) barColor;
+(UIColor*) activeBarColor;
+(UIColor*) passiveBarColor;


+(UIColor*) titleColor;
+(UIColor*) activeTitleColor;
+(UIColor*) passiveTitleColor;

+(UIColor*) barItemColor;
+(UIColor*) activeBarItemColor;
+(UIColor*) passiveBarItemColor;

+(UIColor*) textColor;
+(UIColor*) activeTextColor;
+(UIColor*) passiveTextColor;
@end
