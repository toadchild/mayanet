//
//  UIColor+ColorPalette.m
//  Toolbox
//
//  Created by Paul on 12/17/13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "UIColor+ColorPalette.h"

@implementation UIColor (ColorPalette)

+(UIColor*) barColor
{
    return [UIColor colorWithWhite:72.0/255.0 alpha:1];
}
+(UIColor*) activeBarColor
{
    return [UIColor colorWithWhite:150.0/255.0 alpha:1];
}
+(UIColor*) passiveBarColor
{
    return [UIColor barColor];
}



+(UIColor*) titleColor
{
    return [UIColor colorWithWhite:235.0/255.0 alpha:1];
}
+(UIColor*) activeTitleColor
{
    return [UIColor darkTextColor];
}
+(UIColor*) passiveTitleColor
{
    return [UIColor titleColor];
}

+(UIColor*) barItemColor
{
    return [UIColor colorWithWhite:220.0/255.0 alpha:1];
}

+(UIColor*) activeBarItemColor
{
    return [UIColor darkTextColor];
}

+(UIColor*) passiveBarItemColor
{
    return [UIColor barItemColor];
}

+(UIColor*) textColor
{
    return [UIColor darkTextColor];
}
+(UIColor*) activeTextColor
{
    return [UIColor darkTextColor];
}
+(UIColor*) passiveTextColor
{
    return [UIColor textColor];
}

@end
